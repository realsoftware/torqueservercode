<?php 
include('includes/header.php'); 

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="change_password.php">Change Password</a></li>
            <li class="active">Change Password</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=changepassword" >
                    <div class="card">
                        <div class="header">
                            <h2>Change Password</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">
                        	<div class="row clearfix">
                                <div>
                                	 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
					                     <div class="font-12">Old Password <span style="color:red;">*</span></div>

                                            <input type="password" onkeypress="clearFnameMsg();" class="form-control" name="old_password" id="old_password"  placeholder="Old Password" >
                                        </div>
										<span style="color:red;" id="msg_old_password"></span>
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">New Password <span style="color:red;">*</span></div>

                                            <input type="password" class="form-control" name="new_password"  onkeypress="clearFnameMsg1();" id="new_password" placeholder="New Password" >
                                        </div>
										<span style="color:red;" id="msg_new_password"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Confirm Password <span style="color:red;">*</span></div>

                                            <input type="password" class="form-control" name="confirm_password" onkeypress="clearFnameMsg2();" id="confirm_password" placeholder="Confirm Password"  >
                                        </div>
										<span style="color:red;" id="msg_confirm_password"></span>
                                    </div>
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

<script type="text/javascript">
function formValidation()
{
	old_password=$('#old_password').val();
	new_password=$('#new_password').val();
	confirm_password=$('#confirm_password').val();
	
	if(old_password=="")
	{
		$('#old_password').focus();
		document.getElementById("msg_old_password").innerHTML = "Please enter old password.";
		return false;
	}
	else if(new_password=="")
	{
		$('#new_password').focus();
		document.getElementById("msg_new_password").innerHTML = "Please enter new password.";
		return false;
	}
	else if(confirm_password=="")
	{
		$('#confirm_password').focus();
		document.getElementById("msg_confirm_password").innerHTML = "Please enter confirm password.";
		return false;
	}
	else if(confirm_password!=new_password)
	{
		$('#confirm_password').focus();
		document.getElementById("msg_confirm_password").innerHTML = "New password and confirm password should match.";
		return false;
	}
    else
	{
	return true;
	}
}
function clearFnameMsg()
{
	document.getElementById("msg_old_password").innerHTML = "";
}
function clearFnameMsg1()
{
	document.getElementById("msg_new_password").innerHTML = "";
}
function clearFnameMsg2()
{
	document.getElementById("msg_confirm_password").innerHTML = "";
}
</script>