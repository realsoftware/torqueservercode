<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['enquiry_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Leave Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>LEAVE MANAGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						    <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForLeaveManagement('user_role_acces','MOD020');
							if($accessInfo['fk_download']==5)
							{
							?>
						    <a href="query.php?type=leaveserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-leave.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="leave_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTrip();">Submit</button>
                           <?php
                            }
							}
							else{
                            ?>
 <a href="query.php?type=leaveserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-leave.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="leave_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTrip();">Submit</button>

<?php } ?>

						   </div>
                          </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>Created By</th>
										<th>Apply For Leave</th>
                                        <th>Type Of Leave</th>
                                        <th>From Date</th>
                                        <th>To Date</th>   
                                        <th>No Of Days</th>   
                                        <th>Approved Status</th>   
								 <th align="center">Approve <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								 <input type="checkbox" id="chkHeader"/></th>

                                        <th>Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								 if(@$type_search==1)
								 {
									 $taskInfo=$info;
								 }
								 else
								 {
								 $taskInfo=$display->toShowAllLeaveList();
								 }
								//print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
									// $fk_dealear_code=$taskInfos['fk_dealear_code']; 
									// $fk_manager_code=$taskInfos['fk_manager_code']; 
									
										// $managerInfo=$display->getMnagerName($fk_manager_code);
										// $manager_name=$managerInfo['maganer_name'];
								
										// $dealerInfo=$display->getDealerName($fk_dealear_code);
										// $dealer_name=$dealerInfo['dealer_name'];
										
									
								
 
                                  ?>
                                  <tr>
								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_user_code'].")";?></td>
								 
								  <td><?php echo $taskInfos['apply_for_leave'];?></td>								
								  <td><?php echo $taskInfos['type_of_leave'];?></td>								
							
																 
								  <td>
								   <span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['from_date'])); ?></span>
								  <?php echo $taskInfos['from_date'];?>
								  
								  </td>
								  <td>
								   <span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['to_date'])); ?></span>
								  <?php echo $taskInfos['to_date'];?></td>
								  <td><?php echo $taskInfos['no_of_day'];?></td>
								  <td><?php echo ucfirst($taskInfos['approved_status']);?></td>
								  
								 
								
							
							 <td align="center"><input type="checkbox" <?php if($taskInfos['approved_by_status']==1){ echo"checked";} ?> name="chkChild" class="chkChild" value="<?php echo $taskInfos['leave_id'];?>"/></td>

								  <td><center>
								  
								   <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_view']==3)
							{
							?>
								   <a href="view_leave.php?id=<?php echo $taskInfos['leave_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
							<?php }
							else
							{
								echo "-- ";
							}
									           ?>  
											   <?php 
								  if($accessInfo['fk_edit']==2)
							{
							?>
									            <a href="edit_leave.php?id=<?php echo $taskInfos['leave_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
							<?php }
							else
							{
								echo "-- ";
							}
									           ?>  
 <?php
 if($accessInfo['fk_delete']==4)
							{
							?>		            
									            <a href="query.php?type=leavedelete&id=<?php echo $taskInfos['leave_id'];?>" onclick="return confirm('Are you sure want to delete this leave?')" >
									                <i class="material-icons">delete</i>
									            </a>
												<?php }
                             else{
								 echo "--";
							 }

							} else {?>
												
												 <a href="view_leave.php?id=<?php echo $taskInfos['leave_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
							
									            <a href="edit_leave.php?id=<?php echo $taskInfos['leave_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
									                        
									            <a href="query.php?type=leavedelete&id=<?php echo $taskInfos['leave_id'];?>" onclick="return confirm('Are you sure want to delete this leave?')" >
									                <i class="material-icons">delete</i>
									            </a>
												<?php } ?>
												</center></td>
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
	 "aoColumns": [
            null,
			null,
            null,
            null,
		    null,
		   { "sType": "date-uk" },
		   { "sType": "date-uk" },

            null,
            null
        ]
		   });
});
$('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });

function ApprovedTrip()
{
	    var product_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                product_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_leave&enquiry_id='+product_id,
			success: function(data){
			//alert(data);
			alert("Leave has been approved successfully.");
		}});
}


</script>

