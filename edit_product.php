<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->showProductDetailByID($id);

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="folder_list.php">Folder Management</a></li>
            <li class="active">Edit Prodct</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updateproducttype" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT PRODUCT</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">
                         <input type="hidden" name="folder_id" value="<?php echo $id;?>"/>
                        	<div class="row clearfix">
                                <div>
                                
									<?php if($usertype!="DLR")
									{ 

										$managerDealer_name=$display->getManagerDealerName($info['fk_userunique_code']);
									
								?>
									 <div class="form-group form-float col-sm-12" id="div_dealer_name">
                                        <div class="form-line">
				                        <div class="font-12">Dealer Name <span style="color:red;">*</span></div>

                                            <input type="text" class="form-control"  onkeypress="clearFnameMsg10();"  name="autosearch_dealer_folder" id="autosearch_dealer_folder" placeholder="Dealer" onkeyup="CheckAutoSuggestionDealerFolder('dealer_folder',this.value);" autocomplete="off" value="<?php echo @$managerDealer_name['name']; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_dealer_folder" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_dealer_folder_id" id="autosearch_dealer_folder_id"
                value="<?php echo @$info['fk_userunique_code']; ?>"  />
				                    <span style="color:red;" id="msg_dealer_name"></span>
                                    </div>
									<?php } ?>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
								        <div class="font-12">Prodct Name  <span style="color:red;">*</span></div>

										<?php 
										$commodityType=$display->ShowAllProduct();
										?>
                                            <select type="text" class="form-control show-tick" onkeypress="clearFnameMsg1();" name="product_name" id="product_name" required>
											<?php 
											foreach($commodityType as $commodityTypes)
											{
												   $fk_product_code=$info['fk_product_code'];
											    $code =$commodityTypes['code'];
                                               if(@$fk_product_code == @$code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }	
											?>
											<option value="<?php echo $commodityTypes['code'];?>" <?php echo $aa;?>><?php echo $commodityTypes['product_name'];?></option>
											<?php 
											} ?>
											</select>
                                        </div>
                                    </div>
									 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
					                     <div class="font-12">Sub Product Name <span style="color:red;">*</span></div>

                                            <input type="text" class="form-control" name="sub_product_name" id="sub_product_name"  placeholder="Sub Product Name" value="<?php echo $info['sub_product_name'];?>">
                                        </div>
										<span style="color:red;" id="msg_f_name"></span>
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">File Name <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="file_name" id="file_name" placeholder="File Name" value="<?php echo $info['file_name'];?>" >
                                        </div>
                                    </div> 
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Upload PDF <span style="color:red;">*</span></div></div>

                                            <input type="file" class="form-control" name="pdf" id="pdf">
                                            <label class="form-label"></label>
                                        </div>
										
										<div class="form-group form-float col-sm-12">
										  <iframe src="api/upload/folder/<?php echo $info['file_path'];?>" width="100%" height="500px"></iframe>
                                    </div>
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>
