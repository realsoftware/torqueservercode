<?php 
include('includes/header.php'); 
$unique_code=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
 $taskInfos=$display->toShowAllTripListByID($unique_code);
								
 $fk_dealear_code=$taskInfos['fk_dealear_code']; 
 $fk_manager_code=$taskInfos['fk_manager_code']; 
									
 $managerInfo=$display->getMnagerName($fk_manager_code);
 $manager_name=$managerInfo['maganer_name'];
								
 $dealerInfo=$display->getDealerName($fk_dealear_code);
 $dealer_name=$dealerInfo['dealer_name'];
										
 /////////Calculate in and out time duration	
 $gmtTime=DateTime::CreateFromFormat('Y-m-d H:i:s',$taskInfos['up_date_time']);
 $out_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$taskInfos['cr_date_time']);
 $duration=$gmtTime->diff($out_time_duration);
 $hours=number_format((float)($duration->days*24)+$duration->h+($duration->i/60)+($duration->s/3600),3,'.','');
 $total_running_time= gmdate("H:i:s", $hours*3600);
													
 
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="trip_list.php">Trip Management</a></li>
            <li class="active">View Trip</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_code'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>User Type</strong></div><br/>
										<?php echo $taskInfos['user_type'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Dealer</strong></div><br/>
										<?php 
								    
								        echo $dealer_name;
									
								        ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Manager</strong></div><br/>
										<?php echo $manager_name;?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Start Location</strong></div><br/>
										<?php
                                    
								  echo $taskInfos['trip_start_address'];
									?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>End Location</strong></div><br/>
										<?php
                                    
								  echo $taskInfos['trip_end_address'];
									?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Trip Date</strong></div><br/>
										<?php echo date("d-m-Y", strtotime($taskInfos['trip_date_time']));?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Total Running Time</strong></div><br/>
										<?php echo gmdate("H:i:s",$taskInfos['total_time_hours']*3600);?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Total Trip Time</strong></div><br/>
										<?php
                                   if($taskInfos['type']=="start")
								  {
									  echo"--";
								  }
								  else{
								  echo $total_running_time;
								  }  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Total Distance Covered (km)</strong></div><br/>
										<?php echo $taskInfos['total_km_driven'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved  Distance (km)</strong></div><br/>
										<?php echo $taskInfos['approved_km'];?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Amount (INR)</strong></div><br/>
										<?php echo $taskInfos['approved_amount'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Total Amount (INR)</strong></div><br/>
										<?php echo $taskInfos['total_amoutnt'];?>
										</div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Type</strong></div><br/>
										<?php echo $taskInfos['type'];?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Start Date</strong></div><br/>
<?php 
								  if($taskInfos['cr_date_time']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['cr_date_time']));
								  }
								  else
								  { echo "--";}
								  
								  ?>                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>End Date</strong></div><br/>
										<?php 
								  
								  if($taskInfos['type']=="start")
								  {
									  echo"--";
								  }
								  else{
									  if($taskInfos['up_date_time']!="")
									  {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['up_date_time']));
									  }
									  else{
										  echo "--";
									  }
								  }?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approve</strong></div><br/>
										<?php if($taskInfos['aproved_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_by_remark'];?>
                                        </div>
                                    </div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

