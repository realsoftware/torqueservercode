<?php
session_start();

include('class/MyClass.php');
include('class/Display.php');
include('class/Update.php');
include('class/Add.php');
include('class/Delete.php');	

$global_obj 	= 	new MyClass();
$global_mysqli	=	$global_obj->MyClasss();		// Start DB Connection Object

$display 		= 	new Display();
$update 		= 	new Update();
$add 			= 	new Add();
$delete 		= 	new Delete();

@$type			=	@$_GET['type'];
@$unique_code	=	@$_GET['id'];

switch($type)
{
	case 'checkulogin' :
	$display->CheckLogin();
	break;	
	
	case 'userulogout' :
	$display->CheckLogout();
	break;
    
	case 'userserachReset' :
	$display->resetUserSearch();
	break;
    
	case 'attendenceserachReset' :
	$display->resetAttendenceSearch();
	break;

	case 'taskserachReset' :
	$display->resetTaskSearch();
	break;

    case 'tripserachReset' :
	$display->resetTripSearch();
	break;
	
	case 'folderserachReset' :
	$display->resetFolderSearch();
	break;

    case 'enquiryserachReset' :
	$display->resetEnquirySearch();
	break;
	
	case 'leaveserachReset' :
	$display->resetLeaveSearch();
	break;
	
	 case 'jobcardserachReset' :
	$display->resetJobcardSearch();
	break;
     case 'tadaserachReset' :
	$display->resetTADASearch();
	break;
	///add new pay slip
	case 'addnewpayslip':
	$add->AddPaySlip();
	break;
 
	// For Add user //
	case 'addusertype' :
	$add->AddUserType();
	break;
    
		// For Add product //
	case 'addproducttype' :
	$add->AddProductType();
	break;


	// For Update  //
	case 'updatetask' :
	$update->UpdateTaskDetails($unique_code);
	break;

// For Update  //
	case 'updatepayslip' :
	$update->UpdatePaySlipDetails($unique_code);
	break;	

	// For Update  //
	case 'updatetrip' :
	$update->UpdateTripDetails($unique_code);
	break;
	
	// For Update  //
	case 'updateuser' :
	$update->UpdateUserDetails($unique_code);
	break;
	
	// For change Password  //
	case 'changepassword' :
	$update->changePassword();
	break;
	
		// For Update  Prodct//
	case 'updateproducttype' :
	$update->UpdateProductDetails();
	break;
	
	
		// For Update  Close Enquiry//
	case 'closeenquiry' :
	$update->closeEnquiry();
	break;

		// For Update  Close Enquiry//
	case 'closejobcard' :
	$update->closeJobCard();
	break;
	
		// For Update  Close Reimbursement//
	case 'closeTada' :
	$update->closeTADA();
	break;
	
	// For Update  Close Enquiry//
	case 'approveLeave' :
	$update->closeLeave();
	break;
	
	///delete details
	case 'userdelete' :
	$delete->DeleteUser($unique_code);
	break;
	
	
    ///delete payslip
	case 'payslipdelete' :
	$delete->DeletePaySlip($unique_code);
	break;

	///delete attenendece
	case 'attenendencedelete' :
	$delete->DeleteAttendence($unique_code);
	break;
	
	///delete taska
	case 'taskdelete' :
	$delete->DeleteTask($unique_code);
	break;
	
	///delete trip
	case 'tripdelete' :
	$delete->DeleteTrip($unique_code);
	break;
	///delete product
	case 'productdelete' :
	$delete->DeleteProduct($unique_code);
	break;
	
	///delete Enquiry
	case 'enquirydelete' :
	$delete->DeleteEnquiry($unique_code);
	break;
	///delete Enquiry
	case 'jobcarddelete' :
	$delete->DeleteJOBCARD($unique_code);
	break;
	
		///delete Enquiry
	case 'leavedelete' :
	$delete->DeleteLeave($unique_code);
	break;
	
		///delete tada
	case 'tadadelete' :
	$delete->DeleteTADA($unique_code);
	break;

	///search query
	// For Add user //
	case 'searchusertype' :
	$display->searchUserType();
	break;
	
	// For Attendence search //
	case 'searcattendencetype' :
	$display->toShowAllAttendenceSearch();
	break;
	// For task search //
	case 'searchtask' :
	$display->toShowAllTaskSearch();
	break;
	
	// For trip search //
	case 'searchtrip' :
	$display->toShowAllTripSearch();
	break;
	
	// For folder search //
	case 'searchfolder' :
	$display->toShowAllProductSearch();
	break;
	
	// For enquiry search //
	case 'searchenquiry' :
	$display->toShowAllEnquirySearch();
	break;
	// For enquiry search //
	case 'searchJobcardExport' :
	$display->toShowAllJOBCARDSEARCHDETAIL();
	break;
	
	// For enquiry search //
	case 'searchtadaExport' :
	$display->toShowAllTADASEARCHDETAIL();
	break;
		// For enquiry search //
	case 'searchLeaveExport' :
	$display->toShowAllLeaveSearch();
	break;
	
	default :
	echo 'Nothing to display.';
	
}
?>
