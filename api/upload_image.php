<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$inputPost = filter_input_array(INPUT_POST);

$image_unique_code=$inputPost["image_unique_code"];
$type=$inputPost["type"];


$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required_usercode=dataFormat::$msg_required_usercode;
$msg_rquired_token=dataFormat::$msg_rquired_token;
$msg_required_type=dataFormat::$msg_required_type;
if(@$unique_code=='')
{
   $data		=$msg_required_usercode;
   $res->metadata = array('upload_image' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_rquired_token;
   $res->metadata = array('upload_image' => $data);
   echo json_encode($res);
   return;
}
if(@$type=='')
{
   $data		=$msg_required_type;
   $res->metadata = array('upload_image' => $data);
   echo json_encode($res);
   return;
}


#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
	$imageuploadInfo=$display ->UploadImages($image_unique_code,$unique_code,$type);
	//print_r($imageuploadInfo);die;
	if($imageuploadInfo==false)
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = $imageuploadInfo['message'];
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
     	$res->metadata=array('upload_image' =>$dataArray);
        echo json_encode($res);			
	}
	else
	{
       $res->success = false;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_fail;
       $res->message =$imageuploadInfo['message'];    
	   echo json_encode($res);
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>