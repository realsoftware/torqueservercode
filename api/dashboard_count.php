<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

//$json = file_get_contents('php://input');
////Converts it into a PHP object
//$task_detail = json_decode($json);


$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('dashboard_count' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('dashboard_count' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
	
	$attendencecountInfo=$display ->toShowAllDashboardCountListAttendence($unique_code);
	$tripcountInfo=$display ->toShowAllDashboardCountListTrip($unique_code);
	$closedtaskcountInfo=$display ->toShowAllDashboardCountListCloseTask($unique_code);
	$opentaskcountInfo=$display ->toShowAllDashboardCountListOpenTask($unique_code);
	#######New Added
	$productcountInfo=$display ->toShowAllDashboardCountListProduct($unique_code);
	$enquirycountInfo=$display ->toShowAllDashboardCountListEnquiry($unique_code);
	$jobcountInfo=$display ->toShowAllDashboardCountListJOB($unique_code);
	$leavecountInfo=$display ->toShowAllDashboardCountLeave($unique_code);
	$tadacountInfo=$display ->toShowAllDashboardCountTADA($unique_code);
	$payslipcountInfo=$display ->toShowAllDashboardCountPaySlip($unique_code);
	//$countListInfo=$display ->toShowAllDashboardCountList($unique_code);
	if($attendencecountInfo || $payslipcountInfo || $leavecountInfo || $tadacountInfo || $tripcountInfo || $closedtaskcountInfo || $opentaskcountInfo || $productcountInfo || $enquirycountInfo || $jobcountInfo)
	{
         $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_list;
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
        $dataArray= array();
		$dataArray['attendence']=$attendencecountInfo;
		$dataArray['task']['opentask']=$opentaskcountInfo;
		$dataArray['task']['closetask']=$closedtaskcountInfo;
		$dataArray['trip']=$tripcountInfo;
		$dataArray['product']=$productcountInfo;
		$dataArray['enquiry']=$enquirycountInfo;
		$dataArray['jobcard']=$jobcountInfo;
		$dataArray['leave']=$leavecountInfo;
		$dataArray['tada']=$tadacountInfo;
		$dataArray['payslip']=$payslipcountInfo;
		$res->metadata=array('dashboard_count' =>$dataArray);
        echo json_encode($res);	
	}
	else
	{
       $res->success = true;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_success;
       $res->message =dataFormat::$msg_no_data_list;   
       $dataArray= array();
       $dataArray['attendence']=$attendencecountInfo;
	   $dataArray['task']['opentask']=$opentaskcountInfo;
	   $dataArray['task']['closetask']=$closedtaskcountInfo;
	   $dataArray['trip']=$tripcountInfo;
	   $dataArray['product']=$productcountInfo;
	   $dataArray['enquiry']=$enquirycountInfo;
	   $dataArray['jobcard']=$jobcountInfo;
	   $dataArray['leave']=$leavecountInfo;
	   $dataArray['tada']=$tadacountInfo;
       $dataArray['payslip']=$payslipcountInfo;
	   $res->metadata=array('dashboard_count' =>$dataArray);
       $res->metadata=$dataArray;	   
	   echo json_encode($res);
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

