<?php
include('../class/MyClass.php');
include('class/Display.php');
include('class/Update.php');
include('class/Delete.php');
include('class/Add.php');

$global_obj 	= 	new MyClass();
$global_mysqli	=	$global_obj->MyClasss();		// Start DB Connection Object

$display 		= 	new Display();
$update 		= 	new Update();
$add 		= 	new Add();
$delete 		= 	new Delete();
class sessionData{
	//public $id ="";
	public $expires = true;
	//public $LOGINTOKEN = "";
	public $is_LoggedIn = true;
	//public $platform = null;
} 
class responseData{
}
class dataFormat {
	public $success = false;
   // public $login_status;
	public $message = "";
    public $session;
    public $metadata;     
    public $resCode="";     
    public static $message_invaliduser="Invalid user name or password";     
    public static $msg_loged_user="Logged in successfully.";     
    public static $msg_logedout_user="Logged out successfully.";     
    public static $msg_incorrect_usertoken="Token is not correct.";     
    public static $msg_new_recordadded="Data added successfully.";     
       
    public static $msg_recordadd_fail="Data is not added please try again.";     
    public static $msg_required="Please fill required field.";     
    public static $msg_required_usercode="Please fill user unique code.";     
    public static $msg_rquired_token="Please fill token field.";     
    public static $msg_required_type="Please fill required type.";     
    public static $msg_list="Get list of all data.";     
    public static $msg_detail="Enquiry details.";     
    public static $msg_no_data_list="Data not found.";     
    public static $msg_task_closed="Task has been closed successfully.";     
    public static $msg_task_closedissue="There is an issue in task close.";     
    public static $DeviceId="DeviceId";     
    public static $Plateform="Plateform";     
    public static $Version="Version";     
    public static $unique_code="unique_code";     
    public static $login_token="login_token";     
    public static $type="type";     
    public static $res_success=100;     
    public static $res_fail=101;     
    public static $res_deactiveuser=103;     
    public static $res_acces_token_expired=104;     
    public static $res_other=105;     
    public static $static_value_forholiday=5;   /////change the hours value for holiday in attendence  
    public static $user_pic_url="http://torque.realsoftware.co.in/upload/user_pic/";     
    public static $pan_pic_url="http://torque.realsoftware.co.in/upload/pan_card/";     
    public static $adhar_pic_url="http://torque.realsoftware.co.in/upload/adhar_card/";     
    public static $task_pic_url="http://torque.realsoftware.co.in/api/upload/"; 
    public static $folder_doc_url="http://torque.realsoftware.co.in/api/folder/"; 
    public static $msg_attendence_done="Attendence done successfully.";     
    public static $msg_attendence_alreadydone="Attendance is already punched for the day.";     
    public static $msg_trip_notstarted="Trip has not started yet.";     
    public static $here_map_api_key="Gc3fkvACpFpey8DnpknUBGRisaK7pJsTPULLOJYKzvk";     
    public static $amount_rs=12; 

    public static $msg_trip_closed="Trip has been closed successfully.";      
    public static $msg_enquiry_closed="Enquiry has been closed successfully.";      
    public static $msg_trip_default_latlong="lat long added successfully.";      
    public static $msg_trip_started="Trip has been started successfully.";      
    public static $msg_trip_alreadystarted="Trip is already started.";      
    public static $msg_trip_wrongkey="you have entered wrong key.";      
    public static $limit=10000;  
    public static $msg_new_recordupdated="Data updated successfully."; 	
    public static $msg_jobcard_closed="Job card has been closed successfully."; 
    public static $msg_jobcard_closedissue="There is an issue in job card close.";     
    public static $msg_leave_alreadyadded="Leave is already applied for same date range.";     
    
	 
         
    
}
?>
 