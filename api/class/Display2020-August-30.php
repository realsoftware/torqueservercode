<?php

class Display
{
	
	############################ 		Standard Functions		#########################
	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
    #########Change Password Mail Function 
	public function changePasswordEmail($email,$fName,$newPassword)
	{         
            $subject = "Reset your Torque user password";
            $from = " support@torque.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            $message .= "<p style='color:darkblue;'><img src='' style='width:25px;height:25px;'>&nbsp;&nbsp;&nbsp;<strong>Torque</strong></p>" . "\r\n";
            $message .= "<p>Hey $fName,</p>" . "\r\n";
            $message .= "<p>Sombody recentally asked to reset your torque password. </p>" . "\r\n";
            $message .= "<p>If you did not request a new password , let us know immidiately.</p>" . "\r\n";
            $message .= "<p>Password: $newPassword</p>" . "\r\n";
            //---------------End code-----------------
            mail($email, $subject, $message, $headers);

	}
	########Show All Module List By ID
	public function showAllMenuListByID($unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT *,user_role_acces.id as module_id FROM `user_role_acces` 
		
		LEFT JOIN master_module ON master_module.code =  user_role_acces.fk_access_module_code  
		where  fk_user_unique_code='$unique_code' ORDER BY user_role_acces.id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	############################ 		Authorizaton Functions		#########################
	#########CheckLogin Token
	public function CheckLoginToken($unique_code,$login_token)
	{
		    $mysqli_s 	= 	$this->CONN();
		    $queryy     =   "SELECT * FROM `user` 
								 WHERE token ='$login_token' AND status = '1' AND fk_type_of_usercode IN('DLR','MGR','OFF') ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
				$num		=	$result->num_rows;
				if(@$num>0)
				{				
					return 1;
					$query->close();
					
				}
				else
				{
					return 0;
					
				}		
			
		
	}
	public function CheckLogin($unique_code,$mobile_no)
	{
		$mysqli_s 	= 	$this->CONN();
		$data		=	array();
		if(@$unique_code=='')
		{
			$data		=	"Please Fill User Code/ Login ID";
		}
		else
		{
			if(@$mobile_no=='')
			{
				$data		=	"Please Fill mobile no.";
			}
			else
			{
             $queryy     =   "SELECT * FROM `user` 
								 WHERE unique_code ='$unique_code' AND mobile_no ='$mobile_no' AND status = '1' AND fk_type_of_usercode IN('DLR','MGR','OFF') ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
				$num		=	$result->num_rows;
				if(@$num>0)
				{				
					$data	=	$result->fetch_assoc();
					$query->close();
					
				}
				else
				{
					@$data		=	"";
					
				}		
			}
		}

		return $data;
	}
	


	########Show All Master Activity List
	public function showAllMasterActivityList()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `master_activity`
		where  status='1' ORDER BY activity_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
   ########Show All Master Status List
	public function showAllMasterStatusList()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `master_status`
		where  status='1' ORDER BY status_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
    ##########Show All Task List	
	public function showAllTaskList($task_detail,$unique_code)
	{
		$limit=10;
	    $from_date= $task_detail->from_date;
		$to_date= $task_detail->to_date;
		$activity_type_id= $task_detail->activity_type_id;
		$pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') AND fk_activity_id='$activity_type_id' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $activity_type_id=="")
		{
		     $cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND fk_activity_id='$activity_type_id' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id=="")
		{
			$cond="where fk_userunique_code='$unique_code' AND task_detail.status='1' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT task_id,fk_activity_id as activity_id,fk_status_id as status_id,planned_date,customer_name,mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,status_name,task_detail.created_date,task_detail.updated_date
		from `task_detail`
		LEFT JOIN master_activity ON master_activity.activity_id =  task_detail.fk_activity_id 
		LEFT JOIN master_status ON master_status.status_id =  task_detail.fk_status_id  
		$cond";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
					//  print_r($result);
$i=0;
		foreach($result as $results)
		{
		    $image_unique_code=$results['image_unique_code'];
			$data['id']=$results['image_unique_code'];
			$data['task_id']=$results['task_id'];
			//$data[] = $results;
			  $info=$this->showAllImageList($image_unique_code,$unique_code);
			 // print_r($info);
			 $j=0;
			  foreach($info as $infos)
			  {
				$data['image_list'][$j]['image_name']=$infos['image_name'];  
				$data['image_list'][$j]['image_path']=$infos['image_path'];  
				$j++;
			  }
			  
			$i++;
		}
	
		return @$data;	
	}	
    
	 ##########Show All Task List	
	public function showAllImageList($image_unique_code,$unique_code)
	{		
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' AND fk_image_unique_code='$image_unique_code'";
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;	
	}	
    public function toShowAllTaskList($task_detail,$unique_code)
	{
		$global_mysqli 		= 	$this->CONN();

        $limit=10;
	    $from_date= $task_detail->from_date;
		$to_date= $task_detail->to_date;
		$activity_type_id= $task_detail->activity_type_id;
		$pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') AND fk_activity_id='$activity_type_id' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $activity_type_id=="")
		{
		     $cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND fk_activity_id='$activity_type_id' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id=="")
		{
			$cond="where fk_userunique_code='$unique_code' AND task_detail.status='1' ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		}
       // $mysqli_s 		= 	$this->CONN();
       // $data		=	array();
	   /* $queryy     =   "SELECT task_id,fk_activity_id as activity_id,fk_status_id as status_id,planned_date,customer_name,mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,status_name,task_detail.created_date,task_detail.updated_date
		from `task_detail`
		LEFT JOIN master_activity ON master_activity.activity_id =  task_detail.fk_activity_id 
		LEFT JOIN master_status ON master_status.status_id =  task_detail.fk_status_id  
		$cond";	*/				


		// Query 6
		$queryy61    		=   "SELECT task_id,fk_activity_id as activity_id,fk_status_id as status_id,planned_date,customer_name,
		mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,status_name,task_detail.created_date as task_created_date,task_detail.updated_date as task_updated_date
		from `task_detail`
		LEFT JOIN master_activity ON master_activity.activity_id =  task_detail.fk_activity_id 
		LEFT JOIN master_status ON master_status.status_id =  task_detail.fk_status_id  
		$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			=	@$row611['task_id'];
				$activity_id					=	@$row611['activity_id'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['status_name'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$image_unique_code				=	@$row611['image_unique_code'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_id']	=	$activity_id;
				$data[$j]['status_id']  	=	$status_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['status_name']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' AND fk_image_unique_code LIKE '$image_unique_code%' ";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return $data;
		
	}
	
    #######Task List By Today
	  public function toShowAllTaskListByDateToday($task_detail,$unique_code)
	{
		$global_mysqli 		= 	$this->CONN();

        $limit=10;
	    $pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		  $cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  DATE(planned_date)=CURDATE() ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
		
      	// Query 6
		$queryy61    		=   "SELECT task_id,fk_activity_id as activity_id,fk_status_id as status_id,planned_date,customer_name,
		mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,status_name,task_detail.created_date as task_created_date,task_detail.updated_date as task_updated_date
		from `task_detail`
		LEFT JOIN master_activity ON master_activity.activity_id =  task_detail.fk_activity_id 
		LEFT JOIN master_status ON master_status.status_id =  task_detail.fk_status_id  
		$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			=	@$row611['task_id'];
				$activity_id					=	@$row611['activity_id'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['status_name'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$image_unique_code				=	@$row611['image_unique_code'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_id']	=	$activity_id;
				$data[$j]['status_id']  	=	$status_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['status_name']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' AND fk_image_unique_code LIKE '$image_unique_code%' ";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return $data;
		
	}
	
        #######Task List By  Tommorrow
	  public function toShowAllTaskListByDateTomorrow($task_detail,$unique_code)
	{
		$global_mysqli 		= 	$this->CONN();

        $limit=10;
	    $pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		
		$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  DATE(planned_date)=(CURDATE() + INTERVAL 1 DAY) ORDER BY DATE(planned_date) DESC LIMIT $pag_no,$limit ";
      	// Query 6
		$queryy61    		=   "SELECT task_id,fk_activity_id as activity_id,fk_status_id as status_id,planned_date,customer_name,
		mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,status_name,task_detail.created_date as task_created_date,task_detail.updated_date as task_updated_date
		from `task_detail`
		LEFT JOIN master_activity ON master_activity.activity_id =  task_detail.fk_activity_id 
		LEFT JOIN master_status ON master_status.status_id =  task_detail.fk_status_id  
		$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			=	@$row611['task_id'];
				$activity_id					=	@$row611['activity_id'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['status_name'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$image_unique_code				=	@$row611['image_unique_code'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_id']	=	$activity_id;
				$data[$j]['status_id']  	=	$status_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['status_name']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' AND fk_image_unique_code LIKE '$image_unique_code%' ";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return $data;
		
	}
	########Upload Task Image 
	public function UploadImages($image_unique_code,$unique_code){
    //getinbg file info
    $info = pathinfo($_FILES['image']['name']);
    // get the extension of the file
    $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

    $newname = "$image_unique_code.".$ext; 
    // Path to move uploaded files
    $target_path = "upload/";
    // getting server ip address
    $file_upload_url =$target_path;  
	if($ext=="jpg" || $ext=="JPG" || $ext=="png" || $ext=="PNG")
	{
    if (isset($_FILES['image']['name'])) {
        $target_path = $target_path . $newname;
        $response['file_name'] = $newname;
        try {
            // Throws exception incase file is not being moved
            if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                // make error flag true
                $response['error'] = true;
                $response['message'] = 'Could not move the file!';
            } 
            // File successfully uploaded
            $Path=$file_upload_url .$newname;
            $imgPath=$Path;
			##########Insert Image detail in DB
			$global_mysqli 		= 	$this->CONN();
			$timestamp = date("Y-m-d H:i:s");	

            $queryy2    	=   "INSERT INTO `image_detail` SET  fk_user_unique_code ='$unique_code',
			                                                    fk_image_unique_code='$image_unique_code',
			                                                    image_name          ='$image_unique_code',
			                                                    image_path          ='$newname',
			                                                    created_date        ='$timestamp',
			                                                    updated_date        ='$timestamp'";
			$queryy2 	= 	$global_mysqli->prepare($queryy2);
			$queryy2->execute();
			##################################
            $response['message'] = 'File uploaded successfully!';
            $response['error'] = false;
            $response['file_path'] = $Path;
            $response['file_url']=$imgPath;
        }
        catch (Exception $e) {
            // Exception occurred. Make error flag true
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        }
    } else {
        // File parameter is missing
        $response['error'] = true;
        $response['message'] = 'Not received any file!';
    }
	}
	else
	{
	////////File parameter is missing
        $response['error'] = true;
        $response['message'] = 'Please send jpg or png format only!';	
    }
   return $response;
}
	##################END RANJEET CODE
	
}

?>