<?php

class Display
{
	
	############################ 		Standard Functions		#########################
	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
    #########Change Password Mail Function 
	public function changePasswordEmail($email,$fName,$newPassword)
	{         
            $subject = "Reset your Torque user password";
            $from = " support@torque.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            $message .= "<p style='color:darkblue;'><img src='' style='width:25px;height:25px;'>&nbsp;&nbsp;&nbsp;<strong>Torque</strong></p>" . "\r\n";
            $message .= "<p>Hey $fName,</p>" . "\r\n";
            $message .= "<p>Sombody recentally asked to reset your torque password. </p>" . "\r\n";
            $message .= "<p>If you did not request a new password , let us know immidiately.</p>" . "\r\n";
            $message .= "<p>Password: $newPassword</p>" . "\r\n";
            //---------------End code-----------------
            mail($email, $subject, $message, $headers);

	}
  #########To Show Current Date Attendence statrt
    public function toShowAttendenceSTatus($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT `attendence_status`  FROM `attendence_detail`
			WHERE fk_user_unique_code='$unique_code' AND DATE(created_date) ='$current_date' ORDER BY created_date DESC LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
    #########To Show Current Date Trip statrt or end latest
    public function toShowTripSTatus($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT type FROM `trip_detail`
			WHERE fk_userunique_code='$unique_code' AND DATE(created_date) ='$current_date'  ORDER BY created_date DESC LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	########Show All Module List By ID
	public function showAllMenuListByID($unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT master_module.id,fk_user_unique_code,fk_access_module_code,fk_create,fk_edit,fk_view,fk_delete,fk_download,
		master_module.created_date,updated_date,code,module_name,status,master_module.update_date,
		user_role_acces.id as module_id, master_module.position FROM `user_role_acces` 
		
		LEFT JOIN master_module ON master_module.code =  user_role_acces.fk_access_module_code  
		
		where  fk_user_unique_code='$unique_code' AND  plateform IN ('app','both') ORDER BY master_module.position  ASC";					

		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	############################ 		Authorizaton Functions		#########################
	#########CheckLogin Token
	public function CheckLoginToken($unique_code,$login_token)
	{
		    $mysqli_s 	= 	$this->CONN();
		    $queryy     =   "SELECT * FROM `user` 
								 WHERE token ='$login_token' AND status = '1' AND fk_type_of_usercode IN('DLR','MGR','OFF') ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
				$num		=	$result->num_rows;
				if(@$num>0)
				{				
					return 1;
					$query->close();
					
				}
				else
				{
					return 0;
					
				}		
			
		
	}
	public function CheckLogin($unique_code,$mobile_no)
	{
		$mysqli_s 	= 	$this->CONN();
		$data		=	array();
		if(@$unique_code=='')
		{
			$data		=	"Please Fill User Code/ Login ID";
		}
		else
		{
			if(@$mobile_no=='')
			{
				$data		=	"Please Fill mobile no.";
			}
			else
			{
            $queryy     =   "SELECT * FROM `user` 
								 WHERE unique_code ='$unique_code' AND mobile_no ='$mobile_no' AND status = '1' AND fk_type_of_usercode IN('DLR','MGR','OFF') ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
				$num		=	$result->num_rows;
				if(@$num>0)
				{				
					$data	=	$result->fetch_assoc();
					$query->close();
					
				}
				else
				{
					@$data		=	"";
					
				}		
			}
		}

		return $data;
	}
	


	########Show All Master Activity List
	public function showAllMasterActivityList()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `master_activity`
		where  status='1' ORDER BY activity_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
		########Show All Master Activity List
	public function showAllMasterClaimActivityList()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT id as activity_id,code,article_type as activity_name,status,created_date,update_date FROM `master_claimtype` AS mlt  where  mlt.status='1' ORDER BY mlt.id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
   ########Show All Master Status List
	public function showAllMasterStatusList()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `master_status`
		where  status='1' ORDER BY status_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}  
	 ##########Show All Task List	
	public function showAllImageList($image_unique_code,$unique_code)
	{		
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' AND fk_image_unique_code='$image_unique_code'";
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;	
	}	
   public function toShowAllTaskList($task_detail,$unique_code)
	{
		$global_mysqli 		= 	$this->CONN();

         $limit = dataFormat::$limit;
	    $from_date= $task_detail->from_date;
		$to_date= $task_detail->to_date;
		$activity_type_id= $task_detail->activity_type_id;
		$pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') AND fk_activity_id='$activity_type_id' ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $activity_type_id=="")
		{
		     $cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  (DATE(planned_date) >= '$from_date' AND DATE(planned_date)<= '$to_date') ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id!="")
		{
			$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND fk_activity_id='$activity_type_id' ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $activity_type_id=="")
		{
			$cond="where fk_userunique_code='$unique_code' AND task_detail.status='1' ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
			$queryy61    		=   "SELECT task_id,closed_image_unique_code,fk_activity_id as activity_code,planned_date,customer_name,mobile_no,actual_completion_date
			,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
			task_detail.updated_date as task_updated_date from `task_detail`
			LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
			$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			            =	@$row611['task_id'];
				$activity_id					=	@$row611['activity_code'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['task_status'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$closed_image_unique_code				=	@$row611['closed_image_unique_code'];
				$close_remark				    =	@$row611['close_remark'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_code']	=	$activity_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['task_status']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['closed_image_unique_code']	=	$closed_image_unique_code;
				$data[$j]['close_remark']	=	$close_remark;
				
				
				$queryy62    	=   "SELECT * FROM `image_detail` WHERE fk_user_unique_code='$unique_code' 
                AND (fk_image_unique_code = '$image_unique_code' || fk_image_unique_code = '$closed_image_unique_code') ORDER BY  created_date DESC";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return @$data;
		
	}

    #######Task List By Today
	  public function toShowAllTaskListByDateToday($task_detail,$unique_code)
	{
		$current_date=DATE('Y-m-d');
		$global_mysqli 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		  $cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  DATE(planned_date)='$current_date' ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
		
      	// Query 6
		$queryy61    		=   "SELECT task_id,closed_image_unique_code,fk_activity_id as activity_code,planned_date,customer_name,mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
		$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			            =	@$row611['task_id'];
				$activity_id					=	@$row611['activity_code'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['task_status'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$closed_image_unique_code				=	@$row611['closed_image_unique_code'];
				$close_remark				    =	@$row611['close_remark'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_code']	=	$activity_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['task_status']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['closed_image_unique_code']	=	$closed_image_unique_code;
				$data[$j]['close_remark']	=	$close_remark;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code' 
				AND (fk_image_unique_code = '$image_unique_code' || fk_image_unique_code = '$closed_image_unique_code') ORDER BY  created_date ";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return @$data;
		
	}
	
        #######Task List By  Tommorrow
	  public function toShowAllTaskListByDateTomorrow($task_detail,$unique_code)
	{
        $tommorow_datetime=date('Y-m-d', strtotime(' +1 day'));
		$global_mysqli 		= 	$this->CONN();
        $limit = dataFormat::$limit;
        //$limit=10;
	    $pag_no= $task_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		
		$cond=" where fk_userunique_code='$unique_code' AND task_detail.status='1' AND  DATE(planned_date)='$tommorow_datetime' ORDER BY DATE(task_detail.created_date) DESC LIMIT $pag_no,$limit ";
      	// Query 6
		$queryy61    		=   "SELECT task_id,closed_image_unique_code,fk_activity_id as activity_code,planned_date,customer_name,mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
		$cond";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$task_id			            =	@$row611['task_id'];
				$activity_id					=	@$row611['activity_code'];
				$status_id				     	=	@$row611['status_id'];
				$planned_date					=	@$row611['planned_date'];
				$customer_name					=	@$row611['customer_name'];
				$mobile_no					    =	@$row611['mobile_no'];
				$actual_completion_date			=	@$row611['actual_completion_date'];
				$remarks					    =	@$row611['remarks'];
				$image_unique_code				=	@$row611['image_unique_code'];
				$activity_name					=	@$row611['activity_name'];
				$status_name					=	@$row611['task_status'];
				$task_created_date				=	@$row611['task_created_date'];
				$task_updated_date				=	@$row611['task_updated_date'];
				$closed_image_unique_code				=	@$row611['closed_image_unique_code'];
				$close_remark				    =	@$row611['close_remark'];
				
				
				$data[$j]['task_id']		=	$task_id;
				$data[$j]['activity_code']	=	$activity_id;
				$data[$j]['planned_date']	=	$planned_date;
				$data[$j]['customer_name']	=	$customer_name;
				$data[$j]['mobile_no']  	=	$mobile_no;
				$data[$j]['actual_completion_date']	=	$actual_completion_date;
				$data[$j]['remarks']	    =	$remarks;
				$data[$j]['image_unique_code']	=	$image_unique_code;
				$data[$j]['activity_name']	=	$activity_name;
				$data[$j]['task_status']	=	$status_name;
				$data[$j]['task_created_date']	=	$task_created_date;
				$data[$j]['task_updated_date']	=	$task_updated_date;
				$data[$j]['closed_image_unique_code']	=	$closed_image_unique_code;
				$data[$j]['close_remark']	=	$close_remark;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$unique_code'
				AND (fk_image_unique_code = '$image_unique_code' || fk_image_unique_code = '$closed_image_unique_code') ORDER BY  created_date ";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return @$data;
		
	}
	########Upload Task Image 
	/*public function UploadImages($image_unique_code,$unique_code,$type){
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $attendence_detail->from_date;
		$to_date= $attendence_detail->to_date;
		$attendence_type= $attendence_detail->attendence_type;
		$pag_no= $attendence_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $attendence_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND  (DATE(attendence_date) >= '$from_date' AND DATE(attendence_date)<= '$to_date') AND type='$attendence_type' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $attendence_type=="")
		{
		     $cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND  (DATE(attendence_date) >= '$from_date' AND DATE(attendence_date)<= '$to_date') ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $attendence_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND type='$attendence_type' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $attendence_type=="")
		{
			$cond="where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();
	    // $queryy     =   "SELECT * from attendence_detail  $cond";
		// $query 		= 	$mysqli_s->prepare($queryy);
		// $query->execute();
		// $result 	= 	$query->get_result();
		// foreach($result as $results)
		// {
			// $data[] = $results;
		// }
		
		
		// Query 6
			$queryy61    		=   "SELECT * from attendence_detail  $cond";						
								
			$query61 		= 	$mysqli_s->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$attendence_id			                =	@$row611['attendence_id'];
				$fk_user_unique_code					=	@$row611['fk_user_unique_code'];
				$attendence_date				     	=	@$row611['attendence_date'];
				$in_time					            =	@$row611['in_time'];
				$out_time				             	=	@$row611['out_time'];
				$duration_hours					        =	@$row611['duration_hours'];
				$type                       			=	@$row611['type'];
				$status					                =	@$row611['status'];
				$created_date			            	=	@$row611['created_date'];
				$updated_date				        	=	@$row611['updated_date'];
				$attendence_status				    	=	@$row611['attendence_status'];
				$lat				                    =	@$row611['lat'];
				$long			                    	=	@$row611['long'];
				$image_unique_code			        	=	@$row611['image_unique_code'];
				$attendence_in_address				    =	@$row611['attendence_in_address'];
				$attendence_out_address				    =	@$row611['attendence_out_address'];
				$country				                =	@$row611['country'];
				$state				                    =	@$row611['state'];
				$city				                    =	@$row611['city'];
				$district				                =	@$row611['district'];
				$subdistrict				            =	@$row611['subdistrict'];
				$postal_code				            =	@$row611['postal_code'];
				
				
				$data[$j]['attendence_id']		        =	$attendence_id;
				$data[$j]['fk_user_unique_code']     	=	$fk_user_unique_code;
				$data[$j]['attendence_date']	        =	$attendence_date;
				$data[$j]['in_time']	                =	$in_time;
				$data[$j]['out_time']               	=	$out_time;
				$data[$j]['duration_hours']         	=	$duration_hours;
				$data[$j]['type']	                    =	$type;
				$data[$j]['status']	                    =	$status;
				$data[$j]['created_date']           	=	$created_date;
				$data[$j]['updated_date']	            =	$updated_date;
				$data[$j]['attendence_status']	        =	$attendence_status;
				$data[$j]['lat']                    	=	$lat;
				$data[$j]['long']	                    =	$long;
				$data[$j]['image_unique_code']       	=	$image_unique_code;
				$data[$j]['attendence_in_address']	    =	$attendence_in_address;
				$data[$j]['attendence_out_address']  	=	$attendence_out_address;
				$data[$j]['country']	                =	$country;
				$data[$j]['state']	                    =	$state;
				$data[$j]['city']	                    =	$city;
				$data[$j]['district']	                =	$district;
				$data[$j]['subdistrict']            	=	$subdistrict;
				$data[$j]['postal_code']	            =	$postal_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$fk_user_unique_code' AND fk_image_unique_code = '$image_unique_code' ";						
				$query62 		= 	$mysqli_s->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];
					$type			    =	@$row622['type'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
					$data[$j]['image_list'][$k]['type']             	=	$type;
										
					$k++;
				}
				$j++;
			
		}

		
		
		
		
		return @$data;
	}
	*/
	#########Trip List By Date type from to date
	 // public function toShowAllTripList($trip_detail,$unique_code)
	 // {
  //  getinbg file info
    // $info = pathinfo($_FILES['image']['name']);
  //  get the extension of the file
    // $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

    // $newname = $image_unique_code.".".$ext; 
   // Path to move uploaded files
	// if($type=="task")
	// {
		 // $target_path = "upload/task/";
	// }
   // else
	// {
	  	 // $target_path = "upload/attenedence/";
    // }
  //  getting server ip address
    // $file_upload_url =$target_path;
	// if($ext=="jpg" || $ext=="JPG" || $ext=="png" || $ext=="PNG")
	// {
    // if (isset($_FILES['image']['name'])) {
        // $target_path = $target_path . $newname;
        // $response['file_name'] = $newname;
        // try {
          //  Throws exception incase file is not being moved
            // if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
              //  make error flag true
                // $response['error'] = true;
                // $response['message'] = 'Could not move the file!';
            // } 
          //  File successfully uploaded
            // $Path=$file_upload_url .$newname;
            // $imgPath=$Path;
			// ##########Insert Image detail in DB
			// $global_mysqli 		= 	$this->CONN();
			// $timestamp = date("Y-m-d H:i:s");	

            // $queryy2    	=   "INSERT INTO `image_detail` SET  fk_user_unique_code ='$unique_code',
			                                                    // fk_image_unique_code='$image_unique_code',
			                                                    // image_name          ='$image_unique_code',
			                                                    // image_path          ='$newname',
			                                                    // created_date        ='$timestamp',
			                                                    // updated_date        ='$timestamp',
			                                                    // type                ='$type'
																// ";
			// $queryy2 	= 	$global_mysqli->prepare($queryy2);
			// $queryy2->execute();
			// ##################################
            // $response['message'] = 'File uploaded successfully!';
            // $response['error'] = false;
            // $response['file_path'] = $Path;
            // $response['file_url']=$imgPath;
        // }
        // catch (Exception $e) {
           // Exception occurred. Make error flag true
            // $response['error'] = true;
            // $response['message'] = $e->getMessage();
        // }
    // } else {
      //  File parameter is missing
        // $response['error'] = true;
        // $response['message'] = 'Not received any file!';
    // }
	// }
	// else
	// {
	//////File parameter is missing
        // $response['error'] = true;
        // $response['message'] = 'Please send jpg or png format only!';	
    // }
   // return $response;
// }
 


  #########Attendence List By Date type from to date
	 public function toShowAllAttendencList($attendence_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $attendence_detail->from_date;
		$to_date= $attendence_detail->to_date;
		$attendence_type= $attendence_detail->attendence_type;
		$pag_no= $attendence_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $attendence_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND  (DATE(attendence_date) >= '$from_date' AND DATE(attendence_date)<= '$to_date') AND type='$attendence_type' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $attendence_type=="")
		{
		     $cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND  (DATE(attendence_date) >= '$from_date' AND DATE(attendence_date)<= '$to_date') ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $attendence_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' AND type='$attendence_type' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $attendence_type=="")
		{
			$cond="where fk_user_unique_code='$unique_code' AND attendence_detail.status='1' ORDER BY DATE(attendence_date) DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();
	    // $queryy     =   "SELECT * from attendence_detail  $cond";
		// $query 		= 	$mysqli_s->prepare($queryy);
		// $query->execute();
		// $result 	= 	$query->get_result();
		// foreach($result as $results)
		// {
			// $data[] = $results;
		// }
		
		
		// Query 6
			$queryy61    		=   "SELECT * from attendence_detail  $cond";						
								
			$query61 		= 	$mysqli_s->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$attendence_id			                =	@$row611['attendence_id'];
				$fk_user_unique_code					=	@$row611['fk_user_unique_code'];
				$attendence_date				     	=	@$row611['attendence_date'];
				$in_time					            =	@$row611['in_time'];
				$out_time				             	=	@$row611['out_time'];
				$duration_hours					        =	@$row611['duration_hours'];
				$type                       			=	@$row611['type'];
				$status					                =	@$row611['status'];
				$created_date			            	=	@$row611['created_date'];
				$updated_date				        	=	@$row611['updated_date'];
				$attendence_status				    	=	@$row611['attendence_status'];
				$lat				                    =	@$row611['lat'];
				$long			                    	=	@$row611['long'];
				$image_unique_code			        	=	@$row611['image_unique_code'];
				$attendence_in_address				    =	@$row611['attendence_in_address'];
				$attendence_out_address				    =	@$row611['attendence_out_address'];
				$country				                =	@$row611['country'];
				$state				                    =	@$row611['state'];
				$city				                    =	@$row611['city'];
				$district				                =	@$row611['district'];
				$subdistrict				            =	@$row611['subdistrict'];
				$postal_code				            =	@$row611['postal_code'];
				
				
				$data[$j]['attendence_id']		        =	$attendence_id;
				$data[$j]['fk_user_unique_code']     	=	$fk_user_unique_code;
				$data[$j]['attendence_date']	        =	$attendence_date;
				$data[$j]['in_time']	                =	$in_time;
				$data[$j]['out_time']               	=	$out_time;
				$data[$j]['duration_hours']         	=	$duration_hours;
				$data[$j]['type']	                    =	$type;
				$data[$j]['status']	                    =	$status;
				$data[$j]['created_date']           	=	$created_date;
				$data[$j]['updated_date']	            =	$updated_date;
				$data[$j]['attendence_status']	        =	$attendence_status;
				$data[$j]['lat']                    	=	$lat;
				$data[$j]['long']	                    =	$long;
				$data[$j]['image_unique_code']       	=	$image_unique_code;
				$data[$j]['attendence_in_address']	    =	$attendence_in_address;
				$data[$j]['attendence_out_address']  	=	$attendence_out_address;
				$data[$j]['country']	                =	$country;
				$data[$j]['state']	                    =	$state;
				$data[$j]['city']	                    =	$city;
				$data[$j]['district']	                =	$district;
				$data[$j]['subdistrict']            	=	$subdistrict;
				$data[$j]['postal_code']	            =	$postal_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$fk_user_unique_code' AND fk_image_unique_code = '$image_unique_code' ";						
				$query62 		= 	$mysqli_s->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];
					$type			    =	@$row622['type'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
					$data[$j]['image_list'][$k]['type']             	=	$type;
										
					$k++;
				}
				$j++;
			
		}

		
		
		
		
		return @$data;
	}
	
	#########Sales Officer List
	 public function toShowAllOfficerList($sales_officer_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $sales_officer_unique_code= $sales_officer_detail->sales_officer_unique_code;
		$pag_no= $sales_officer_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		//////autosugession list
		 if($sales_officer_unique_code!="")
		{
		    $queryy     =   "SELECT * from user WHERE (unique_code LIKE '%$sales_officer_unique_code%' || f_name LIKE '%$sales_officer_unique_code%') AND fk_type_of_usercode='OFF'";
		}
		//////case for all list 
		if($sales_officer_unique_code=="")
		{
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
			$queryy     =   "SELECT * from user  WHERE fk_dealear_code='$unique_code' AND fk_type_of_usercode='OFF'";
			}
			if($user_type=="MGR")
			{
			$queryy     =  "SELECT * from user  WHERE fk_manager_code='$unique_code' AND fk_type_of_usercode='OFF'";
			}
		     //$cond=" where fk_userunique_code='$unique_code' AND status='1' ORDER BY trip_date_time DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$row61 	= 	$query->get_result();
		$j=0;
		foreach($row61 as $row611)
			{
			$officer_unique_code=	$row611['unique_code'];
			$fk_type_of_usercode=	$row611['fk_type_of_usercode'];
			$fk_cat_code        =	$row611['fk_cat_code'];
			$fk_typeofcommodity_code        =	$row611['fk_typeofcommodity_code'];
			$fk_dealear_code        =	$row611['fk_dealear_code'];
			$fk_manager_code        =	$row611['fk_manager_code'];
			$l_name        =	$row611['l_name'];
			$f_name        =	$row611['f_name'];
			$m_name        =	$row611['m_name'];
			$mobile_no        =	$row611['mobile_no'];
			$alternate_no        =	$row611['alternate_no'];
			$email_id        =	$row611['email_id'];
			$address        =	$row611['address'];
			$queryy62    	=   "SELECT* FROM trip_latlong_history   WHERE 
            fk_userunique_code = '$officer_unique_code' Order by created_date desc LIMIT 1 ";	
            $query 		= 	$mysqli_s->prepare($queryy62);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$data[$j]['unique_code']		        =	$officer_unique_code;
			$data[$j]['fk_type_of_usercode']		    =	$fk_type_of_usercode;
			$data[$j]['fk_cat_code']		            =	$fk_cat_code;
			$data[$j]['fk_typeofcommodity_code']		        =	$fk_typeofcommodity_code;
			$data[$j]['fk_dealear_code']		        =	$fk_dealear_code;
			$data[$j]['fk_manager_code']		        =	$fk_manager_code;
			$data[$j]['l_name']		                    =	$l_name;
			$data[$j]['f_name']		        =	$f_name;
			$data[$j]['m_name']		        =	$m_name;
			$data[$j]['mobile_no']		        =	$mobile_no;
			$data[$j]['alternate_no']		        =	$alternate_no;
			$data[$j]['email_id']		        =	$email_id;
			$data[$j]['address']		        =	$address;
			##################History Table List
			$data[$j]['fk_trip_id']		                =	$row['fk_trip_id']	;
			$data[$j]['lat']		                    =	$row['lat']	;
			$data[$j]['long']		                    =	$row['long']	;
			$data[$j]['total_time_hours']		        =	$row['total_time_hours']	;
			$data[$j]['total_km_between_latlong']		=	$row['total_km_between_latlong']	;
			$data[$j]['created_date']		            =	$row['created_date']	;
			$data[$j]['updated_date']		            =	$row['updated_date']	;
			$data[$j]['trip_start_address']		        =	$row['trip_start_address']	;
			$data[$j]['trip_end_address']		        =	$row['trip_end_address']	;
			$data[$j]['country']		                =	$row['country']	;
			$data[$j]['state']		                    =	$row['state']	;
			$data[$j]['city']		                    =	$row['city']	;
			$data[$j]['district']		                =	$row['district']	;
			$data[$j]['subdistrict']		            =	$row['subdistrict']	;
			$data[$j]['postal_code']		            =	$row['postal_code']	;
			$j++;
			}
		
		return @$data;
	}
	###########Count All For APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListAttendence($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
            			#######Total Count current month for attendence list	
			$queryy     =  "SELECT *  FROM attendence_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  attendence_detail.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND type='Present'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count=mysqli_num_rows($query);
			#######Total Count Today for attendence list
			$queryy     =  "SELECT *  FROM attendence_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  attendence_detail.fk_user_unique_code  
                           WHERE DATE(created_date) ='$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND type='Present'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for attendence list	
			$queryy     =  "SELECT *  FROM attendence_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  attendence_detail.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND type='Present'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count=mysqli_num_rows($query);
			
			
			#######Total Count Today for attendence list
			$queryy     =  "SELECT *  FROM attendence_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  attendence_detail.fk_user_unique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND type='Present'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today,'monthcount'=>$count);
			 return $data;			
	}

	###########Count All For APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListTrip($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM trip_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  trip_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')
";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM trip_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  trip_detail.fk_userunique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM trip_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  trip_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM trip_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  trip_detail.fk_userunique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}
	
###########Count All Folder APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListProduct($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM product_folder_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  product_folder_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM product_folder_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  product_folder_detail.fk_userunique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM product_folder_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  product_folder_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM product_folder_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  product_folder_detail.fk_userunique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}
	
	###########Count All Enquiry APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListEnquiry($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND `user`.`unique_code`='$unique_code'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND `user`.`unique_code`='$unique_code'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			
			###########Enquiry Status Count
			 #######Open Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(expected_delivert_date) >= MONTH(CURRENT_DATE())  AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='open cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_opencases=mysqli_num_rows($query);
			#######Total Count Today for Open Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='open cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_opencases=mysqli_num_rows($query);
			 #######won  Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='won cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_woncases=mysqli_num_rows($query);
			#######Total Count Today for won Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='won cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_woncases=mysqli_num_rows($query);
			 #######lost  Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='lost cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_lostcases=mysqli_num_rows($query);
			#######Total Count Today for lost cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='lost cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_lostcases=mysqli_num_rows($query);
			 #######dropped cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='dropped cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_droppedcases=mysqli_num_rows($query);
			#######Total Count Today for dropped cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND enquiry_status='dropped cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_droppedcases=mysqli_num_rows($query);
			#######################################
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			
			###########Enquiry Status Count
			 #######Open Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(expected_delivert_date) >= MONTH(CURRENT_DATE())  AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='open cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_opencases=mysqli_num_rows($query);
			#######Total Count Today for Open Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='open cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_opencases=mysqli_num_rows($query);
			 #######won  Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='won cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_woncases=mysqli_num_rows($query);
			#######Total Count Today for won Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='won cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_woncases=mysqli_num_rows($query);
			 #######lost  Cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='lost cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_lostcases=mysqli_num_rows($query);
			#######Total Count Today for lost cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='lost cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_lostcases=mysqli_num_rows($query);
			 #######dropped cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='dropped cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip_droppedcases=mysqli_num_rows($query);
			#######Total Count Today for dropped cases
			$queryy     =  "SELECT *  FROM enquiry_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  enquiry_details.fk_usercode  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND enquiry_status='dropped cases'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip_droppedcases=mysqli_num_rows($query);
			
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip,'daycount_opencases'=>$count_today_trip_opencases,'monthcount_opencases'=>$count_current_month_trip_opencases,
		   'daycount_woncases'=>$count_today_trip_woncases,'monthcount_woncases'=>$count_current_month_trip_woncases,
		   'daycount_lostcases'=>$count_today_trip_lostcases,'monthcount_lostcases'=>$count_current_month_trip_lostcases,
		   'daycount_droppedcases'=>$count_today_trip_droppedcases,'monthcount_droppedcases'=>$count_current_month_trip_droppedcases);
			 return $data;
			
	}
    ###########Count All JOB APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListJOB($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM job_card_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  job_card_details.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM job_card_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  job_card_details.fk_user_unique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM job_card_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  job_card_details.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM job_card_details
                           LEFT JOIN `user` ON `user`.`unique_code` =  job_card_details.fk_user_unique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}
###########Count All Leave APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountLeave($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_leave
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_leave.fk_user_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_leave
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_leave.fk_user_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_leave
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_leave.fk_user_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_leave
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_leave.fk_user_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}
###########Count All TADA APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountTADA($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_tada
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_tada.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_tada
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_tada.fk_user_unique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_tada
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_tada.fk_user_unique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_tada
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_tada.fk_user_unique_code  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}
###########Count All Pay Slip APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountPaySlip($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
          #######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_salary_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_salary_detail.fk_userunique_id  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_salary_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_salary_detail.fk_userunique_id  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
			if($user_type=="MGR")
			{
			#######Total Count current month for Trip List	
			$queryy     =  "SELECT *  FROM hr_salary_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_salary_detail.fk_userunique_id  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_trip=mysqli_num_rows($query);
			#######Total Count Today for Trip List
			$queryy     =  "SELECT *  FROM hr_salary_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  hr_salary_detail.fk_userunique_id  
                           WHERE DATE(created_date) = '$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code')";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_trip=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_trip,'monthcount'=>$count_current_month_trip);
			 return $data;
			
	}

	##################END RANJEET CODE

	###########Count All For APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListOpenTask($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
            #######Total Count current month for Task List	
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE  (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND task_status='open'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_task=mysqli_num_rows($query);
			#######Total Count Today for Task List
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE DATE(created_date) ='$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND task_status='open'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_task=mysqli_num_rows($query);
			
			}
			if($user_type=="MGR")
			{
				#######Total Count current month for Task List	
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE  (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND task_status='open'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_task=mysqli_num_rows($query);
			#######Total Count Today for Task List
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE DATE(created_date) ='$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND task_status='open'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_task=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_task,'monthcount'=>$count_current_month_task);
		
			 return $data;
			
			
	}
	###########Count All For APP Dashboard Current Date And Current Month
	public function toShowAllDashboardCountListCloseTask($unique_code)
	{
		$current_date=DATE('Y-m-d');
		$mysqli_s 		= 	$this->CONN();
		//////case for all list 
		$data=array();
			$user_type=substr($unique_code,0,3);
			if($user_type=="DLR")
			{
            #######Total Count current month for Task List	
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND task_status='close'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_task=mysqli_num_rows($query);
			#######Total Count Today for Task List
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE DATE(created_date) ='$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_dealear_code`='$unique_code') AND task_status='close'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_task=mysqli_num_rows($query);
			
			}
			if($user_type=="MGR")
			{
				#######Total Count current month for Task List	
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE MONTH(created_date) = MONTH(CURRENT_DATE()) AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND task_status='close'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_current_month_task=mysqli_num_rows($query);
			#######Total Count Today for Task List
			$queryy     =  "SELECT *  FROM task_detail
                           LEFT JOIN `user` ON `user`.`unique_code` =  task_detail.fk_userunique_code  
                           WHERE DATE(created_date) ='$current_date' AND (`user`.`unique_code`='$unique_code' || `user`.`fk_manager_code`='$unique_code') AND task_status='close'";
	        $query 		= 	$mysqli_s->query($queryy);
			$count_today_task=mysqli_num_rows($query);
			}
      	   $data=array('daycount'=>$count_today_task,'monthcount'=>$count_current_month_task);
		
			 return $data;
			
			
	}
	
	##################END RANJEET CODE
	
	########Upload Task Image 
	public function UploadImages($image_unique_code,$unique_code,$type){
    //getinbg file info
    $info = pathinfo($_FILES['image']['name']);
    // get the extension of the file
    $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

    $newname = $image_unique_code.".".$ext; 
    // Path to move uploaded files
	if($type=="task")
	{
		 $target_path = "upload/task/";
	}
	if($type=="remark")
	{
		 $target_path = "upload/enquiry/";
	}
	if($type=="closeenquiry")
	{
		 $target_path = "upload/enquiry/";
	}
    if($type=="attenedence")
	{
	  	 $target_path = "upload/attenedence/";
    }
	if($type=="tada")
	{
		 $target_path = "upload/tada/";
	}
    // getting server ip address
    $file_upload_url =$target_path;
	if($ext=="jpg" || $ext=="JPG" || $ext=="png" || $ext=="PNG")
	{
    if (isset($_FILES['image']['name'])) {
        $target_path = $target_path . $newname;
        $response['file_name'] = $newname;
        try {
            // Throws exception incase file is not being moved
            if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                // make error flag true
                $response['error'] = true;
                $response['message'] = 'Could not move the file!';
            } 
            // File successfully uploaded
            $Path=$file_upload_url .$newname;
            $imgPath=$Path;
			##########Insert Image detail in DB
			$global_mysqli 		= 	$this->CONN();
			$timestamp = date("Y-m-d H:i:s");	

            $queryy2    	=   "INSERT INTO `image_detail` SET  fk_user_unique_code ='$unique_code',
			                                                    fk_image_unique_code='$image_unique_code',
			                                                    image_name          ='$image_unique_code',
			                                                    image_path          ='$newname',
			                                                    created_date        ='$timestamp',
			                                                    updated_date        ='$timestamp',
			                                                    type                ='$type'
																";
			$queryy2 	= 	$global_mysqli->prepare($queryy2);
			$queryy2->execute();
			##################################
            $response['message'] = 'File uploaded successfully!';
            $response['error'] = false;
            $response['file_path'] = $Path;
            $response['file_url']=$imgPath;
        }
        catch (Exception $e) {
            // Exception occurred. Make error flag true
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        }
    } else {
        // File parameter is missing
        $response['error'] = true;
        $response['message'] = 'Not received any file!';
    }
	}
	else
	{
	////////File parameter is missing
        $response['error'] = true;
        $response['message'] = 'Please send jpg or png format only!';	
    }
   return $response;
}
  
#########Trip List By Date type from to date
	 public function toShowAllTripList($trip_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $trip_detail->from_date;
		$to_date= $trip_detail->to_date;
		$pag_no= $attendence_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="")
		{
		     $cond=" where fk_userunique_code='$unique_code' AND status='1' AND  (DATE(trip_date_time) >= '$from_date' AND DATE(trip_date_time)<= '$to_date') ORDER BY trip_date_time DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="")
		{
		     $cond=" where fk_userunique_code='$unique_code' AND status='1' ORDER BY trip_date_time DESC LIMIT $pag_no,$limit ";
		}
		
      	// Query 6
	    $data		=	array();
	    $queryy     =   "SELECT * from trip_detail  $cond";
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	##########Enquiry Master List
	public function showMasterCustomerType()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT code,customer_type as name from `master_type_of_enquiery_customertype`
		where  status='1' ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All State
	public function ShowAllState()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(state_code) as code ,state_name as name FROM `master_pincode_withstate` where status=1 ORDER BY state_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All Model Interested
	public function ShowAllModelInterested()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT model_code as code ,model_name as name FROM `master_model_interested` where model_status=1 ORDER BY model_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All Implemented Interested
	public function ShowAllImplementedInterested()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,implement_interested_type as name FROM `master_implement_interested` where status=1 ORDER BY implement_interested_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All Source Of Enquiery
	public function ShowAllSourceOfEnquiery()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,source_of_enquiery as name FROM `master_source_enquiery` where status=1 ORDER BY source_of_enquiery ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Commodity Type
		public function ShowAllTypeCommodity()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,commodity_uses_type as name FROM `master_commodity_usestype` where status=1 ORDER BY commodity_uses_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Commodity Type
		public function ShowAllArticleType()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,article_type as name FROM `master_article_type` where status=1 ORDER BY article_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
########Show Commodity Type
		public function ShowAllTypeBuyer()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,buyer_type as name FROM `master_buyer_type` where status=1 ORDER BY buyer_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Commodity Type
		public function ShowAllTypeFinanace()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,finance_type as name FROM `master_finance_type` where status=1 ORDER BY finance_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Commodity Type
		public function ShowAllTypeFinanacerName()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,financier_name as name FROM `master_financier` where status=1 ORDER BY financier_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
    ########Show All District By state ID
	public function showAllDistrictByID($districtInfo)
	{
		$id=$districtInfo->state_code;
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(dist_code) as code,dist_name as name FROM `master_pincode_withstate` where state_code='$id' AND status=1 ORDER BY dist_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show All City By state ID
	public function showAllCityByID($cityInfo)
	{
		$id=$cityInfo->dist_code;
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT taluka_code as code,taluka_name as name FROM `master_taluka_city` where dist_code='$id' AND status=1 ORDER BY taluka_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	###########Show All Pincode by district ID
	public function showAllPincodeByID($cityInfo)
	{
	    $id=$cityInfo->dist_code;
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(pin_code) as name FROM `master_pincode_withstate` where dist_code='$id' AND status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	##########Enquiry Details
	 public function toShowEnquiryDetails($enquiry_detail,$unique_code)
	{
	    $enquiry_code= $enquiry_detail->enquiry_code;
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT *  FROM `enquiry_details`
			WHERE fk_usercode='$unique_code' AND enquiry_code ='$enquiry_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	#########Enqiury List By Date type from to date
	 public function toShowAllEnquiryList($enquiry_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $enquiry_detail->from_date;
		$to_date= $enquiry_detail->to_date;
		$type= $enquiry_detail->enquiry_type;
		$types=explode(',',$type);
		$enquiry_type = join("','",$types);  

		$pag_no= $enquiry_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $enquiry_type!="")
		{
		  $cond=" where fk_usercode='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND enquiry_status IN('$enquiry_type') ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $enquiry_type=="")
		{
		   $cond=" where fk_usercode='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date')
		   ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $enquiry_type!="")
		{
		    $cond=" where fk_usercode='$unique_code' AND status='1' AND enquiry_status IN('$enquiry_type')
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $enquiry_type=="")
		{
			$cond="where fk_usercode='$unique_code' AND status='1' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();


		    $queryy    		=   "SELECT enquiry_code,customer_name,enquiry_status,expected_delivert_date as estimated_deliery_date ,fk_model_intrested as model,created_date from enquiry_details  $cond";			


			$query 		= 	$mysqli_s->prepare($queryy);
	    	$query->execute();
		    $result 	= 	$query->get_result();
		    foreach($result as $results)
		    {
			$data[] = $results;
		    }			
			/*$query61 		= 	$mysqli_s->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$attendence_id			                =	@$row611['attendence_id'];
				$fk_user_unique_code					=	@$row611['fk_user_unique_code'];
				$attendence_date				     	=	@$row611['attendence_date'];
				$in_time					            =	@$row611['in_time'];
				$out_time				             	=	@$row611['out_time'];
				$duration_hours					        =	@$row611['duration_hours'];
				$type                       			=	@$row611['type'];
				$status					                =	@$row611['status'];
				$created_date			            	=	@$row611['created_date'];
				$updated_date				        	=	@$row611['updated_date'];
				$attendence_status				    	=	@$row611['attendence_status'];
				$lat				                    =	@$row611['lat'];
				$long			                    	=	@$row611['long'];
				$image_unique_code			        	=	@$row611['image_unique_code'];
				$attendence_in_address				    =	@$row611['attendence_in_address'];
				$attendence_out_address				    =	@$row611['attendence_out_address'];
				$country				                =	@$row611['country'];
				$state				                    =	@$row611['state'];
				$city				                    =	@$row611['city'];
				$district				                =	@$row611['district'];
				$subdistrict				            =	@$row611['subdistrict'];
				$postal_code				            =	@$row611['postal_code'];
				
				
				$data[$j]['attendence_id']		        =	$attendence_id;
				$data[$j]['fk_user_unique_code']     	=	$fk_user_unique_code;
				$data[$j]['attendence_date']	        =	$attendence_date;
				$data[$j]['in_time']	                =	$in_time;
				$data[$j]['out_time']               	=	$out_time;
				$data[$j]['duration_hours']         	=	$duration_hours;
				$data[$j]['type']	                    =	$type;
				$data[$j]['status']	                    =	$status;
				$data[$j]['created_date']           	=	$created_date;
				$data[$j]['updated_date']	            =	$updated_date;
				$data[$j]['attendence_status']	        =	$attendence_status;
				$data[$j]['lat']                    	=	$lat;
				$data[$j]['long']	                    =	$long;
				$data[$j]['image_unique_code']       	=	$image_unique_code;
				$data[$j]['attendence_in_address']	    =	$attendence_in_address;
				$data[$j]['attendence_out_address']  	=	$attendence_out_address;
				$data[$j]['country']	                =	$country;
				$data[$j]['state']	                    =	$state;
				$data[$j]['city']	                    =	$city;
				$data[$j]['district']	                =	$district;
				$data[$j]['subdistrict']            	=	$subdistrict;
				$data[$j]['postal_code']	            =	$postal_code;
				
				
				$queryy62    	=   "SELECT * from `image_detail` WHERE fk_user_unique_code='$fk_user_unique_code' AND fk_image_unique_code = '$image_unique_code' ";						
				$query62 		= 	$mysqli_s->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];
					$type			    =	@$row622['type'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
					$data[$j]['image_list'][$k]['type']             	=	$type;
										
					$k++;
				}
				$j++;
			
		}*/
		return @$data;
	}
	public function showMasterReasonLost()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT code,reason_for_lost as name from `master_reason`
		where  status='1' ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
       public function toShowAllVisitList($visit_detail,$unique_code)
	{
		$global_mysqli 		= 	$this->CONN();

        $limit = dataFormat::$limit;
	    $enquiry_code= $visit_detail->enquiry_code;
		$pag_no= $visit_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		      	// Query 6
			$queryy61    		=   "SELECT* from visit_remark where fk_user_unique_code='$unique_code' AND status='1' AND  fk_enquiry_code= '$enquiry_code' ORDER BY created_date DESC LIMIT $pag_no,$limit ";						
								
			$query61 		= 	$global_mysqli->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$visit_remark_id			 =	@$row611['visit_remark_id'];
				$visit_remark_code			 =	@$row611['visit_remark_code'];
				$fk_user_unique_code		 =	@$row611['fk_user_unique_code'];
				$fk_enquiry_code			 =	@$row611['fk_enquiry_code'];
				$remark					     =	@$row611['remark'];
				$remark_date				 =	@$row611['remark_date'];
				$created_date			     =	@$row611['created_date'];
				$image_unique_code			 =	@$row611['image_unique_code'];
				
				
				$data[$j]['remark_id']		    =	$visit_remark_id;
				$data[$j]['visit_remark_code']	=	$visit_remark_code;
				$data[$j]['user_unique_code']	=	$fk_user_unique_code;
				$data[$j]['enquiry_code']	=	$fk_enquiry_code;
				$data[$j]['remark']  	=	$remark;
				$data[$j]['remark_date']	=	$remark_date;
				$data[$j]['created_date']	    =	$created_date;
				$data[$j]['image_unique_code']	    =	$image_unique_code;
				
			    $queryy62    	=   "SELECT * FROM `image_detail` WHERE fk_user_unique_code='$unique_code' 
                AND fk_image_unique_code = '$image_unique_code' AND type='remark' ORDER BY  created_date DESC";						
				$query62 		= 	$global_mysqli->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		
		
		
		return @$data;
		
	}
    ##########Product Master List
	public function showMasterProduct()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT code,product_name as name from `master_product`
		where  status='1' ORDER BY product_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
    #########Product List
	 public function toShowAllProductList($product_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $product_code= $product_detail->product_code;
		$pag_no= $product_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
      	// Query 6
	    $data		=	array();
		// Query 6
		    $queryy    		=   "SELECT code,sub_product_name,fk_userunique_code,
			created_date,file_name,file_path from product_folder_detail  where fk_userunique_code='$unique_code' AND fk_product_code='$product_code' ORDER BY created_date DESC LIMIT $pag_no,$limit";			
			$query 		= 	$mysqli_s->prepare($queryy);
	    	$query->execute();
		    $result 	= 	$query->get_result();
		    foreach($result as $results)
		    {
			$data[] = $results;
		    }				
		return @$data;
	}

#########Product List
	 public function toShowAllProductList_App($product_detail,$unique_code)
	{
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $product_code= $product_detail->product_code;
		$pag_no= $product_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
      	// Query 6
	    $data		=	array();

                       $user_type=substr($unique_code,0,3);
			
                        if($user_type=="DLR")
			{
			 $queryy    		=   "SELECT code,sub_product_name,fk_userunique_code,
			created_date,file_name,file_path from product_folder_detail  where fk_userunique_code='$unique_code' AND fk_product_code='$product_code' ORDER BY created_date DESC LIMIT $pag_no,$limit";
			}
			else if($user_type=="MGR")
			{
			 $queryy    		=   "SELECT code,sub_product_name,fk_userunique_code,
			created_date,file_name,file_path from product_folder_detail  where fk_userunique_code=(SELECT `fk_dealear_code` FROM `user` WHERE `unique_code`='$unique_code') AND fk_product_code='$product_code' ORDER BY created_date DESC LIMIT $pag_no,$limit";
			}
			else 
			{
			 $queryy    		=   "SELECT code,sub_product_name,fk_userunique_code,
			created_date,file_name,file_path from product_folder_detail  where fk_userunique_code=(SELECT `fk_dealear_code` FROM `user` WHERE `unique_code`='$unique_code') AND fk_product_code='$product_code' ORDER BY created_date DESC LIMIT $pag_no,$limit";
			}	

		// Query 6
		    //$queryy    		=   "SELECT code,sub_product_name,fk_userunique_code,
			//created_date,file_name,file_path from product_folder_detail  where fk_userunique_code=(SELECT `fk_dealear_code` FROM `user` WHERE `unique_code`='$unique_code') AND //fk_product_code='$product_code' ORDER BY created_date DESC LIMIT $pag_no,$limit";		
			$query 		= 	$mysqli_s->prepare($queryy);
	    	$query->execute();
		    $result 	= 	$query->get_result();
		    foreach($result as $results)
		    {
			$data[] = $results;
		    }				
		return @$data;
	}
    ##########JOB Card Details
	 public function toShowJobCardDetails($jobcard_detail,$unique_code)
	{
	        $job_card_code= $jobcard_detail->job_card_code;
		    $mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT fk_user_unique_code,job_card_code,fk_type_of_work_code,customer_name,address,fk_taluka_code,fk_state_code,fk_district_code,
            pin_code,mobile_no,model,date_of_sale,registration_no,hours_completed,engine_no,customer_complaints,work_done_other_than_customercomplaints,
            expected_cost,expected_date_of_rapid,under_warranty,company_ref_no,created_date,updated_date,close_remark,job_card_close_datetime,job_card_create_remark,
            job_card_type  FROM `job_card_details`
			WHERE fk_user_unique_code='$unique_code' AND job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	############To Show All Job Card List By Date
	public function toShowAllJobCardList($jobcard_detail,$unique_code)
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
        $limit = dataFormat::$limit;
	    $from_date= $jobcard_detail->from_date;
		$to_date= $jobcard_detail->to_date;
		$job_type= $jobcard_detail->type;
		$pag_no= $jobcard_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $job_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND job_card_details.status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') AND job_card_type='$job_type' ORDER BY DATE(job_card_details.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $job_type=="")
		{
		     $cond=" where fk_user_unique_code='$unique_code' AND job_card_details.status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') ORDER BY DATE(job_card_details.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $job_type!="")
		{
			$cond=" where fk_user_unique_code='$unique_code' AND job_card_details.status='1' AND job_card_type='$job_type' ORDER BY DATE(job_card_details.created_date) DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $job_type=="")
		{
			$cond="where fk_user_unique_code='$unique_code' AND job_card_details.status='1' ORDER BY DATE(job_card_details.created_date) DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
			$queryy    		=   "SELECT fk_user_unique_code,job_card_code,fk_type_of_work_code,work_type,customer_name,address,
			mobile_no,model,date_of_sale,registration_no,hours_completed,engine_no,customer_complaints,work_done_other_than_customercomplaints,
            expected_cost,expected_date_of_rapid,under_warranty,company_ref_no,created_date,close_remark,job_card_close_datetime,
			job_card_create_remark,
            job_card_type  FROM `job_card_details` 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
			$cond";						
								
			$query 		= 	$mysqli_s->prepare($queryy);
		    $query->execute();
			$result 	= 	$query->get_result();
			foreach($result as $results)
			{
				$data[] = $results;
			}
		
			return @$data;
		
	}
	########Show Type Of Wrok
		public function ShowAllTypeWork()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,work_type as name FROM `master_type_of_work` where status=1 ORDER BY work_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Fuel Type
		public function ShowAllTypeFuel()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,oil_type as name FROM `master_oil_type` where status=1 ORDER BY oil_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	########Show Fuel Type
		public function showAllWorkType()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,work_type as name FROM `master_type_of_work` where status=1 ORDER BY work_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
       ########Show oil charges
		public function oilChargesList($jobcardInfo,$unique_code)
		{
	    $mysqli_s 		= 	$this->CONN();
		$job_card_code                  = $jobcardInfo->job_card_code;
        $data		=	array();
		$queryy     =   "SELECT oil_charge_id,fk_oil_type_code as oil_type_code,make,unit_liter,amount_per_liter,created_date FROM 
		`job_card_service_oil_charges` where fk_job_card_code='$job_card_code' AND fk_user_unique_code='$unique_code' ORDER BY oil_charge_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show out side work
		public function outSideWorkList($jobcardInfo,$unique_code)
		{
	    $mysqli_s 		= 	$this->CONN();
		$job_card_code                  = $jobcardInfo->job_card_code;
        $data		=	array();
        $queryy     =   "SELECT out_side_work_id,description,bill_no,total,created_date FROM 
		`job_card_service_out_side_work` where fk_job_card_code='$job_card_code' AND fk_user_unique_code='$unique_code' ORDER BY out_side_work_id ASC";		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show labour charge
		public function labourChargesList($jobcardInfo,$unique_code)
		{
	    $mysqli_s 		= 	$this->CONN();
		$job_card_code                  = $jobcardInfo->job_card_code;
        $data		=	array();
        $queryy     =   "SELECT labour_cost_id,time_taken as `time`,cost,created_date FROM 
		`job_card_service_labour_cost` where fk_job_card_code='$job_card_code' AND fk_user_unique_code='$unique_code' ORDER BY labour_cost_id ASC";		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Spare Charge
		public function spareChargesList($jobcardInfo,$unique_code)
		{
	    $mysqli_s 		= 	$this->CONN();
		$job_card_code                  = $jobcardInfo->job_card_code;
        $data		=	array();
        $queryy     =   "SELECT spare_charge_id,part_no,description,cost,quantity,complaint_code,made_code,total_cost,created_date FROM 
		`job_card_service_spare_charge` where fk_job_card_code='$job_card_code' AND fk_user_unique_code='$unique_code' ORDER BY spare_charge_id ASC";		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	#########TADA List By Date type from to date
	 public function toShowAllTADAList($tada_detail,$unique_code)
	{
		
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $tada_detail->from_date;
		$to_date= $tada_detail->to_date;
		$status= $tada_detail->status;
		$claim_no= $tada_detail->claim_no;
		
		$pag_no= $tada_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $status!="" && $claim_no!="")
		{
		 $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND tada_approve_status='$status' AND claim_type='$claim_no' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $status!="" && $claim_no=="")
		{
		   $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND tada_approve_status='$status' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $status=="" && $claim_no!="")
		{
		    $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND claim_type='$claim_no' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status!="" && $claim_no!="")
		{
		    $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND tada_approve_status='$status' AND claim_type='$claim_no'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status=="" && $claim_no!="")
		{
		    $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND claim_type='$claim_no'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status!="" && $claim_no=="")
		{
		    $cond=" where fk_user_unique_code='$unique_code' AND status='1' AND tada_approve_status='$status'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status=="" && $claim_no=="")
		{
			$cond="  where fk_user_unique_code='$unique_code' AND status='1' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();
		// Query 6
		    // $queryy    		=   "SELECT* from hr_tada  $cond";			
			// $query 		= 	$mysqli_s->prepare($queryy);
	    	// $query->execute();
		    // $result 	= 	$query->get_result();
		    // foreach($result as $results)
		    // {
			// $data[] = $results;
		    // }	
              	// Query 6
			$queryy61    		=   "SELECT* from hr_tada  $cond ";						
								
			$query61 		= 	$mysqli_s->prepare($queryy61);
			$query61->execute();
			$row61			=	$query61->get_result();
			$j = 0;
			foreach($row61 as $row611)
			{
				$tada_id		            	 =	@$row611['tada_id'];
				$fk_user_unique_code			 =	@$row611['fk_user_unique_code'];
				$tada_code              		 =	@$row611['tada_code'];
				$claim_type         			 =	@$row611['claim_type'];
				$amount					         =	@$row611['amount'];
				$remarks         				 =	@$row611['remarks'];
				$image_code             		 =	@$row611['image_code'];
				$status         				 =	@$row611['status'];
				$created_date         			 =	@$row611['created_date'];
				$updated_date   			     =	@$row611['updated_date'];
				$tada_approve_status 			 =	@$row611['tada_approve_status'];
				
				
				$data[$j]['tada_id']		                =	$tada_id;
				$data[$j]['fk_user_unique_code']		    =	$fk_user_unique_code;
				$data[$j]['tada_code']		                =	$tada_code;
				$data[$j]['claim_type']		                =	$claim_type;
				$data[$j]['amount']		                    =	$amount;
				$data[$j]['remarks']		                =	$remarks;
				$data[$j]['image_code']		                =	$image_code;
				$data[$j]['status']		                    =	$status;
				$data[$j]['created_date']		            =	$created_date;
				$data[$j]['updated_date']		            =	$updated_date;
				$data[$j]['tada_approve_status']		    =	$tada_approve_status;


				
			    $queryy62    	=   "SELECT * FROM `image_detail` WHERE fk_user_unique_code='$unique_code' 
                AND fk_image_unique_code = '$image_code' AND type='tada' ORDER BY  created_date DESC";				
				$query62 		= 	$mysqli_s->prepare($queryy62);
				$query62->execute();
				$row62			=	$query62->get_result();
				$k = 0;
				foreach($row62 as $row622)
				{
					$image_id			=	@$row622['image_id'];
					$image_name		    =	@$row622['image_name'];
					$image_path			=	@$row622['image_path'];

					$fk_image_unique_code   =	@$row622['fk_image_unique_code'];
					$created_date		    =	@$row622['created_date'];
					$updated_date		    =	@$row622['updated_date'];
					
					$data[$j]['image_list'][$k]['image_id']				=	$image_id;
					$data[$j]['image_list'][$k]['image_name']			=	$image_name;
					$data[$j]['image_list'][$k]['image_path']			=	$image_path;
					$data[$j]['image_list'][$k]['image_unique_code']	=	$fk_image_unique_code;
										
					$k++;
				}
				$j++;
			
		}
		   return @$data;
	}
       #########Leave List By Date type from to date
	     public function toShowAllLeaveList($leave_detail,$unique_code)
	     {
		
	 	$mysqli_s 		= 	$this->CONN();
        $limit = dataFormat::$limit;
       // $limit=10;
	    $from_date= $leave_detail->from_date;
		$to_date= $leave_detail->to_date;
		$status= $leave_detail->status;
		$leave_code= $leave_detail->leave_code;
		
		$pag_no= $leave_detail->pag_no;
		if($pag_no=="")
		{
			$pag_no=0;
		}
		if($from_date!="" && $to_date!="" && $status!="" && $leave_code!="")
		{
		 $cond=" where fk_user_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND approved_status='$status' AND leave_code='$leave_code' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $status!="" && $leave_code=="")
		{
		   $cond=" where fk_user_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND approved_status='$status' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date!="" && $to_date!="" && $status=="" && $leave_code!="")
		{
		    $cond=" where fk_user_code='$unique_code' AND status='1' AND  (DATE(created_date) >= '$from_date' AND DATE(created_date)<= '$to_date') 
		  AND leave_code='$leave_code' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status!="" && $leave_code!="")
		{
		    $cond=" where fk_user_code='$unique_code' AND status='1' AND approved_status='$status' AND claim_type='$leave_code'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status=="" && $leave_code!="")
		{
		    $cond=" where fk_user_code='$unique_code' AND status='1' AND claim_type='$leave_code'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status!="" && $leave_code=="")
		{
		    $cond=" where fk_user_code='$unique_code' AND status='1' AND approved_status='$status'
			ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
		if($from_date=="" && $to_date=="" && $status=="" && $leave_code=="")
		{
			$cond="  where fk_user_code='$unique_code' AND status='1' ORDER BY created_date DESC LIMIT $pag_no,$limit ";
		}
      	// Query 6
	    $data		=	array();
		// Query 6
		    $queryy    		=   "SELECT* from hr_leave  $cond";			
			$query 		= 	$mysqli_s->prepare($queryy);
	    	$query->execute();
		    $result 	= 	$query->get_result();
		    foreach($result as $results)
		    {
			$data[] = $results;
		    }			
		   return @$data;
	}
    
#########Leave List By Date type from to date
	     public function toShowAllPayList($unique_code)
	     {
			$mysqli_s 		= 	$this->CONN();
			$limit = dataFormat::$limit;
		   // $limit=10;
		   
			// Query 6
			$data		=	array();
		// Query 6
		    $queryy    		=   "SELECT* from pay_slip  WHERE YEAR(created_date)=YEAR(CURDATE()) AND status=1 AND fk_userunique_code='$unique_code' ORDER BY created_date DESC LIMIT 12 ";			
			$query 		= 	$mysqli_s->prepare($queryy);
	    	$query->execute();
		    $result 	= 	$query->get_result();
		    foreach($result as $results)
		    {
			$data[] = $results;
		    }			
		   return @$data;
	}
    #########To Show Leave PL SL in user access menu API
    public function toShowLeaveCount($unique_code)
	{
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT `pl_leave`,sl_leave  FROM `user`
			WHERE unique_code='$unique_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	###########Get total  PL leave taken by User
	 public function toGetTotalPLLeaveTakenByUser($unique_code)
	{
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT SUM(`no_of_day`) as day  FROM `hr_leave`
			WHERE fk_user_code='$unique_code' AND type_of_leave='PL' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	###########Get total  SL leave taken by User
	 public function toGetTotalSLLeaveTakenByUser($unique_code)
	{
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT SUM(`no_of_day`) as day  FROM `hr_leave`
			WHERE fk_user_code='$unique_code' AND type_of_leave='SL' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	#####Close ENquiry Detail
	   public function toShowCloseEnquiryDetail($unique_code,$close_enquiry_detail)
	     {
			$enquiry_code= $close_enquiry_detail->enquiry_code;
			$mysqli_m 	= 	$this->CONN();
		    // Query 6

		    $queryy    		=   "SELECT enquiry_code,enquiry_status,competition_tractor_baught_madel,
			fk_code_implement_interested,fk_code_reason_lost,close_enuiry_remark,close_lat,close_long,image_unique_code,close_enquiry_image_uniue_code
			from enquiry_details  WHERE enquiry_code='$enquiry_code' AND status=1 AND fk_usercode='$unique_code' LIMIT 1 ";			
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;
	}
	#####Close Job Card Detail
	   public function toShowCloseJobCardDetail($unique_code,$close_jobcard_detail)
	     {
			$job_card_code= $close_jobcard_detail->job_card_code;
			$mysqli_m 	= 	$this->CONN();
		    // Query 6
		    $queryy    		=   "SELECT job_card_code,close_remark,job_card_close_lat,job_card_close_long
			from job_card_details  WHERE job_card_code='$job_card_code' AND status=1 AND fk_user_unique_code='$unique_code' LIMIT 1 ";			
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;
	}
}

?>
