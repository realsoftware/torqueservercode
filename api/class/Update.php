<?php

class Update
{
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	#############UPdate Logged In User Status
	public function UpdateUserLoggedStatus($unique_code,$login_token)
	{
		$mysqli_s 				= 	$this->CONN();
		
		    $queryy2    =   "UPDATE  `user`   SET 	`is_loggedin` 		='0' 	
											WHERE 	`unique_code`=  '$unique_code' AND `token` =  '$login_token' ";					
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();

	}
    ############Update Token and device ID and logged in status
	public function UpdateUserToken($unique_code,$mobile_no,$LoginToken,$DeviceId,$Plateform,$Version)
	{            
		$mysqli_s 				= 	$this->CONN();
		
		    $queryy2    =   "UPDATE  `user`   SET 	`is_loggedin` 		='1' ,
													`device_id` 	=  '$DeviceId',
													`plateform` 	=  '$Plateform',
													`version` 	    =  '$Version',
													`token` 	    =  '$LoginToken'
											WHERE 	`unique_code` 	 				=  '$unique_code' AND mobile_no='$mobile_no' ";					
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
		
	}
	###########Closed Task 
	public function closedTask($taskInfo,$unique_code)
	{
		$mysqli_s 					= 	$this->CONN();
        $remark= $taskInfo->remark;
        $task_id= $taskInfo->task_id;
		$image_unique_code= $taskInfo->image_unique_code;
		$timestamp = date("Y-m-d H:i:s");	
		$actual_completion_date = date("Y-m-d");	
        ////insert query task
		if($unique_code!="")
		{
		$queryy2    =   "UPDATE  `task_detail`   SET 	`task_status` 			=  'close' ,
														`actual_completion_date` 			=  '$actual_completion_date' ,
														`close_remark` 		=  '$remark' ,
														`closed_image_unique_code` 		=  '$image_unique_code' ,
														`updated_date` 		=  '$timestamp' WHERE fk_userunique_code='$unique_code' AND task_id='$task_id'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
		return 1;
		}
		else
		{
			return 0;
		}
	}
	#########Cron Job run at 12 Night to close Trip and attaendence
	public function cronJobAttandenceTripClose()
	{
		$timestamp = date("Y-m-d H:i:s");	
	    $mysqli_s 					= 	$this->CONN();
        $queryy2    =   "UPDATE  `trip_detail`   SET 	`type` 			=  'close',
				                                        `updated_date`    = '$timestamp'
													     WHERE `type`='start'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
		$queryy2    =   "UPDATE  `attendence_detail`   SET 	`attendence_status` 			=  'out',
		                                                  `updated_date`    = '$timestamp'
														  WHER attendence_status='in'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();

	}
	########################Add New Enquiry Record
	public function updateEnquiry($enquiryInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
		$mysqli_s 					= $this->CONN();
        $enquiry_code               = $enquiryInfo->enquiry_code;
        $fk_customer_type           = $enquiryInfo->customer_type;
        $customer_name              = $enquiryInfo->customer_name;
        $address                    = $enquiryInfo->address;
        $fk_taluka_code             = $enquiryInfo->taluka_code;
        $fk_district_code           = $enquiryInfo->district_code;
        $fk_state_code              = $enquiryInfo->state_code;
        $fk_pin_code                = $enquiryInfo->pin_code;
        $mobile_no                  = $enquiryInfo->mobile_no;
        $fk_model_intrested         = $enquiryInfo->model_intrested;
        $fk_implemented_intrested   = $enquiryInfo->implemented_intrested;
        $implement_model            = $enquiryInfo->implement_model;
        $fk_source_of_enquiry       = $enquiryInfo->source_of_enquiry;
        $fk_tractor_application     = $enquiryInfo->tractor_application;
        $agricultural_land          = $enquiryInfo->agricultural_land;
        $irrigated_land             = $enquiryInfo->irrigated_land;
        $non_irrigated_land         = $enquiryInfo->non_irrigated_land;
        $fk_type_of_commodity_uses  = $enquiryInfo->type_of_commodity_uses;
        $fk_type_of_buyer           = $enquiryInfo->type_of_buyer;
        $existing_tractor           = $enquiryInfo->existing_tractor;
        $expected_delivert_date     = $enquiryInfo->expected_delivert_date;
        $fk_finanace_type           = $enquiryInfo->finanace_type;
        $fk_financiar_name          = $enquiryInfo->financiar_name;
        $image_unique_code          = $enquiryInfo->image_unique_code;
        $lat                        = $enquiryInfo->lat;
        $long                       = $enquiryInfo->long;
        $remark                       = $enquiryInfo->remark;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `enquiry_details`   SET
														
														`fk_usercode` 		            =  '$unique_code' ,
														`fk_customer_type` 		        =  '$fk_customer_type' ,
														`customer_name` 		        =  '$customer_name' ,
														`address` 		                =  '$address' ,
														`fk_taluka_code` 		        =  '$fk_taluka_code' ,
														`fk_district_code` 		        =  '$fk_district_code' ,
														`fk_state_code` 		        =  '$fk_state_code' ,
														`fk_pin_code` 		            =  '$fk_pin_code' ,
														`mobile_no` 		            =  '$mobile_no' ,
														`fk_model_intrested` 		    =  '$fk_model_intrested' ,
														`fk_implemented_intrested` 	    =  '$fk_implemented_intrested' ,
														`implement_model` 		        =  '$implement_model' ,
														`fk_source_of_enquiry` 		    =  '$fk_source_of_enquiry' ,
														`fk_tractor_application` 		=  '$fk_tractor_application' ,
														`agricultural_land` 		    =  '$agricultural_land' ,
														`irrigated_land` 		        =  '$irrigated_land' ,
														`non_irrigated_land` 		    =  '$non_irrigated_land' ,
														`fk_type_of_commodity_uses`     =  '$fk_type_of_commodity_uses' ,
														`fk_type_of_buyer` 		        =  '$fk_type_of_buyer' ,
														`existing_tractor` 		        =  '$existing_tractor' ,
														`expected_delivert_date` 	    =  '$expected_delivert_date' ,
														`fk_finanace_type` 		        =  '$fk_finanace_type' ,
														`fk_financiar_name` 		    =  '$fk_financiar_name' ,
														`image_unique_code` 		    =  '$image_unique_code' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`latlong_address` 		        =  '$complete_address' ,
														`updated_date` 		            =  '$timestamp' ,
														`latlong_country` 		        =  '$Country' ,
														`latlong_state` 		        =  '$State' ,
														`latlong_city` 		            =  '$City' ,
														`latlong_district` 		        =  '$District' ,
														`latlong_subdistrict` 		    =  '$Subdistrict' ,
														`remark` 	            	    =  '$remark' ,
														`latlong_postalcode` 		    =  '$PostalCode'   WHERE enquiry_code='$enquiry_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
	
########################Add New Enquiry Record
	public function closeEnquiry($enquiryInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
		$mysqli_s 					= $this->CONN();
        $enquiry_code               = $enquiryInfo->enquiry_code;
        $enuiry_status            = $enquiryInfo->enuiry_status;
        $competition_tractor_bought_model     = $enquiryInfo->competition_tractor_bought_model;
        $code_implementation_baought           = $enquiryInfo->implementation_bought;
        $image_unique_code           = $enquiryInfo->image_unique_code;
        $code_reason          = $enquiryInfo->reason_for_lost;
        $close_remark          = $enquiryInfo->remark;
        $lat                        = $enquiryInfo->lat;
        $long                       = $enquiryInfo->long;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `enquiry_details`   SET
														
														`enquiry_status`                  =  '$enuiry_status' ,
														`updated_date` 		              =  '$timestamp' ,
														`fk_code_reason_lost` 	          =  '$code_reason' ,
														`fk_code_implement_interested`    =  '$code_implementation_baought' ,
														`competition_tractor_baught_madel` =  '$competition_tractor_bought_model' ,
														`close_enquiry_image_uniue_code`  =  '$image_unique_code' ,
														`close_lat` 		              =  '$lat' ,
														`close_long` 		              =  '$long' ,
														`close_enquiry_address` 		  =  '$complete_address' ,
														`close_enquiry_country` 		  =  '$Country' ,
														`close_enquiry_state` 		      =  '$State' ,
														`close_enquiry_city` 		      =  '$City' ,
														`close_enquiry_district` 	      =  '$District' ,
														`close_enquiry_subdistrict` 	  =  '$Subdistrict' ,
														`close_enquiry_postalcode` 		  =  '$PostalCode' ,
														`close_enuiry_remark` 	          =  '$close_remark'    WHERE enquiry_code='$enquiry_code' AND fk_usercode='$unique_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
	public function updateJobCard($jobcardInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
    	$mysqli_s 					    = $this->CONN();
        $job_card_code                  = $jobcardInfo->job_card_code;
        $fk_type_of_work_code           = $jobcardInfo->fk_type_of_work_code;
        $customer_name                  = $jobcardInfo->customer_name;
        $address                        = $jobcardInfo->address;
        $fk_taluka_code                 = $jobcardInfo->taluka_code;
        $fk_district_code               = $jobcardInfo->district_code;
        $fk_state_code                  = $jobcardInfo->state_code;
        $pin_code                       = $jobcardInfo->pin_code;
        $mobile_no                      = $jobcardInfo->mobile_no;
        $model                          = $jobcardInfo->model;
        $date_of_sale                   = $jobcardInfo->date_of_sale;
        $registration_no                = $jobcardInfo->registration_no;
        $hours_completed                = $jobcardInfo->hours_completed;
        $engine_no                      = $jobcardInfo->engine_no;
        $customer_complaints            = $jobcardInfo->customer_complaints;
        $work_done_other_than_customercomplaints             = $jobcardInfo->work_done_other_than_customercomplaints;
        $expected_cost                  = $jobcardInfo->expected_cost;
        $expected_date_of_rapid         = $jobcardInfo->expected_date_of_rapid;
        $under_warranty                 = $jobcardInfo->under_warranty;
        $company_ref_no                 = $jobcardInfo->company_ref_no;
        $job_card_create_lat            = $jobcardInfo->lat;
        $job_card_create_long           = $jobcardInfo->long;
        $job_card_create_remark         = $jobcardInfo->remark;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$job_card_create_lat,$job_card_create_long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	   $queryy2    =   "UPDATE  job_card_details  SET  fk_type_of_work_code                        ='$fk_type_of_work_code', 
	                                                   customer_name                               ='$customer_name', 
													   address                                     ='$address', 
													   fk_taluka_code                              ='$fk_taluka_code', 
													   fk_state_code                               ='$fk_state_code', 
													   fk_district_code                            ='$fk_district_code', 
													   pin_code                                    ='$pin_code', 
													   mobile_no                                   ='$mobile_no', 
													   model                                       ='$model', 
													   date_of_sale                                ='$date_of_sale', 
													   registration_no                             ='$registration_no', 
													   hours_completed                             ='$hours_completed', 
													   engine_no                                   ='$engine_no', 
													   customer_complaints                         ='$customer_complaints', 
													   work_done_other_than_customercomplaints     ='$work_done_other_than_customercomplaints', 
													   expected_cost                               ='$expected_cost', 
													   expected_date_of_rapid                      ='$expected_date_of_rapid', 
													   under_warranty                              ='$under_warranty', 
													   company_ref_no                              ='$company_ref_no', 
													   updated_date                                ='$timestamp', 
													   job_card_create_lat                         ='$job_card_create_lat', 
													   job_card_create_long                        ='$job_card_create_long', 
													   job_card_create_address                     ='$complete_address', 
													   job_card_create_country                     ='$Country', 
													   job_card_create_state                       ='$State', 
													   job_card_create_city                        ='$City', 
													   job_card_create_district                    ='$District', 
													   job_card_create_subdistrict                 ='$Subdistrict', 
													   job_card_create_postalcode                  ='$PostalCode',
													   job_card_create_remark                      ='$job_card_create_remark'   WHERE job_card_code='$job_card_code'";
	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
	###########Closed JOB Card 
	public function closedJobCard($jobcardInfo,$unique_code)
	{
	    $api_key_heremap=dataFormat::$here_map_api_key;		
		$mysqli_s 					= 	$this->CONN();
        $remark= $jobcardInfo->remark;
        $job_card_code= $jobcardInfo->job_card_code;
        $lat= $jobcardInfo->lat;
        $long= $jobcardInfo->long;
		$timestamp = date("Y-m-d H:i:s");	
		 #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

		if($job_card_code!="")
		{
		$queryy2    =   "UPDATE  `job_card_details`   SET 	`job_card_type` 			=  'close' ,
														`close_remark` 		=  '$remark' ,
														job_card_close_lat                         ='$lat', 
													   job_card_close_long                        ='$long', 
													   job_card_close_address                     ='$complete_address', 
													   job_card_close_country                     ='$Country', 
													   job_card_close_state                       ='$State', 
													   job_card_close_city                        ='$City', 
													   job_card_close_district                    ='$District', 
													   job_card_close_subdistrict                 ='$Subdistrict', 
													   job_card_close_postalcode                  ='$PostalCode',
														`job_card_close_datetime` 		=  '$timestamp' 
														 WHERE fk_user_unique_code='$unique_code' AND job_card_code='$job_card_code'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
		return 1;
		}
		else
		{
			return 0;
		}
	}
	########################Update TADA Record
	public function updateTADA($tadaInfo,$unique_code)
	{
		$mysqli_s 					= $this->CONN();
		$tada_code                  = $tadaInfo->tada_code;
        $claim_type                    = $tadaInfo->claim_type;
        $amount                    = $tadaInfo->amount;
        $remarks                    = $tadaInfo->remarks;
        $image_code                    = $tadaInfo->image_code;
	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `hr_tada`   SET
														
														`claim_type` 		            =  '$claim_type' ,
														`amount` 		        =  '$amount' ,
														`remarks` 		                =  '$remarks' ,
														`image_code` 		        =  '$image_code' ,
														`updated_date` 	            	    =  '$timestamp' 
														  WHERE tada_code='$tada_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
      ########################Update LEAVE Record
	public function updateLeave($leaveInfo,$unique_code)
	{
		$mysqli_s 					= $this->CONN();
		 $leave_code                     = $leaveInfo->leave_code;
        $apply_for_leave                = $leaveInfo->apply_for_leave;
        $type_of_leave                  = $leaveInfo->type_of_leave;
        $from_date                      = $leaveInfo->from_date;
        $to_date                        = $leaveInfo->to_date;
        $no_of_day                      = $leaveInfo->no_of_day;
        $day_type                       = $leaveInfo->day_type;
        $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
		$queryy2    =   "UPDATE  `hr_leave`   SET
														
														`apply_for_leave` 		            =  '$apply_for_leave' ,
														`type_of_leave` 		            =  '$type_of_leave' ,
														`from_date` 		                =  '$from_date' ,
														`to_date` 		                    =  '$to_date' ,
														`no_of_day` 		                =  '$no_of_day' ,
														`day_type` 		                    =  '$day_type' ,
														`updated_date` 	            	    =  '$timestamp' 
														  WHERE leave_code='$leave_code' AND fk_user_code='$unique_code'";	
	  
	$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
	
	}
	#########Update Job Card Serviec
	########################Add New Job Card Service Record
	public function EditJobCardService($jobcardInfo,$unique_code)
	{
    	$mysqli_s 					    = $this->CONN();
        $job_card_code                  = $jobcardInfo['job_card_code'];
        $oil_charges                    = $jobcardInfo['oil_charges'];
        $out_side_work                    = $jobcardInfo['out_side_work'];
        $labour_charges                    = $jobcardInfo['labour_charges'];
        $spare_charges                    = $jobcardInfo['spare_charges'];
	  // $abc=$oil_charges[0]['oil_type_code'];
        $timestamp = date("Y-m-d H:i:s");
      // $myfile = file_put_contents('logs.txt', $jobcardInfo.PHP_EOL , FILE_APPEND | LOCK_EX);		
		#######Insert query of enquiry table		
		$queryy1    =   " DELETE FROM `job_card_service_oil_charges` WHERE `fk_job_card_code` =  '$job_card_code' ";				
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
	foreach($oil_charges as $oil_chargess)
	{
	$oil_code=$oil_chargess['oil_type_code'];	
	$make=$oil_chargess['make'];	
	$unit_liter=$oil_chargess['unit_liter'];	
	$total=$oil_chargess['amount_per_liter'];	
    $queryy2    =   "INSERT INTO job_card_service_oil_charges 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	fk_oil_type_code, 
	make, 
	unit_liter, 
	amount_per_liter, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$oil_code', 
	'$make', 
	'$unit_liter', 
	'$total', 
	'$timestamp'
	)";
			$query2 	= 	$mysqli_s->prepare($queryy2);
			

	     	$query2->execute();
	}
	
	##########Insert Out Side Work
		$queryy1    =   " DELETE FROM `job_card_service_out_side_work` WHERE `fk_job_card_code` =  '$job_card_code' ";				
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
	foreach($out_side_work as $out_side_works)
	{
	$description=$out_side_works['description'];	
	$bill_no=$out_side_works['bill_no'];	
	$total=$out_side_works['total'];	
    $queryy2    =   "INSERT INTO job_card_service_out_side_work 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	description, 
	bill_no,  
	total, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$description', 
	'$bill_no', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
	##########Insert Labour Charges
	$queryy1    =   " DELETE FROM `job_card_service_labour_cost` WHERE `fk_job_card_code` =  '$job_card_code' ";				
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
	foreach($labour_charges as $labour_chargess)
	{
	$description=$labour_chargess['description'];	
	$time=$labour_chargess['time_taken'];	
	$total=$labour_chargess['cost'];	
    $queryy2    =   "INSERT INTO job_card_service_labour_cost 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	description, 
	time_taken,  
	cost, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$description', 
	'$time', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
	
	##########Insert Spare Charges
	$queryy1    =   " DELETE FROM `job_card_service_spare_charge` WHERE `fk_job_card_code` =  '$job_card_code' ";				
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
	foreach($spare_charges as $spare_chargess)
	{
	$part_no=$spare_chargess['part_no'];	
	$description=$spare_chargess['description'];	
	$cost=$spare_chargess['cost'];	
	$quantity=$spare_chargess['quantity'];	
	$complaint_code=$spare_chargess['complaint_code'];	
	$made_code=$spare_chargess['made_code'];	
	$total=$spare_chargess['total_cost'];	
    $queryy2    =   "INSERT INTO job_card_service_spare_charge 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	part_no, 
	description, 
	cost,  
	quantity,  
	complaint_code,  
	made_code,  
	total_cost, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$part_no', 
	'$description', 
	'$cost', 
	'$quantity', 
	'$complaint_code', 
	'$made_code', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
		
     ######Check value jobcard exist or not

           $queryy     =   "SELECT * FROM `job_card_service_oil_charges` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
			$num1		=	$result->num_rows;
			$queryy1     =   "SELECT * FROM `job_card_service_out_side_work` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy1);
			$query->execute();
			$result 	= 	$query->get_result();
			$num2		=	$result->num_rows;
			 $queryy2     =   "SELECT * FROM `job_card_service_labour_cost` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy2);
			$query->execute();
			$result 	= 	$query->get_result();
			 $num3		=	$result->num_rows;
			 $queryy3     =   "SELECT * FROM `job_card_service_spare_charge` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy3);
			$query->execute();
		    $result 	= 	$query->get_result();
			$num4		=	$result->num_rows;
			if($num1>0 && $num2>0 && $num3>0 && $num4>0)
			{
		     return true;
			}
			else
			{
			 return false;
			}
		
	}
}
?>