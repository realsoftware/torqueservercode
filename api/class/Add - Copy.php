<?php
class Add
{	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	########################Add New User Type	
	public function addNewTask($taskInfo,$unique_code)
	{            
		$mysqli_s 					= 	$this->CONN();
		
        $activity_id= $taskInfo->activity_id;
		$planned_date= $taskInfo->planned_date;
		$customer_name= $taskInfo->customer_name;
		$mobile_no= $taskInfo->mobile_no;
		$actual_completion_date= $taskInfo->actual_completion_date;
		$status_id= $taskInfo->status_id;
		$remarks= $taskInfo->remarks;
		$timestamp = date("Y-m-d H:i:s");	
        ////insert query task
		
		if($activity_id!="")
		{
		$image_unique_code= $taskInfo->image_unique_code;
		$queryy2    =   "INSERT INTO  `task_detail`   SET 	`fk_activity_id` 			=  '$activity_id' ,
														`fk_userunique_code` 			=  '$unique_code' ,
														`planned_date` 		=  '$planned_date' ,
														`customer_name` 			=  '$customer_name' ,
														`mobile_no` 		=  '$mobile_no' ,
														`actual_completion_date`=  '$actual_completion_date' ,
														`fk_status_id` 		=  '$status_id' ,
														`remarks` 		=  '$remarks' ,
														`image_unique_code` 		=  '$image_unique_code' ,
														`created_date` 		=  '$timestamp' ,
														`updated_date` 		=  '$timestamp'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
		return 1;
		}
		else
		{
			return 0;
		}
	}
	
	########################Add New Attendec Record
	public function addAttendence($attendecInfo,$unique_code)
	{            
		$mysqli_s 					= 	$this->CONN();
		
        $attendence_type= $attendecInfo->attendence_type;
        $lat= $attendecInfo->lat;
        $long= $attendecInfo->long;
        $image_unique_code= $attendecInfo->image_unique_code;
		$attendence_date = date("Y-m-d");	
	    $timestamp = date("Y-m-d H:i:s");	
	    $in_time   = date("H:i:s");	
	    $out_time  = date("H:i:s");	
        //////Get in time value 
		 $q11 = "SELECT * FROM `attendence_detail`  WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'  ORDER BY attendence_id desc LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $in_time_value = $result11['in_time'];
			$duration_calculate_attendence_time = $result11['attendence_date'];
			#######Check in current date trip is started or not
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$attendence_date'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip>0)
			{
        ////insert query attendence
		if($attendence_type!="")
		{
			if($attendence_type=="in")
			{
			#######Check in current date there is a record inserted or not	
			$query      		=   "SELECT * FROM `attendence_detail`  WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'";
			$sql       	 		=   $mysqli_s->query($query); 
			
		    $num  				=   @mysqli_num_rows($sql);
			
			if(@$num==0)
			{
			#######Insert query of missing date

		    $q11 = "SELECT attendence_date FROM attendence_detail WHERE fk_user_unique_code = '$unique_code'  ORDER BY attendence_id desc LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $missing_date_current = $result11['attendence_date'];
			#######Get Day no between two dates
			if($missing_date_current)
			{
			// Calculating the difference in timestamps 
            $diff = strtotime($attendence_date) - strtotime($missing_date_current); 
            // 1 day = 24 hours 
            // 24 * 60 * 60 = 86400 seconds 
            $no_day_missing= abs(round($diff / 86400));

			####################################
			for($i=1;$i<=$no_day_missing; $i++)
			{
		    $missing_date_with_onedayadd = strtotime($i." day", strtotime($missing_date_current));
            $new_missing_date = date("Y-m-d", $missing_date_with_onedayadd);
			$type=date('l', strtotime($new_missing_date));////get day full name for holiday like saturday sunday
            if($type=='Saturday' || $type=='Sunday')
			{
				$present_type="holiday";
			}
			else
			{
			   $present_type="Absent";
			}
			$queryy2    =   "INSERT INTO  `attendence_detail`   SET
														`fk_user_unique_code` 			=  '$unique_code' ,
														`attendence_date` 		        =  '$new_missing_date' ,
														`in_time` 		             	=  '--' ,
														`out_time` 		             	=  '--' ,
														`type` 		                    =  '$present_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`attendence_status` 		    =  '$attendence_type',
														`lat` 		                    =  '$lat',
														`long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code'
														
														";	
			if($new_missing_date!=$attendence_date)
			{				
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			}
			}
			}
            ###########END Code	
			$type=date('l', strtotime($attendence_date));////get day full name for holiday like saturday sunday
            if($type=='Saturday' || $type=='Sunday')
			{
				$present_type="holiday";
			}
			else
			{
			   $present_type="Present";
			}			
			$queryy2    =   "INSERT INTO  `attendence_detail`   SET
														`fk_user_unique_code` 			=  '$unique_code' ,
														`attendence_date` 		        =  '$attendence_date' ,
														`in_time` 		             	=  '$in_time' ,
														`type` 		                    =  '$present_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`attendence_status` 		    =  '$attendence_type',
														`lat` 		                    =  '$lat',
														`long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code'
														";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 5;////attendence done
			}
			else{
			return 6;///////attendence already done
			}
			#########END 
			}////in condition closed
            else			
			{
			/////////Calculate in and out time duration	
			$in_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$duration_calculate_attendence_time.' '.$in_time_value);
			$out_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$attendence_date.' '.$out_time);
			$duration=$in_time_duration->diff($out_time_duration);
			$hours=number_format((float)($duration->days*24)+$duration->h+($duration->i/60)+($duration->s/3600),3,'.','');
			$hours_value=dataFormat::$static_value_forholiday;
			if($hours<$hours_value)
			{
				$present_type="halfday";
			}
			else
			{
			   $present_type="Present";	
			}
			//$time_duration=$in_time_value-$out_time;
			$queryy2    =   "UPDATE  `attendence_detail`   SET 
														`out_time` 			=  '$out_time' ,
														`duration_hours` 		    =  '$hours' ,
														`type` 		    =  '$present_type' ,
														`updated_date` 		=  '$timestamp',
														`attendence_status`  =  '$attendence_type',
														`lat` 		                    =  '$lat',
														`long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code'
														 WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 1;

			}///////out condition closed
		
		}
		else
		{
			return 0;
		}
	
	}//trip start condition closed
	else{
		return 4;///trip not started
	}
	}
	
	


########################Add New Trip Record
	public function addNewTrip($tripInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
        $amount_rs=dataFormat::$amount_rs;		
		$mysqli_s 					= 	$this->CONN();
        $trip_type= $tripInfo->trip_type;
        $lat= $tripInfo->lat;
        $long= $tripInfo->long;
	    $timestamp = date("Y-m-d H:i:s");	
	    $trip_date_time = date("Y-m-d H:i:s");	
	    $trip_date = date("Y-m-d");	
			#######Check in current date trip is started or not
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip==0)
			{
            ////insert query trip
		
			if($trip_type=="start")
			{
			#######Insert query of trip table
		    $queryy2    =   "INSERT INTO  `trip_detail`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`start_lat` 		            =  '$lat' ,
														`start_long` 		            =  '$long' ,
														`trip_date_time` 		        =  '$trip_date_time' ,
														`type` 		                    =  '$trip_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
	        $last_id 	= 	$mysqli_s->insert_id;
			#######Insert query of trip history table
		    $queryy3    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$last_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp'";	
			$query3 	= 	$mysqli_s->prepare($queryy3);
	     	$query3->execute();

			return 5;////trip started
			#########END 
			}////start condition closed
            else			
			{
				return 4; //failed to insert
			}///////out condition closed
	}//trip start condition closed
	else{
		    //////Get trip  time value 
		    $q11 = "SELECT * FROM `trip_latlong_history`  WHERE trip_history_id=(SELECT MAX(trip_history_id) FROM `trip_latlong_history`) AND fk_userunique_code='$unique_code' AND DATE(created_date)='$trip_date' LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $trip_id = $result11['fk_trip_id'];
            $start_lat = trim($result11['lat']);
            $start_long = trim($result11['long']);
			######Get distance hours from api 
            $url =  "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=$api_key_heremap&waypoint0=geo!$start_lat,$start_long&waypoint1=geo!$lat,$long&mode=fastest;car;traffic:disabled";
          //  $url = "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=Gc3fkvACpFpey8DnpknUBGRisaK7pJsTPULLOJYKzvk&waypoint0=geo!28.7041,77.1025&waypoint1=geo!25.3176,82.9739&mode=fastest;car;traffic:disabled";
            $data = file_get_contents($url);
            $result=json_decode($data,true);
            //print_r($result);
            $distance=$result['response']['route'][0]['summary']['distance'];
            $time=$result['response']['route'][0]['summary']['baseTime'];
            $total_time_inhours=trim($time)/3600;
            $total_distance_inkm=$distance/1000;
            if($trip_type=="default")
			{		
			#######Insert query of trip history table
		    $queryy2    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$trip_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`total_time_hours` 		        =  '$total_time_inhours' ,
														`total_km_between_latlong` 		=  '$total_distance_inkm' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 7;////trip default lat long
			#########END 
			}////start condition closed
            if($trip_type=="close")
			{
			#######Insert query of trip history table
		    $queryy9    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$trip_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`total_time_hours` 		        =  '$total_time_inhours' ,
														`total_km_between_latlong` 		=  '$total_distance_inkm' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp'";	
			$query9 	= 	$mysqli_s->prepare($queryy9);
	     	$query9->execute();
	
			//////Get trip  time value 
		    $q11 = "SELECT SUM(total_km_between_latlong) as total_km_between_latlong,SUM(total_time_hours) AS total_time_hours FROM trip_latlong_history 
			WHERE fk_trip_id='$trip_id' AND fk_userunique_code='$unique_code' AND DATE(created_date)='$trip_date' LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
			//print_r($result11);
            $total_km_between_latlong = $result11['total_km_between_latlong'];
            $total_time_hours = $result11['total_time_hours'];
			$total_amount = $total_km_between_latlong*$amount_rs;

            #####Get Actual Length and Actual Time
	    	$q11 = "SELECT * FROM `trip_detail`  WHERE trip_id='$trip_id' AND fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start' LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
		//	print_r($result11);
            $actual_start_lat = $result11['start_lat'];
            $actual_start_long = $result11['start_long'];			
            $url =  "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=$api_key_heremap&waypoint0=geo!$actual_start_lat,$actual_start_long&waypoint1=geo!$lat,$long&mode=fastest;car;traffic:disabled";
            $data = file_get_contents($url);
            $result=json_decode($data,true);
         ///  print_r($result);
            $actual_distance=$result['response']['route'][0]['summary']['distance'];
            $actual_time=$result['response']['route'][0]['summary']['baseTime'];
            $actual_total_time_inhours=trim($actual_time)/3600;
            $actual_total_distance_inkm=$actual_distance/1000;

			#######update query of trip  table
		    $queryy2    =   "UPDATE  `trip_detail`   SET 
														`end_lat` 		         	=  '$lat' ,
														`end_long` 		            =  '$long' ,
														`total_time_hours` 		    =  '$total_time_hours' ,
														`total_km_driven` 		    =  '$total_km_between_latlong' ,
														`total_amoutnt` 		    =  '$total_amount' ,
														`actual_distance_km` 	    =  '$actual_total_distance_inkm' ,
														`actual_time_hurs` 		    =  '$actual_total_time_inhours' ,
														`type` 		                =  '$trip_type' ,
														`updated_date` 		        =  '$timestamp'
														 WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND trip_id='$trip_id'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 8;/////updated master table trip closed
			}////closed condition closed
		}
	}
	
	#############################END Ranjeet Code
	
	
}

?>