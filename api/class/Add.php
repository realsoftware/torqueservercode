<?php
class Add
{	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	########################Add New User Type	
	public function addNewTask($taskInfo,$unique_code)
	{            
		$mysqli_s 					= 	$this->CONN();
		
        $activity_id= $taskInfo->activity_id;
		$planned_date= $taskInfo->planned_date;
		$customer_name= $taskInfo->customer_name;
		$mobile_no= $taskInfo->mobile_no;
		$actual_completion_date= $taskInfo->actual_completion_date;
		$status_id= $taskInfo->status_id;
		$remarks= $taskInfo->remarks;
		$timestamp = date("Y-m-d H:i:s");	
        ////insert query task
		
		if($activity_id!="")
		{
		$image_unique_code= $taskInfo->image_unique_code;
		$queryy2    =   "INSERT INTO  `task_detail`   SET 	`fk_activity_id` 			=  '$activity_id' ,
														`fk_userunique_code` 			=  '$unique_code' ,
														`planned_date` 		=  '$planned_date' ,
														`customer_name` 			=  '$customer_name' ,
														`mobile_no` 		=  '$mobile_no' ,
														`actual_completion_date`=  '$actual_completion_date' ,
														`fk_status_id` 		=  '$status_id' ,
														`remarks` 		=  '$remarks' ,
														`image_unique_code` 		=  '$image_unique_code' ,
														`created_date` 		=  '$timestamp' ,
														`updated_date` 		=  '$timestamp'";	
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
		return 1;
		}
		else
		{
			return 0;
		}
	}
	
	########################Add New Attendec Record
	public function addAttendence($attendecInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;				
		$mysqli_s 					= 	$this->CONN();
		
        $attendence_type= $attendecInfo->attendence_type;
        $capture_at     = $attendecInfo->capture_at;
        $lat= $attendecInfo->lat;
        $long= $attendecInfo->long;
        $image_unique_code= $attendecInfo->image_unique_code;
		$attendence_date = date("Y-m-d");	
	    $timestamp = date("Y-m-d H:i:s");	
	    $in_time   = date("H:i:s");	
	    $out_time  = date("H:i:s");	
		#############Add Location and address
		 $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

        //////Get in time value 
		 $q11 = "SELECT * FROM `attendence_detail`  WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'  ORDER BY attendence_id desc LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $in_time_value = $result11['in_time'];
			$duration_calculate_attendence_time = $result11['attendence_date'];
		
        ////insert query attendence
		if($attendence_type!="")
		{
			
			if($attendence_type=="in")
			{
			#######Check in current date trip is started or not
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$attendence_date'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip>0)
			{	
			#######Check in current date there is a record inserted or not	
			$query      		=   "SELECT * FROM `attendence_detail`  WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'";
			$sql       	 		=   $mysqli_s->query($query); 
			
		    $num  				=   @mysqli_num_rows($sql);
		
			if(@$num==0)
			{
			#######Insert query of missing date

		    $q11 = "SELECT attendence_date FROM attendence_detail WHERE fk_user_unique_code = '$unique_code'  ORDER BY attendence_id desc LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $missing_date_current = $result11['attendence_date'];
			#######Get Day no between two dates
			if($missing_date_current)
			{
			// Calculating the difference in timestamps 
            $diff = strtotime($attendence_date) - strtotime($missing_date_current); 
            // 1 day = 24 hours 
            // 24 * 60 * 60 = 86400 seconds 
            $no_day_missing= abs(round($diff / 86400));

			####################################
			for($i=1;$i<=$no_day_missing; $i++)
			{
		    $missing_date_with_onedayadd = strtotime($i." day", strtotime($missing_date_current));
            $new_missing_date = date("Y-m-d", $missing_date_with_onedayadd);
			$type=date('l', strtotime($new_missing_date));////get day full name for holiday like saturday sunday
            if($type=='Sunday')
			{
				$present_type="holiday";
			}
			else
			{
			   $present_type="Absent";
			}
			if($present_type=='Absent' || $present_type=='holiday')
			{
				$queryy2    =   "INSERT INTO  `attendence_detail`   SET
														`fk_user_unique_code` 			=  '$unique_code' ,
														`attendence_date` 		        =  '$new_missing_date' ,
														`in_time` 		             	=  '--' ,
														`out_time` 		             	=  '--' ,
														`type` 		                    =  '$present_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`attendence_status` 		    =  '',
														`lat` 		                    =  '',
														`long` 		                    =  '',
														`image_unique_code` 		    =  '',
														`attendence_in_address` 		=  '',
														`country` 		                =  '',
														`state` 		                =  '',
														`city` 		                    =  '',
														`district` 		                =  '',
														`subdistrict` 		            =  '',
														`postal_code` 		            =  '',
														`start_capture_at` 		        =  ''
														
														";

			}
			else{
			$queryy2    =   "INSERT INTO  `attendence_detail`   SET
														`fk_user_unique_code` 			=  '$unique_code' ,
														`attendence_date` 		        =  '$new_missing_date' ,
														`in_time` 		             	=  '--' ,
														`out_time` 		             	=  '--' ,
														`type` 		                    =  '$present_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`attendence_status` 		    =  '$attendence_type',
														`lat` 		                    =  '$lat',
														`long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code',
														`attendence_in_address` 		=  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`start_capture_at` 		        =  '$capture_at'
														
														";
			}														
			if($new_missing_date!=$attendence_date)
			{				
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			}
			}
			}
            ###########END Code	
			$type=date('l', strtotime($attendence_date));////get day full name for holiday like saturday sunday
            if($type=='Sunday')
			{
				$present_type="holiday";
				$capture_at 		        =  '';
			}
			else
			{
			   $present_type="Present";
			}
           
			$queryy2    =   "INSERT INTO  `attendence_detail`   SET
														`fk_user_unique_code` 			=  '$unique_code' ,
														`attendence_date` 		        =  '$attendence_date' ,
														`in_time` 		             	=  '$in_time' ,
														`type` 		                    =  '$present_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`attendence_status` 		    =  '$attendence_type',
														`lat` 		                    =  '$lat',
														`long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code',
														`attendence_in_address` 		=  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`start_capture_at` 		        =  '$capture_at'
														";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 5;////attendence done
			}
			
			
			else{
			return 6;///////attendence already done
			}
			}//trip start condition closed
	        else{
		    return 4;///trip not started
	        }
			#########END 
			}////in condition closed
            else			
			{
			#######Check in current date there is a record inserted or not	
			$query      		=   "SELECT * FROM `attendence_detail`  WHERE fk_user_unique_code='$unique_code' AND attendence_status='in' AND DATE(attendence_date)='$attendence_date'";
			$sql       	 		=   $mysqli_s->query($query); 
			
		    $num  				=   @mysqli_num_rows($sql);
	
			if(@$num==1)
			{	
			/////////Calculate in and out time duration	
			$in_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$duration_calculate_attendence_time.' '.$in_time_value);
			$out_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$attendence_date.' '.$out_time);
			$duration=$in_time_duration->diff($out_time_duration);
			$hours=number_format((float)($duration->days*24)+$duration->h+($duration->i/60)+($duration->s/3600),3,'.','');
			$hours_value=dataFormat::$static_value_forholiday;
			if($hours<$hours_value)
			{
				$present_type="halfday";
			}
			else
			{
			   $present_type="Present";	
			}
			//$time_duration=$in_time_value-$out_time;
			$queryy2    =   "UPDATE  `attendence_detail`   SET 
														`out_time` 			=  '$out_time' ,
														`duration_hours` 		    =  '$hours' ,
														`type` 		    =  '$present_type' ,
														`updated_date` 		=  '$timestamp',
														`attendence_status`  =  '$attendence_type',
														`end_lat` 		                    =  '$lat',
														`end_long` 		                    =  '$long',
														`image_unique_code` 		    =  '$image_unique_code',
														`attendence_out_address` 		=  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`end_capture_at` 		        =  '$capture_at'
														 WHERE fk_user_unique_code='$unique_code' AND DATE(attendence_date)='$attendence_date'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 1;

			}///////out condition closed
			
			else{
				return 6;
			}///////cosed case when exist out
		}
		}
		else
		{
			return 0;
		}
	
	//}//trip start condition closed
	// else{
		// return 4;///trip not started
	// }
	}
	
#############Calculate areal distance 
	public function distance($lat1, $lon1, $lat2, $lon2) {

    $pi80 = M_PI / 180;
    $lat1 *= $pi80;
    $lon1 *= $pi80;
    $lat2 *= $pi80;
    $lon2 *= $pi80;

    $r = 6372.797; // mean radius of Earth in km
    $dlat = $lat2 - $lat1;
    $dlon = $lon2 - $lon1;
    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $km = $r * $c;

    //echo '<br/>'.$km;
    return $km;
}
########################Add New Trip Record
	public function addNewTrip($tripInfo,$unique_code)
	{
		$mysqli_s 					= 	$this->CONN();
        $api_key_heremap=dataFormat::$here_map_api_key;	
        #########Get Labour Cost
		$q11 = "SELECT labour_cost FROM `user`  WHERE unique_code='$unique_code' LIMIT 1";
		$query11 		= 	$mysqli_s->prepare($q11);
		$query11->execute();
		$result11		=	$query11->get_result()->fetch_assoc();
        $labour_cost = $result11['labour_cost'];
        #############################		
        $amount_rs=$labour_cost;//dataFormat::$amount_rs;		
		
        $capture_at= $tripInfo->capture_at;
        $trip_type= $tripInfo->trip_type;
        $lat= $tripInfo->lat;
        $long= $tripInfo->long;
	    $timestamp = date("Y-m-d H:i:s");	
	    $trip_date_time = date("Y-m-d H:i:s");	
	    $trip_date = date("Y-m-d");	
		#############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

			
            ////insert query trip
			if($trip_type=="start")
			{
			#######Check in current date trip is started or not
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip==0)
			{	
			#######Insert query of trip table
		    $queryy2    =   "INSERT INTO  `trip_detail`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`start_lat` 		            =  '$lat' ,
														`start_long` 		            =  '$long' ,
														`trip_date_time` 		        =  '$trip_date_time' ,
														`type` 		                    =  '$trip_type' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														
														`trip_start_address` 		    =  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`start_capture_at` 		        =  '$capture_at'
														
														";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
	        $last_id 	= 	$mysqli_s->insert_id;
			#######Insert query of trip history table
		    $queryy3    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$last_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`trip_start_address` 		    =  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`capture_at` 		            =  '$capture_at'
														";	
			$query3 	= 	$mysqli_s->prepare($queryy3);
	     	$query3->execute();

			return 5;////trip started
			}
			else{
				return 12;/// already started trip
			}
			}////start condition closed
          
	       if($trip_type=="default")
		   {
			#######Check in current date trip is started or not
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip>0)
			{
		    //////Get trip  time value 
		    $q11 = "SELECT * FROM `trip_latlong_history`  WHERE trip_history_id=(SELECT MAX(trip_history_id) FROM `trip_latlong_history` WHERE fk_userunique_code='$unique_code' AND DATE(created_date)='$trip_date' LIMIT 1)";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $trip_id = $result11['fk_trip_id'];
            $start_lat = trim($result11['lat']);
            $start_long = trim($result11['long']);
			///////////added for time
            $capture_at_old = trim($result11['capture_at']);
			// Calculating the difference in timestamps 
            $ping_intervel = strtotime($capture_at) - strtotime($capture_at_old); 
            // 1 day = 24 hours 
            // 24 * 60 * 60 = 86400 seconds 
            $ping_intervel_time= abs(round($ping_intervel /60));
			
			######Get distance hours from api 
            $url =  "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=$api_key_heremap&waypoint0=geo!$start_lat,$start_long&waypoint1=geo!$lat,$long&mode=fastest;car;traffic:disabled";
          //  $url = "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=Gc3fkvACpFpey8DnpknUBGRisaK7pJsTPULLOJYKzvk&waypoint0=geo!28.7041,77.1025&waypoint1=geo!25.3176,82.9739&mode=fastest;car;traffic:disabled";
            $data = file_get_contents($url);
            $result=json_decode($data,true);
            //print_r($result);
            $distance=$result['response']['route'][0]['summary']['distance'];
            $time=$result['response']['route'][0]['summary']['baseTime'];
            $total_time_inhours=trim($time)/3600;
            $total_distance_inkm=$distance/1000;
			$speed=$total_distance_inkm/$total_time_inhours;
            $areal_distance=$this->distance($start_lat,$start_long,$lat,$long);
	        $areal_dis=   number_format($areal_distance, 2, '.', ' ');
			#######Insert query of trip history table
		    $queryy2    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$trip_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`total_time_hours` 		        =  '$total_time_inhours' ,
														`total_km_between_latlong` 		=  '$total_distance_inkm' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														
														`trip_start_address` 		    =  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`capture_at` 		            =  '$capture_at',
														`areal_distance` 		        =  '$areal_dis',
														`speed` 		                =  '$speed',
														`ping_interval` 		        =  '$ping_intervel_time'
														";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 7;////trip default lat long
			#########END 
			}
		   else{
				return 6;/// Trip not started yet
			}
         
	       }
         if($trip_type=="close")
		   {
			$query      		=   "SELECT * FROM `trip_detail`  WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_trip  				=   @mysqli_num_rows($sql);
			if($num_trip>0)
			{ 
		    //////Get trip  time value 
		    $q11 = "SELECT * FROM `trip_latlong_history`  WHERE trip_history_id=(SELECT MAX(trip_history_id) FROM `trip_latlong_history` WHERE fk_userunique_code='$unique_code' AND DATE(created_date)='$trip_date' LIMIT 1)";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
            $trip_id = $result11['fk_trip_id'];
            $start_lat = trim($result11['lat']);
            $start_long = trim($result11['long']);
			///////////added for time
            $capture_at_old = trim($result11['capture_at']);
			// Calculating the difference in timestamps 
            $ping_intervel = strtotime($capture_at) - strtotime($capture_at_old); 
            // 1 day = 24 hours 
            // 24 * 60 * 60 = 86400 seconds 
            $ping_intervel_time= abs(round($ping_intervel /60));
			
			######Get distance hours from api 
            $url =  "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=$api_key_heremap&waypoint0=geo!$start_lat,$start_long&waypoint1=geo!$lat,$long&mode=fastest;car;traffic:disabled";
          //  $url = "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=Gc3fkvACpFpey8DnpknUBGRisaK7pJsTPULLOJYKzvk&waypoint0=geo!28.7041,77.1025&waypoint1=geo!25.3176,82.9739&mode=fastest;car;traffic:disabled";
            $data = file_get_contents($url);
            $result=json_decode($data,true);
            //print_r($result);
            $distance=$result['response']['route'][0]['summary']['distance'];
            $time=$result['response']['route'][0]['summary']['baseTime'];
            $total_time_inhours=trim($time)/3600;
            $total_distance_inkm=$distance/1000;
			$speed=$total_distance_inkm/$total_time_inhours;
            $areal_distance=$this->distance($start_lat,$start_long,$lat,$long);
	        $areal_dis=   number_format($areal_distance, 2, '.', ' ');
	
			#######Insert query of trip history table
		    $queryy9    =   "INSERT INTO  `trip_latlong_history`   SET
														`fk_userunique_code` 			=  '$unique_code' ,
														`fk_trip_id` 			        =  '$trip_id' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`total_time_hours` 		        =  '$total_time_inhours' ,
														`total_km_between_latlong` 		=  '$total_distance_inkm' ,
														`created_date` 		            =  '$timestamp' ,
														`updated_date` 		            =  '$timestamp',
														`trip_start_address` 		    =  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`capture_at` 		            =  '$capture_at',
														`areal_distance` 		        =  '$areal_dis',
													    `speed` 		                =  '$speed',
													     `ping_interval` 		        =  '$ping_intervel_time'



														";	
			$query9 	= 	$mysqli_s->prepare($queryy9);
	     	$query9->execute();
	
			//////Get trip  time value 
			$total_km_between_latlong=0;
			$total_time_hours=0;
		    $q11 = "SELECT total_km_between_latlong,total_time_hours,speed,areal_distance,ping_interval FROM trip_latlong_history 
			WHERE fk_trip_id='$trip_id' AND fk_userunique_code='$unique_code' AND DATE(created_date)='$trip_date'";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$row62			=	$query11->get_result();
			foreach($row62 as $row622)
			{
				$ping_interval=$row622['ping_interval'];
				$speed=$row622['speed'];
				$total_km_between_latlong1= $row622['areal_distance'];
				if($ping_interval<=5)
				{
				if($speed>3 &&  $total_km_between_latlong1<15)
				{
			      //$total_km_between_latlong+= $row622['total_km_between_latlong'];//
			     $total_km_between_latlong+= (double)$row622['areal_distance'];//areal distance
                 $total_time_hours+= (double)$row622['total_time_hours'];
				}
				}
				else{
					
				$total_km_between_latlong+= (double)$row622['areal_distance'];//areal distance
                $total_time_hours+= (double)$row622['total_time_hours'];
				}
			}
		       $total_km_between_latlong1=$total_km_between_latlong*1.15;
		       $total_amount= ($total_km_between_latlong*1.15)*$amount_rs;
			//print_r($result11);
            #####Get Actual Length and Actual Time
	    	$q11 = "SELECT * FROM `trip_detail`  WHERE trip_id='$trip_id' AND fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND type='start' LIMIT 1";
		
			$query11 		= 	$mysqli_s->prepare($q11);
			$query11->execute();
			$result11		=	$query11->get_result()->fetch_assoc();
		//	print_r($result11);
            $actual_start_lat = $result11['start_lat'];
            $actual_start_long = $result11['start_long'];			
            $url =  "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apikey=$api_key_heremap&waypoint0=geo!$actual_start_lat,$actual_start_long&waypoint1=geo!$lat,$long&mode=fastest;car;traffic:disabled";
            $data = file_get_contents($url);
            $result=json_decode($data,true);
         ///  print_r($result);
            $actual_distance=$result['response']['route'][0]['summary']['distance'];
            $actual_time=$result['response']['route'][0]['summary']['baseTime'];
            $actual_total_time_inhours=trim($actual_time)/3600;
            $actual_total_distance_inkm=$actual_distance/1000;

			#######update query of trip  table
		    $queryy2    =   "UPDATE  `trip_detail`   SET 
														`end_lat` 		         	=  '$lat' ,
														`end_long` 		            =  '$long' ,
														`total_time_hours` 		    =  '$total_time_hours' ,
														`total_km_driven` 		    =  '$total_km_between_latlong1' ,
														`total_amoutnt` 		    =  '$total_amount' ,
														`actual_distance_km` 	    =  '$actual_total_distance_inkm' ,
														`actual_time_hurs` 		    =  '$actual_total_time_inhours' ,
														`type` 		                =  '$trip_type' ,
														`updated_date` 		        =  '$timestamp',
														`trip_end_address` 		        =  '$complete_address',
														`country` 		                =  '$Country',
														`state` 		                =  '$State',
														`city` 		                    =  '$City',
														`district` 		                =  '$District',
														`subdistrict` 		            =  '$Subdistrict',
														`postal_code` 		            =  '$PostalCode',
														`end_capture_at` 		        =  '$capture_at'
														 WHERE fk_userunique_code='$unique_code' AND DATE(trip_date_time)='$trip_date' AND trip_id='$trip_id'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 8;/////updated master table trip closed
			}////closed condition closed
			else
			{
				return 6;/// Trip not started yet
			}
		   }
         else{
			 return 4;///////wrong key
		 }
	}
	
########################Add New Enquiry Record
	public function addNewEnquiry($enquiryInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
	    $mysqli_s 					= $this->CONN();
        $enquiry_code               = $enquiryInfo->enquiry_code;
        $fk_customer_type           = $enquiryInfo->customer_type;
        $customer_name              = $enquiryInfo->customer_name;
        $address                    = $enquiryInfo->address;
        $fk_taluka_code             = $enquiryInfo->taluka_code;
        $fk_district_code           = $enquiryInfo->district_code;
        $fk_state_code              = $enquiryInfo->state_code;
        $fk_pin_code                = $enquiryInfo->pin_code;
        $mobile_no                  = $enquiryInfo->mobile_no;
        $fk_model_intrested         = $enquiryInfo->model_intrested;
        $fk_implemented_intrested   = $enquiryInfo->implemented_intrested;
        $implement_model            = $enquiryInfo->implement_model;
        $fk_source_of_enquiry       = $enquiryInfo->source_of_enquiry;
        $fk_tractor_application     = $enquiryInfo->tractor_application;
        $agricultural_land          = $enquiryInfo->agricultural_land;
        $irrigated_land             = $enquiryInfo->irrigated_land;
        $non_irrigated_land         = $enquiryInfo->non_irrigated_land;
        $fk_type_of_commodity_uses  = $enquiryInfo->type_of_commodity_uses;
        $fk_type_of_buyer           = $enquiryInfo->type_of_buyer;
        $existing_tractor           = $enquiryInfo->existing_tractor;
        $expected_delivert_date     = $enquiryInfo->expected_delivert_date;
        $fk_finanace_type           = $enquiryInfo->finanace_type;
        $fk_financiar_name          = $enquiryInfo->financiar_name;
        $image_unique_code          = $enquiryInfo->image_unique_code;
        $lat                        = $enquiryInfo->lat;
        $long                       = $enquiryInfo->long;
        $remark                       = $enquiryInfo->remark;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	   $queryy2    =   "INSERT INTO  `enquiry_details`   SET
														`enquiry_code` 			        =  '$enquiry_code' ,
														`fk_usercode` 		            =  '$unique_code' ,
														`fk_customer_type` 		        =  '$fk_customer_type' ,
														`customer_name` 		        =  '$customer_name' ,
														`address` 		                =  '$address' ,
														`fk_taluka_code` 		        =  '$fk_taluka_code' ,
														`fk_district_code` 		        =  '$fk_district_code' ,
														`fk_state_code` 		        =  '$fk_state_code' ,
														`fk_pin_code` 		            =  '$fk_pin_code' ,
														`mobile_no` 		            =  '$mobile_no' ,
														`fk_model_intrested` 		    =  '$fk_model_intrested' ,
														`fk_implemented_intrested` 	    =  '$fk_implemented_intrested' ,
														`implement_model` 		        =  '$implement_model' ,
														`fk_source_of_enquiry` 		    =  '$fk_source_of_enquiry' ,
														`fk_tractor_application` 		=  '$fk_tractor_application' ,
														`agricultural_land` 		    =  '$agricultural_land' ,
														`irrigated_land` 		        =  '$irrigated_land' ,
														`non_irrigated_land` 		    =  '$non_irrigated_land' ,
														`fk_type_of_commodity_uses`     =  '$fk_type_of_commodity_uses' ,
														`fk_type_of_buyer` 		        =  '$fk_type_of_buyer' ,
														`existing_tractor` 		        =  '$existing_tractor' ,
														`expected_delivert_date` 	    =  '$expected_delivert_date' ,
														`fk_finanace_type` 		        =  '$fk_finanace_type' ,
														`fk_financiar_name` 		    =  '$fk_financiar_name' ,
														`image_unique_code` 		    =  '$image_unique_code' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`latlong_address` 		        =  '$complete_address' ,
														`created_date` 		            =  '$timestamp' ,
														`latlong_country` 		        =  '$Country' ,
														`latlong_state` 		        =  '$State' ,
														`latlong_city` 		            =  '$City' ,
														`latlong_district` 		        =  '$District' ,
														`latlong_subdistrict` 		    =  '$Subdistrict' ,
														`latlong_postalcode` 		    =  '$PostalCode',
														`remark` 		                =  '$remark'
														
														";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
	
	public function addNewVisitRemark($visitRemarkInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
		$mysqli_s 					= $this->CONN();
        $enquiry_code               = $visitRemarkInfo->enquiry_code;
        $visit_remark_code           = $visitRemarkInfo->visit_remark_code;
        $fk_user_unique_code        = $visitRemarkInfo->fk_user_unique_code;
        $remark                     = $visitRemarkInfo->remark;
        $image_unique_code           = $visitRemarkInfo->image_unique_code;
        $lat                        = $visitRemarkInfo->lat;
        $long                       = $visitRemarkInfo->long;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "INSERT INTO  `visit_remark`   SET
														`fk_enquiry_code` 			    =  '$enquiry_code' ,
														`fk_user_unique_code` 		    =  '$unique_code' ,
														`visit_remark_code` 		    =  '$visit_remark_code' ,
														`remark` 		                =  '$remark' ,
														`image_unique_code` 		    =  '$image_unique_code' ,
														`lat` 		                    =  '$lat' ,
														`long` 		                    =  '$long' ,
														`address` 		                =  '$complete_address' ,
														`created_date` 		            =  '$timestamp' ,
														`country`        		        =  '$Country' ,
														`state`         		        =  '$State' ,
														`city`      		            =  '$City' ,
														`district`      		        =  '$District' ,
														`subdistrict`       		    =  '$Subdistrict' ,
														`postalcode` 	        	    =  '$PostalCode'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}

########################Add New Job Card Record
	public function addNewJobCard($jobcardInfo,$unique_code)
	{
        $api_key_heremap=dataFormat::$here_map_api_key;		
    	$mysqli_s 					    = $this->CONN();
        $job_card_code                  = $jobcardInfo->job_card_code;
        $fk_type_of_work_code           = $jobcardInfo->fk_type_of_work_code;
        $customer_name                  = $jobcardInfo->customer_name;
        $address                        = $jobcardInfo->address;
        $fk_taluka_code                 = $jobcardInfo->taluka_code;
        $fk_district_code               = $jobcardInfo->district_code;
        $fk_state_code                  = $jobcardInfo->state_code;
        $pin_code                       = $jobcardInfo->pin_code;
        $mobile_no                      = $jobcardInfo->mobile_no;
        $model                          = $jobcardInfo->model;
        $date_of_sale                   = $jobcardInfo->date_of_sale;
        $registration_no                = $jobcardInfo->registration_no;
        $hours_completed                = $jobcardInfo->hours_completed;
        $engine_no                      = $jobcardInfo->engine_no;
        $customer_complaints            = $jobcardInfo->customer_complaints;
        $work_done_other_than_customercomplaints             = $jobcardInfo->work_done_other_than_customercomplaints;
        $expected_cost                  = $jobcardInfo->expected_cost;
        $expected_date_of_rapid         = $jobcardInfo->expected_date_of_rapid;
        $under_warranty                 = $jobcardInfo->under_warranty;
        $company_ref_no                 = $jobcardInfo->company_ref_no;
        $job_card_create_lat            = $jobcardInfo->lat;
        $job_card_create_long           = $jobcardInfo->long;
        $job_card_create_remark         = $jobcardInfo->remark;
        #############Add Location and address
		    $url="https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$job_card_create_lat,$job_card_create_long&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=$api_key_heremap";
            $data2 = file_get_contents($url);
            $result2=json_decode($data2,true);
		    $complete_address=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
		    $Country=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Country'];
		    $State=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['State'];
		    $City=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['City'];
		    $District=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['District'];
		    $Subdistrict=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['Subdistrict'];
		    $PostalCode=   $result2['Response']['View'][0]['Result'][0]['Location']['Address']['PostalCode'];

	    $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	   $queryy2    =   "INSERT INTO job_card_details 
	( 
	job_card_code, 
	fk_user_unique_code,
	fk_type_of_work_code, 
	customer_name, 
	address, 
	fk_taluka_code, 
	fk_state_code, 
	fk_district_code, 
	pin_code, 
	mobile_no, 
	model, 
	date_of_sale, 
	registration_no, 
	hours_completed, 
	engine_no, 
	customer_complaints, 
	work_done_other_than_customercomplaints, 
	expected_cost, 
	expected_date_of_rapid, 
	under_warranty, 
	company_ref_no, 
	created_date, 
	job_card_create_lat, 
	job_card_create_long, 
	job_card_create_address, 
	job_card_create_country, 
	job_card_create_state, 
	job_card_create_city, 
	job_card_create_district, 
	job_card_create_subdistrict, 
	job_card_create_postalcode,
	job_card_create_remark
	)
	VALUES
	(
	'$job_card_code', 
	'$unique_code', 
	'$fk_type_of_work_code', 
	'$customer_name', 
	'$address', 
	'$fk_taluka_code', 
	'$fk_state_code', 
	'$fk_district_code', 
	'$pin_code', 
	'$mobile_no', 
	'$model', 
	'$date_of_sale', 
	'$registration_no', 
	'$hours_completed', 
	'$engine_no', 
	'$customer_complaints', 
	'$work_done_other_than_customercomplaints', 
	'$expected_cost', 
	'$expected_date_of_rapid', 
	'$under_warranty', 
	'$company_ref_no', 
	'$timestamp', 
	'$job_card_create_lat', 
	'$job_card_create_long', 
	'$complete_address', 
	'$Country', 
	'$State', 
	'$City', 
	'$District', 
	'$Subdistrict', 
	'$PostalCode',
	'$job_card_create_remark'
	);
";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return true;
		
	}
	
    ########################Add New Job Card Service Record
	public function addNewJobCardService($jobcardInfo,$unique_code)
	{
    	$mysqli_s 					    = $this->CONN();
        $job_card_code                  = $jobcardInfo['job_card_code'];
        $oil_charges                    = $jobcardInfo['oil_charges'];
        $out_side_work                    = $jobcardInfo['out_side_work'];
        $labour_charges                    = $jobcardInfo['labour_charges'];
        $spare_charges                    = $jobcardInfo['spare_charges'];
	  // $abc=$oil_charges[0]['oil_type_code'];
        $timestamp = date("Y-m-d H:i:s");
      // $myfile = file_put_contents('logs.txt', $jobcardInfo.PHP_EOL , FILE_APPEND | LOCK_EX);		
		#######Insert query of enquiry table
	foreach($oil_charges as $oil_chargess)
	{
	$oil_code=$oil_chargess['oil_type_code'];	
	$make=$oil_chargess['make'];	
	$unit_liter=$oil_chargess['unit_liter'];	
	$total=$oil_chargess['amount_per_liter'];	
    $queryy2    =   "INSERT INTO job_card_service_oil_charges 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	fk_oil_type_code, 
	make, 
	unit_liter, 
	amount_per_liter, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$oil_code', 
	'$make', 
	'$unit_liter', 
	'$total', 
	'$timestamp'
	)";
			$query2 	= 	$mysqli_s->prepare($queryy2);
			

	     	$query2->execute();
	}
	
	##########Insert Out Side Work
	foreach($out_side_work as $out_side_works)
	{
	$description=$out_side_works['description'];	
	$bill_no=$out_side_works['bill_no'];	
	$total=$out_side_works['total'];	
    $queryy2    =   "INSERT INTO job_card_service_out_side_work 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	description, 
	bill_no,  
	total, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$description', 
	'$bill_no', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
	##########Insert Labour Charges
	foreach($labour_charges as $labour_chargess)
	{
	$description=$labour_chargess['description'];	
	$time=$labour_chargess['time_taken'];	
	$total=$labour_chargess['cost'];	
    $queryy2    =   "INSERT INTO job_card_service_labour_cost 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	description, 
	time_taken,  
	cost, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$description', 
	'$time', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
	
	##########Insert Spare Charges
	foreach($spare_charges as $spare_chargess)
	{
	$part_no=$spare_chargess['part_no'];	
	$description=$spare_chargess['description'];	
	$cost=$spare_chargess['cost'];	
	$quantity=$spare_chargess['quantity'];	
	$complaint_code=$spare_chargess['complaint_code'];	
	$made_code=$spare_chargess['total_cost'];	
	$total=$spare_chargess['total'];	
    $queryy2    =   "INSERT INTO job_card_service_spare_charge 
	(
	fk_job_card_code, 
	fk_user_unique_code, 
	part_no, 
	description, 
	cost,  
	quantity,  
	complaint_code,  
	made_code,  
	total_cost, 
	created_date
	)
	VALUES
	( 
	'$job_card_code', 
	'$unique_code', 
	'$part_no', 
	'$description', 
	'$cost', 
	'$quantity', 
	'$complaint_code', 
	'$made_code', 
	'$total', 
	'$timestamp'
	)";
	$query2 	= 	$mysqli_s->prepare($queryy2);
	$query2->execute();
	}
		
     ######Check value jobcard exist or not

           $queryy     =   "SELECT * FROM `job_card_service_oil_charges` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
			$num1		=	$result->num_rows;
			$queryy1     =   "SELECT * FROM `job_card_service_out_side_work` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy1);
			$query->execute();
			$result 	= 	$query->get_result();
			$num2		=	$result->num_rows;
			 $queryy2     =   "SELECT * FROM `job_card_service_labour_cost` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy2);
			$query->execute();
			$result 	= 	$query->get_result();
			 $num3		=	$result->num_rows;
			 $queryy3     =   "SELECT * FROM `job_card_service_spare_charge` 
								 WHERE fk_job_card_code ='$job_card_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy3);
			$query->execute();
		    $result 	= 	$query->get_result();
			$num4		=	$result->num_rows;
			if($num1>0 && $num2>0 && $num3>0 && $num4>0)
			{
		     return true;
			}
			else
			{
			 return false;
			}
		
	}
	############Tada Create 
	public function addNewTADA($tadaInfo,$unique_code)
	{
    	$mysqli_s 					    = $this->CONN();
        $tada_code                  = $tadaInfo->tada_code;
        $claim_type                    = $tadaInfo->claim_type;
        $amount                    = $tadaInfo->amount;
        $remarks                    = $tadaInfo->remarks;
        $image_code                    = $tadaInfo->image_code;
        $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	   $queryy2    =   "INSERT INTO hr_tada 
	(
	fk_user_unique_code, 
	tada_code, 
	claim_type, 
	amount, 
	remarks, 
	image_code, 
	created_date
	)
	VALUES
	( 
	'$unique_code', 
	'$tada_code', 
	'$claim_type', 
	'$amount', 
	'$remarks', 
	'$image_code', 
	'$timestamp'
	)";
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();

}

	############Leave Submit API 
	public function addNewLeave($leaveInfo,$unique_code)
	{
    	$mysqli_s 					    = $this->CONN();
        $leave_code                     = $leaveInfo->leave_code;
        $apply_for_leave                = $leaveInfo->apply_for_leave;
        $type_of_leave                  = $leaveInfo->type_of_leave;
        $from_date                      = $leaveInfo->from_date;
        $to_date                        = $leaveInfo->to_date;
        $no_of_day                      = $leaveInfo->no_of_day;
        $day_type                       = $leaveInfo->day_type;
        $timestamp = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
		$query      		=   "SELECT * FROM `hr_leave`  WHERE fk_user_code='$unique_code' 
		AND DATE(from_date)>='$from_date' AND DATE(to_date)<='$to_date'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num_leave  				=   @mysqli_num_rows($sql);
			if($num_leave==0)
			{
	   $queryy2    =   "INSERT INTO hr_leave 
	(leave_code, 
	fk_user_code, 
	apply_for_leave, 
	type_of_leave, 
	from_date, 
	to_date, 
	no_of_day, 
	day_type, 
	created_date
	)
	VALUES
	('$leave_code', 
	'$unique_code', 
	'$apply_for_leave', 
	'$type_of_leave', 
	'$from_date', 
	'$to_date', 
	'$no_of_day', 
	'$day_type', 
	'$timestamp'
	)";
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			return 1;
			}
			else{
				return 2;
			}

}
}
?>