<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

//$json ='{"job_card_code":"JC_SLSROFF0010_20201230_172957","oil_charges":[{"amount_per_liter":"99.999","created_date":"","make":"ufu","oil_charge_id":"","oil_type_code":"OTYP0004","unit_liter":"hth"},{"amount_per_liter":"99.999","created_date":"","make":"ufu","oil_charge_id":"","oil_type_code":"OTYP0003","unit_liter":"hth"},{"amount_per_liter":"3","created_date":"","make":"ff","oil_charge_id":"","oil_type_code":"OTYP0002","unit_liter":"gg"}],"out_side_work":[{"bill_no":"jfj","created_date":"","description":"hfhf","out_side_work_id":"","total":"66"},{"bill_no":"jfj","created_date":"","description":"hfhf","out_side_work_id":"","total":"66"}],"labour_charges":[{"cost":"6","created_date":"","description":"","labour_cost_id":"","time":""},{"cost":"6","created_date":"","description":"","labour_cost_id":"","time":""}],"spare_charges":[{"complaint_code":"nn","cost":"3","created_date":"","description":"bb","made_code":"18.0","part_no":"hj","quantity":"6","spare_charge_id":"","total_cost":"0"},{"complaint_code":"nn","cost":"3","created_date":"","description":"bb","made_code":"18.0","part_no":"hj","quantity":"6","spare_charge_id":"","total_cost":"0"}]}'; //file_get_contents('php://input');
$json =file_get_contents('php://input');
 //$myfile = file_put_contents('logs.txt', $json.PHP_EOL , FILE_APPEND | LOCK_EX);
//Converts it into a PHP object
//$abc = json_encode($json,JSON_UNESCAPED_SLASHES);
//print_r($abc);
$js= trim($json, '"');  
$jsonData = stripslashes(html_entity_decode($js));

$jobcardInfo=json_decode($jsonData,true);
//$jobcardInfo = stripslashes(json_decode($json));
//print_r($jobcardInfo);
// $abc=$jobcardInfo['oil_charges'];
// $myfile = file_put_contents('logs.txt', $jsonData.PHP_EOL , FILE_APPEND | LOCK_EX);

$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('add_new_job_card_service' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('add_new_job_card_service' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
    $addvalue=$add->addNewJobCardService($jobcardInfo,$unique_code);
	if($addvalue==true)
	{
	//print_r($addvalue);die;
	///Get all message for trip started 
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_new_recordadded;
		$res->session->unique_code=$unique_code;
		$res->session->expires = false;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
		$data->job_card_code= $jobcardInfo['job_card_code'];
		$res->metadata = array('add_new_job_card_service' => $data);
        echo json_encode($res);
	}
	else{
		$res->success = false;
		$res->resCode = dataFormat::$res_fail;
		$res->session->unique_code=$unique_code;
		$res->session->expires = false;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
		$res->message ='Service card not created please try again.';
        echo json_encode($res);
	}
	
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

