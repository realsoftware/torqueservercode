<?php
include 'response.php';
include 'opendb.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$userId= $inputPost["userId"];

    //**************Variable For user ***********
    $fName = $inputPost["fName"];
    $lName = $inputPost["lName"];
    $phone = $inputPost["phone"];
    
    $address = $_POST['address'];
    $pinCode = $_POST['pinCode'];
  
    $state = $_POST['state'];
    $gender = $_POST['gender'];
    $profession = $_POST['profession'];
    $city=$_POST['city'];
    $dob = $_POST['dob'];
   
    
    ///////////////////Getting Lat long from pincode//////////
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$pinCode."&sensor=false";
    $details=file_get_contents($url);
    $result = json_decode($details,true);

    $lat=$result['results'][0]['geometry']['location']['lat'];

    $lng=$result['results'][0]['geometry']['location']['lng'];
    
    ///////////////////////////////////////////////////////////////////
    $url1 = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=".$pinCode;
     $geocode = simplexml_load_file($url1);
      $city = $geocode->result->address_component[1]->long_name;
    if ($geocode->result->address_component[2]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[2]->long_name;
    else if ($geocode->result->address_component[3]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[3]->long_name;
    else if ($geocode->result->address_component[4]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[4]->long_name;
    else if ($geocode->result->address_component[5]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[5]->long_name;
    $final = $city.":".$state;
     $stateCity=explode(':',$final);
    $city1 =$stateCity[0];
    $state1 =$stateCity[1];
    
//*************Update Query*****
        $LoginToken = time() . uniqid() . $userId;
      $sql = "UPDATE  user SET fName='$fName',lName='$lName',address='$address',pinCode='$pinCode',state='$state1',city='$city1',"
                . "sex='$gender',profession='$profession',dob='$dob',lat='$lat',lng='$lng' WHERE userId='$userId'";
        $result = mysqli_query($con, $sql);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Profile Updated Successfully.";
            $res->session = new sessionData();
            $res->session->id = $userId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
            $data = new responseData();
            $data->userId = $userId;
            $data->fName = $fName;
            $data->lName = $lName;

            $data->phone = $phone;
            //$data->otp = $otpNo;
            //$data->otpKey=$id;
	    $data->address = $address;
            $data->pinCode = $pinCode;
            $data->state = $state1;
            $data->gender = $gender;
            $data->lat = $lat;
            $data->lng = $lng;
            $data->profession = $profession;
            $data->city = $city1;
            $data->country ='India';
            $data->dob = $dob;
            $res->metadata = array('data' => $data);
            echo json_encode($res);
        }
 else {
      $res1 = new dataFormat();
        $data1 = new responseData();
        $res1->success = false;
        $res1->message = "Failed To Update.";
        echo json_encode($res1);
 }
?>