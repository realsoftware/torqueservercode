<?php
include 'response.php';
include 'opendb.php';
include 'distanceFinder.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$usrId = $inputPost["userId"];
$usrLat=$inputPost["lat"];
$usrLong=$inputPost["long"];
$usrLoc=$inputPost["location"];
////////////Getting Range from database//////
 $rangeQuery="select dist from rangeneighbour";
$res = mysqli_query($con, $rangeQuery);
 while($row= mysqli_fetch_assoc($res))
        {
         
            $range=$row["dist"];
        }
////////////////////////////////////////////
function GetLatLongFromLocationAlt($address)
{
$address = str_replace(" ", "+", $address); 

$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
$json = json_decode($json);
$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
return $lat.'~'.$long;
}
if((strlen($usrLat)<2 or strlen($usrLong)<2) &&(strlen($usrLoc)>5))
{
    $latLong=GetLatLongFromLocationAlt($usrLoc);
    //$usrLat=split('~',$latLong)[0];
   // $usrLong=split('~',$latLong)[1];
     $usrLat=explode('~',$latLong)[0];
    $usrLong=explode('~',$latLong)[1];
}
// calculate here all nearest driver 
//below query for finding 10 nearest driver to calculate distance in miles replace 6371 with 3959
//$sql="SELECT *, ( 6371 * acos( cos( radians($usrLat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($usrLong) ) + sin( radians($usrLat) ) * sin( radians( lat ) ) ) ) AS distance FROM driver HAVING distance < 10 and service=1 ORDER BY distance LIMIT 0 , 10";
//$sql="SELECT * FROM driver where service=1 LIMIT 0, 10";
$sql="SELECT *, ( 6371 * acos( cos( radians($usrLat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($usrLong) ) + sin( radians($usrLat) ) * sin( radians( lat ) ) ) ) AS distance FROM  user HAVING distance < $range  ORDER BY distance LIMIT 0 , 10";
$query = mysqli_query($con, $sql);

if (mysqli_num_rows($query)>0)
{
    $LoginToken = time() . uniqid() . $usrId;
        $res = new dataFormat();
        $res->success = true;
        $res->message = "Get list of all nearest Neighbour";
        $res->session = new sessionData();
        $res->session->id = $usrId;
        $res->session->expires = FALSE;
        $res->session->platform = $Plateform;
        $res->session->LOGINTOKEN = $LoginToken;
        $dataArray= array();
        while($urow= mysqli_fetch_assoc($query))
        {
            $data = new responseData();
          $data->userId = $urow["userId"];
    $data->email = $urow["emailId"];
    $data->password = $urow["password"];
    
    $data->fName = $urow["fName"];
    $data->lName = $urow["lName"];
    $data->dob = $urow["dob"];
    $data->gender = $urow["sex"];
    $data->profession = $urow["profession"];
    $data->phone = $urow["mobileNo"];
    $data->address = $urow["address"];
    $data->pinCode = $urow["pinCode"];
    $data->state = $urow["state"];
    $data->country= $urow["country"];
    $data->lat = $urow["lat"];
    $data->lng= $urow["lng"];
    $data->img =$urow["imagePath"];  
    $data->city = $urow["city"];
    $data->loginType =$urow["loginType"];
            $dataArray['shopandsave'][] = $data;
        }
        $res->metadata=$dataArray;
        echo json_encode($res);
	
}
else
{
    $LoginToken = time() . uniqid() . $usrId;
    $res = new dataFormat();
    $res->success = false;
    $res->message = "Neighbors not available";
    $res->session = new sessionData();
    $res->session->id = $usrId;
    $res->session->expires = FALSE;
    $res->session->platform = $Plateform;
    $res->session->LOGINTOKEN = $LoginToken;
    $data=new responseData();
    $res->metadata=$data;
    echo json_encode($res);
}
?>