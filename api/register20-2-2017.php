<?php
include 'response.php';
include 'opendb.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$pinCode = $inputPost["pincode"];
$loginType = $inputPost["loginType"];  //login type will be fb or default
$lat1 = $inputPost["lat"];
$lng1 = $inputPost["lng"];
$dataArray = array();
///////////////////Getting Lat long from pincode//////////
if($pinCode!='')
{
$url ="http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($pinCode)."&sensor=false";
    $details=file_get_contents($url);
    $result = json_decode($details,true);

    $lat=$result['results'][0]['geometry']['location']['lat'];

    $lng=$result['results'][0]['geometry']['location']['lng'];
    $url1 = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=".$pinCode;
     $geocode = simplexml_load_file($url1);
      $city = $geocode->result->address_component[1]->long_name;
    if ($geocode->result->address_component[2]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[2]->long_name;
    else if ($geocode->result->address_component[3]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[3]->long_name;
    else if ($geocode->result->address_component[4]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[4]->long_name;
    else if ($geocode->result->address_component[5]->type == "administrative_area_level_1")
        $state = $geocode->result->address_component[5]->long_name;
    $final = $city.":".$state;
     $stateCity=explode(':',$final);
    $city1 =$stateCity[0];
    $state1 =$stateCity[1];
//***************Function For Auto Generate User Id*****

function GenUserId($length = 4) {
    $chars = '0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .=substr($chars, $index, 1);
    }
    return $result;
}
$uId = 'USR' . GenUserId();
//**************Variable For Customer ***********
$email = $inputPost["email"];
$password = $inputPost["password"];
///////////////////////////////////////////////CheckinemailId already Exist/////
$query = mysqli_query($con, "SELECT * FROM user WHERE emailId ='$email' LIMIT 1");
$row = mysqli_num_rows($query);
$urow = mysqli_fetch_assoc($query);
if ($row >= 1) {
    $res1 = new dataFormat();
    $res1->success = true;
    $res1->message = "Email Id Already exist.";
    $res1->session = new sessionData();
    $res1->session->id = $urow["userId"];
    $res1->session->expires = FALSE;
    $res1->session->platform = $Plateform;
    $res1->session->LoginToken = time() . uniqid() . $urow["userId"];
    $data = new responseData();
    $data->userId = $urow["userId"];
    $data->email = $urow["emailId"];
    $data->password = $urow["password"];
    
    $data->fName = $urow["fName"];
    $data->lName = $urow["lName"];
    $data->dob = $urow["dob"];
    $data->gender = $urow["sex"];
    $data->profession = $urow["profession"];
    $data->phone = $urow["mobileNo"];
    $data->address = $urow["address"];
    $data->pinCode = $urow["pinCode"];
    $data->state = $urow["state"];
    $data->country= $urow["country"];
    $data->lat = $urow["lat"];
    $data->lng= $urow["lng"];
    $data->img =$urow["imagePath"];
    $data->city = $urow["city"];
  
    $data->loginType = $loginType;
    $res1->metadata = array('data' => $data);
    echo json_encode($res1);
    exit;
}
/////////////////////////////////////////////////////////////////////////////////
else {
    if ($loginType == 'facebook') {
        $password = '';
        $fbtype = 1;
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,pinCode,state,country,userType,logintype,city,deviceId,plateForm,version,lat,lang)"
                . " VALUES ('$uId','$password','$email','$pinCode','$state1','India','USER','facebook','$city1','$DeviceId','$Plateform','$Version','$lat','$lng')";
        $result = mysqli_query($con, $sql);
        //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        ///////////////////////////////////////////
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
            ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }
    ///////////////////////Gmail Case//////////////
    if ($loginType == 'gmail') {
        $password = '';
        //$fbtype=1;
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,pinCode,state,country,userType,logintype,city,deviceId,plateForm,version,lat,lng)"
                . " VALUES ('$uId','$password','$email','$pinCode','$state1','India','USER','gmail','$city1','$DeviceId','$Plateform','$Version','$lat','$lng')";
        $result = mysqli_query($con, $sql);
         //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
            ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }  if ($loginType == 'default') {
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,pinCode,state,country,userType,logintype,city,deviceId,plateForm,version,lat,lng)"
                . " VALUES ('$uId','$password','$email','$pinCode','$state1','India','USER','default','$city1','$DeviceId','$Plateform','$Version','$lat','$lng')";
        $result = mysqli_query($con, $sql);
         //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
             ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }
}
}
else
{
function GenUserId($length = 4) {
    $chars = '0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .=substr($chars, $index, 1);
    }
    return $result;
}
$uId = 'USR' . GenUserId();
//**************Variable For Customer ***********
$email = $inputPost["email"];
$password = $inputPost["password"];
///////////////////////////////////////////////CheckinemailId already Exist/////
$query = mysqli_query($con, "SELECT * FROM user WHERE emailId ='$email' LIMIT 1");
$row = mysqli_num_rows($query);
$urow = mysqli_fetch_assoc($query);
if ($row >= 1) {
    $res1 = new dataFormat();
    $res1->success = true;
    $res1->message = "Email Id Already exist.";
    $res1->session = new sessionData();
    $res1->session->id = $urow["userId"];
    $res1->session->expires = FALSE;
    $res1->session->platform = $Plateform;
    $res1->session->LoginToken = time() . uniqid() . $urow["userId"];
    $data = new responseData();
    $data->userId = $urow["userId"];
    $data->email = $urow["emailId"];
    $data->password = $urow["password"];
    
    $data->fName = $urow["fName"];
    $data->lName = $urow["lName"];
    $data->dob = $urow["dob"];
    $data->gender = $urow["sex"];
    $data->profession = $urow["profession"];
    $data->phone = $urow["mobileNo"];
    $data->address = $urow["address"];
    $data->pinCode = $urow["pinCode"];
    $data->state = $urow["state"];
    $data->country= $urow["country"];
    $data->lat = $urow["lat"];
    $data->lng= $urow["lng"];
    $data->img =$urow["imagePath"];
    $data->city = $urow["city"];
  
    $data->loginType = $loginType;
    $res1->metadata = array('data' => $data);
    echo json_encode($res1);
    exit;
}
/////////////////////////////////////////////////////////////////////////////////
else {
    if ($loginType == 'facebook') {
        $password = '';
        $fbtype = 1;
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,country,userType,logintype,deviceId,plateForm,version,lat,lang)"
                . " VALUES ('$uId','$password','$email','India','USER','facebook','$DeviceId','$Plateform','$Version','$lat1','$lng1')";
        $result = mysqli_query($con, $sql);
         //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
            ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }
    ///////////////////////Gmail Case//////////////
    if ($loginType == 'gmail') {
        $password = '';
        //$fbtype=1;
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,country,userType,logintype,deviceId,plateForm,version,lat,lng)"
                . " VALUES ('$uId','$password','$email','India','USER','gmail','$DeviceId','$Plateform','$Version','$lat1','$lng1')";
        $result = mysqli_query($con, $sql);
         //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
            ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }  if ($loginType == 'default') {
        $LoginToken = time() . uniqid() . $uId;
        $sql = "INSERT INTO user(userId,password,emailId,country,userType,logintype,deviceId,plateForm,version,lat,lng)"
                . " VALUES ('$uId','$password','$email','India','USER','default','$DeviceId','$Plateform','$Version','$lat1','$lng1')";
        $result = mysqli_query($con, $sql);
         //////////////Code for Mail/////////////////
         $to = $email;
            $subject = "Congratulation For Succeefull Registration";
            $from = "support@thepadosi.com";
            //$retname = $name.$LastName;
            $headers = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            $message = "<h2 style='color:#FF0000;'>Congratulations! You have registered successfully. ! </h2>";

            $message .= "<p>Dear User</p>" . "\r\n";
            $message .= "<p>Please revert for any further information, all your technical queries, </p>" . "\r\n";


            $message .= "<p><strong>Kind Regards</strong></p>" . "\r\n";

            $message .= "<p>Team Management</p>" . "\r\n";

            $message .= "<p><strong>XYZ. Ltd.</strong></p>" . "\r\n";
            $message .= "<p><strong> Call Us:</strong> +91-3634634634/4634634634</p>" . "\r\n";
            $message .= "<p><strong>Email Us:</strong>info@nefghb.com</p>" . "\r\n";


            //---------------End code-----------------
            mail($to, $subject, $message, $headers);
        if ($result > 0) {
            $res = new dataFormat();
            $res->success = true;
            $res->message = "Registration Completed Successfully.";
            $res->session = new sessionData();
            $res->session->id = $uId;
            $res->session->expires = FALSE;
            $res->session->platform = $Plateform;
            $res->session->LOGINTOKEN = $LoginToken;
             ///////////Query to fetch all Data/////////
             $qry="SELECT* from user WHERE userId='$uId'";
            $query1 = mysqli_query($con, $qry);
            while ($urow = mysqli_fetch_assoc($query1)) {
            $result_array[] = $urow;
            }
           $dataArray['shopandsave'][] = $result_array;
           $res->metadata = $dataArray;
           echo json_encode($res);
        }
    }
}
}
?>