<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$json = file_get_contents('php://input');
////Converts it into a PHP object
$close_enquiry_detail = json_decode($json);

$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('close_enquiry_detail' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('close_enquiry_detail' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);

if($loginTokenInfo)
{
	$payListInfo=$display ->toShowCloseEnquiryDetail($unique_code,$close_enquiry_detail);
	//print_r($taskListInfo);die;
	if($payListInfo)
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = "Enquiry close detail";
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
        $dataArray= array();
        $dataArray['close_enquiry_detail'] = $payListInfo;
        $res->metadata=$dataArray;
        echo json_encode($res);		
	}
	else
	{
       $res->success = true;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_fail;
       $res->message ="Enquiry close detail not found";   
       $res->metadata=array('close_enquiry_detail' =>$payListInfo);
	   echo json_encode($res);	   
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

