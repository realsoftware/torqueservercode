<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$json = file_get_contents('php://input');
// Converts it into a PHP object
$user_detail = json_decode($json);
$unique_code= $user_detail->unique_code;
$mobile_no= $user_detail->mobile_no;
$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('login' => $data);
   echo json_encode($res);
   return ;
}
if(@$mobile_no=='')
{
   $data		=$msg_required;
   $res->metadata = array('login' => $data);
   echo json_encode($res);
   return;
}
#######################
$LoginToken=time().uniqid().$unique_code;
$userinfo=$display ->CheckLogin($unique_code,$mobile_no);
if ($userinfo) {
    $user_pic_url = dataFormat::$user_pic_url;
    $adhar_pic_url = dataFormat::$adhar_pic_url;
    $pan_pic_url = dataFormat::$pan_pic_url;
    $updateToken=$update ->UpdateUserToken($unique_code,$mobile_no,$LoginToken,$DeviceId,$Plateform,$Version);
    $res->success = true;
    $res->resCode = dataFormat::$res_success;
    $res->message = dataFormat::$msg_loged_user;
    
    $res->session->user_id=$userinfo['user_id'];
    $res->session->expires = FALSE;
    $res->session->platform = $Plateform;
    $res->session->LOGINTOKEN=$LoginToken;
    
    $data->user_id=$userinfo['user_id'];
    $data->user_unique_code=$userinfo['unique_code'];
    $data->user_type_code=$userinfo['fk_type_of_usercode'];
    $data->category_type_code=$userinfo['fk_cat_code'];
    $data->type_commodity_code=$userinfo['fk_typeofcommodity_code'];
    $data->dealear_code=$userinfo['fk_dealear_code'];
    $data->manager_code=$userinfo['fk_manager_code'];
    $data->officer_code=$userinfo['fk_officer_code'];
    $data->f_name=$userinfo['f_name'];
    $data->m_name=$userinfo['m_name'];
    $data->l_name=$userinfo['l_name'];
    $data->mobile_no=$userinfo['mobile_no'];
    $data->alternate_no=$userinfo['alternate_no'];
    $data->email_id=$userinfo['email_id'];
    $data->city_code=$userinfo['fk_city_code'];
    $data->district_code=$userinfo['fk_district_code'];
    $data->pin_code=$userinfo['pin_code'];
    $data->state_code=$userinfo['fk_state_code'];
	if($userinfo['pancard_image_path']!="")
	{
    $data->pancard_image_path=$pan_pic_url.$userinfo['pancard_image_path'];
	}
	else
	{
	$data->pancard_image_path="";
	}
	if($userinfo['adhar_card_image_path']!="")
	{
    $data->adhar_card_image_path=$adhar_pic_url.$userinfo['adhar_card_image_path'];
	}
	else
	{
    $data->adhar_card_image_path="";
	}
	if($userinfo['user_profile_image_path']!="")
	{
    $data->user_profile_image_path=$user_pic_url.$userinfo['user_profile_image_path'];
	}
	else
	{
    $data->user_profile_image_path="";
	}
    $data->pancard_no=$userinfo['pancard_no'];
    $data->salary=$userinfo['salary'];
    $data->ta_da=$userinfo['ta_da'];
    $data->user_createdate=$userinfo['user_createdate'];
    $data->update_date=$userinfo['update_date'];
    $data->is_loggedin=1;
    $res->metadata = array('login' => $data);
    echo json_encode($res);
} else {
    $res->success = false;
	$res->session->expires = true;
    $res->session->is_LoggedIn = FALSE;
	$res->session->LOGINTOKEN = $LoginToken;
	$res->resCode = dataFormat::$res_fail;
    $res->message =dataFormat::$message_invaliduser;
    echo json_encode($res);
}

?>
