<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$json = file_get_contents('php://input');
//Converts it into a PHP object
$taskInfo = json_decode($json);
$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('add_new_task' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('add_new_task' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
     $addvalue=$add->addNewTask($taskInfo,$unique_code);
	if($addvalue==1)
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_new_recordadded;
		$res->session->unique_code=$unique_code;
		$res->session->expires = false;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
		$data->activity_id=$taskInfo->activity_id;
		$data->planned_date=$taskInfo->planned_date;
		$data->customer_name=$taskInfo->customer_name;
		$data->mobile_no=$taskInfo->mobile_no;
		$data->actual_completion_date=$taskInfo->actual_completion_date;
		$data->status_id=$taskInfo->status_id;
		$data->remarks=$taskInfo->remarks;
		$res->metadata = array('add_new_task' => $data);
        echo json_encode($res);
	}
	else
	{
       $res->success = false;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_other;
       $res->message =dataFormat::$msg_recordadd_fail;    
	   echo json_encode($res);
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

