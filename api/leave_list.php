<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$json = file_get_contents('php://input');
////Converts it into a PHP object
$list_detail = json_decode($json);

$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('leave_list' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('leave_list' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);

if($loginTokenInfo)
{
	$leaveListInfo=$display ->toShowAllLeaveList($leave_detail,$unique_code);
	//print_r($taskListInfo);die;
	if($leaveListInfo)
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_list;
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
        $dataArray= array();
        $dataArray['leave_list'] = $leaveListInfo;
        $res->metadata=$dataArray;
        echo json_encode($res);		
	}
	else
	{
       $res->success = true;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_fail;
       $res->message =dataFormat::$msg_no_data_list;   
       $res->metadata=array('leave_list' =>$leaveListInfo);
	   echo json_encode($res);	   
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

