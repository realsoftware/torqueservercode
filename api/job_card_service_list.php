<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$json = file_get_contents('php://input');
//Converts it into a PHP object
$jobcardInfo = json_decode($json);
$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('job_card_service_list' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('job_card_service_list' => $data);
   echo json_encode($res);
   return;
}
#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
	$oil_charges=$display ->oilChargesList($jobcardInfo,$unique_code);
	$out_side_work=$display ->outSideWorkList($jobcardInfo,$unique_code);
	$labour_charges=$display ->labourChargesList($jobcardInfo,$unique_code);
	$spare_charges=$display ->spareChargesList($jobcardInfo,$unique_code);
	//print_r($menuListInfo);die;
	if(!empty($oil_charges) || !empty($out_side_work) || !empty($labour_charges) || !empty($spare_charges) )
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_list;
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
        $dataArray= array();
		$dataArray['oil_charge_list']=$oil_charges;
		$dataArray['out_side_list']=$out_side_work;
		$dataArray['labour_charges_list']=$labour_charges;
		$dataArray['spare_charges_list']=$spare_charges;
		$res->metadata=array('job_card_service_list' =>$dataArray);
        echo json_encode($res);	
	}
	else
	{
       $res->success = true;
       $res->session->expires = true;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_success;
       $res->message =dataFormat::$msg_no_data_list;   
       $dataArray= array();
		$dataArray['oil_charge_list']=$oil_charges;
		$dataArray['out_side_list']=$out_side_work;
		$dataArray['labour_charges_list']=$labour_charges;
		$dataArray['spare_charges_list']=$spare_charges;
		$res->metadata=array('job_card_service_list' =>$dataArray);
        echo json_encode($res);	
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

