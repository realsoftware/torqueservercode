<?php
include 'class/response.php';
$heder = getallheaders();
$DeviceId=dataFormat::$DeviceId;
$Plateform=dataFormat::$Plateform;
$Version=dataFormat::$Version;
$unique_code=dataFormat::$unique_code;
$login_token=dataFormat::$login_token;

$DeviceId = $heder[$DeviceId];
$Plateform = $heder[$Plateform];
$Version = $heder[$Version];
$unique_code = $heder[$unique_code];
$login_token = $heder[$login_token];

$json = file_get_contents('php://input');
////Converts it into a PHP object
$task_detail = json_decode($json);

$res = new dataFormat();
$data = new responseData();
$res->session = new sessionData();
$msg_required=dataFormat::$msg_required;
if(@$unique_code=='')
{
   $data		=$msg_required;
   $res->metadata = array('task_list_bydate' => $data);
   echo json_encode($res);
   return ;
}
if(@$login_token=='')
{
   $data		=$msg_required;
   $res->metadata = array('task_list_bydate' => $data);
   echo json_encode($res);
   return;
}


#######Chcek login token is available
$loginTokenInfo=$display->CheckLoginToken($unique_code,$login_token);
	//print_r($loginTokenInfo);die;

if($loginTokenInfo)
{
	$todaytaskListInfo=$display ->toShowAllTaskListByDateToday($task_detail,$unique_code);
	$tomorrowtaskListInfo=$display ->toShowAllTaskListByDateTomorrow($task_detail,$unique_code);
	$tripStatus=$display ->toShowTripSTatus($unique_code);
	$attStatus=$display ->toShowAttendenceSTatus($unique_code);
	if($attStatus['attendence_status']=='out')
	{
		$flag=true;
	}
	else{
		$flag=false;
	}
	//print_r($taskListInfo);die;
	if($todaytaskListInfo || $tomorrowtaskListInfo)
	{
        $res->success = true;
		$res->resCode = dataFormat::$res_success;
		$res->message = dataFormat::$msg_list;
		$res->session->unique_code=$unique_code;
		$res->session->expires = FALSE;
		$res->session->platform = $Plateform;
		$res->session->LOGINTOKEN=$login_token;
		$data->attendence_status=$attStatus['attendence_status'];
		$data->attendence_display_flag=$flag;
		$data->trip_status=$tripStatus['type'];

     	$dataArray['today_task_list']=$todaytaskListInfo;
		$dataArray['tomorrow_task_list']=$tomorrowtaskListInfo;
		//$dataArray['currentstatus']=$data;
       
		$res->metadata=array('task_list_bydate' =>$dataArray);
		$res->metadata['currentstatus']=$data;
		//$res->metadata=array('task_list_bydate' =>$arra);
        echo json_encode($res);			
	}
	else
	{
       $res->success = true;
       $res->session->expires = false;
       $res->session->is_LoggedIn = true;
	   $res->session->unique_code=$unique_code;
	   $res->session->platform = $Plateform;
	   $res->session->LOGINTOKEN=$login_token;
       $res->resCode = dataFormat::$res_success;
	   $data->attendence_status=$attStatus['attendence_status'];
	   $data->trip_status=$tripStatus['type'];
       $res->message =dataFormat::$msg_no_data_list;   
       $data->attendence_display_flag=$flag;	   
	   $res->metadata['currentstatus']=$data;
	   echo json_encode($res);
	}
   
}
else{
   $res->success = false;
   $res->session->expires = false;
   $res->session->is_LoggedIn = true;
   $res->resCode = dataFormat::$res_acces_token_expired;
   $res->message =dataFormat::$msg_incorrect_usertoken;
   echo json_encode($res);
}
?>

