<?php
include 'response.php';
include 'opendb.php';
$heder = getallheaders();
$inputPost = filter_input_array(INPUT_POST);
$DeviceId = $heder["DeviceId"];
$Plateform = $heder["Plateform"];
$Version = $heder["Version"];
$userId=$inputPost["userId"];
$title=$inputPost["title"];
$content=$inputPost["description"];

$imgPath="";

function UploadImages($fUniqueName){
    //getinbg file info
    $info = pathinfo($_FILES['image']['name']);
    // get the extension of the file
    $ext = $info['extension']; 
    $newname = "$fUniqueName.".$ext; 
    // Path to move uploaded files
    $target_path = "docs/";
    // getting server ip address
    $server_ip = gethostbyname(gethostname());
    // final file url that is being uploaded
    //$file_upload_url = 'http://' . $server_ip . '/' . 'autocab' . '/' . $target_path;
	$file_upload_url = 'http://thepadosi.com/admin/'.$target_path;
       // $file_upload_url = 'http://localhost/Neighbour/'.$target_path;
       
        
    if (isset($_FILES['image']['name'])) {
        $target_path = $target_path . $newname;
        $response['file_name'] = $newname;
        try {
            // Throws exception incase file is not being moved
            if (!move_uploaded_file($_FILES['image']['tmp_name'], '../'.$target_path)) {
                // make error flag true
                $response['error'] = true;
                $response['message'] = 'Could not move the file!';
            } 
            // File successfully uploaded
            $Path=$target_path;
            $imgPath=$Path;
            $response['message'] = 'File uploaded successfully!';
            $response['error'] = false;
            $response['file_path'] = $Path;
            $response['file_url']=$imgPath;
        }
        catch (Exception $e) {
            // Exception occurred. Make error flag true
            $response['error'] = true;
            $response['message'] = $e->getMessage();
        }
    } else {
        // File parameter is missing
        $response['error'] = true;
        $response['message'] = 'Not received any file!';
    }
    return $response;
}
function GenUserId($length = 4) {
    $chars = '0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .=substr($chars, $index, 1);
    }
    return $result;
}
$id=GenUserId();
$uniqeId=$userId.$id;
   
	$resp = UploadImages($uniqeId);

	$LoginToken = time() . uniqid() . $userId;
	$imgPath = $resp["file_url"];
       /////////////function for generate user Id////////////
  function contentId($length = 4) {
        $chars = '0123456789';
        $count = strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= substr($chars, $index, 1);
        }
        return $result;
    }
    $user = 'CON';
    $cId = $user . contentId();
 /////////////////End code////////////////////////////////////
    $query="INSERT INTO content (userId,contentId,title,contentDescription, imagePath)"
             . "VALUES(UPPER('$userId'),UPPER('$cId'),'$title','$content','$imgPath')";    
  
   
    $result = mysqli_query($con, $query);
    if ($result > 0) {
        $res = new dataFormat();
        $res->success = true;
        $res->message = "New Content added Successfully.";
        $res->session = new sessionData();
        $res->session->id = $userId;
        $res->session->expires = FALSE;
        $res->session->platform = $Plateform;
        $res->session->LOGINTOKEN = $LoginToken;
        $data = new responseData();
        $data->userId = $userId;
        $data->feedId = $cId;
        $data->title = $title;
        $data->description = $content;
        $data->image =$imgPath;
        
        $res->metadata = array('data' => $data);
        echo json_encode($res);
    }
?>