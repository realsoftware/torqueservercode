<?php

class Display
{
	
	############################ 		Standard Functions		#########################
	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	############Get User Create update delete acces
	public function getAccessDetailForUserManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	#########Get User Menu List for admin and sub admin
	public function getMenuList($usertype,$unique_code)
	{
		 $mysqli_s 	= 	$this->CONN();
		  $data		=	array();
		    if($usertype=="sadmin")
			{
			
           // Query 6
		    $queryy    		=   "SELECT * FROM `master_module`
			WHERE  admin_module_name!='' AND plateform IN ('web','both') ORDER BY position asc";						
								
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
			foreach($result as $results)
			{
				$data[] = $results;
			}
			}
			if($usertype=="ADM" || $usertype=="DLR" || $usertype=="MGR")
			{
            // Query 6
		    $queryy    		=   "SELECT * FROM `user_role_acces`
            LEFT JOIN master_module ON  master_module.code =user_role_acces.fk_access_module_code
			WHERE user_role_acces.fk_user_unique_code='$unique_code' AND master_module.admin_module_name!='' AND plateform IN ('web','both') ORDER BY master_module.position asc";						
								
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result 	= 	$query->get_result();
			foreach($result as $results)
			{
				$data[] = $results;
			}			}
			
		
			 return @$data;
	}
	public function ShowDefaultInformation()
	{
        $mysqli_s 	= 	$this->CONN();
		$data       =   array();
        
		$queryy     =   "SELECT * FROM `configuration` ";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$row		=	$query->get_result();
		
		foreach($row as $rows)
        {           
            @$data[$rows['configuration_name']] = @$rows['configuration_value'];         
        }
        
		return @$data; 
	}
	
	#########Get Manager Dealer Name By ID
	public function getManagerDealerName($id)
	{
		    $mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT *,CONCAT(f_name, ' ', l_name, '(', unique_code,')') AS name FROM `user`
			WHERE  unique_code='$id' ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	#########Total No User  FOR DASHBOARD
	public function totalRegisteredUser()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where fk_dealear_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where  fk_manager_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}		

		
		
		
		
		
		
		
		// $queryy     =   " SELECT * FROM `user` 
						// WHERE status =1 AND fk_type_of_usercode IN('DLR','MGR','OFF','ADM')";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	
	#########Total No Task  FOR DASHBOARD
	public function totalCraetedTask()
	{
        $mysqli_m 	= 	$this->CONN();
		 $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND task_detail.status=1 order by task_detail.created_date desc";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  task_detail.status=1 order by task_detail.created_date desc";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE task_detail.status=1 order by task_detail.created_date desc";
		}
       
		// $queryy     =   " SELECT * FROM `task_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	#########Total No Trip  FOR DASHBOARD
	public function totalCraetedTrip()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,trip_detail.created_date as cr_date_time,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 order by trip_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 order by trip_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE trip_detail.status=1 order by trip_detail.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
    ###########All Task List 
	 public function toShowAllTaskList()
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();

        $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND task_detail.status=1 order by task_detail.created_date desc";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  task_detail.status=1 order by task_detail.created_date desc";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE task_detail.status=1 order by task_detail.created_date desc";
		}
        
        // Query 6
		// $queryy    		=   "SELECT task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		// activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		// ,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		// task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		// LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    // LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		// WHERE task_detail.status=1 order by task_detail.created_date desc";						
								
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		    return @$data;
		
	}
	###########All Attendence List
	 public function toShowAllAttendencList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		
	    $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || fk_user_unique_code='$unique_code') AND attendence_detail.status=1 order by attendence_detail.created_date DESC ";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || fk_user_unique_code='$unique_code') AND attendence_detail.status=1 order by attendence_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE attendence_detail.status=1 order by attendence_detail.created_date DESC ";
		}
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	
	#########Trip List By Date type from to date
	 public function toShowAllTripList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,trip_detail.created_date as cr_date_time,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 order by trip_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 order by trip_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time ,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE trip_detail.status=1 order by trip_detail.created_date DESC ";
		}
        
	    // $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time from trip_detail  
		 // LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        // LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		// WHERE trip_detail.status=1 order by trip_detail.created_date DESC ";
		
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	
	############################ 		Authorizaton Functions		#########################
	
	public function CheckLogin()
	{
		$mysqli_s 	= 	$this->CONN();
		
		$username	=	@$_POST['username'];
		$password	=	@$_POST['password'];
		
		if(@$username=='')
		{
			@$_SESSION['TEMP']['msz']		=	"Please Fill Username";
			header("Location:index.php");
		}
		else
		{
			if(@$password=='')
			{
				@$_SESSION['TEMP']['msz']		=	"Please Fill Password";
				header("Location:index.php");
			}
			else
			{
		  		$queryy     =   "SELECT * FROM `user` 
								WHERE unique_code = ? AND password = ? AND status = '1' ";			
				$query 		= 	$mysqli_s->prepare($queryy);
				$query->bind_param("ss", $username, $password);
				$query->execute();
				$result 	= 	$query->get_result();
				$num		=	$result->num_rows;
				if(@$num>0)
				{				
					$data	=	$result->fetch_assoc();
					
					$_SESSION['TORQUE_USERID']		=	@$data['unique_code'];
					$_SESSION['f_name']		=	@$data['f_name'];
					$_SESSION['l_name']		=	@$data['l_name'];
					$_SESSION['TORQUE_USERTYPE']	=	@$data['fk_type_of_usercode'];
					
					$query->close();
					
					header("Location:dashboard.php");
				}
				else
				{
					@$_SESSION['TEMP']['msz']		=	"User ID/Password doesn't match";
					header("Location:index.php");
				}		
			}
		}

		return;
	}
		
	public function CheckLogout()
	{
		unset($_SESSION['TORQUE_USERID']);		
		header("Location:".TORQUE_FRONTURL."index.php");
	}
##################END RANJEET CODE
	public function ShowUserDetailById($userid)
	{
        $mysqli_s 		= 	$this->CONN();
	    
		if(@$userid)
		{
			$queryy     =   "SELECT * FROM `user` WHERE user.user_id = ? ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->bind_param("i", $userid);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
		}
	}
	########Show User Type
		public function ShowAllUserType()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT * FROM `master_user_type` where status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Category Type
		public function ShowAllCategory()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT * FROM `master_category` where status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	
	########Show Commodity Type
		public function ShowAllTypeCommodity()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT * FROM `master_commodity` where status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#########Show User Details For Update 
	 public function userDetailByID($unique_code)
	 {
		$mysqli_s 		= 	$this->CONN();
	    $queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		where user.status=1 AND unique_code='$unique_code' LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;	
	}
	############Get Manager or Dealer Name 
	public function getMnagerName($fk_manager_code)
	{
	    $mysqli_s 		= 	$this->CONN();
	    $queryy     =   "SELECT CONCAT(f_name, ' ', l_name, '(', unique_code,')') AS maganer_name FROM `user`
			WHERE  unique_code='$fk_manager_code' AND fk_type_of_usercode='MGR'
									ORDER BY user.user_id LIMIT 1 ";
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;
	}
	public function getDealerName($fk_dealear_code)
	{
	    $mysqli_s 		= 	$this->CONN();
			 $queryy     =   "SELECT CONCAT(f_name, ' ', l_name, '(', unique_code,')') AS dealer_name FROM `user`
			WHERE  unique_code='$fk_dealear_code' AND fk_type_of_usercode='DLR'
									ORDER BY user.user_id LIMIT 1 ";						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;
	}
	########Show All User Details
		public function allUserDetail()
		{
	    $unique_code		=	TORQUE_USERID;		
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where fk_dealear_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where  fk_manager_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where user.status=1 AND fk_type_of_usercode!='sadmin' ORDER BY user_createdate desc";	
		}		
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Read Write Access Type
		public function ShowAllReadWriteAccess()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT * FROM `master_useraccessrights` where is_enable=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show Module Type
		public function ShowAllModuleType()
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT * FROM `master_module` where status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All State
	public function ShowAllState()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(state_code),state_name FROM `master_pincode_withstate` where status=1 ORDER BY state_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	########Show All District By state ID
	public function showAllDistrictByID($id)
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(dist_code),dist_name FROM `master_pincode_withstate` where state_code='$id' AND status=1 ORDER BY dist_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show All District By state ID
	public function showAllDistrict()
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(dist_code),dist_name FROM `master_pincode_withstate` where status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Get Module Access 
	public function moduleAccessDetailByID($unique_code,$module_code)
	{
		
		$mysqli_s 		= 	$this->CONN();
	    $queryy     =   "SELECT * FROM `user_role_acces` where  fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' ORDER BY id ASC LIMIT 1 ";						
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$row		=	$query->get_result()->fetch_assoc();
		$query->close();
		return @$row;	
	   
	}
	########Show All City By state ID
	public function showAllCityByID($id)
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT * FROM `master_taluka_city` where dist_code='$id' AND status=1 ORDER BY taluka_name ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	###########Show All Pincode by district ID
	public function showAllPincodeByID($id)
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT distinct(pin_code) FROM `master_pincode_withstate` where dist_code='$id' AND status=1 ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#########Get Trip Detail By ID
	public function tripDetailByID($trip_id)
	{
		 $mysqli_s 	= 	$this->CONN();
		
		$queryy     =   " SELECT*  FROM `trip_detail` 
		LEFT JOIN user ON user.unique_code = trip_detail.fk_userunique_code
						WHERE trip_id = '$trip_id' ";						
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
	}
	#########Get Task Detail By ID
	public function taskDetailByID($task_id)
	{
		 $mysqli_s 	= 	$this->CONN();
		
		$queryy     =   " SELECT*  FROM `task_detail` 
						WHERE task_id = '$task_id' ";						
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
	}
	#########Trip List HistoryTable
	 public function toShowAllTripHistoryList($trip_id)
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		
		$queryy     =   "SELECT * from trip_latlong_history  
		WHERE fk_trip_id='$trip_id' order by trip_latlong_history.created_date DESC";
		
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	
	###########All Attendence List
	 public function toShowAllAttendencListByID($id)
	{
	 	$mysqli_s 		= 	$this->CONN();
      	$queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE  attendence_id='$id' AND  attendence_detail.status=1 order by attendence_detail.created_date DESC ";
		//}
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
	}
	public function showAllImage($image_unique_code)
	{
		$mysqli_s 		= 	$this->CONN();
	    $data		=	array();
      	$queryy     =   "SELECT * from `image_detail` WHERE fk_image_unique_code = '$image_unique_code'";
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
				
	}
	 public function toShowAllTaskListByID($id)
	{
		$mysqli_s 		= 	$this->CONN();
       
	     $queryy     =   "SELECT approved_by_remark,approved_by,image_unique_code,closed_image_unique_code,f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE task_id='$id' AND task_detail.status=1 order by task_detail.created_date desc";
						
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
		
	}
     public function showAllImageTask($image_unique_code,$close_image_unique_code)
	{
		$mysqli_s 		= 	$this->CONN();
	    $data		=	array();
      	$queryy     =   "SELECT * from `image_detail` WHERE fk_image_unique_code = '$image_unique_code' || fk_image_unique_code='$close_image_unique_code' ";
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
				
	}
	 public function toShowAllTripListByID($id)
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    
	     $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time ,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE trip_id='$id' AND trip_detail.status=1 order by trip_detail.created_date DESC ";
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
       
	}
	#############Folder List
	 public function toShowAllProductList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE product_folder_detail.created_by='$unique_code' AND  product_folder_detail.status=1 order by product_folder_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE product_folder_detail.created_by='$unique_code' AND  product_folder_detail.status=1 order by product_folder_detail.created_date DESC";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  WHERE
		product_folder_detail.status=1 order by product_folder_detail.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	#############Show All Product
	public function ShowAllProduct()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT* FROM `master_product` where status=1 ORDER BY product_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#########Show Product Detail By ID
	public function showProductDetailByID($id)
	{
			$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    
	     echo$queryy     =   "SELECT * from product_folder_detail where id='$id'";
		 $query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
	}
	#########Total No Product  FOR DASHBOARD
	public function totalCraetedProduct()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT* from product_folder_detail
		WHERE created_by='$unique_code' AND  product_folder_detail.status=1";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	  $queryy     =   "SELECT* from product_folder_detail
		WHERE created_by='$unique_code' AND  product_folder_detail.status=1";		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT* from product_folder_detail
		WHERE product_folder_detail.status=1";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	#############Folder List
	 public function toShowAllEnquiryList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		 LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND enquiry_details.status=1 order by enquiry_details.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		 LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND  enquiry_details.status=1 order by enquiry_details.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
	    LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		WHERE  enquiry_details.status=1 order by enquiry_details.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
    #########Total No Enqiry  FOR DASHBOARD
	public function totalCraetedEnquiry()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from enquiry_details  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND  enquiry_details.status=1 order by enquiry_details.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT * from enquiry_details  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND  enquiry_details.status=1 order by enquiry_details.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *  from enquiry_details  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE enquiry_details.status=1 order by enquiry_details.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	
	
	#########Total No JOBCARD  FOR DASHBOARD
	public function totalCraeteJobCard()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from job_card_details  
		 LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND  job_card_details.status=1 order by job_card_details.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT * from job_card_details  
		 LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND  job_card_details.status=1 order by job_card_details.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *  from job_card_details  
		 LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE job_card_details.status=1 order by job_card_details.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	#########Total No JOBCARD  FOR DASHBOARD
	public function totalCraetedTADA()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from hr_tada  
		 LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND  hr_tada.status=1 order by hr_tada.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT * from hr_tada  
		 LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND  hr_tada.status=1 order by hr_tada.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *  from hr_tada  
		 LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE hr_tada.status=1 order by hr_tada.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	#########Total No Salary Slip  FOR DASHBOARD
	public function totalCraetedSalarySlip()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from hr_salary_detail  
		 LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_salary_detail.fk_userunique_id='$unique_code') AND  hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT * from hr_salary_detail  
		 LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_salary_detail.fk_userunique_id='$unique_code') AND  hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *  from hr_salary_detail  
		 LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	
	#########Total No LEAVE  FOR DASHBOARD
	public function totalCraetedLeave()
	{
        $mysqli_m 	= 	$this->CONN();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from hr_leave  
		 LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND  hr_leave.status=1 order by hr_leave.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT * from hr_leave  
		 LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND  hr_leave.status=1 order by hr_leave.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *  from hr_leave  
		 LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE hr_leave.status=1 order by hr_leave.created_date DESC ";
		}
        
		
		// $queryy     =   " SELECT * FROM `trip_detail` 
						// WHERE status =1";						
		$query 		= 	$mysqli_m->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		$num		=	$result->num_rows;
		$query->close();
		
		return @$num;
	}
	
	##########Enquiry Details
	 public function toShowEnquiryDetails($id)
	{
	    $enquiry_code= $enquiry_detail->enquiry_code;
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT *,enquiry_details.created_date as cr_date,enquiry_details.updated_date as up_date  FROM `enquiry_details`
			LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
		    LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		    LEFT JOIN master_model_interested ON master_model_interested.model_code =  enquiry_details.fk_model_intrested  
		    LEFT JOIN master_implement_interested ON master_implement_interested.code =  enquiry_details.fk_implemented_intrested  
		    LEFT JOIN master_source_enquiery ON master_source_enquiery.code =  enquiry_details.fk_source_of_enquiry  
		    LEFT JOIN master_buyer_type ON master_buyer_type.code =  enquiry_details.fk_type_of_buyer  
		    LEFT JOIN master_finance_type ON master_finance_type.code =  enquiry_details.fk_finanace_type  
		    LEFT JOIN master_financier ON master_financier.code =  enquiry_details.fk_financiar_name  
			

			WHERE enquiry_id ='$id' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	########Show  Enquiry Visit Remark
		public function ShowAllEnquiryVisitRemark($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT remark,visit_remark.created_date,image_detail.image_path FROM `visit_remark` 
	    LEFT JOIN image_detail ON image_detail.fk_image_unique_code =  visit_remark.image_unique_code  
		where fk_enquiry_code='$code' ORDER BY visit_remark_id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	public function showMasterReasonLost()
	{
	 	$mysqli_s 		= 	$this->CONN();
        $data		=	array();
	    $queryy     =   "SELECT code,reason_for_lost as name from `master_reason`
		where  status='1' ORDER BY id ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	#############Show All Implemented Interested
	public function ShowAllImplementedInterested()
	{
        $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT code,implement_interested_type as name FROM `master_implement_interested` where status=1 ORDER BY implement_interested_type ASC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	##########Search User Data
		public function searchUserType()
		{
	    $autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    $unique_code		=	TORQUE_USERID;		
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$user_type=substr($unique_code,0,3);
		if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(user_createdate) >= '$from_date' AND DATE(user_createdate)<= '$to_date')";
			}
			
		##########Dealer Case 
		if($user_type=='DLR')
		{
			
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		where fk_dealear_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' $cond ORDER BY user_createdate desc";	
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode   
		where  fk_manager_code='$unique_code' AND user.status=1 AND fk_type_of_usercode!='sadmin' $cond ORDER BY user_createdate desc";	
		}
		#####################Admin Super admin case
	    else 

		{
	    $queryy     =   "SELECT * FROM `user`
		LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  user.fk_city_code  
		LEFT JOIN master_category ON master_category.code =  user.fk_cat_code  
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		
   
         
		where user.status=1 AND fk_type_of_usercode!='sadmin' $cond ORDER BY user_createdate desc";	
		}		
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		if($cond!="")
		{			
	    $_SESSION['user_info'] = $data;
		}
		
		header("Location:users.php?search=1");
		//return @$data;
	}
	public  function resetUserSearch()
	{
		$_SESSION['user_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		header("Location:users.php");

	}
	###########All Attendence Search
	 public function toShowAllAttendenceSearch()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		
	    $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $attendence_type                				=  @$_POST['attendence_type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['attendence_type']                 =$attendence_type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(attendence_detail.created_date) >= '$from_date' AND DATE(attendence_detail.created_date)<= '$to_date')";
			}
		    if($attendence_type!="")
			{
				$cond.=" AND attendence_detail.type like '%$attendence_type%'";
			}
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || fk_user_unique_code='$unique_code') AND attendence_detail.status=1 $cond order by attendence_detail.created_date DESC ";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || fk_user_unique_code='$unique_code') AND attendence_detail.status=1 $cond order by attendence_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,attendence_detail.created_date as date_time from attendence_detail 
        LEFT JOIN user ON user.unique_code =  attendence_detail.fk_user_unique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE attendence_detail.status=1 $cond order by attendence_detail.created_date DESC ";
		}
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		//return @$data;
		if($cond!="")
		{			
	    $_SESSION['attendence_info'] = $data;
		}
		
		header("Location:attendence_list.php?search=1");
		
	}
	public  function resetAttendenceSearch()
	{
		$_SESSION['attendence_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['attendence_type'] ='';
		header("Location:attendence_list.php");

	}
	 ###########All Task Search 
	 public function toShowAllTaskSearch()
	{
		$mysqli_s 		= 	$this->CONN();
        $data		=	array();

        $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $task_type                				=  @$_POST['task_type'];	
	    $customer_name                				=  @$_POST['customer_name'];	
	    $mobile_no                				=  @$_POST['mobile_no'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['task_type']                 =$task_type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
		$_SESSION['customer_name'] =$customer_name;
		$_SESSION['mobile_no'] =$mobile_no;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(task_detail.created_date) >= '$from_date' AND DATE(task_detail.created_date)<= '$to_date')";
			}
		    if($task_type!="")
			{
				$cond.=" AND task_detail.task_status like '%$task_type%'";
			}
			 if($customer_name!="")
			{
				$cond.=" AND task_detail.customer_name like '%$customer_name%'";
			}
			 if($mobile_no!="")
			{
				$cond.=" AND task_detail.mobile_no like '%$mobile_no%'";
			}
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND task_detail.status=1 $cond order by task_detail.created_date desc";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  task_detail.status=1 $cond order by task_detail.created_date desc";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT f_name,l_name,aproved_status,task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		WHERE task_detail.status=1 $cond order by task_detail.created_date desc";
		}
        
        // Query 6
		// $queryy    		=   "SELECT task_id,fk_userunique_code,task_detail.updated_date,task_detail.created_date,fk_activity_id as activity_code,
		// activity_name,planned_date,customer_name,task_detail.mobile_no,actual_completion_date
		// ,remarks,image_unique_code,activity_name,task_status,task_detail.created_date as task_created_date,close_remark,
		// task_detail.updated_date as task_updated_date,fk_dealear_code,fk_manager_code from `task_detail`
		// LEFT JOIN master_activity ON master_activity.code =  task_detail.fk_activity_id 
	    // LEFT JOIN user ON user.unique_code =  task_detail.fk_userunique_code 

		// WHERE task_detail.status=1 order by task_detail.created_date desc";						
								
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
//return @$data;
		if($cond!="")
		{			
	    $_SESSION['task_info'] = $data;
		}
		
		header("Location:task_list.php?search=1");
				
	}
	public  function resetTaskSearch()
	{
		$_SESSION['task_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['task_type'] ='';
		$_SESSION['customer_name'] ='';
		$_SESSION['mobile_no'] ='';
		header("Location:task_list.php");

	}
	#########Trip List By Date type from to date
	 public function toShowAllTripSearch()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $type                				=  @$_POST['type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['type']                 =$type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(trip_detail.trip_date_time) >= '$from_date' AND DATE(trip_detail.trip_date_time)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND trip_detail.type like '%$type%'";
			}
			
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,trip_detail.created_date as cr_date_time,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 $cond order by trip_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || fk_userunique_code='$unique_code') AND  trip_detail.status=1 $cond order by trip_detail.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,trip_detail.created_date as cr_date_time ,trip_detail.updated_date as up_date_time from trip_detail  
		 LEFT JOIN user ON user.unique_code =  trip_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE trip_detail.status=1 $cond order by trip_detail.created_date DESC ";
		}
        
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
if($cond!="")
		{			
	    $_SESSION['trip_info'] = $data;
		}
		
		header("Location:trip_list.php?search=1");
			}
	public  function resetTripSearch()
	{
		$_SESSION['trip_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['type'] ='';
		header("Location:trip_list.php");

	}
	#############Folder List
	 public function toShowAllProductSearch()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
			$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $product_name                			=  @$_POST['product_name'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['product_name']                 =$product_name;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(product_folder_detail.created_date) >= '$from_date' AND DATE(product_folder_detail.created_date)<= '$to_date')";
			}
		    if($product_name!="")
			{
				$cond.=" AND master_product.product_name like '%$product_name%'";
			}
			
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE product_folder_detail.created_by='$unique_code' AND  product_folder_detail.status=1 $cond order by product_folder_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE product_folder_detail.created_by='$unique_code' AND  product_folder_detail.status=1 $cond order by product_folder_detail.created_date DESC";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,product_folder_detail.created_by as created_user,product_folder_detail.id as folder_id,product_folder_detail.created_date as cr_date_time from product_folder_detail  
		 LEFT JOIN master_product ON master_product.code =  product_folder_detail.fk_product_code  
		 LEFT JOIN user ON user.unique_code =  product_folder_detail.fk_userunique_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  WHERE
		product_folder_detail.status=1 $cond order by product_folder_detail.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
        if($cond!="")
		{			
	    $_SESSION['folder_info'] = $data;
		}
		
		header("Location:folder_list.php?search=1");
	}
	public  function resetFolderSearch()
	{
		$_SESSION['folder_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['product_name'] ='';
		header("Location:folder_list.php");

	}
#############Folder List
	 public function toShowAllEnquirySearch()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $type                			=  @$_POST['type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['type']                 =$type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(enquiry_details.created_date) >= '$from_date' AND DATE(enquiry_details.created_date)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND enquiry_status like '%$type%'";
			}
			
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		 LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND enquiry_details.status=1 $cond order by enquiry_details.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		 LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		 LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || enquiry_details.fk_usercode='$unique_code') AND  enquiry_details.status=1 $cond  order by enquiry_details.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT *,enquiry_details.address as address_enq,enquiry_details.created_date as cr_date_time,enquiry_details.updated_date as up_date_time from enquiry_details  
		LEFT JOIN user ON user.unique_code =  enquiry_details.fk_usercode  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
	    LEFT JOIN master_type_of_enquiery_customertype ON master_type_of_enquiery_customertype.code =  enquiry_details.fk_customer_type  
		WHERE  enquiry_details.status=1 $cond order by enquiry_details.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
        if($cond!="")
		{			
	    $_SESSION['enquiry_info'] = $data;
		}
		
		header("Location:enquiry_list.php?search=1");
	}
	public  function resetEnquirySearch()
	{
		$_SESSION['enquiry_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['type'] ='';
		header("Location:enquiry_list.php");

	}
	
	public  function resetLeaveSearch()
	{
		$_SESSION['enquiry_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['type'] ='';
		header("Location:leave_list.php");

	}
	###########All Attendence List
	 public function toShowAllPaySlipList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		
	    $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,hr_salary_detail.created_date as date_time from hr_salary_detail 
        LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
        WHERE (fk_dealear_code='$unique_code'  || hr_salary_detail.fk_userunique_id='$unique_code')		
		AND hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC";
		}
		###############Manager Case
		else if($user_type=='MGR')
		{
	    $queryy     =   "SELECT *,hr_salary_detail.created_date as date_time from hr_salary_detail 
        LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
        WHERE (fk_manager_code='$unique_code'  || hr_salary_detail.fk_userunique_id='$unique_code')		
		AND hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC";
		}
		#####################Admin Super admin case
		else
		{
	     $queryy     =   "SELECT *,hr_salary_detail.created_date as date_time from hr_salary_detail 
        LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE hr_salary_detail.status=1 order by hr_salary_detail.created_date DESC ";
		}
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	 public function toShowPaySlipDetails($id)
	{
	    $enquiry_code= $enquiry_detail->enquiry_code;
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * from hr_salary_detail
			LEFT JOIN user ON user.unique_code =  hr_salary_detail.fk_userunique_id
			WHERE hr_salary_detail.fk_userunique_id ='$id' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	
	 public function toShowUserDetails($id)
	{
	    $enquiry_code= $enquiry_detail->enquiry_code;
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * from user 
			WHERE unique_code ='$id' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	 public function toShowAllJobCardList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,job_card_details.created_date as cr_date_time,job_card_details.job_card_close_datetime as up_date_time from job_card_details  
        LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  		
		LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code   
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND job_card_details.status=1 order by job_card_details.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT *,job_card_details.created_date as cr_date_time,job_card_details.job_card_close_datetime as up_date_time from job_card_details  
		LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
		LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND  job_card_details.status=1 order by job_card_details.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT *,job_card_details.created_date as cr_date_time,job_card_details.job_card_close_datetime as up_date_time from job_card_details  
        LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code 
        LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  		
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode   
		WHERE  job_card_details.status=1 order by job_card_details.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	#################All Leave List
	 public function toShowAllLeaveList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT * from hr_leave  
        LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  		
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND hr_leave.status=1 order by hr_leave.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT * from hr_leave  
		LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND  hr_leave.status=1 order by hr_leave.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT * from hr_leave  
        LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code 
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode   
		WHERE  hr_leave.status=1 order by hr_leave.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	############To Show All Job Card List By Date
	public function toShowAllJobCardDetail($id)
	{
		    $mysqli_m 	= 	$this->CONN();

		    $queryy     =   " SELECT*,job_card_details.address as job_address,
            job_card_details.pin_code as job_pin_code,
            job_card_details.mobile_no as job_mobile_no,
            job_card_details.created_date as cr_date
			FROM `job_card_details` 
			LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN master_taluka_city ON master_taluka_city.taluka_code =  job_card_details.fk_taluka_code  
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code
			WHERE job_card_id ='$id' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;  		
	}	
	########Show  Enquiry Visit Remark
		public function ShowAllLabourCharges($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT* from job_card_service_labour_cost
		where fk_job_card_code='$code' ORDER BY created_date DESC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	########Show  Enquiry Visit Remark
		public function ShowAllOilCharges($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT*,job_card_service_oil_charges.created_date AS cr_date FROM job_card_service_oil_charges LEFT JOIN master_oil_type ON master_oil_type.code = job_card_service_oil_charges.fk_oil_type_code
        WHERE fk_job_card_code='$code' ORDER BY job_card_service_oil_charges.created_date DESC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	########Show  Enquiry Visit Remark
		public function ShowAllServiceOutSideWork($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT* from job_card_service_out_side_work
		where fk_job_card_code='$code' ORDER BY created_date DESC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	########Show  Enquiry Visit Remark
		public function ShowAllSparCharges($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT*,job_card_service_spare_charge.created_date AS cr_date FROM job_card_service_spare_charge
        WHERE fk_job_card_code='$code' ORDER BY job_card_service_spare_charge.created_date DESC";					
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	

	public function toShowAllLeaveDetail($id)
	{
	 	$mysqli_m 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$queryy     =   "SELECT * from hr_leave  
        LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code 
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode   
		WHERE leave_id='$id' ";
							
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;  	
	}
		public  function resetJobcardSearch()
	{
		$_SESSION['enquiry_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['type'] ='';
		header("Location:jobcard_list.php");

	}
	#############Folder List
	 public function toShowAllJOBCARDSEARCHDETAIL()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $type                			=  @$_POST['type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['type']                 =$type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}

		    if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(job_card_details.created_date) >= '$from_date' AND DATE(job_card_details.created_date)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND job_card_type like '%$type%'";
			}
			
if($user_type=='DLR')
		{
		$queryy     =   "SELECT job_card_id,fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime

			FROM `job_card_details` 
			LEFT JOIN USER ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
		WHERE (fk_dealear_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND job_card_details.status=1 $cond order by job_card_details.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT job_card_id,fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime

			FROM `job_card_details` 
			LEFT JOIN USER ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
		WHERE (fk_manager_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND job_card_details.status=1 $cond order by job_card_details.created_date desc ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT job_card_id,fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime

			FROM `job_card_details` 
			LEFT JOIN USER ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
		WHERE job_card_details.status=1 $cond order by job_card_details.created_date desc  
	";
		}	
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
        if($cond!="")
		{			
	    $_SESSION['enquiry_info'] = $data;
		}
		
		header("Location:jobcard_list.php?search=1");
	}
	#############Folder List
	 public function toShowAllLeaveSearch()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $type                			=  @$_POST['type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['type']                 =$type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}

		    if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(hr_leave.created_date) >= '$from_date' AND DATE(hr_leave.created_date)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND approved_status like '%$type%'";
			}
			
if($user_type=='DLR')
		{
		$queryy     =   "SELECT fk_user_code,apply_for_leave,type_of_leave,from_date,to_date,no_of_day,day_type,approved_status,remarks,hr_leave.created_date,hr_leave.updated_date,leave_id from hr_leave  
		 LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND  hr_leave.status=1 $cond order by hr_leave.created_date DESC ";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT fk_user_code,apply_for_leave,type_of_leave,from_date,to_date,no_of_day,day_type,approved_status,remarks,hr_leave.created_date,hr_leave.updated_date,leave_id from hr_leave  
		 LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_leave.fk_user_code='$unique_code') AND  hr_leave.status=1 $cond order by hr_leave.created_date DESC";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT fk_user_code,apply_for_leave,type_of_leave,from_date,to_date,no_of_day,day_type,approved_status,remarks,hr_leave.created_date,hr_leave.updated_date,leave_id  from hr_leave  
		LEFT JOIN user ON user.unique_code =  hr_leave.fk_user_code  
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE hr_leave.status=1 $cond order by hr_leave.created_date DESC 
	";
		}	
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
        if($cond!="")
		{			
	    $_SESSION['enquiry_info'] = $data;
		}
		
		header("Location:leave_list.php?search=1");
	}
#######Reimbursement List
 public function toShowAllReimbursementList()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,hr_tada.created_date as cr_date_time from hr_tada  
        LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 		
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_dealear_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND hr_tada.status=1 order by hr_tada.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT *,hr_tada.created_date as cr_date_time  from hr_tada 
		LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
		LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  
		WHERE (fk_manager_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND  hr_tada.status=1 order by hr_tada.created_date DESC ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT *,hr_tada.created_date as cr_date_time from hr_tada  
        LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code 
        LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 
		LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode   
		WHERE  hr_tada.status=1 order by hr_tada.created_date DESC";
		}
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
		return @$data;
	}
	#############Get current month Pdf
	public function getCurrentMonthSalarySlip($user_code)
	{
		$mysqli_s 		= 	$this->CONN();
       
	     $queryy     =   "SELECT pdf_path from pay_slip
		WHERE fk_userunique_code='$user_code' AND MONTH(created_date)=MONTH(CURRENT_DATE()) AND status=1 LIMIT 1";
						
		$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$result		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$result;
	}
	############To Show TADA By Id
	public function toShowAllTADADetail($id)
	{
		    $mysqli_m 	= 	$this->CONN();

		    $queryy     =   " SELECT tada_approve_status,close_remark,image_code from hr_tada
			WHERE tada_id ='$id' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
			return @$row;  		
	}
	
	#############TADA Search
	 public function toShowAllTADASEARCHDETAIL()
	{
	 	$mysqli_s 		= 	$this->CONN();
      	// Query 6
	    $data		=	array();
		$unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
		$autosearch_manager_name_id				=  @$_POST['autosearch_manager_name_id'];	
	    $autosearch_dealer_name_id				=  @$_POST['autosearch_dealer_name_id'];	
	    $officer_id             				=  @$_POST['officer_id'];	
	    $from_date              				=  @$_POST['from_date'];	
	    $to_date                				=  @$_POST['to_date'];	
	    $type                			=  @$_POST['type'];	
		$_SESSION['autosearch_manager_name_id'] =$autosearch_manager_name_id;
		$_SESSION['autosearch_dealer_name_id']  =$autosearch_dealer_name_id;
		$_SESSION['officer_id']                 =$officer_id;
		$_SESSION['type']                 =$type;
		$_SESSION['from_date'] =$from_date;
		$_SESSION['to_date'] =$to_date;
	    if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(hr_tada.created_date) >= '$from_date' AND DATE(hr_tada.created_date)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND tada_approve_status like '%$type%'";
			}
			
if($user_type=='DLR')
		{
		$queryy     =   "SELECT *,hr_tada.created_date as cr_date_time
		FROM hr_tada
        LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 		
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  	
		WHERE (fk_dealear_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND hr_tada.status=1 $cond order by hr_tada.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT *,hr_tada.created_date as cr_date_time
		FROM hr_tada
        LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 		
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  		
		WHERE (fk_manager_code='$unique_code'  || hr_tada.fk_user_unique_code='$unique_code') AND hr_tada.status=1 $cond order by hr_tada.created_date desc ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT *,hr_tada.created_date as cr_date_time
		FROM hr_tada
        LEFT JOIN user ON user.unique_code =  hr_tada.fk_user_unique_code  
        LEFT JOIN master_claimtype ON master_claimtype.code =  hr_tada.claim_type 		
        LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode  	
		WHERE hr_tada.status=1 $cond order by hr_tada.created_date desc  
	";
		}	
       	$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
        if($cond!="")
		{			
	    $_SESSION['enquiry_info'] = $data;
		}
		
		header("Location:reimbursement_list.php?search=1");
	}
	public  function resetTADASearch()
	{
		$_SESSION['enquiry_info'] = '';
		$_SESSION['autosearch_manager_name_id'] ='';
		$_SESSION['autosearch_dealer_name_id']  ='';
		$_SESSION['officer_id']                 ='';
		$_SESSION['from_date'] ='';
		$_SESSION['to_date'] ='';
		$_SESSION['type'] ='';
		header("Location:reimbursement_list.php");

	}
	############Get User Create update delete acces
	public function getAccessDetailForAttendenceManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForTaskManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForTripManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForFolderManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForEnquiryManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForPayManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForJOBManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForLeaveManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	public function getAccessDetailForRemManagement($table,$module_code)
	{
		$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
		$unique_code		=	TORQUE_USERID;	
		$mysqli_m 	= 	$this->CONN();

		    $queryy     =   "SELECT * FROM `$table`
			WHERE fk_user_unique_code='$unique_code' AND fk_access_module_code='$module_code' LIMIT 1 ";						
			$query 		= 	$mysqli_m->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
			
	}
	//////close Enquiry
		public function closeEnquiryImageDetails($closeImageCode,$type)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		if($type=='closeenquiry')
		{
		$queryy     =   "SELECT* from image_detail
		where fk_image_unique_code='$closeImageCode' AND type='closeenquiry' ORDER BY created_date DESC";	
		}
        if($type=='tada')
		{
		$queryy     =   "SELECT* from image_detail
		where fk_image_unique_code='$closeImageCode' AND type='tada' ORDER BY created_date DESC";	
		}
        if($type=='closeTask')
		{
		$queryy     =   "SELECT* from image_detail
		where fk_image_unique_code='$closeImageCode' AND type='task' ORDER BY created_date DESC";	
		}	
        if($type=='attendence')
		{
		$queryy     =   "SELECT* from image_detail
		where fk_image_unique_code='$closeImageCode' AND type='attendence' ORDER BY created_date DESC";	
		}			
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	
	public function startTaskImageDetails($startImageCode)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		
		$queryy     =   "SELECT* from image_detail
		where fk_image_unique_code='$startImageCode' AND type='task' ORDER BY created_date DESC";	
	
		$query 		= 	$mysqli_s->prepare($queryy);
		$query->execute();
		$result 	= 	$query->get_result();
		foreach($result as $results)
		{
			$data[] = $results;
		}
	
		return @$data;
	}
	##############New Added in 2021-09-02
		public function ShowAllLabourCharges1($code)
		{
	    $mysqli_s 		= 	$this->CONN();
		$queryy     =   "SELECT SUM(cost) as cost from job_card_service_labour_cost
		where fk_job_card_code='$code'";	

						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;
	}
	
	########Show  Enquiry Visit Remark
		public function ShowAllOilCharges1($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT SUM(amount_per_liter) as amount_per_liter FROM job_card_service_oil_charges 
		WHERE fk_job_card_code='$code'";					
		
						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;

	}
	########Show  Enquiry Visit Remark
		public function ShowAllServiceOutSideWork1($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT SUM(total) as total from job_card_service_out_side_work
		where fk_job_card_code='$code'";					
		
						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;

	}
	
	########Show  Enquiry Visit Remark
		public function ShowAllSparCharges1($code)
		{
	    $mysqli_s 		= 	$this->CONN();
        $data		=	array();
		$queryy     =   "SELECT SUM(total_cost) as total_cost FROM job_card_service_spare_charge
        WHERE fk_job_card_code='$code' ORDER BY job_card_service_spare_charge.created_date DESC";					
		
						
			$query 		= 	$mysqli_s->prepare($queryy);
			$query->execute();
			$row		=	$query->get_result()->fetch_assoc();
			$query->close();
		
			return @$row;

	}
	
}

?>