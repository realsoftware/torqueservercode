<?php
include('class/Constants.php');	

//$constant 		= 	new Constants();
class Add
{	

	// Common Alert Message
	var $msz_warning_emptyfields	=  'All Fields must be filled out for a successful submission';
	var $msz_warning_emptyid		=  'Some Error is occoured, please go to <a href="index.php" style="text-decoration:underline;">Dashboard</a> and continue to proceed.';
	
	var $msz_warning_class			=  ' alert-warning';
	var $msz_success_class			=  ' alert-success';
	var $msz_error_class			=  ' alert-error';

	
	############################ 		Standard Functions		#########################
		
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	
	
	public function ReplaceContent($text)
	{
		/*$text 	=	@str_replace("�",'&trade;',$text);
		$text 	=	@str_replace("�",'&reg;',$text);
		$text 	=	@str_replace("�",'&copy',$text);*/
		
		$text 	=	@str_replace("'",'&#039;',$text);
		$text 	=	@str_replace("�",'&rsquo;',$text);
		$text 	=	@str_replace("�",'&lsquo;',$text);
		/*$text 	=	@str_replace('"','&ldquo;',$text);
		$text 	=	@str_replace("�",'&ldquo;',$text);
		$text 	=	@str_replace("�",'&rdquo;',$text);*/
		
		return @trim($text);
	}
	
	########################Add New User Type	
	public function AddUserType()
	{            
		        $mysqli_s 					= 	$this->CONN();
				$usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
				if($usertype=="sadmin")
				{
					$created_by="Super Admin";
				}
				if($usertype=="ADM")
				{
					$created_by="Sub Admin";
				}
		
				$module_code		=@$_POST['module_code'];

				$user_type		=	$this->ReplaceContent(@$_POST['user_type']);
				$category_name		=@$_POST['category_name'];
				$category_name1		=implode(",", $category_name);
				$commodity_type		=	$this->ReplaceContent(@$_POST['commodity_type']);
				$dealer_name	=	$this->ReplaceContent(@$_POST['autosearch_dealer_name_id']);
				
				$manager_name			=	$this->ReplaceContent(@$_POST['autosearch_manager_name_id']);
				$officer_name				=	$this->ReplaceContent(@$_POST['officer_name']);
				
				$f_name				=	$this->ReplaceContent(@$_POST['f_name']);
				$m_name				=	$this->ReplaceContent(@$_POST['m_name']);
				$l_name					=	$this->ReplaceContent(@$_POST['l_name']);
				$mobile_no				=	$this->ReplaceContent(@$_POST['mobile_no']);
				$alternate_no					=	$this->ReplaceContent(@$_POST['alternate_no']);
				$email_id					=	$this->ReplaceContent(@$_POST['email_id']);
				$address					=	$this->ReplaceContent(@$_POST['address']);
				$city					=	$this->ReplaceContent(@$_POST['city']);
				$district					=	$this->ReplaceContent(@$_POST['district']);
				$pin_code					=	$this->ReplaceContent(@$_POST['pin_code']);
				$state					=	$this->ReplaceContent(@$_POST['state']);
				$pan_no					=	$this->ReplaceContent(@$_POST['pan_no']);
				
				$adhar_no					=	$this->ReplaceContent(@$_POST['adhar_no']);
				
				
				$pl_leave					=	$this->ReplaceContent(@$_POST['pl_leave']);
				$sl_leave					=	$this->ReplaceContent(@$_POST['sl_leave']);
				$salary					     ="";
				$ta_da					=	$this->ReplaceContent(@$_POST['ta_da']);
				$shift					=	$this->ReplaceContent(@$_POST['shift']);
				$labour_cost					=	$this->ReplaceContent(@$_POST['labour_cost']);
				$other_city					=	$this->ReplaceContent(@$_POST['other_city']);
                $timestamp = date("Y-m-d H:i:s");	

				$password="";
                ######Unique Code Creation 
                if($user_type=="ADM")
				{
				$queryy2    	=   "INSERT INTO dummy_admin (NAME)VALUES('ADM')";
				$query2 	= 	$mysqli_s->prepare($queryy2);
				$query2->execute();
				$last_id 	= 	$mysqli_s->insert_id;
                 if($last_id<10)
				 {
					 $unique_code="ADM00000".$last_id;
				 }
				 else{
					 $unique_code="ADM0000".$last_id;
				 }
				 $password=$unique_code."_"."123";
				}
				if($user_type=="DLR")
				{
				$queryy2    	=   "INSERT INTO dummy_dealer (NAME)VALUES('DLR')";
				$query2 	= 	$mysqli_s->prepare($queryy2);
				$query2->execute();
				$last_id 	= 	$mysqli_s->insert_id;
                 if($last_id<10)
				 {
					 $unique_code="DLR00000".$last_id;
				 }
				 else{
					 $unique_code="DLR0000".$last_id;
					 
				 }
				 $password=$unique_code."_"."123";
				}
				
				if($user_type=="MGR")
				{
				$queryy2    	=   "INSERT INTO dummy_manager (NAME)VALUES('MGR')";
				$query2 	= 	$mysqli_s->prepare($queryy2);
				$query2->execute();
				$last_id 	= 	$mysqli_s->insert_id;
                 if($last_id<10)
				 {
					 $unique_code="MGR00000".$last_id;
				 }
				 else{
					 $unique_code="MGR0000".$last_id;
				 }
				 $password=$unique_code."_"."123";
				}
				if($user_type=="OFF")
				{
				$queryy2    	=   "INSERT INTO dummy_officer (NAME)VALUES('OFF')";
				$query2 	= 	$mysqli_s->prepare($queryy2);
				$query2->execute();
				$last_id 	= 	$mysqli_s->insert_id;	
				if(count($category_name)>1)
				{
					if($last_id<10)
				 {
					 $unique_code="SLSROFF000".$last_id;
				 }
				 else{
					 $unique_code="SLSROFF00".$last_id;
				 }
				}
					
				else
				{
				if($category_name[0]=="SAL")
				{
					if($last_id<10)
				 {
					 $unique_code="SALOFF000".$last_id;
				 }
				 else{
					 $unique_code="SALOFF00".$last_id;
				 }
				}
				if($category_name[0]=="SER")
				{
					if($last_id<10)
				 {
					 $unique_code="SEROFF000".$last_id;
				 }
				 else{
					 $unique_code="SEROFF00".$last_id;
				 }
				}
				}
				}
				
								#######Image Upload 
					$file_tmp 			= 	@$_FILES['pan_image']['tmp_name']; 
					$file_type			= 	@$_FILES['pan_image']['type']; 
					$file_name			= 	@$_FILES['pan_image']['name'];
					$extension_pancard = pathinfo($_FILES["pan_image"]["name"], PATHINFO_EXTENSION);
   
						    
						    $pan_card_path=Constants::$pan_file_path;
							$fileext	=	$file_name;
							$pan_card_image=Constants::$pancard_image_name;
							
							if($file_name!="")
							{
							$file_name 	= 	$unique_code.$pan_card_image.'.'.$extension_pancard;
							}
							else{
								$file_name="";
							}
							
							@move_uploaded_file(@$file_tmp,@$pan_card_path.@$file_name);
							/////////Adhar Image Uplaod
							$file_tmp1 			= 	@$_FILES['adhar_card_image']['tmp_name']; 
					        $file_type1			= 	@$_FILES['adhar_card_image']['type']; 
					        $file_name1			= 	@$_FILES['adhar_card_image']['name'];
					        $extension_adharcard = pathinfo($_FILES["adhar_card_image"]["name"], PATHINFO_EXTENSION);
						    $adhar_card_path=Constants::$adhar_file_path;
							$fileext1	=	$file_name1;
							
							
							$adhar_card_image=Constants::$adharcard_image_name;
							if($file_name1!="")
							{
							$file_name1 	= 	$unique_code.$adhar_card_image.'.'.$extension_adharcard;
							}
							else{
								$file_name1="";
							}
							
							@move_uploaded_file(@$file_tmp1,$adhar_card_path.@$file_name1);
							
							/////////User Pic Image Uplaod
							$file_tmp2 			= 	@$_FILES['user_pic']['tmp_name']; 
					        $file_type2			= 	@$_FILES['user_pic']['type']; 
					        $file_name2			= 	@$_FILES['user_pic']['name'];
                            $extension_userpic = pathinfo($_FILES["user_pic"]["name"], PATHINFO_EXTENSION);

						    $user_pic_path=Constants::$user_pic_path;
							$fileext2	=	$file_name2;
							$user_pic_image=Constants::$userpic_image_name;
							if($file_name2!="")
							{
							$file_name2 	= 	$unique_code.$user_pic_image.'.'.$extension_userpic;
							}
							else{
								$file_name2="";
							}
							@move_uploaded_file(@$file_tmp2,$user_pic_path.@$file_name2);

                ############END Code  				
				$queryy2="INSERT INTO user 
	(
	unique_code, 
	fk_type_of_usercode, 
	fk_cat_code, 
	password, 
	fk_typeofcommodity_code, 
	fk_dealear_code, 
	fk_manager_code, 
	fk_officer_code, 
	f_name, 
	m_name, 
	l_name, 
	mobile_no, 
	alternate_no, 
	email_id, 
	address, 
	fk_city_code, 
	fk_district_code, 
	pin_code, 
	fk_state_code, 
	pancard_image_path, 
	pancard_no, 
	adhar_card_image_path, 
	adhar_card_no, 
	user_profile_image_path, 
	salary, 
	ta_da, 
	user_createdate, 
	update_date,
	created_by,
	labour_cost,
    shift_time,
	other_city,
	pl_leave,
	sl_leave
											
	)
	VALUES
	( 
	'$unique_code', 
	'$user_type', 
	'$category_name1', 
	'$password', 
	'$commodity_type', 
	'$dealer_name', 
	'$manager_name', 
	'$officer_name', 
	'$f_name', 
	'$m_name', 
	'$l_name', 
	'$mobile_no', 
	'$alternate_no', 
	'$email_id', 
	'$address', 
	'$city', 
	'$district', 
	'$pin_code', 
	'$state', 
	'$file_name', 
	'$pan_no', 
	'$file_name1', 
	'$adhar_no', 
	'$file_name2', 
	'$salary', 
	'$ta_da', 
	'$timestamp', 
	'$timestamp',
	'$created_by',
	'$labour_cost',
	'$shift',
	'$other_city',
	'$pl_leave',
	'$sl_leave'
											
	);
 ";			/////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			########Insert Role assignment value
			$j=1;
			foreach($module_code as $module_codes)
			{
				$readwrite=@$_POST['readwrite_'.$j];
				foreach($readwrite as $readwrites)
				{
				if($readwrites==1)
				{
					@$create=1;
				}
				if($readwrites==2)
				{
					@$edit=2;
				}
				if($readwrites==3)
				{
					@$view=3;
				}
				if($readwrites==4)
				{
					@$delete=4;
				}
				if($readwrites==5)
				{
					@$download=5;
				}
				if($readwrites==6)
				{
					@$approve=6;
				}
				}
			$role_assign_query="INSERT INTO user_role_acces 
	(
	fk_user_unique_code, 
	fk_access_module_code, 
	fk_create, 
	fk_edit, 
	fk_view, 
	fk_delete, 
	fk_download, 
	fk_approve, 
	created_date, 
	updated_date
	)
	VALUES
	(
	'$unique_code', 
	'$module_codes', 
	'$create', 
	'$edit', 
	'$view', 
	'$delete', 
	'$download', 
	'$approve', 
	'$timestamp', 
	'$timestamp'
	);
";
			$query3 	= 	$mysqli_s->prepare($role_assign_query);
			$query3->execute();
			$j++;}
			@$_SESSION['TEMP']['msz'] 			= 	"User ($unique_code) type create successfully. ";
			header("Location:add-user.php");
		
	}
	#############################END Ranjeet Code
	function CurrentDateTime24Hour()
	{
		$currenttime	=	@date('Y-m-d H:i:s');
		
		return $currenttime;
	}
	########################Add New User Type	
	public function AddProductType()
	{   
		        $mysqli_s 					= 	$this->CONN();
				$usertype	    =	@$_SESSION['TORQUE_USERTYPE'];	
				$unique_code	=	@$_SESSION['TORQUE_USERID'];	
				if($usertype=="DLR")
				{
				$dealer_name	=$unique_code;
				}
				else
				{
				$dealer_name	=	$this->ReplaceContent(@$_POST['autosearch_dealer_folder_id']);
				}
				
				$product_code					=	$this->ReplaceContent(@$_POST['product_name']);
				$sub_product_name			    =	$this->ReplaceContent(@$_POST['sub_product_name']);
				$name					    =	$this->ReplaceContent(@$_POST['file_name']);
                $timestamp = date("Y-m-d H:i:s");	
                $datetime = date("YmdHis");	
				
				$queryy     =   "SELECT MAX(id) as id FROM `product_folder_detail` ";			
				$query 		= 	$mysqli_s->prepare($queryy);
				$query->execute();
		        $result 	= 	$query->get_result();
				$data	=	$result->fetch_assoc();
				//print_r($data);				

				$last_id=	@$data['id'];
                 if($last_id<10)
				 {
					 $num=$last_id+1;
					 $code=$product_code."SP000".$num;
				 }
				 else{
					 $num=$last_id+1;
				      $code=$product_code."SP00".$num;
					 
				 }
				
				#######Image Upload 
					$file_tmp 			= 	@$_FILES['pdf']['tmp_name']; 
					$file_type			= 	@$_FILES['pdf']['type']; 
					$file_name			= 	@$_FILES['pdf']['name'];
					$extension_pancard = pathinfo($_FILES["pdf"]["name"], PATHINFO_EXTENSION); 
						    $pan_card_path=Constants::$product_pdf_path;
							$fileext	=	$file_name;
							$pan_card_image=Constants::$product_pdf_name;
							
							if($file_name!="")
							{
							$file_name 	= 	$unique_code.$pan_card_image."_".$datetime.'.'.$extension_pancard;
							}
							else{
								$file_name="";
							}
							@move_uploaded_file(@$file_tmp,@$pan_card_path.@$file_name);	
                ############END Code  				
				 $queryy2="INSERT INTO product_folder_detail 
	(
	CODE, 
	fk_product_code, 
	sub_product_name, 
	fk_userunique_code, 
	created_date, 
	created_by, 
	file_name, 
	file_path
	)
	VALUES
	(
	'$code', 
	'$product_code', 
	'$sub_product_name', 
	'$dealer_name', 
	'$timestamp', 
	'$unique_code', 
	'$name', 
	'$file_name'								
	);
 ";	/////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Product has been added successfully. ";
			header("Location:folder_list.php");
		
	}
########################Add New Pay SLIP	
	public function AddPaySlip()
	{            
		        $mysqli_s 					= 	$this->CONN();				
				$user_unique_code			=	$this->ReplaceContent(@$_POST['autosearch_user_name_id']);
				$basic      				=	$this->ReplaceContent(@$_POST['basic']);
				
				$hra				=	$this->ReplaceContent(@$_POST['hra']);
				$flexi				=	$this->ReplaceContent(@$_POST['flexi']);
				$conveyence					=	$this->ReplaceContent(@$_POST['conveyence']);
				$lta				=	$this->ReplaceContent(@$_POST['lta']);
				$medical					=	$this->ReplaceContent(@$_POST['medical']);
				$pf					=	$this->ReplaceContent(@$_POST['pf']);
				$esic					=	$this->ReplaceContent(@$_POST['esic']);
				$gratuity					=	$this->ReplaceContent(@$_POST['gratuity']);
				$income_tax					=	$this->ReplaceContent(@$_POST['income_tax']);
				$loan					=	$this->ReplaceContent(@$_POST['loan']);
				$designation					=	$this->ReplaceContent(@$_POST['designation']);
				$bonus					=	$this->ReplaceContent(@$_POST['bonus']);
				$pf_no					=	$this->ReplaceContent(@$_POST['pf_no']);
				$uan_number					=	$this->ReplaceContent(@$_POST['uan_number']);
				$timestamp = date("Y-m-d H:i:s");	
				
			$query      		=   "SELECT * FROM `hr_salary_detail`  WHERE fk_userunique_id='$user_unique_code'";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num  				=   @mysqli_num_rows($sql);
			if($num==0)
			{
				
				$queryy2="INSERT INTO hr_salary_detail 
	( 
	fk_userunique_id, 
	hra, 
	flexi, 
	conveyence, 
	lta, 
	medical, 
	pf, 
	esic, 
	gratuaity, 
	income_tax, 
	loan, 
	created_date,
	basic,
	other_deduction,
    bonus,
    pf_no,
    uan_no
	)
	VALUES
	(
	'$user_unique_code', 
	'$hra', 
	'$flexi', 
	'$conveyence', 
	'$lta', 
	'$medical', 
	'$pf', 
	'$esic', 
	'$gratuity', 
	'$income_tax', 
	'$loan', 
	'$timestamp',
	'$basic',
	'$designation',
	'$bonus',
	'$pf_no',
	'$uan_number'
	)";			/////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
            $last_id 	= 	$mysqli_s->insert_id;
			###########Insert in history Tbale
			
				
				$queryy22="INSERT INTO hr_salary_detail_history 
	( 
	fk_userunique_id, 
	hra, 
	flexi, 
	conveyence, 
	lta, 
	medical, 
	pf, 
	esic, 
	gratuaity, 
	income_tax, 
	loan, 
	created_date,
	basic,
	other_deduction,
    bonus,
    pf_no,
    uan_no,
	fk_pay_slip_id
	)
	VALUES
	(
	'$user_unique_code', 
	'$hra', 
	'$flexi', 
	'$conveyence', 
	'$lta', 
	'$medical', 
	'$pf', 
	'$esic', 
	'$gratuity', 
	'$income_tax', 
	'$loan', 
	'$timestamp',
	'$basic',
	'$designation',
	'$bonus',
	'$pf_no',
	'$uan_number',
	'$last_id'
	)";			/////change superadmin id	
			$query22 	= 	$mysqli_s->prepare($queryy22);
			$query22->execute();

			#################End##################
			
			@$_SESSION['TEMP']['msz'] 			= 	"Pay Slip has been created successfully. ";
			header("Location:pay_slip.php");
			}
			else{
				@$_SESSION['TEMP']['msz'] 			= 	"Pay Slip has been already added for this user. ";
			header("Location:pay_slip.php");

			}
		
	}

}

?>