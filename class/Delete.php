<?php

class Delete
{
	
	// Common Alert Message
	var $msz_warning_emptyfields	=  'All Fields must be filled out for a successful submission';
	var $msz_warning_emptyid		=  'Some Error is occoured, please go to <a href="index.php" style="text-decoration:underline;">Dashboard</a> and continue to proceed.';
	
	var $msz_warning_class				=  ' alert-warning';
	var $msz_success_class				=  ' alert-success';
	var $msz_error_class				=  ' alert-error';

	
	############################ 		Standard Functions		#########################
	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	
	
	############################ 		Sample		#########################
	
	
	public function DeleteUser($id)
	{  

		
		
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `user` set status='0' WHERE `unique_code` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"User has been Deleted Successfully.";
		header("Location:users.php");
	}
	############Delete Attenendenec Data
	public function DeleteAttendence($id)
	{  

		
		
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `attendence_detail` set status='0' WHERE `attendence_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Attendence has been Deleted Successfully.";
		header("Location:attendence_list.php");
	}
	############Delete Task Data
	public function DeleteTask($id)
	{  

		
		
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `task_detail` set status='0' WHERE `task_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Task has been Deleted Successfully.";
		header("Location:task_list.php");
	}
	############Delete Trip Data
	public function DeleteTrip($id)
	{  

		
		
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `trip_detail` set status='0' WHERE `trip_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Trip has been Deleted Successfully.";
		header("Location:trip_list.php");
	}
	############Delete Trip Data
	public function DeleteProduct($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `product_folder_detail` set status='0' WHERE `id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Product has been Deleted Successfully.";
		header("Location:folder_list.php");
	}
	############Delete Trip Data
	public function DeleteEnquiry($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `enquiry_details` set status='0' WHERE `enquiry_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Enquiry has been Deleted Successfully.";
		header("Location:enquiry_list.php");
	}
		############Delete Trip Data
	public function DeleteJOBCARD($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `job_card_details` set status='0' WHERE `job_card_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Jobcard has been Deleted Successfully.";
		header("Location:jobcard_list.php");
	}
		############Delete Trip Data
	public function DeleteLeave($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `hr_leave` set status='0' WHERE `leave_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Leave data has been Deleted Successfully.";
		header("Location:leave_list.php");
	}
	############Delete PaySlip Data
	public function DeletePaySlip($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `hr_salary_detail` set status='0' WHERE `pay_slip_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"Pay slip has been Deleted Successfully.";
		header("Location:pay_slip.php");
	}
		############Delete TADA Data
	public function DeleteTADA($id)
	{  
		$mysqli_s 				= 	$this->CONN();
		
        $queryy1    =   " UPDATE  `hr_tada` set status='0' WHERE `tada_id` =  '$id'";
		$query1 	= 	$mysqli_s->prepare($queryy1);
		$query1->execute();
		@$_SESSION['TEMP']['msz']= 	"TADA data has been Deleted Successfully.";
		header("Location:reimbursement_list.php");
	}
}
?>