<?php
session_start();
class Update
{
	
	// Common Alert Message
	var $msz_warning_emptyfields		=  'All Fields must be filled out for a successful submission';
	var $msz_warning_someemptyfields	=  'Some Fields must be filled out for a successful submission';
	var $msz_warning_emptyid			=  'Some Error is occoured, please go to <a href="index.php" style="text-decoration:underline;">Dashboard</a> and continue to proceed.';
	
	var $msz_warning_class				=  ' alert-warning';
	var $msz_success_class				=  ' alert-success';
	var $msz_error_class				=  ' alert-error';
	
	
	############################ 		Standard Functions		#########################

	
	public function CONN()
	{
        global $global_mysqli;
		return @$global_mysqli;
	}
	public function ReplaceContent($text)
	{
		/*$text 	=	@str_replace("�",'&trade;',$text);
		$text 	=	@str_replace("�",'&reg;',$text);
		$text 	=	@str_replace("�",'&copy',$text);*/
		
		$text 	=	@str_replace("'",'&#039;',$text);
		$text 	=	@str_replace("�",'&rsquo;',$text);
		$text 	=	@str_replace("�",'&lsquo;',$text);
		//$text 	=	@str_replace('"','&ldquo;',$text);
		/*$text 	=	@str_replace("�",'&ldquo;',$text);
		$text 	=	@str_replace("�",'&rdquo;',$text);*/
		
		return @trim($text);
	}
	
	public function UpdateTripDetails($id)
	{
		$timestamp = date("Y-m-d H:i:s");	

		$mysqli_s 				= 	$this->CONN();

		$unique_code		=	TORQUE_USERID;		

		$a_distance		 =@$_POST['a_distance'];
		$a_amount		 =@$_POST['a_amount'];
		$remarks	     =@$_POST['remarks'];
		$trip_update_query="update trip_detail SET approved_amount='$a_amount', approved_km='$a_distance',approved_by_remark='$remarks',approved_by='$unique_code',updated_date='$timestamp' where trip_id='$id'";
		    $query3 	= 	$mysqli_s->prepare($trip_update_query);
			$query3->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Trip detail updated successfully. ";
			header("Location:trip_list.php");


	}
	public function UpdateTaskDetails($id)
	{
		$timestamp = date("Y-m-d H:i:s");	

		$mysqli_s 				= 	$this->CONN();

		$unique_code		=	TORQUE_USERID;		

		$plan_date		 =@$_POST['plan_date'];
		$task_type		 =@$_POST['task_type'];
		$start_remarks	     =@$_POST['remarks'];
		$closed_remarks	     =@$_POST['closed_remarks'];
		$updated_remarks	     =@$_POST['updated_remarks'];
		$trip_update_query="update task_detail SET planned_date='$plan_date', task_status='$task_type',remarks='$start_remarks',close_remark='$closed_remarks',approved_by_remark='$updated_remarks',approved_by='$unique_code',updated_date='$timestamp' where task_id='$id'";
		   $query3 	= 	$mysqli_s->prepare($trip_update_query);
			$query3->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Task detail updated successfully. ";
			header("Location:task_list.php");


	}
	public function UpdateUserDetails($unique_code)
	{ 
	           $usertype	=	@$_SESSION['TORQUE_USERTYPE'];	
				if($usertype=="sadmin")
				{
					$updated_by="Super Admin";
				}
				if($usertype=="ADM")
				{
					$updated_by="Sub Admin";
				}
				$mysqli_s 				= 	$this->CONN();
				$module_code		=@$_POST['module_code'];
				//print_r($module_code);die;

				$category_name		=@$_POST['category_name'];
				$category_name1		=implode(",", $category_name);
				$commodity_type		=	$this->ReplaceContent(@$_POST['commodity_type']);
				$dealer_name	=	$this->ReplaceContent(@$_POST['autosearch_dealer_name_id']);
				
				$manager_name			=	$this->ReplaceContent(@$_POST['autosearch_manager_name_id']);
				$officer_name				=	$this->ReplaceContent(@$_POST['officer_name']);
				
				$f_name				=	$this->ReplaceContent(@$_POST['f_name']);
				$m_name				=	$this->ReplaceContent(@$_POST['m_name']);
				$l_name					=	$this->ReplaceContent(@$_POST['l_name']);
				$mobile_no				=	$this->ReplaceContent(@$_POST['mobile_no']);
				$alternate_no					=	$this->ReplaceContent(@$_POST['alternate_no']);
				$email_id					=	$this->ReplaceContent(@$_POST['email_id']);
				$address					=	$this->ReplaceContent(@$_POST['address']);
				$city					=	$this->ReplaceContent(@$_POST['city']);
				$district					=	$this->ReplaceContent(@$_POST['district']);
				$pin_code					=	$this->ReplaceContent(@$_POST['pin_code']);
				$state					=	$this->ReplaceContent(@$_POST['state']);
				$pan_no					=	$this->ReplaceContent(@$_POST['pan_no']);
				
				$adhar_no					=	$this->ReplaceContent(@$_POST['adhar_no']);
				
				
				$salary					="";
				$sl_leave					=	$this->ReplaceContent(@$_POST['sl_leave']);
				$pl_leave					=	$this->ReplaceContent(@$_POST['pl_leave']);
				$ta_da					=	$this->ReplaceContent(@$_POST['ta_da']);
				$shift					=	$this->ReplaceContent(@$_POST['shift']);
				$labour_cost			=	$this->ReplaceContent(@$_POST['labour_cost']);
				if($city!="")
				{
					$other_city="";
				}
				else{
					
				$other_city					=	$this->ReplaceContent(@$_POST['other_city']);
                }
                $timestamp = date("Y-m-d H:i:s");	

				#######Image Upload 
					$file_tmp 			= 	@$_FILES['pan_image']['tmp_name']; 
					$file_type			= 	@$_FILES['pan_image']['type']; 
					$file_name			= 	@$_FILES['pan_image']['name'];
					$extension_pancard = pathinfo($_FILES["pan_image"]["name"], PATHINFO_EXTENSION);
   
						    
						    $pan_card_path=Constants::$pan_file_path;
							$fileext	=	$file_name;
							$pan_card_image=Constants::$pancard_image_name;
							
							if($file_name!="")
							{
							$file_name 	= 	$unique_code.$pan_card_image.'.'.$extension_pancard;
							}
							else{
								$file_name="";
							}
							
							@move_uploaded_file(@$file_tmp,@$pan_card_path.@$file_name);
							/////////Adhar Image Uplaod
							$file_tmp1 			= 	@$_FILES['adhar_card_image']['tmp_name']; 
					        $file_type1			= 	@$_FILES['adhar_card_image']['type']; 
					        $file_name1			= 	@$_FILES['adhar_card_image']['name'];
					        $extension_adharcard = pathinfo($_FILES["adhar_card_image"]["name"], PATHINFO_EXTENSION);
						    $adhar_card_path=Constants::$adhar_file_path;
							$fileext1	=	$file_name1;
							
							
							$adhar_card_image=Constants::$adharcard_image_name;
							if($file_name1!="")
							{
							$file_name1 	= 	$unique_code.$adhar_card_image.'.'.$extension_adharcard;
							}
							else{
								$file_name1="";
							}
							
							@move_uploaded_file(@$file_tmp1,$adhar_card_path.@$file_name1);
							
							/////////User Pic Image Uplaod
							$file_tmp2 			= 	@$_FILES['user_pic']['tmp_name']; 
					        $file_type2			= 	@$_FILES['user_pic']['type']; 
					        $file_name2			= 	@$_FILES['user_pic']['name'];
                            $extension_userpic = pathinfo($_FILES["user_pic"]["name"], PATHINFO_EXTENSION);

						    $user_pic_path=Constants::$user_pic_path;
							$fileext2	=	$file_name2;
							$user_pic_image=Constants::$userpic_image_name;
							if($file_name2!="")
							{
							$file_name2 	= 	$unique_code.$user_pic_image.'.'.$extension_userpic;
							}
							else{
								$file_name2="";
							}
							@move_uploaded_file(@$file_tmp2,$user_pic_path.@$file_name2);

                ############END Code  				
				$queryy2="UPDATE user  SET 	 fk_cat_code                         ='$category_name1',
				                             fk_typeofcommodity_code             ='$commodity_type',
											 fk_dealear_code                     ='$dealer_name',
											 fk_manager_code                     ='$manager_name',
											 fk_officer_code                     ='$officer_name',
											 f_name                              ='$f_name',
											 m_name                              ='$m_name',
											 l_name                              ='$l_name',
											 mobile_no                           ='$mobile_no',
											 alternate_no                        ='$alternate_no',
											 email_id                            ='$email_id',
											 address                             ='$address',
											 fk_city_code                        ='$city',
											 fk_district_code                    ='$district',
											 pin_code                            ='$pin_code',
											 fk_state_code                       ='$state',
											 pancard_image_path                  ='$file_name',
											 pancard_no                          ='$pan_no',
											 adhar_card_image_path               ='$file_name1',
											 adhar_card_no                       ='$adhar_no',
											 user_profile_image_path             ='$file_name2',
											 salary                              ='$salary',
											 ta_da                               ='$ta_da',
											 labour_cost                         ='$labour_cost',
											 shift_time                          ='$shift',
											 other_city                          = '$other_city',
											 updated_by                          = '$updated_by',
											 update_date                         ='$timestamp', 
											 pl_leave                            ='$pl_leave', 
											 sl_leave                            ='$sl_leave'
											  WHERE unique_code='$unique_code'";			
 
            /////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			#########Delete Module Acces befor addcslashes
			$queryy2="DELETE from user_role_acces where fk_user_unique_code='$unique_code'";
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			
			########Insert Role assignment value
			$j=1;
			foreach($module_code as $module_codes)
			{
				$readwrite=@$_POST['readwrite_'.$j];
				foreach($readwrite as $readwrites)
				{
				if($readwrites==1)
				{
					@$create=1;
				}
				if($readwrites==2)
				{
					@$edit=2;
				}
				if($readwrites==3)
				{
					@$view=3;
				}
				if($readwrites==4)
				{
					@$delete=4;
				}
				if($readwrites==5)
				{
					@$download=5;
				}
				if($readwrites==6)
				{
					@$approve=6;
				}
				}
			$role_assign_query="INSERT INTO user_role_acces 
	(
	fk_user_unique_code, 
	fk_access_module_code, 
	fk_create, 
	fk_edit, 
	fk_view, 
	fk_delete, 
	fk_download, 
	fk_approve, 
	created_date, 
	updated_date
	)
	VALUES
	(
	'$unique_code', 
	'$module_codes', 
	'$create', 
	'$edit', 
	'$view', 
	'$delete', 
	'$download', 
	'$approve', 
	'$timestamp', 
	'$timestamp'
	);
";
			$query3 	= 	$mysqli_s->prepare($role_assign_query);
			$query3->execute();
			$j++;}
			@$_SESSION['TEMP']['msz'] 			= 	"User ($unique_code) updated successfully. ";
			header("Location:users.php");
	}	
	##########Change Password
	public function changePassword()
	{
	 $mysqli_s 			    	= 	$this->CONN();	
	 $userid		            =	TORQUE_USERID;
	 $old_password		     	=	$this->ReplaceContent(@$_POST['old_password']);
	 $new_password			    =	$this->ReplaceContent(@$_POST['new_password']);
	 $confirm_password			=	$this->ReplaceContent(@$_POST['confirm_password']);
	 $queryy2                   ="select password from user where unique_code = '$userid' and password='$old_password'";
	 $query 		            = 	$mysqli_s->prepare($queryy2);
	 $query->execute();
	 $result 	                = 	$query->get_result();
	 $num		=	$result->num_rows;
	if(@$num<=0)
	{
		$_SESSION['TEMP']['msz_class'] 		= 	$this->msz_success_class;
		$_SESSION['TEMP']['msz'] 				= 	"Old password does not exist.";
		header("Location:change_password.php");
	}
	else
	{
		$queryy2="UPDATE user  SET 	  password='$new_password' WHERE unique_code='$userid'";			
		$query2 	= 	$mysqli_s->prepare($queryy2);
		$query2->execute();
        $_SESSION['TEMP']['msz_class'] 		= 	$this->msz_success_class;
		$_SESSION['TEMP']['msz'] 				= 	"Password has been changed successfully..";
		header("Location:change_password.php");	
	}
    }
	########################Update  Product Folder Management	
	public function UpdateProductDetails()
	{  
		        $mysqli_s 					= 	$this->CONN();
				$usertype	    =	@$_SESSION['TORQUE_USERTYPE'];	
				$unique_code	=	@$_SESSION['TORQUE_USERID'];	
				if($usertype=="DLR")
				{
				$dealer_name	=$unique_code;
				}
				else
				{
				$dealer_name	=	$this->ReplaceContent(@$_POST['autosearch_dealer_folder_id']);
				}
				
				$product_code					=	$this->ReplaceContent(@$_POST['product_name']);
				$sub_product_name			    =	$this->ReplaceContent(@$_POST['sub_product_name']);
				$name					    =	$this->ReplaceContent(@$_POST['file_name']);
				$folder_id					    =	$this->ReplaceContent(@$_POST['folder_id']);
                $timestamp = date("Y-m-d H:i:s");	
                $datetime = date("YmdHis");	
				
				$queryy     =   "SELECT file_path FROM `product_folder_detail`  WHERE id='$folder_id'";			
				$query 		= 	$mysqli_s->prepare($queryy);
				$query->execute();
		        $result 	= 	$query->get_result();
				$data	=	$result->fetch_assoc();
				//print_r($data);				

				$file_path=	@$data['file_path'];
                
				#######Image Upload 
					$file_tmp 			= 	@$_FILES['pdf']['tmp_name']; 
					$file_type			= 	@$_FILES['pdf']['type']; 
					$file_name			= 	@$_FILES['pdf']['name'];
					$extension_pancard = pathinfo($_FILES["pdf"]["name"], PATHINFO_EXTENSION); 
						    $pan_card_path=Constants::$product_pdf_path;
							$fileext	=	$file_name;
							$pan_card_image=Constants::$product_pdf_name;
							
							if($file_name!="")
							{
							$file_name 	= 	$file_path;//$unique_code.$pan_card_image."_".$datetime.'.'.$extension_pancard;
							}
							else{
								$file_name="";
							}
							@move_uploaded_file(@$file_tmp,@$pan_card_path.@$file_name);	
                ############END Code  				
				 $queryy2="UPDATE product_folder_detail SET
	
	fk_product_code='$product_code', 
	sub_product_name='$sub_product_name', 
	fk_userunique_code='$dealer_name',  
	updated_date='$timestamp', 
	created_by='$unique_code',  
	file_name='$name',  
	file_path='$file_name' WHERE id='$folder_id'";
 /////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Product has been updated successfully. ";
			header("Location:folder_list.php");
		
	}
	public function closeEnquiry()
	{

		$mysqli_s 				           	= $this->CONN();
        $enquiry_code                       =	$this->ReplaceContent(@$_POST['enquiry_id']);
        $enuiry_status                      =	$this->ReplaceContent(@$_POST['enquiry_status']);
        $competition_tractor_bought_model   =	$this->ReplaceContent(@$_POST['comp_tractor_bought']);
        $code_implementation_baought        =	$this->ReplaceContent(@$_POST['implementation_bought']);
        $code_reason                        =	$this->ReplaceContent(@$_POST['reason_for_lost']);
        $close_remark                       =	$this->ReplaceContent(@$_POST['remark']);
       
	    $timestamp                          = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `enquiry_details`   SET
														
														`enquiry_status`                  =  '$enuiry_status' ,
														`updated_date` 		              =  '$timestamp' ,
														`fk_code_reason_lost` 	          =  '$code_reason' ,
														`fk_implemented_intrested`         =  '$code_implementation_baought' ,
														`competition_tractor_baught_madel` =  '$competition_tractor_bought_model' ,
														
														`close_enuiry_remark` 	          =  '$close_remark'    WHERE enquiry_id='$enquiry_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Enquiry has been updated successfully. ";
			header("Location:enquiry_list.php");
		
	}
	
	public function UpdatePaySlipDetails($id)
	{

		$mysqli_s 					= 	$this->CONN();				
				$user_unique_code			=	$this->ReplaceContent(@$_POST['autosearch_user_name_id']);
				$basic      				=	$this->ReplaceContent(@$_POST['basic']);
				
				$hra				=	$this->ReplaceContent(@$_POST['hra']);
				$flexi				=	$this->ReplaceContent(@$_POST['flexi']);
				$conveyence					=	$this->ReplaceContent(@$_POST['conveyence']);
				$lta				=	$this->ReplaceContent(@$_POST['lta']);
				$medical					=	$this->ReplaceContent(@$_POST['medical']);
				$pf					=	$this->ReplaceContent(@$_POST['pf']);
				$esic					=	$this->ReplaceContent(@$_POST['esic']);
				$gratuity					=	$this->ReplaceContent(@$_POST['gratuity']);
				$income_tax					=	$this->ReplaceContent(@$_POST['income_tax']);
				$loan					=	$this->ReplaceContent(@$_POST['loan']);
			    $designation					=	$this->ReplaceContent(@$_POST['designation']);
				$bonus					=	$this->ReplaceContent(@$_POST['bonus']);
				$pf_no					=	$this->ReplaceContent(@$_POST['pf_no']);
				$uan_number					=	$this->ReplaceContent(@$_POST['uan_number']);
				$pay_slip_id					=	$this->ReplaceContent(@$_POST['hr_id']);
				$timestamp = date("Y-m-d H:i:s");	
				
			
				
				$queryy2="UPDATE  hr_salary_detail  SET
	fk_userunique_id='$user_unique_code',
	hra='$hra' ,
	flexi='$flexi', 
	conveyence='$conveyence', 
	lta='$lta',
	medical='$medical',
	pf='$pf',
	esic='$esic', 
	gratuaity='$gratuity', 
	income_tax='$income_tax', 
	loan='$loan', 
	updated_date='$timestamp',
	other_deduction='$designation',
	bonus='$bonus',
	pf_no='$pf_no',
	uan_no='$uan_number',
	basic='$basic' where fk_userunique_id='$id'";
				/////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			##########For History Table
			$query      		=   "SELECT * FROM `hr_salary_detail_history`  WHERE fk_userunique_id='$user_unique_code' AND fk_pay_slip_id='$pay_slip_id' AND MONTH(created_date) = MONTH(CURRENT_DATE())";
			$sql       	 		=   $mysqli_s->query($query); 
		    $num  				=   @mysqli_num_rows($sql);
			if($num==0)
			{
			
				$queryy22="INSERT INTO hr_salary_detail_history 
	( 
	fk_userunique_id, 
	hra, 
	flexi, 
	conveyence, 
	lta, 
	medical, 
	pf, 
	esic, 
	gratuaity, 
	income_tax, 
	loan, 
	created_date,
	basic,
	other_deduction,
    bonus,
    pf_no,
    uan_no,
	fk_pay_slip_id
	)
	VALUES
	(
	'$id', 
	'$hra', 
	'$flexi', 
	'$conveyence', 
	'$lta', 
	'$medical', 
	'$pf', 
	'$esic', 
	'$gratuity', 
	'$income_tax', 
	'$loan', 
	'$timestamp',
	'$basic',
	'$designation',
	'$bonus',
	'$pf_no',
	'$uan_number',
	'$pay_slip_id'
	)";			/////change superadmin id	
			$query22 	= 	$mysqli_s->prepare($queryy22);
			$query22->execute();
					
			}
			else{
				$queryy2="UPDATE  hr_salary_detail_history  SET
	fk_userunique_id='$user_unique_code',
	hra='$hra' ,
	flexi='$flexi', 
	conveyence='$conveyence', 
	lta='$lta',
	medical='$medical',
	pf='$pf',
	esic='$esic', 
	gratuaity='$gratuity', 
	income_tax='$income_tax', 
	loan='$loan', 
	updated_date='$timestamp',
	other_deduction='$designation',
	bonus='$bonus',
	pf_no='$pf_no',
	uan_no='$uan_number',
	basic='$basic' where fk_userunique_id='$id' AND fk_pay_slip_id='$pay_slip_id' AND MONTH(created_date) = MONTH(CURRENT_DATE())";
				/////change superadmin id	
			$query2 	= 	$mysqli_s->prepare($queryy2);
			$query2->execute();
			}
			#############End Code
			@$_SESSION['TEMP']['msz'] 			= 	"Pay Slip has been updated successfully. ";
			header("Location:pay_slip.php");
			
	}
	public function closeLeave()
	{

		$mysqli_s 				           	= $this->CONN();
        $enquiry_code                       =	$this->ReplaceContent(@$_POST['enquiry_id']);
        $enuiry_status                      =	$this->ReplaceContent(@$_POST['enquiry_status']);
        $close_remark                       =	$this->ReplaceContent(@$_POST['remark']);
       
	    $timestamp                          = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `hr_leave`   SET
														
														`approved_status`                  =  '$enuiry_status' ,
														`updated_date` 		              =  '$timestamp' ,														
														`leave_approved_remark` 	          =  '$close_remark'   
														WHERE leave_id='$enquiry_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Leave has been updated successfully. ";
			header("Location:leave_list.php");
		
	}

     public function closeJobCard()
	{

		$mysqli_s 				           	= $this->CONN();
        $enquiry_code                       =	$this->ReplaceContent(@$_POST['enquiry_id']);
        $enuiry_status                      =	$this->ReplaceContent(@$_POST['enquiry_status']);
        $close_remark                       =	$this->ReplaceContent(@$_POST['remark']);
       
	    $timestamp                          = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `job_card_details`   SET
														
														`job_card_type`                  =  '$enuiry_status' ,
														`job_card_close_datetime`  =  '$timestamp' ,														
														`close_remark` 	          =  '$close_remark'   
														WHERE job_card_id='$enquiry_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"Jobcard has been updated successfully. ";
			header("Location:jobcard_list.php");
		
	}
 public function closeTADA()
	{

		$mysqli_s 				           	= $this->CONN();
        $enquiry_code                       =	$this->ReplaceContent(@$_POST['enquiry_id']);
        $enuiry_status                      =	$this->ReplaceContent(@$_POST['enquiry_status']);
        $close_remark                       =	$this->ReplaceContent(@$_POST['remark']);
       
	    $timestamp                          = date("Y-m-d H:i:s");	
		#######Insert query of enquiry table
	
	    $queryy2    =   "UPDATE  `hr_tada`   SET
														
														`tada_approve_status`                  =  '$enuiry_status' ,
														`updated_date`  =  '$timestamp' ,														
														`close_remark` 	          =  '$close_remark'   
														WHERE tada_id='$enquiry_code'";	
			$query2 	= 	$mysqli_s->prepare($queryy2);
	     	$query2->execute();
			@$_SESSION['TEMP']['msz'] 			= 	"TADA has been updated successfully. ";
			header("Location:reimbursement_list.php");
		
	}

}
?>