<?php

 class Constants
{
	public static $adhar_file_path	=  'upload/adhar_card/';
	public static $pan_file_path	=  'upload/pan_card/';
	public static $user_pic_path	=  'upload/user_pic/';
	public static $pancard_image_name	=  '_pancard';
	public static $adharcard_image_name	=  '_adharcard';
	public static $userpic_image_name	=  '_userpic';
	public static $product_pdf_name	=  '_product_pdf';
	public static $product_pdf_path	=  'api/upload/folder/';
    public static $base_url	=  'http://lms.realsoftware.co.in/';

}
?>
