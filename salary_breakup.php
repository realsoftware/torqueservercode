<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->toShowPaySlipDetails($id);

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="salary_breakup.php">HR Management</a></li>
            <li class="active">Salary Breakup</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updatepayslip&id=<?php echo $id;?>" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>Salary Breakup</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
							 <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>PIRTICULARS</th>
                                        <th>COMPENSATION COMPONENTS (PER MONTH)</th>
                                        <th>COMPENSATION COMPONENTS (PER YEAR)</th>
                                    </tr>
                                </thead>  
                                 <tr>
								 <td><strong>A</strong>
								 </td>
								  <td><strong>SALARY</strong>
								 </td>
								  <td>
								 </td>
								  <td>
								 </td>
								 </tr>	
                                 <tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Basic
								 </td>
								  <td><?php echo $info['basic'];?>
								 </td>
								  <td><?php echo 12*$info['basic'];?>
								 </td>
								 </tr>	
<tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>HRA
								 </td>
								  <td><?php echo $info['hra'];?>
								 </td>
								  <td><?php echo 12*$info['hra'];?>
								 </td>
								 </tr>
<tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Flexi
								 </td>
								  <td><?php echo $info['flexi'];?>
								 </td>
								  <td><?php echo 12*$info['flexi'];?>
								 </td>
								 </tr>
<tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Medical Allowance
								 </td>
								  <td><?php echo $info['medical'];?>
								 </td>
								  <td><?php echo 12*$info['medical'];?>
								 </td>
								 </tr>	
<tr>
								 <td>
								 </td>
								  <td><strong>Sub Total(A)</strong>
								 </td>
								  <td><?php echo $info['basic']+$info['hra']+$info['flexi']+$info['medical'];?>
								 </td>
								  <td><?php echo 12*($info['basic']+$info['hra']+$info['flexi']+$info['medical']);?>
								 </td>
								 </tr>	

<tr>
								 <td><strong>B</strong>
								 </td>
								  <td><strong>REIMBURSEMENTS</strong>
								 </td>
								  <td>
								 </td>
								  <td>
								 </td>
								 </tr>	
                                 <tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Conveyance
								 </td>
								  <td><?php echo $info['conveyence'];?>
								 </td>
								  <td><?php echo 12*$info['conveyence'];?>
								 </td>
								 </tr>	

<tr>
								 <td>
								 </td>
								  <td><strong>Sub Total(B)</strong>
								 </td>
								  <td><?php echo $info['conveyence'];?>
								 </td>
								  <td><?php echo 12*$info['conveyence'];?>
								 </td>
								 </tr>

<tr>
								 <td><strong>C</strong>
								 </td>
								  <td><strong>ANNUAL BENEFITS</strong>
								 </td>
								  <td>
								 </td>
								  <td>
								 </td>
								 </tr>	
                                 <tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>LTA
								 </td>
								  <td><?php echo $info['lta'];?>
								 </td>
								  <td><?php echo 12*$info['lta'];?>
								 </td>
								 </tr>	

<tr>
								 <td>
								 </td>
								  <td><strong>Sub Total(C)</strong>
								 </td>
								  <td><?php echo $info['lta'];?>
								 </td>
								  <td><?php echo 12*$info['lta'];?>
								 </td>
								 </tr>

<tr>
								 <td><strong>D</strong>
								 </td>
								  <td><strong>RETIRALS
</strong>
								 </td>
								  <td>
								 </td>
								  <td>
								 </td>
								 </tr>	
                                 <tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Gratuity

								 </td>
								  <td><?php echo $info['gratuaity'];?>
								 </td>
								  <td><?php echo 12*$info['gratuaity'];?>
								 </td>
								 </tr>	
<tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>Provident Fund

								 </td>
								  <td><?php echo $info['pf'];?>
								 </td>
								  <td><?php echo 12*$info['pf'];?>
								 </td>
								 </tr>
<tr>
								 <td><img src="assets/images/gola.png"/>
								 </td>
								  <td>ESIC

								 </td>
								  <td><?php echo $info['esic'];?>
								 </td>
								  <td><?php echo 12*$info['esic'];?>
								 </td>
								 </tr>	
<tr>
								 <td>
								 </td>
								  <td><strong>Sub Total(D)</strong>
								 </td>
								  <td><?php echo $info['gratuaity']+$info['pf']+$info['esic'];?>
								 </td>
								  <td><?php echo 12*($info['gratuaity']+$info['pf']+$info['esic']);?>
								 </td>
								 </tr>	
								<tr>
								 <td>
								 </td>
								 <?php $totalA= $info['basic']+$info['hra']+$info['flexi']+$info['medical'];
								$tatalD= $info['gratuaity']+$info['pf']+$info['esic'];
								$totalC=$info['lta'];
								$totalB=$info['conveyence'];
								$totalCtc=$totalA+$totalB+$totalC+$tatalD;
								$totalctc12=12*$totalCtc;
								 ?>
								  <td><strong>Total CTC(A+B+C+D)</strong>
								 </td>
								  <td><?php echo $totalCtc;?>
								 </td>
								  <td><?php echo $totalctc12;?>
								 </td>
								 </tr>
<tr>
								 <td>
								 E
								 </td>
								  <td><strong>Bonus</strong>
								 </td>
								  <td><?php echo $info['bonus'];?>
								 </td>
								  <td><?php echo 12*$info['bonus'];?>
								 </td>
								 </tr>									 
                            </table>
							
						
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
function PFValue(val)
{
	pf=val*12/100;
	$('#pf').val(pf);
}

 function orderETCandPointonly(e)
{
	clearFnameMsg2();
	clearFnameMsg12
	var code=e.charCode? e.charCode : e.keyCode
	if (code!=8)
	{  //if the key isn't the backspace key (which we should allow)
		if (code==13 || code==9 || code==46)
		{
			return true;
		}
		else
		{
			if (code<48||code>57) //if not a number
			return false; //disable key press
		}
	}
}
function formValidation()
{
	category_name=$('#category_name').val();
	f_name=$('#f_name').val();
	mobile_no=$('#mobile_no').val();
	email_id=$('#email_id').val();
	address=$('#address').val();
	state=$('#state').val();
	district=$('#district').val();
	pin_code=$('#pin_code').val();
	labour_cost=$('#labour_cost').val();
	user_type=$('#user_type').val();
	dealer_name=$('#autosearch_dealer_name').val();
	manager_name=$('#autosearch_manager_name').val();
	adhar_no=$('#adhar_no').val();
	pancard_no=$('#pan_no').val();
    adhar_length=adhar_no.length;
	letters = /^[0-9a-zA-Z]+$/;
	//other_city=$('#other_city').val();
	//alert(city);
	if(category_name=="")
	{
		$('#category_name').focus();
		document.getElementById("msg_category").innerHTML = "Please select category name.";
		return false;
	}
	else if(user_type=="OFF" && manager_name=="" && dealer_name=="")
	{
	
	// dealer_name=$('#autosearch_dealer_name').val();
	// if(manager_name=="")
	// {
		$('#autosearch_manager_name').focus();
		document.getElementById("msg_manager_name").innerHTML = "Please enter manager name.";
		// return false;
	// }
	// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(user_type=="MGR" && dealer_name=="")
	{
	    // dealer_name=$('#autosearch_dealer_name').val();
		// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(f_name=="")
	{
		$('#f_name').focus();
		document.getElementById("msg_f_name").innerHTML = "Please enter first name.";
		return false;
	}
	else if(mobile_no=="")
	{
		$('#mobile_no').focus();
		document.getElementById("msg_mobile_no").innerHTML = "Please enter mobile no.";
		return false;
	}
	// else if(email_id=="")
	// {
		// $('#email_id').focus();
		// document.getElementById("msg_email_id").innerHTML = "Please enter email ID.";
		// return false;
	// }
	else if(address=="")
	{
		$('#address').focus();
		document.getElementById("msg_address").innerHTML = "Please select address.";
		return false;
	}
	else if(state=="")
	{
		$('#state').focus();
		document.getElementById("msg_state").innerHTML = "Please enter state name.";
		return false;
	}
	else if(district=="")
	{
		$('#district').focus();
		document.getElementById("msg_district").innerHTML = "Please select district name.";
		return false;
	}
	// else if(other_city=="")
	// {
		// $('#other_city').focus();
		// document.getElementById("msg_other_city").innerHTML = "Please enter other city.";
		// return false;
	// }
	else if(pin_code=="")
	{
		$('#pin_code').focus();
		document.getElementById("msg_pin_code").innerHTML = "Please select PIN Code.";
		return false;
	}
	else if(adhar_no!="" && adhar_length<16)
	{
	    $('#adhar_no').focus();
		document.getElementById("msg_adhar_no").innerHTML = "Please enter 16 digit adhar no.";
		return false;
	}
	else if(pancard_no!="" && !pancard_no.match(letters))
	{
	    $('#pan_no').focus();
		document.getElementById("msg_pan_no").innerHTML = "Please enter only alphanumeric value.";
		return false;
	}
	else if(labour_cost=="")
	{
		$('#labour_cost').focus();
		document.getElementById("msg_labour_cost").innerHTML = "Please enter labour cost.";
		return false;
	}
	else
	{
	return true;
	}
}
function clearFnameMsg()
{
	document.getElementById("msg_f_name").innerHTML = "";
}
function clearFnameMsg1()
{
	document.getElementById("msg_category").innerHTML = "";
}
function clearFnameMsg2()
{
	document.getElementById("msg_mobile_no").innerHTML = "";
}
function clearFnameMsg3()
{
	document.getElementById("msg_email_id").innerHTML = "";
}
function clearFnameMsg4()
{
	document.getElementById("msg_address").innerHTML = "";
}
function clearFnameMsg5()
{
	document.getElementById("msg_state").innerHTML = "";
}
function clearFnameMsg6()
{
	document.getElementById("msg_district").innerHTML = "";
}
function clearFnameMsg7()
{
	document.getElementById("msg_pin_code").innerHTML = "";
}
function clearFnameMsg8()
{
	document.getElementById("msg_labour_cost").innerHTML = "";
}
function clearFnameMsg9()
{
	document.getElementById("msg_manager_name").innerHTML = "";
}
function clearFnameMsg10()
{
	document.getElementById("msg_dealer_name").innerHTML = "";
}

function clearFnameMsg11()
{
	document.getElementById("msg_other_city").innerHTML = "";
}
function clearFnameMsg12()
{
	document.getElementById("msg_adhar_no").innerHTML = "";
}
function clearFnameMsg13()
{
	document.getElementById("msg_pan_no").innerHTML = "";
}

</script>