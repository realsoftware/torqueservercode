<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['attendence_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Attendence Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>ATTENDENCE MANAGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						   <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForAttendenceManagement('user_role_acces','MOD001');
							if($accessInfo['fk_download']==5)
							{
							?>
						    <a href="query.php?type=attendenceserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-attendence.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="attendence_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedAttendence();">Submit</button>
                           <?php
                            }
							}
							else{
                            ?>
                             <a href="query.php?type=attendenceserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-attendence.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="attendence_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedAttendence();">Submit</button>

                            <?php
							}
								?>
						   </div>
                       
                        </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>Created By</th>
                                       <!-- <th>User Type</th>-->
                                        <th>Attedence Date</th>
                                        <th>In Time</th>
                                        <th>Out Time</th>
                                        <th>Total Working (hours)</th>
                                        <th>In Location</th>
                                        <th>Out Location</th>
                                        <!--<th>Dealer</th>
                                        <th>Manager</th>-->
                                        <th>Type</th>
                                        <!--<th>Created Date</th>-->
									    <th align="center">Approve <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkHeader"/></th>
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								  if(@$type_search==1)
								 {
									 $taskInfo=$info;
								 }
								 else
								 {
								 $taskInfo=$display->toShowAllAttendencList();
								 }
								 //print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
									$fk_dealear_code=$taskInfos['fk_dealear_code']; 
									$fk_manager_code=$taskInfos['fk_manager_code']; 
									
										$managerInfo=$display->getMnagerName($fk_manager_code);
										$manager_name=$managerInfo['maganer_name'];
								
										$dealerInfo=$display->getDealerName($fk_dealear_code);
										$dealer_name=$dealerInfo['dealer_name'];
								
                                  ?>
                                  <tr>
								 <!-- <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name'];?></td>-->
								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_user_unique_code'].")";?></td>
								  <!--<td><?php echo $taskInfos['user_type'];?></td>-->
								  <td><?php 
								    if($taskInfos['attendence_date']!="")
									 {
								  ?>
								  <span style="display:none;"><?php echo date("Ymd", strtotime($taskInfos['attendence_date']));?></span>
								  <?php
								  echo date("d-m-Y", strtotime($taskInfos['attendence_date']));
									 }
									 else{
										 ?>
										 <span style="display:none;">--</span>

										 <?php
										 echo "--";
									 }
								  ?></td>
																 
								  <td><?php echo $taskInfos['in_time'];?></td>
								 
								  <td><?php echo $taskInfos['out_time'];?></td>
								  
								  
								  <td><?php echo gmdate("H:i:s", $taskInfos['duration_hours']*3600);?></td>
								  <td><?php echo $taskInfos['attendence_in_address'];?></td>
								  <td><?php echo $taskInfos['attendence_out_address'];?></td>
								
								 <!-- <td><?php echo $dealer_name;?></td>
								  <td><?php echo $manager_name;?></td>-->
								  <td><?php echo ucfirst($taskInfos['type']);?></td>
								 
							    <!--  <td><?php 
								    if($taskInfos['date_time']!="")
									 {
								  
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['date_time']));
									 }
									 else{
										 echo "--";
									 }
								  ?></td>-->
                                  <td align="center"><input type="checkbox" <?php if($taskInfos['aproved_status']==1){ echo"checked";} ?> name="chkChild" class="chkChild" value="<?php echo $taskInfos['attendence_id'];?>"/></td>

								  <td valign="center"><center>
									          <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_view']==3)
							{
							?>              
									            <a href="view_attendence.php?id=<?php echo $taskInfos['attendence_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
<?php }
							else
							{
								echo "-- ";
							}
									           ?> 
 <?php
 if($accessInfo['fk_delete']==4)
							{
							?>															   
									            <a href="query.php?type=attenendencedelete&id=<?php echo $taskInfos['attendence_id'];?>" onclick="return confirm('Are you sure want to delete this data?')" >
									                <i class="material-icons">delete</i>
									            </a>
							<?php }
                             else{
								 echo "--";
							 }

							} else {?>
							  <a href="view_attendence.php?id=<?php echo $taskInfos['attendence_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
							 <a href="query.php?type=attenendencedelete&id=<?php echo $taskInfos['attendence_id'];?>" onclick="return confirm('Are you sure want to delete this data?')" >
									                <i class="material-icons">delete</i>
									            </a>


	<?php } ?>							
												
												
												
												</center></td>
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
				       // "order": [[ 1, "desc" ]],
					         "aoColumns": [
            null,
           
            { "sType": "date-uk" },
			 null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
          
          
        ]

		   });
});
$('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });

function ApprovedAttendence()
{
	    var attendence_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                attendence_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_attendence&attendence_id='+attendence_id,
			success: function(data){
			//alert(data);
			alert("Task has been approved successfully.");
			location.reload();
		}});
}

</script>