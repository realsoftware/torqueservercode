<?php
session_start();
error_reporting(E_ALL);
$usertype	=	@$_SESSION['TORQUE_USERTYPE'];

include('class/MyClass.php');
include('class/Display.php');
include('class/Update.php');
include('class/Delete.php');

$global_obj 	= 	new MyClass();
$global_mysqli	=	$global_obj->MyClasss();		// Start DB Connection Object

$display 		= 	new Display();
$update 		= 	new Update();
$delete 		= 	new Delete();

$userid		=	TORQUE_USERID;
$trip_id=$_GET['trip_id'];
?>
<html>
<head>
<title>Torque Trip History</title>

<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 4px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #f2f2f2;
  color: #000;
}
</style>
</head>
<body >

                                  <table id="customers">
                                <thead>
                                    <tr>
                                        <th>Trip ID</th>
                                        <th>Lat</th>
										<th>Long</th>
                                        <th>Total Running Time</th>
										<th>Total Distance Covered (km)</th>
                                        <th>Address</th>
                                        <th>Start Date</th>
                                       <!-- <th>End Date</th>-->
								     </tr>
                                </thead>  
                                 <?php
								 $taskInfo=$display->toShowAllTripHistoryList($trip_id);
								// print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
										
									    $total_running_time= gmdate("H:i:s", $hours*3600);
								
 
                                  ?>
                                  <tr>
								  <td><?php echo $taskInfos['fk_trip_id'];?></td>
								  <td><?php echo $taskInfos['lat'];?></td>
								  <td><?php echo $taskInfos['long'];?></td>
								  <td><?php echo gmdate("H:i:s",$taskInfos['total_time_hours']*3600);?></td>
								  
								  <td><?php echo $taskInfos['total_km_between_latlong'];?></td>
								  <td><?php echo $taskInfos['trip_start_address'];?></td>
								  <td><?php echo date("d-m-Y H:i:s", strtotime($taskInfos['created_date']));?></td>
								<!--  <td><?php echo date("d-m-Y H:i:s", strtotime($taskInfos['updated_date']));?></td>-->
								 
								  </tr>
								 <?php } ?>								  
                            </table>  </body>
</html>

</body></html>







