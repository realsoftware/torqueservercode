<?php
session_start();
error_reporting(0);
$localhost="localhost";
$user='root';//"u729543671_lms";
$pwd='';//"Aarna!@3";
$database='ticketsystem';//"u729543671_lms";
$con = mysqli_connect($localhost, $user, $pwd) or die(mysqli_error());
mysqli_select_db($con,$database) or die(mysqli_error());

$user_id=$_GET['user_id'];

$query="SELECT*
			FROM `hr_salary_detail_history` 
			LEFT JOIN user ON user.unique_code =  hr_salary_detail_history.fk_userunique_id  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			WHERE fk_userunique_id ='$user_id' 
			AND MONTH(hr_salary_detail_history.created_date) = MONTH(CURRENT_DATE()) LIMIT 1 ";
$sql1=	mysqli_query($con,$query);
	  $info = mysqli_fetch_array($sql1);	
//$info=$display->toShowAllUserSalaryCreate($user_id);
//print_r($info);die;
// Create a function for converting the amount in words
function AmountInWords(float $amount)
{
   $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $x < $count_length ) {
      $get_divider = ($x == 2) ? 10 : 100;
      $amount = floor($num % $get_divider);
      $num = floor($num / $get_divider);
      $x += $get_divider == 10 ? 1 : 2;
      if ($amount) {
       $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
       $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
       $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
       '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
       '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
        }
   else $string[] = null;
   }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
}
##########Get Leave for user
$total_leave=0;
$query="SELECT no_of_day
			FROM `hr_leave` 
			WHERE fk_user_code ='$user_id' 
			AND MONTH(created_date) = MONTH(CURRENT_DATE()) ";
$result=	mysqli_query($con,$query);
while($row = mysqli_fetch_array($result))
			{
			  $total_leave=$total_leave+$row['no_of_day'];
			}
			$pl_leave=$info['pl_leave'];
			$sl_leave=$info['sl_leave'];
            $total_balance_leave=$pl_leave+$sl_leave;
		    $leave_taken=abs($total_balance_leave-$total_leave);
			//echo $total_leave;die;
			
?>
<!DOCTYPE html>
<html lang="">
    <head>
        <!-- meta tags -->
        <meta charset="utf-8" />
        <meta name="description" content="ECO-NIWAS REPORT CARD" />
        <meta name="keywords" content="ECO-NIWAS REPORT CARD" />
        <meta name="author" content="ECO-NIWAS REPORT CARD" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="google-site-verification" content="ECO-NIWAS REPORT CARD" />
        
        <title>: : Torque Solution Salary Slip</title>

        <!-- styles -->
        <style type="text/css">
        	body {
				font-family:Verdana, Arial, Helvetica, sans-serif;
				font-size: 11px;
				color:#202020;
				background: #fff;
			}
        	.drk-grn{color:#34afb4;}
        	.orng{color:#f39c12;}
        	.grn{color:#2eb000;}
        	.hot-and-dry{color:#c40405;}
        	.warm-and-humid{color:#e49706;}
        	.composite{color:#e5c36a;}
        	.temperate{color:#f5a5c0;}
        	.cold{color:#04a0c8;}

        	.hot-and-dry-b{border-color:#c40405 !important;}
        	.warm-and-humid-b{border-color:#e49706 !important;}
        	.composite-b{border-color:#e5c36a !important;}
        	.temperate-b{border-color:#f5a5c0 !important;}
        	.cold-b{border-color:#04a0c8 !important;}

        	table tr td.meter{
        		background:url('pti/meter-bg.png') !important;
        		background-repeat: no-repeat !important;20
        		background-size:100% !important;
			    height: 67px !important;
			    padding:0px !important;
			    width: 100% !important;
			    position: relative !important;
			    border: 1px solid #9b9b9b !important;
        	}

			.u-here{
				position: absolute;
				width:20px;
				top:-39px;
				right:-5px;
			}
			.u-here span{
				position: absolute;
				top:-20px !important;
				left:-165px;
				width:320px;
				text-align:center;
			}
			.basline{
				position: absolute;
				width:12px;
				top:-29px;
				right:-10px;
			}
			.basline span{
				margin-left:-20px;
			}
			.page_break { page-break-before: always; }
        </style>
		<style>
    @page { margin:110px 50px 60px;}
    #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 2px; background-color: #FFF; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 150px; background-color: lightblue; }
	#footer .page:after { content: counter(page); }
  </style>
  
    </head>

	<body style="padding:0px !important; margin:0px !important; float:left !important; width:100% !important;">
		<div id="header" style="background:#fff; margin:0px !important; padding:0px !important; float:left; width:100%; height:1px !important; margin-bottom:0px !important">
			<!--<p class="page">
				<img src="assets/images/ic_launcher.png" title="Torque Solutions" alt="Torque Solutions" width="100px" height="100px" />
				<a href=""></a>
			</p>
		</div>-->
		<!--<div id="footer" style="background:#fff; text-align:center; height:0px;border-top:0px solid #000"> 
           
			<table border=0 width="100%" style="background:url('pti/report_footer.png'); height:60px; width:695px;">
				<tr>
					<td style="text-align:center;"><p class="page">Page No. <a href="ibmphp.blogspot.com"></a></p></td>
				</tr>
			</table>
		</div>-->
		<table cellspacing="0" cellpadding="0" style="width:100%;margin-top:0px;border:0px solid #000 !important;color:#000;text-align:left;">
			<tr>
			<td><img src="assets/images/ic_launcher.png" title="Torque Solutions" alt="Torque Solutions" width="80px" height="80px" /></td>
				<td style="padding-top:0px;font-weight:700;font-size:20px;">
				Pay Slip For the month of <?php echo date('F, Y');?></td>
			</tr>
			</table>
<br/>
<table id='customers' cellpadding="0" cellspacing="0" style="padding:0px; margin:0px; width:100%; margin:0px auto; text-align: center; border:1px solid #000;">
<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="2">Employee Pay Slip Summary</td>
		    </tr>
		 <tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Employee Name</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo $info['f_name'].' '.$info['l_name']; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Designation</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo $info['user_type']; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Date Of Joining</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo date('d-m-Y',strtotime($info['user_createdate'])); ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Pay Date</td>
			<td style="background:#FFF; color:#000; padding:10px 5px; font-size:14px;text-align:left;border: 1px solid black;"><?php echo date("d-m-Y", strtotime(date('m', strtotime('+1 month')).'/01/'.date('Y').' 00:00:00'));
; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">PF A/C Number</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo $info['pf_no']; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">UAN Number</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo $info['uan_no']; ?></td>
		    </tr>
			</table>

<br/>
<table id='customers' cellpadding="0" cellspacing="0" style="padding:0px; margin:0px; width:100%; margin:0px auto; text-align: center; border:1px solid #000;">
 <thead>
    <tr>
<th style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Earnings</th>
<th style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Amount</th>
<th style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Deductions</th>
<th style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Amount</th>
</tr>
</thead>
  <tbody>

<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Basic</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['basic'],2); ?></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">PF</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['pf'],2); ?></td>


</tr>
		
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">HRA</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['hra'],2); ?></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">ESIC</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['esic'],2); ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">FLEXI</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['flexi'],2); ?></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Income Tax</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['income_tax'],2); ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Medical Allowance</td>
			<td style="background:#FFF; color:#000; padding:10px 5px; font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['medical'],2); ?></td>
			<td style="background:#FFF; color:#000; padding:10px 5px; font-size:14px;text-align:left;border: 1px solid black;">Other Deductions</td>
			<td style="background:#FFF; color:#000; padding:10px 5px; font-size:14px;text-align:left;border: 1px solid black;"><?php 
			$total_gross=$info['basic']+$info['hra']+$info['flexi']+$info['medical'];
			if($total_balance_leave<$total_leave)
			{
		    $leave= abs($total_balance_leave-$total_leave);
            
            $total_days_in_month=date('t');
	        $other_deduction=$total_gross/$total_days_in_month*$leave;////// change no of leaves		
			echo number_format(round($other_deduction),2);
            }
			else{
				echo $other_deduction=0;
			}
			?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong>Gross Earning</strong></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong><?php echo number_format($info['basic']+$info['hra']+$info['flexi']+$info['medical'],2) ;?></strong></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong>Total Deductions</strong></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong><?php echo number_format(round($info['pf']+$info['esic']+$info['income_tax']+$other_deduction),2) ; ?></strong></td>
		    </tr>
			<tr>
			<td colspan="4" style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong>REIMBURSEMENTS</strong></td>
            </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;">Bonus</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($info['bonus'],2); ?></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong>Total Reimbursements</strong>
</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong><?php echo number_format($info['bonus'],2); ?></strong></td>
		    </tr>

			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3"><strong>NETPAY</strong></td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><strong>AMOUNT</strong></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3">Gross Earning</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($total_gross,2) ;?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3">Total Deductions</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format(round($info['pf']+$info['esic']+$info['income_tax']+$other_deduction),2) ; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3">Total Reimbursements</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format(round($info['bonus']),2) ; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:right;border: 1px solid black;" colspan="3">Total Net Payabale</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo number_format($total_gross+$info['bonus']-($info['pf']+$info['esic']+$info['income_tax']+$other_deduction),2); ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3">Paid Days</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php echo date('t');?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="3">LOP Days</td>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;"><?php if($total_balance_leave<$total_leave){echo abs($total_balance_leave-$total_leave);}else{echo "";}; ?></td>
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="4"><strong>Total Net Payable Rs.</strong>
			<?php echo $get_amount= AmountInWords($total_gross+$info['bonus']-($info['pf']+$info['esic']+$info['income_tax']+$other_deduction));
 ?> Only</td>
			
		    </tr>
			<tr>
			<td style="background:#FFF; color:#000; padding:10px 5px;font-size:14px;text-align:left;border: 1px solid black;" colspan="4">**Total Net Payabale=Gross Earning-Total Deductions+Total Reimbursements</td>
			
		    </tr>
			</tbody>
			</table>
		