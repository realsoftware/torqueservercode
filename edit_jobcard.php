<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->toShowAllJobCardDetail($id);

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="jobcard_list.php">Jobcard Management</a></li>
            <li class="active">Edit Jobcard</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=closejobcard" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT JOBCARD</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">
                         <input type="hidden" name="enquiry_id" value="<?php echo $id;?>"/>
                        	<div class="row clearfix">
                                <div>
                                
								
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
								        <div class="font-12">Jobcard Status  <span style="color:red;">*</span></div>
                                            <select type="text" class="form-control show-tick"  name="enquiry_status" id="enquiry_status" required>
											<option value="open" <?php if($info['job_card_type']=='open'){ echo "selected";}?>>Open</option>
											<option value="close" <?php if($info['job_card_type']=='close'){ echo "selected";}?>>Close</option>
											</select>
                                        </div>
                                    </div>
									 

									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Close Remark <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="remark" id="remark" placeholder="Remark" value="<?php echo $info['close_remark'];?>" >
                                        </div>
                                    </div> 
									<!--<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Upload PDF <span style="color:red;">*</span></div></div>

                                            <input type="file" class="form-control" name="pdf" id="pdf">
                                            <label class="form-label"></label>
                                        </div>-->
										
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

<script>
$(document).ready(function() {
   val=$('#enquiry_status').val();
   	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}

});
function showDiv(val)
{
	//alert(val);
	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}
}
</script>
