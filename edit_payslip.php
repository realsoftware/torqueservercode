<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->toShowPaySlipDetails($id);

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="pay_slip.php">HR Management</a></li>
            <li class="active">Edit Pay Slip</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updatepayslip&id=<?php echo $id;?>" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT PAY SLIP</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                          
  <div class="col-sm-6">
    <fieldset>
    <legend>&nbsp;&nbsp;compensation Per Month</legend>

  <div class="form-group form-float col-sm-12" id="div_manager_name" >
                                        <div class="form-line">
									 <div class="font-12">User <span style="color:red;">*</span></div>                                        
										 <input type="text" required class="form-control" name="autosearch_user_name" onkeypress="clearFnameMsg9();" id="autosearch_user_name" placeholder="User" onkeyup="CheckAutoSuggestionUserPaySlip('user_name',this.value);" autocomplete="off" value="<?php echo $info['f_name'].' '.@$info['l_name']." (".$info['fk_userunique_id'].")"; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_user_name" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_user_name_id" id="autosearch_user_name_id"
                value="<?php echo @$info['fk_userunique_id']; ?>"  />
										
										<span style="color:red;" id="msg_manager_name"></span>
                                    </div>
									<input type="hidden" name="hr_id" value="<?php echo $info['pay_slip_id'];?>"/>
									
									 <div class="form-group form-float col-sm-12" id="div_officer_name" >
                                        <div class="form-line">
					                 <div class="font-12">BASIC <span style="color:red;"></span></div>

                                            <input type="text" required class="form-control" onchange="PFValue(this.value);" name="basic" value="<?php echo $info['basic'];?>" id="basic" placeholder="Basic" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
					                     <div class="font-12">HRA <span style="color:red;">*</span></div>

                                            <input type="text" required onkeypress="clearFnameMsg();" value="<?php echo $info['hra'];?>" class="form-control" name="hra" id="hra"  placeholder="HRA" >
                                        </div>
										<span style="color:red;" id="msg_f_name"></span>
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Flexi <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="flexi" value="<?php echo $info['flexi'];?>" id="flexi" placeholder="Flexi" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Conveyence <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="conveyence" value="<?php echo $info['conveyence'];?>" id="conveyence" placeholder="Conveyence"  >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">LTA <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="lta" value="<?php echo $info['lta'];?>" id="lta" placeholder="LTA"  >
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Medical <span style="color:red;"></span></div>
                                            <input type="text" class="form-control"  name="medical" value="<?php echo $info['medical'];?>" id="medical" placeholder="Medical" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">PF <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="pf" readonly id="pf" value="<?php echo $info['pf'];?>" placeholder="PF" />
                                        </div>
										<span style="color:red;" id="msg_email_id"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">ESIC <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="esic" placeholder="ESIC" value="<?php echo $info['esic'];?>" id="esic" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Gratuity <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="gratuity" placeholder="Gratuity" value="<?php echo $info['gratuaity'];?>"  id="gratuity" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Income TAX <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="income_tax" value="<?php echo $info['income_tax'];?>" placeholder="Income TAX"  id="income_tax" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">LOAN <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="loan" value="<?php echo $info['loan'];?>" placeholder="LOAN"  id="loan" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>

<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Other Deductions <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="designation" value="<?php echo $info['other_deduction'];?>" placeholder="Other Deductions"  id="designation" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Bonus <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="bonus" placeholder="BONUS" value="<?php echo $info['bonus'];?>" id="bonus" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">PF Number <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="pf_no" placeholder="PF NUMBER" value="<?php echo $info['pf_no'];?>" id="pf_no" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">UAN Number <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="uan_number" value="<?php echo $info['uan_no'];?>" placeholder="UAN NUMBER"  id="uan_number" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>

 <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>

                                   
                                   </fieldset>
 
                                </div>

  <div class="col-sm-6">

  <fieldset>
    <legend>&nbsp;&nbsp;compensation Per Year</legend>

  								
									
									 <div class="form-group form-float col-sm-12" id="div_officer_name" >
                                        <div class="form-line">
					                 <div class="font-12">BASIC <span style="color:red;"></span></div>

                                            <input type="text" disabled style="background:lightgray;" required class="form-control" name="" value="<?php echo $info['basic']*12;?>" id="basic" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
					                     <div class="font-12">HRA </div>

                                            <input type="text" disabled style="background:lightgray;"  value="<?php echo $info['hra']*12;?>" class="form-control"  >
                                        </div>
										<span style="color:red;" id="msg_f_name"></span>
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Flexi <span style="color:red;"></span></div>

                                            <input type="text" disabled style="background:lightgray;" class="form-control" value="<?php echo $info['flexi']*12;?>"  >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Conveyence <span style="color:red;"></span></div>

                                            <input type="text" disabled style="background:lightgray;" class="form-control" value="<?php echo $info['conveyence']*12;?>"  >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">LTA <span style="color:red;"></span></div>

                                            <input type="text" disabled style="background:lightgray;" class="form-control" value="<?php echo $info['lta']*12;?>"   >
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Medical <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;"  value="<?php echo $info['medical']*12;?>" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">PF <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['pf']*12;?>" />
                                        </div>
										<span style="color:red;" id="msg_email_id"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">ESIC <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['esic']*12;?>"  >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Gratuity <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['gratuaity']*12;?>" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Income TAX <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['income_tax']*12;?>" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">LOAN <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['loan']*12;?>">
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Other Deductions <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['other_deduction']*12;?>" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Bonus <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" disabled style="background:lightgray;" value="<?php echo $info['bonus']*12;?>" >
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>



                            </div>
                        </div>
						</fieldset>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
function PFValue(val)
{
	pf=val*12/100;
	$('#pf').val(pf);
}

 function orderETCandPointonly(e)
{
	clearFnameMsg2();
	clearFnameMsg12
	var code=e.charCode? e.charCode : e.keyCode
	if (code!=8)
	{  //if the key isn't the backspace key (which we should allow)
		if (code==13 || code==9 || code==46)
		{
			return true;
		}
		else
		{
			if (code<48||code>57) //if not a number
			return false; //disable key press
		}
	}
}
function formValidation()
{
	category_name=$('#category_name').val();
	f_name=$('#f_name').val();
	mobile_no=$('#mobile_no').val();
	email_id=$('#email_id').val();
	address=$('#address').val();
	state=$('#state').val();
	district=$('#district').val();
	pin_code=$('#pin_code').val();
	labour_cost=$('#labour_cost').val();
	user_type=$('#user_type').val();
	dealer_name=$('#autosearch_dealer_name').val();
	manager_name=$('#autosearch_manager_name').val();
	adhar_no=$('#adhar_no').val();
	pancard_no=$('#pan_no').val();
    adhar_length=adhar_no.length;
	letters = /^[0-9a-zA-Z]+$/;
	//other_city=$('#other_city').val();
	//alert(city);
	if(category_name=="")
	{
		$('#category_name').focus();
		document.getElementById("msg_category").innerHTML = "Please select category name.";
		return false;
	}
	else if(user_type=="OFF" && manager_name=="" && dealer_name=="")
	{
	
	// dealer_name=$('#autosearch_dealer_name').val();
	// if(manager_name=="")
	// {
		$('#autosearch_manager_name').focus();
		document.getElementById("msg_manager_name").innerHTML = "Please enter manager name.";
		// return false;
	// }
	// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(user_type=="MGR" && dealer_name=="")
	{
	    // dealer_name=$('#autosearch_dealer_name').val();
		// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(f_name=="")
	{
		$('#f_name').focus();
		document.getElementById("msg_f_name").innerHTML = "Please enter first name.";
		return false;
	}
	else if(mobile_no=="")
	{
		$('#mobile_no').focus();
		document.getElementById("msg_mobile_no").innerHTML = "Please enter mobile no.";
		return false;
	}
	// else if(email_id=="")
	// {
		// $('#email_id').focus();
		// document.getElementById("msg_email_id").innerHTML = "Please enter email ID.";
		// return false;
	// }
	else if(address=="")
	{
		$('#address').focus();
		document.getElementById("msg_address").innerHTML = "Please select address.";
		return false;
	}
	else if(state=="")
	{
		$('#state').focus();
		document.getElementById("msg_state").innerHTML = "Please enter state name.";
		return false;
	}
	else if(district=="")
	{
		$('#district').focus();
		document.getElementById("msg_district").innerHTML = "Please select district name.";
		return false;
	}
	// else if(other_city=="")
	// {
		// $('#other_city').focus();
		// document.getElementById("msg_other_city").innerHTML = "Please enter other city.";
		// return false;
	// }
	else if(pin_code=="")
	{
		$('#pin_code').focus();
		document.getElementById("msg_pin_code").innerHTML = "Please select PIN Code.";
		return false;
	}
	else if(adhar_no!="" && adhar_length<16)
	{
	    $('#adhar_no').focus();
		document.getElementById("msg_adhar_no").innerHTML = "Please enter 16 digit adhar no.";
		return false;
	}
	else if(pancard_no!="" && !pancard_no.match(letters))
	{
	    $('#pan_no').focus();
		document.getElementById("msg_pan_no").innerHTML = "Please enter only alphanumeric value.";
		return false;
	}
	else if(labour_cost=="")
	{
		$('#labour_cost').focus();
		document.getElementById("msg_labour_cost").innerHTML = "Please enter labour cost.";
		return false;
	}
	else
	{
	return true;
	}
}
function clearFnameMsg()
{
	document.getElementById("msg_f_name").innerHTML = "";
}
function clearFnameMsg1()
{
	document.getElementById("msg_category").innerHTML = "";
}
function clearFnameMsg2()
{
	document.getElementById("msg_mobile_no").innerHTML = "";
}
function clearFnameMsg3()
{
	document.getElementById("msg_email_id").innerHTML = "";
}
function clearFnameMsg4()
{
	document.getElementById("msg_address").innerHTML = "";
}
function clearFnameMsg5()
{
	document.getElementById("msg_state").innerHTML = "";
}
function clearFnameMsg6()
{
	document.getElementById("msg_district").innerHTML = "";
}
function clearFnameMsg7()
{
	document.getElementById("msg_pin_code").innerHTML = "";
}
function clearFnameMsg8()
{
	document.getElementById("msg_labour_cost").innerHTML = "";
}
function clearFnameMsg9()
{
	document.getElementById("msg_manager_name").innerHTML = "";
}
function clearFnameMsg10()
{
	document.getElementById("msg_dealer_name").innerHTML = "";
}

function clearFnameMsg11()
{
	document.getElementById("msg_other_city").innerHTML = "";
}
function clearFnameMsg12()
{
	document.getElementById("msg_adhar_no").innerHTML = "";
}
function clearFnameMsg13()
{
	document.getElementById("msg_pan_no").innerHTML = "";
}

</script>