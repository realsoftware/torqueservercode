<?php 
include('includes/header.php'); 
$unique_code=$_GET['id'];
$info=$display->userDetailByID($unique_code);

$infoPaySlip=$display->toShowPaySlipDetails($unique_code);
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="users.php">User Management</a></li>
            <li class="active">Edit User</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updateuser&id=<?php echo $unique_code;?>" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>Edit User Detail</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Type Of User <span style="color:red;">*</span></div>
										<?php 
										$userType=$display->ShowAllUserType();
										//print_r($userType);
										?>
                                            <select type="text" disabled class="form-control show-tick" name="user_type" id="user_type" required  onchange="showHideUserType(this.value);">
											<?php 
											foreach($userType as $userTypes)
											{
											    $fk_type_of_usercode=$info['fk_type_of_usercode'];
											    $user_code =$userTypes['code'];
                                               if(@$fk_type_of_usercode == @$user_code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }	
											?>
											<option value="<?php echo $userTypes['code'];?>" <?php echo $aa;?>><?php echo $userTypes['user_type'];?></option>
											<?php 
											} ?>
											</select>
                                        </div>
                                    </div>
                                     
                                  
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										   <div class="font-12">Category <span style="color:red;">*</span></div>
											<?php 
                                             $categoryType=$display->ShowAllCategory();
										//	print_r($info);
											 $fk_cat_code=trim($info['fk_cat_code']);
                                             $fk_cat_code_new=explode(',',$fk_cat_code);
										     ?>
                                            <select type="text" class="form-control show-tick" name="category_name[]" multiple="multiple" id="category_name" onchange="clearFnameMsg1();">
											<?php 
											foreach($categoryType as $categoryTypes)
											{
											    $cat_code =trim($categoryTypes['code']);
                                               if(in_array($cat_code, $fk_cat_code_new)){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }
											?>
											<option value="<?php echo $categoryTypes['code'];?>" <?php echo $aa;?>><?php echo $categoryTypes['category_name'];?></option>
											<?php 
											} ?>
											</select>

                                        </div>
									    <span style="color:red;" id="msg_category"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
											 <div class="font-12">Type Of Commodity(Vehicle) <span style="color:red;">*</span></div>
										<?php 
										$commodityType=$display->ShowAllTypeCommodity();
										?>
                                            <select type="text" class="form-control show-tick" name="commodity_type" id="commodity_type" required>
											<?php 
											foreach($commodityType as $commodityTypes)
											{
												$fk_typeofcommodity_code=trim($info['fk_typeofcommodity_code']);
											    $commodity_code =trim($commodityTypes['code']);
                                               if(@$fk_typeofcommodity_code == @$commodity_code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }
									
											?>
											<option value="<?php echo $commodityTypes['code'];?>" <?php echo $aa;?>><?php echo $commodityTypes['commodity_type'];?></option>
											<?php 
											} ?>
											</select>
                                        </div>
                                    </div>
									
										 <div class="form-group form-float col-sm-12" id="div_manager_name" style="display:none;">
                                        <div class="form-line">
										<?php
										$managerDealer_name=$display->getManagerDealerName($info['fk_manager_code']);
										?>
									 <div class="font-12">Manager Name <span style="color:red;">*</span></div>                                        
										 <input type="text" class="form-control" name="autosearch_manager_name" onkeypress="clearFnameMsg9();" id="autosearch_manager_name" placeholder="Manager" onkeyup="CheckAutoSuggestion('manager_name',this.value);" autocomplete="off" value="<?php echo @$managerDealer_name['name']; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_manager_name" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_manager_name_id" id="autosearch_manager_name_id"
                value="<?php echo @$info['fk_manager_code']; ?>"  />
				                     <span style="color:red;" id="msg_manager_name"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12" id="div_dealer_name"  style="display:none;">
                                        <div class="form-line">
										<?php
										$managerDealer_name=$display->getManagerDealerName($info['fk_dealear_code']);
										?>
				                        <div class="font-12">Dealer Name <span style="color:red;">*</span></div>

                                            <input type="text" class="form-control" name="autosearch_dealer_name" onkeypress="clearFnameMsg10();" id="autosearch_dealer_name" placeholder="Dealer" onkeyup="CheckAutoSuggestionDealer('dealer_name',this.value);" autocomplete="off" value="<?php echo @$managerDealer_name['name']; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_dealer_name" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_dealer_name_id" id="autosearch_dealer_name_id"
                value="<?php echo @$info['fk_dealear_code']; ?>"  />
				                    <span style="color:red;" id="msg_dealer_name"></span>
                                    </div>
									
									
									
									
									
									<!-- <div class="form-group form-float col-sm-12" id="div_manager_name" style="display:none;">
                                        <div class="form-line">
										<div class="font-12">Manager Name<span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="manager_name" id="manager_name" placeholder="Manager" value="<?php echo @$info['fk_manager_code'];?>" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12" id="div_dealer_name"  style="display:none;">
                                        <div class="form-line">
										<div class="font-12">Dealer Name <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="dealer_name" id="dealer_name" placeholder="Dealer" value="<?php echo @$info['fk_dealear_code'];?>">
                                        </div>
                                    </div>-->
									
									 <div class="form-group form-float col-sm-12" id="div_officer_name" style="display:none;">
                                        <div class="form-line">
										<div class="font-12">Officer Name <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="officer_name" id="officer_name" placeholder="Officer" value="<?php echo @$info['fk_officer_code'];?>">
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">First Name <span style="color:red;">*</span></div>
                                            <input type="text" class="form-control" name="f_name" id="f_name" onkeypress="clearFnameMsg();"  placeholder="First Name" value="<?php echo @$info['f_name'];?>">
                                        </div>
									<span style="color:red;" id="msg_f_name"></span>	
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Middle Name <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="m_name" id="m_name" placeholder="Middle Name" value="<?php echo @$info['m_name'];?>">
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Last Name<span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="l_name" id="l_name" placeholder="Last Name"  value="<?php echo @$info['l_name'];?>">
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Mobile No <span style="color:red;">*</span></div>
                                            <input type="text" class="form-control" maxlength="10" onkeypress="return orderETCandPointonly(event)" name="mobile_no" id="mobile_no" placeholder="Mobile No" value="<?php echo @$info['mobile_no'];?>"  >
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>

                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Alternate No <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" maxlength="10" onkeypress="return orderETCandPointonly(event)" value="<?php echo @$info['alternate_no'];?>" name="alternate_no" id="alternate_no" placeholder="Alternate No" >
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Email ID <span style="color:red;"></span></div>
                                            <input type="email" class="form-control" name="email_id" onkeypress="clearFnameMsg3();" id="email_id" placeholder="Email ID"  value="<?php echo @$info['email_id'];?>" >
                                        </div>
										<span style="color:red;" id="msg_email_id"></span>

                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Address <span style="color:red;">*</span></div>
                                            <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo @$info['address'];?>"  id="address" onkeypress="clearFnameMsg4();">
                                        </div>
										<span style="color:red;" id="msg_address"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
											<div class="font-12">State <span style="color:red;">*</span></div>
											<?php 
                                             $categoryType=$display->ShowAllState();
											// print_r($categoryType);
										     ?>
                                            <select type="text" class="form-control show-tick" name="state" id="state"  onchange="showDistrict(this.value);clearFnameMsg5();">
									        <option value="">-Select-One-</option>										

											<?php 
											foreach($categoryType as $categoryTypes)
											{
												 $fk_state_code=$info['fk_state_code'];
											    $state_code =$categoryTypes['state_code'];
                                               if(@$fk_state_code == @$state_code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }
											?>
											<option value="<?php echo $categoryTypes['state_code'];?>" <?php echo $aa;?>><?php echo $categoryTypes['state_name'];?></option>
											<?php 
											} ?>
											</select>
                                        </div>
										<span style="color:red;" id="msg_state"></span>

                                    </div>
									<div class="form-group form-float col-sm-12">
									<input type="hidden" value="<?php echo $info['fk_district_code'];?>" id="dis_fk_id"/>
									<input type="hidden" value="<?php echo $info['pin_code'];?>" id="fk_pincode"/>
                                        <div class="form-line">
											<div class="font-12">District <span style="color:red;">*</span></div>
											<div id="dist_val">
                                            <select type="text" class="form-control show-tick" name="district" id="district"  onchange="showCity(this.value);showPinCode(this.value);clearFnameMsg6();">
												
											</select>
											</div>
                                        </div>
										<span style="color:red;" id="msg_district"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12" id="taluka_div">
									<input type="hidden" value="<?php echo $info['fk_city_code'];?>" id="city_fk_id"/>

                                        <div class="form-line">
                                            
											
											<div class="font-12">Taluka/City <span style="color:red;">*</span></div>
											<div id="city_val">
                                            <select type="text" class="form-control show-tick" name="city" id="city"  >
												 <option value="">-Select-One-</option>										

											</select>
											</div>
                                        </div>
										<span style="color:red;" id="msg_city"></span>
                                    </div>
							<?php 
							$otcity=$info['other_city'];
							?>
                                     <div class="form-group form-float col-sm-12" id="ot_city" style="display:none;">
                                        <div class="form-line">
										<div class="font-12">Other City <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="other_city" id="other_city" value="<?php echo $info['other_city'];?>" placeholder="Other City" onchange="clearFnameMsg11();">
                                        </div>
										<span style="color:red;" id="msg_other_city"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                            
											<div class="font-12">Pin Code <span style="color:red;">*</span></div>
											<div id="pincode_val">
                                            <select type="text" class="form-control show-tick" name="pin_code" id="pin_code" onchange="clearFnameMsg7();" >
												 <option value="">-Select-One-</option>										

											</select>
											</div>
                                        </div>
										<span style="color:red;" id="msg_pin_code"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">PAN Card NO<span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="pan_no" id="pan_no" maxlength="10" onkeypress="clearFnameMsg13();" placeholder="PAN Card No" value="<?php echo @$info['pancard_no'];?>" >
                                        </div>
										<span style="color:red;" id="msg_pan_no"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
									   <div class="font-12">PAN Card Image</div>

                                            <input type="file" class="form-control" name="pan_image" id="pan_image"  >
                                            <label class="form-label"></label>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
										<img src="<?php
                                        if($info['pancard_image_path']!="")
										{
										echo "upload/pan_card/".$info['pancard_image_path'];
										}
										else
										{
											echo "assets/images/icon_user.png";
										}
										
										?>" width="56px;" height="56px;"/>
                                        
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">ADHAR No <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" maxlength="16" onkeypress="return orderETCandPointonly(event)" name="adhar_no" id="adhar_no"  placeholder="Adhar Card No"  value="<?php echo @$info['adhar_card_no'];?>">
                                        </div>
										<span style="color:red;" id="msg_adhar_no"></span>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							            <div class="font-12">ADHAR Card Image</div>

                                            <input type="file" class="form-control" name="adhar_card_image" id="adhar_card_image">
                                            <label class="form-label"></label>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
										<img src="<?php
                                        if($info['adhar_card_image_path']!="")
										{
										echo "upload/adhar_card/".$info['adhar_card_image_path'];
										}
										else
										{
											echo "assets/images/icon_user.png";
										}
										
										?>" width="56px;" height="56px;"/>
                                        
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Profile Image</div>

                                            <input type="file" class="form-control" name="user_pic" id="user_pic">
                                            <label class="form-label"></label>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
										<img src="<?php
                                        if($info['user_profile_image_path']!="")
										{
										echo "upload/user_pic/".$info['user_profile_image_path'];
										}
										else
										{
											echo "assets/images/icon_user.png";
										}
										
										?>" width="56px;" height="56px;"/>
                                        
                                    </div>
									<?php if(!empty($infoPaySlip))
									{
										?>
									<div class="form-group form-float col-sm-12">
                                       
                                           <a href="edit_payslip.php?id=<?php echo $unique_code;?>"> <input type="button" class="btn bg-blue waves-effect pull-left" value="Pay Slip"></a>

                                    </div>
									<?php
									} else {?>
									<div class="form-group form-float col-sm-12">
                                       
                                           <a href="add_payslip.php?id=<?php echo $unique_code;?>"> <input type="button" class="btn bg-blue waves-effect pull-left" value="Pay Slip"></a>

                                    </div>
									
									<?php } ?>
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Pl Leave <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="pl_leave" onkeypress="return orderETCandPointonly(event)" id="pl_leave" placeholder="PL Leave" value="<?php echo $info['pl_leave'];?>">
                                        </div>
                                    </div>
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">SL Leave <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="sl_leave" onkeypress="return orderETCandPointonly(event)" id="sl_leave" placeholder="SL Leave" value="<?php echo $info['sl_leave'];?>">
                                        </div>
                                    </div>
									
									
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">TA/DA <span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="ta_da" id="ta_da" placeholder="TA/DA Eligiblity" value="<?php echo $info['ta_da'];?>">
                                        </div>
                                    </div>

                                     <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Running Cost (per km)<span style="color:red;"> *</span></div>
                                            <input type="text" class="form-control" name="labour_cost" id="labour_cost" onkeypress="clearFnameMsg8();" placeholder="Running Cost" value="<?php echo $info['labour_cost'];?>">
                                        </div>
										<span style="color:red;" id="msg_labour_cost"></span>

                                    </div>
									
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Shift<span style="color:red;"> *</span></div>
                                        <select type="text" class="form-control show-tick" name="shift" id="shift" required >
										
												 <option value="Night" <?php if($info['shift_time']=="Night") { echo "selected";}?>>Night</option>										
												 <option value="Day" <?php if($info['shift_time']=="Day") { echo "selected";}?>>Day</option>										

										</select>                  
									</div>
                                    </div>
                                   
                                     <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="body table-responsive">    
                                        <table class='table table-hover table-striped'>
                                <thead>
                                    <tr>
                                        <th>Approval Access</th>
                                        <th>Create </th>
                                        <th>Edit</th>
                                        <th>View</th>
                                        <th>Delete</th>
                                        <th>Download</th>
                                        <th>Approve</th>
                                    </tr>
									</thead>
									<?php 
										$moduleInfo=$display->ShowAllModuleType();
										$accessreadwriteInfo=$display->ShowAllReadWriteAccess();
										$i=1;
										foreach($moduleInfo as $moduleInfos)
										{
                                        $module_code=$moduleInfos['code'];											
										$module_details=$display->moduleAccessDetailByID($unique_code,$module_code);
									    $id=$module_details['fk_access_module_code'];
									    $fk_create=$module_details['fk_create'];
									    $fk_edit=$module_details['fk_edit'];
									    $fk_view=$module_details['fk_view'];
									    $fk_delete=$module_details['fk_delete'];
									    $fk_download=$module_details['fk_download'];
									    $fk_approve=$module_details['fk_approve'];
                                        // print_r($module_details);
										if($id==$module_code)
										{
											$chk="checked";
										}
										else
										{
											$chk="";
										}
										//print_r($module_details);
										?>
									<tr>
                                    <td> <input type="checkbox" onchange="headerChange('<?php echo $i;?>');" <?php echo $chk;?> class="chkHeader_<?php echo $i;?>"  name="module_code[]" value="<?php echo $moduleInfos['code'];?>" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $moduleInfos['module_name'];?></td>
                                   <?php
								   $j=1;
								   foreach($accessreadwriteInfo as $accessreadwriteInfos)
								   {
									   ///////create
									   if($j==1)
									   {
										if($fk_create==1)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
									   ///////edit
									   if($j==2)
									   {
										if($fk_edit==2)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
                                      ///////view
									   if($j==3)
									   {
										if($fk_view==3)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
                                        ///////delete
									   if($j==4)
									   {
										if($fk_delete==4)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
									   ///////download
									   if($j==5)
									   {
										if($fk_download==5)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
									    ///////approve
									   if($j==6)
									   {
										if($fk_approve==6)
										{
											 $chk_access="checked";
										}
										else
										{
											$chk_access="";
										}
									   }
								   ?>
								   <td> 
								   <input type="checkbox" onchange="childRead('<?php echo $i;?>');"  name="readwrite_<?php echo $i;?>[]" <?php echo @$chk_access;?> class="chkChildread_<?php echo $i;?>" value="<?php echo $accessreadwriteInfos['id'];?>">
								   </td>
								   <?php
								   $j++;
								   }?>
                                    
									</tr>
										<?php $i++; }
										?>
									
                                </thead>

                                
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                    <!--
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="repassword" id="repassword" required>
                                            <label class="form-label">Re Password</label>
                                        </div>
                                    </div>-->

                                    <input type="hidden" name="role" id="role" value="100" />

                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" onclick="return formValidation();" name="btnAdd">SUBMIT</button>
                                    </div>

                                   
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

<script>
$( document ).ready(function() {
	
	state_id=$('#state').val();
	dis_fk_id=$('#dis_fk_id').val();
	city_fk_id=$('#city_fk_id').val();
	fk_pincode=$('#fk_pincode').val();
	//alert(fk_pincode);
	$.ajax({
			url: 'ajax.php?type=showdistEdit&id='+state_id+'&dist_id='+dis_fk_id, 
			success: function(data){
			$('#dist_val').html(data);
			//$('#pincode_val').html(data);
		}});
	

	$.ajax({
			url: 'ajax.php?type=showcityEdit&id='+dis_fk_id+'&city_id='+city_fk_id, 
			success: function(data){
			//$('#city_val').html(data);
			if(data!=1)
			{
            $("#taluka_div").show();				
			$('#city_val').html(data);
			$("#ot_city").hide();
			}
			else{
				$("#ot_city").show();
				$("#taluka_div").hide();
			}
		}});
	



	$.ajax({
			url: 'ajax.php?type=showpincodeEdit&id='+dis_fk_id+'&pin_code='+fk_pincode, 
			success: function(data){
			$('#pincode_val').html(data);
		}});
	
	
	
	
	vals=$('#user_type').val();
if(vals=="ADM")
{
	$('#div_officer_name').hide();
	$('#div_dealer_name').hide();
	$('#div_manager_name').hide();
}
if(vals=="MGR")
{

	$('#div_dealer_name').show();
	$('#div_officer_name').hide();
	$('#div_manager_name').hide();

	
}
if(vals=="DLR")
{
	$('#div_officer_name').hide();
	$('#div_dealer_name').hide();
	$('#div_manager_name').hide();
}
if(vals=="OFF")
{
	$('#div_dealer_name').show();
	$('#div_manager_name').show();
    $('#div_officer_name').hide();

}
});
function showHideUserType(val)
{
if(val=="ADM")
{
	$('#div_officer_name').hide();
	$('#div_dealer_name').hide();
	$('#div_manager_name').hide();
}
if(val=="MGR")
{

	$('#div_dealer_name').show();
	$('#div_officer_name').hide();
	$('#div_manager_name').hide();

	
}
if(val=="DLR")
{
	$('#div_officer_name').hide();
	$('#div_dealer_name').hide();
	$('#div_manager_name').hide();
}
if(val=="OFF")
{
	$('#div_dealer_name').show();
	$('#div_manager_name').show();
    $('#div_officer_name').hide();

}

}
function showDistrict(val)
{
	$.ajax({
			url: 'ajax.php?type=showdist&id='+val, 
			success: function(data){
			$('#dist_val').html(data);
			//$('#pincode_val').html(data);
		}});
	
}
function showCity(val)
{
	$.ajax({
			url: 'ajax.php?type=showcity&id='+val, 
			success: function(data){
			if(data!=1)	 
		    {
            $("#taluka_div").show();				
			$('#city_val').html(data);
			$("#ot_city").hide();
			}
			else{
				$("#ot_city").show();
				$("#taluka_div").hide();
			}
		//	$('#city_val').html(data);
		}});
	
}

function showPinCode(val)
{
	$.ajax({
			url: 'ajax.php?type=showpincode&id='+val, 
			success: function(data){
			$('#pincode_val').html(data);
		}});
	
}
</script>
<script type="text/javascript">

function headerChange(k)
{  
 if($('.chkChildread_'+k).prop("checked") == true){
$( '.chkChildread_'+k ).prop( "checked", false );
}
else{
$( '.chkChildread_'+k ).prop( "checked", true );
}
}
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
function childRead(k)
	   {
//$('.chkHeader_'+k).prop('checked',true);
lengths=$('.chkChildread_'+k).length;
chklength=$('.chkChildread_'+k+':checked').length;
//alert(chklength);
$('.chkHeader_'+k).prop('checked',true);
if(chklength==0){
$( '.chkHeader_'+k ).prop( "checked", false );
            }else{
$( '.chkHeader_'+k ).prop( "checked", true );
				   
}
}



/////module
 $('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });
		
///////create		
$('#chkHeaderCreate').on('change', function() {     
                $('.chkChildread_1').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChildread_1').change(function(){ //".checkbox" change 
		
            if($('.chkChildread_1:checked').length == $('.chkChildread_1').length){
                   $('#chkHeaderCreate').prop('checked',true);
            }else{
                   $('#chkHeaderCreate').prop('checked',false);
            }
        });
/////////Edit
$('#chkHeaderEdit').on('change', function() {     
                $('.chkChildread_2').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChildread_2').change(function(){ //".checkbox" change 
		
            if($('.chkChildread_2:checked').length == $('.chkChildread_2').length){
                   $('#chkHeaderEdit').prop('checked',true);
            }else{
                   $('#chkHeaderEdit').prop('checked',false);
            }
        });

//////////view
$('#chkHeaderView').on('change', function() {     
                $('.chkChildread_3').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChildread_3').change(function(){ //".checkbox" change 
		
            if($('.chkChildread_3:checked').length == $('.chkChildread_3').length){
                   $('#chkHeaderView').prop('checked',true);
            }else{
                   $('#chkHeaderView').prop('checked',false);
            }
        });


///////////delete
$('#chkHeaderDelete').on('change', function() {     
                $('.chkChildread_4').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChildread_4').change(function(){ //".checkbox" change 
		
            if($('.chkChildread_4:checked').length == $('.chkChildread_4').length){
                   $('#chkHeaderDelete').prop('checked',true);
            }else{
                   $('#chkHeaderDelete').prop('checked',false);
            }
        });

/////////Download
$('#chkHeaderDownload').on('change', function() {     
                $('.chkChildread_5').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChildread_5').change(function(){ //".checkbox" change 
		
            if($('.chkChildread_5:checked').length == $('.chkChildread_5').length){
                   $('#chkHeaderDownload').prop('checked',true);
            }else{
                   $('#chkHeaderDownload').prop('checked',false);
            }
        });
		
		function orderETCandPointonly(e)
{
	clearFnameMsg2();
	clearFnameMsg12
	var code=e.charCode? e.charCode : e.keyCode
	if (code!=8)
	{  //if the key isn't the backspace key (which we should allow)
		if (code==13 || code==9 || code==46)
		{
			return true;
		}
		else
		{
			if (code<48||code>57) //if not a number
			return false; //disable key press
		}
	}
}
function formValidation()
{
	category_name=$('#category_name').val();
	f_name=$('#f_name').val();
	mobile_no=$('#mobile_no').val();
	email_id=$('#email_id').val();
	address=$('#address').val();
	state=$('#state').val();
	district=$('#district').val();
	pin_code=$('#pin_code').val();
	labour_cost=$('#labour_cost').val();
	user_type=$('#user_type').val();
	dealer_name=$('#autosearch_dealer_name').val();
	manager_name=$('#autosearch_manager_name').val();
	adhar_no=$('#adhar_no').val();
	pancard_no=$('#pan_no').val();
    adhar_length=adhar_no.length;
	letters = /^[0-9a-zA-Z]+$/;
	//other_city=$('#other_city').val();
	//alert(city);
	if(category_name=="")
	{
		$('#category_name').focus();
		document.getElementById("msg_category").innerHTML = "Please select category name.";
		return false;
	}
	else if(user_type=="OFF" && manager_name=="" && dealer_name=="")
	{
	
	// dealer_name=$('#autosearch_dealer_name').val();
	// if(manager_name=="")
	// {
		$('#autosearch_manager_name').focus();
		document.getElementById("msg_manager_name").innerHTML = "Please enter manager name.";
		// return false;
	// }
	// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(user_type=="MGR" && dealer_name=="")
	{
	    // dealer_name=$('#autosearch_dealer_name').val();
		// if(dealer_name=="")
	// {
		$('#autosearch_dealer_name').focus();
		document.getElementById("msg_dealer_name").innerHTML = "Please enter dealer name.";
		return false;
	//}
	}
	else if(f_name=="")
	{
		$('#f_name').focus();
		document.getElementById("msg_f_name").innerHTML = "Please enter first name.";
		return false;
	}
	else if(mobile_no=="")
	{
		$('#mobile_no').focus();
		document.getElementById("msg_mobile_no").innerHTML = "Please enter mobile no.";
		return false;
	}
	// else if(email_id=="")
	// {
		// $('#email_id').focus();
		// document.getElementById("msg_email_id").innerHTML = "Please enter email ID.";
		// return false;
	// }
	else if(address=="")
	{
		$('#address').focus();
		document.getElementById("msg_address").innerHTML = "Please select address.";
		return false;
	}
	else if(state=="")
	{
		$('#state').focus();
		document.getElementById("msg_state").innerHTML = "Please enter state name.";
		return false;
	}
	else if(district=="")
	{
		$('#district').focus();
		document.getElementById("msg_district").innerHTML = "Please select district name.";
		return false;
	}
	// else if(other_city=="")
	// {
		// $('#other_city').focus();
		// document.getElementById("msg_other_city").innerHTML = "Please enter other city.";
		// return false;
	// }
	else if(pin_code=="")
	{
		$('#pin_code').focus();
		document.getElementById("msg_pin_code").innerHTML = "Please select PIN Code.";
		return false;
	}
	else if(adhar_no!="" && adhar_length<16)
	{
	    $('#adhar_no').focus();
		document.getElementById("msg_adhar_no").innerHTML = "Please enter 16 digit adhar no.";
		return false;
	}
	else if(pancard_no!="" && !pancard_no.match(letters))
	{
	    $('#pan_no').focus();
		document.getElementById("msg_pan_no").innerHTML = "Please enter only alphanumeric value.";
		return false;
	}
	else if(labour_cost=="")
	{
		$('#labour_cost').focus();
		document.getElementById("msg_labour_cost").innerHTML = "Please enter labour cost.";
		return false;
	}
	else
	{
	return true;
	}
}
function clearFnameMsg()
{
	document.getElementById("msg_f_name").innerHTML = "";
}
function clearFnameMsg1()
{
	document.getElementById("msg_category").innerHTML = "";
}
function clearFnameMsg2()
{
	document.getElementById("msg_mobile_no").innerHTML = "";
}
function clearFnameMsg3()
{
	document.getElementById("msg_email_id").innerHTML = "";
}
function clearFnameMsg4()
{
	document.getElementById("msg_address").innerHTML = "";
}
function clearFnameMsg5()
{
	document.getElementById("msg_state").innerHTML = "";
}
function clearFnameMsg6()
{
	document.getElementById("msg_district").innerHTML = "";
}
function clearFnameMsg7()
{
	document.getElementById("msg_pin_code").innerHTML = "";
}
function clearFnameMsg8()
{
	document.getElementById("msg_labour_cost").innerHTML = "";
}
function clearFnameMsg9()
{
	document.getElementById("msg_manager_name").innerHTML = "";
}
function clearFnameMsg10()
{
	document.getElementById("msg_dealer_name").innerHTML = "";
}

function clearFnameMsg11()
{
	document.getElementById("msg_other_city").innerHTML = "";
}
function clearFnameMsg12()
{
	document.getElementById("msg_adhar_no").innerHTML = "";
}
function clearFnameMsg13()
{
	document.getElementById("msg_pan_no").innerHTML = "";
}

</script>