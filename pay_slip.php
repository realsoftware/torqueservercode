<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['attendence_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">HR Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>PAY SLIP</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						  <!--  <a href="query.php?type=attendenceserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-attendence.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="attendence_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedAttendence();">Submit</button>-->
                             <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForPayManagement('user_role_acces','MOD018');
							if($accessInfo['fk_create']>0)
							{
							?>                            
							<a href="add_payslip.php"> <button type="button" class="btn bg-blue waves-effect" >Add</button></a>
							<?php
                            }
							}
							else{
                            ?>
							
							<a href="add_payslip.php"> <button type="button" class="btn bg-blue waves-effect" >Add</button></a>
                            <?php } ?>
							</div>
                       
                        </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Basic</th>
                                        <th>HRA</th>
                                        <th>FLEXI</th>
                                        <th>Conveyence</th>
                                        <th>LTA</th>
                                        <th>Medical</th>
                                        <th>PF</th>
                                        <th>ESIC</th>
                                        <th>Gratuaity</th>
                                        <th>Income Tax</th>
                                        <th>Loan</th>
                                        <th>Other Deductions</th>
                                        <th>Bonus</th>
                                        <th>PF No</th>
                                        <th>UAN No</th>
										<th>Salary Slip</th>   
                                        <th>Created Date</th>
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>  
                                 <?php
						          $taskInfo=$display->toShowAllPaySlipList();
								
								 //print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
								  $user_code=$taskInfos['fk_userunique_id'];
								  $salaryInfo=$display->getCurrentMonthSalarySlip($user_code);
								?>
                                  <tr>
								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_id'].")";?></td>
								  
																 
								  <td><?php echo $taskInfos['basic'];?></td>
								  <td><?php echo $taskInfos['hra'];?></td>
								 
								  <td><?php echo $taskInfos['flexi'];?></td>
								  
								  
								  <td><?php echo $taskInfos['conveyence'];?></td>
								  <td><?php echo $taskInfos['lta'];?></td>
								
								 <td><?php echo $taskInfos['medical'];?></td>
								 <td><?php echo $taskInfos['pf'];?></td>
								 <td><?php echo $taskInfos['esic'];?></td>
								 <td><?php echo $taskInfos['gratuaity'];?></td>
								 <td><?php echo $taskInfos['income_tax'];?></td>
								 <td><?php echo $taskInfos['loan'];?></td>
								 <td><?php echo $taskInfos['other_deduction'];?></td>
								 <td><?php echo $taskInfos['bonus'];?></td>
								 <td><?php echo $taskInfos['pf_no'];?></td>
								 <td><?php echo $taskInfos['uan_no'];?></td>
								  <td>
								  <?php
								  if($salaryInfo!="")
								  {
								  ?>
							
								  <a href="<?php echo $salaryInfo['pdf_path']; ?>" target="_blank">
								  <img  src="assets/images/pdf-16.png" title="Salary Slip" alt="Salary Slip"/></a>
								  <?php } else { echo "";}?>
								  </td>
							     <td><?php 
								    if($taskInfos['date_time']!="")
									 {
								  ?>
			 <span style="display:none;"><?php  echo date("Ymd H:i:s", strtotime($taskInfos['date_time'])); ?></span>

								  <?php
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['date_time']));
									 }
									 else{
										 ?>
				 <span style="display:none;">--</span>

										 <?php
										 echo "--";
									 }
								  ?></td>

								  <td valign="center"><center>
									       <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_view']==3)
							{
							?>
 <a href="salary_breakup.php?id=<?php echo $taskInfos['fk_userunique_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 							
									            
<?php }
							else
							{
								echo "-- ";
							}
									           ?> 
											   <?php
								  if($accessInfo['fk_edit']==2)
							{
							?>                       
									             <a href="edit_payslip.php?id=<?php echo $taskInfos['fk_userunique_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>  
<?php }
							else
							{
								echo "-- ";
							}
									           ?> 
 <?php
 if($accessInfo['fk_delete']==4)
							{
							?>													
									            <a href="query.php?type=payslipdelete&id=<?php echo $taskInfos['pay_slip_id'];?>" onclick="return confirm('Are you sure want to delete this data?')" >
									                <i class="material-icons">delete</i>
									            </a>
												<?php }
                             else{
								 echo "--";
							 }

							} else {?>
							 <a href="salary_breakup.php?id=<?php echo $taskInfos['fk_userunique_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
												
												 <a href="edit_payslip.php?id=<?php echo $taskInfos['fk_userunique_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>              
									            <a href="query.php?type=payslipdelete&id=<?php echo $taskInfos['pay_slip_id'];?>" onclick="return confirm('Are you sure want to delete this data?')" >
									                <i class="material-icons">delete</i>
									            </a>
													<?php } ?>							

												
												</center></td>
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
				     //   "order": [[ 9, "desc" ]]


		   });
});
$('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });

function ApprovedAttendence()
{
	    var attendence_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                attendence_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_attendence&attendence_id='+attendence_id,
			success: function(data){
			//alert(data);
			alert("Task has been approved successfully.");
		}});
}

</script>