<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['folder_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Folder Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>FOLDER MANAGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						   <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForFolderManagement('user_role_acces','MOD006');
							if($accessInfo['fk_create']>0)
							{
							?>
						    <a href="query.php?type=folderserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-folder.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="folder_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                           
						      <a href="add_new_product.php"><button type="button" class="btn bg-blue waves-effect">Add New Product</button></a>
                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTrip();">Submit</button>
                            <?php
                            }
							}
							else{
                            ?>
							 <a href="query.php?type=folderserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-folder.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="folder_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                           
						      <a href="add_new_product.php"><button type="button" class="btn bg-blue waves-effect">Add New Product</button></a>
                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTrip();">Submit</button>

							<?php } ?>
							
							</div>
                          </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>Created By</th>
										<?php if($usertype!="DLR")
									{ ?>
                                        <th>Dealer Name</th>
									<?php } ?>
										<th>Product Name</th>
                                        <th>Sub Product Name</th>
                                        <th>File Name</th>
                                        <th>File</th>   
                                        <th>Created Date</th>   
								 <th align="center">Approve <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								 <input type="checkbox" id="chkHeader"/></th>

                                        <th>Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								  if(@$type_search==1)
								 {
									 $taskInfo=$info;
								 }
								 else
								 {
								 $taskInfo=$display->toShowAllProductList();
								 }
								//print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
									$fk_dealear_code=$taskInfos['fk_dealear_code']; 
									$fk_manager_code=$taskInfos['fk_manager_code']; 
									
										$managerInfo=$display->getMnagerName($fk_manager_code);
										$manager_name=$managerInfo['maganer_name'];
								
										$dealerInfo=$display->getDealerName($fk_dealear_code);
										$dealer_name=$dealerInfo['dealer_name'];
										
									
								
 
                                  ?>
                                  <tr>
								  <td><?php echo $taskInfos['created_user'];?></td>
								 <?php if($usertype!="DLR")
									{ ?>

								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_code'].")";//echo $taskInfos['fk_userunique_code'];?></td>
									<?php }?>
								  <td><?php echo $taskInfos['product_name'];?></td>								
							
																 
								  <td><?php echo $taskInfos['sub_product_name'];?></td>
								  <td><?php echo $taskInfos['file_name'];?></td>
								  <td>
								  
								  <a href="api/upload/folder/<?php echo $taskInfos['file_path'];?>" target="_blank">
								  <img src="assets/images/pdf-16.png"/></a></td>
								<td>
								 <?php 
								  if($taskInfos['cr_date_time']!="")
								  {
									  ?>
		 <span style="display:none;"><?php  echo date("Ymd H:i:s", strtotime($taskInfos['cr_date_time'])); ?></span>
  
									  <?php
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['cr_date_time']));
								  }
								  else
								  {

?>
								   <span style="display:none;">--</span>

<?php									  echo "--";}
								  
								  ?></td>
							 <td align="center"><input type="checkbox" <?php if($taskInfos['aproved_status']==1){ echo"checked";} ?> name="chkChild" class="chkChild" value="<?php echo $taskInfos['folder_id'];?>"/></td>

								  <td><center>
								   <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_edit']==2)
							{
							?>
							
									            <a href="edit_product.php?id=<?php echo $taskInfos['folder_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
									           							<?php }
							else
							{
								echo "-- ";
							}
									           ?>  
											   <?php
 if($accessInfo['fk_delete']==4)
							{
							?>											                
									            <a href="query.php?type=productdelete&id=<?php echo $taskInfos['folder_id'];?>" onclick="return confirm('Are you sure want to delete this trip?')" >
									                <i class="material-icons">delete</i>
									            </a>
												<?php }
                             else{
								 echo "--";
							 }

							} else {?>
												 <a href="edit_product.php?id=<?php echo $taskInfos['folder_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
									                        
									            <a href="query.php?type=productdelete&id=<?php echo $taskInfos['folder_id'];?>" onclick="return confirm('Are you sure want to delete this trip?')" >
									                <i class="material-icons">delete</i>
									            </a>
													<?php } ?>
												</center></td>
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
	
    $('#usertbl').DataTable({
		
	 "aoColumns": [
            null,
			null,
            null,
            null,
		    null,
		    null,
		   { "sType": "date-uk" },

            null,
            null
        ]
		   });
});
$('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });

function ApprovedTrip()
{
	    var product_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                product_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_product&product_id='+product_id,
			success: function(data){
			//alert(data);
			alert("Product has been approved successfully.");
		}});
}


</script>

