<?php 
include('includes/header.php'); 
$trip_id=$_GET['id'];
$info=$display->tripDetailByID($trip_id);
//print_r($info);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="trip_list.php">Trip Management</a></li>
            <li class="active">Edit Trip</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updatetrip&id=<?php echo $trip_id;?>" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>Edit trip Detail</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                             		 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Approved Distance(km) <span style="color:red;">*</span></div>
                                            <input type="text" class="form-control" name="a_distance" id="a_distance"  required  placeholder="Approved Distance (km)" value="<?php echo @$info['approved_km'];?>" onchange="calculateApprovedAmount();">
                                        </div>
									<span style="color:red;" id="msg_f_name"></span>	
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Approved Amount(Rs) <span style="color:red;">*</span></div>
                                            <input type="text" class="form-control" name="a_amount" id="a_amount" placeholder="Approved Amount" value="<?php echo @$info['approved_amount'];?>" required readonly>
                                        </div>
                                    </div>
									<!-- <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Trip Type<span style="color:red;"></span></div>
                                            <input type="text" class="form-control" name="trip_type" id="trip_type" placeholder="Type"  value="<?php echo @$info['l_name'];?>">
                                        </div>
                                    </div>-->
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Update Remark <span style="color:red;"></span></div>
                                        <textarea class="form-control"  name="remarks" id="remarks" placeholder="Remarks"><?php echo @$info['approved_by_remark'];?></textarea>
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>

                                    </div>
									

                                    <input type="hidden" name="role" id="role" value="100" />

                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>


<script>
function calculateApprovedAmount()
{
	approved_km=$('#a_distance').val();
	labour_cost="<?php echo @$info['labour_cost'];?>";
	total=approved_km*labour_cost;
	$('#a_amount').val(total);
}
		function orderETCandPointonly(e)
{
	clearFnameMsg2();
	clearFnameMsg12
	var code=e.charCode? e.charCode : e.keyCode
	if (code!=8)
	{  //if the key isn't the backspace key (which we should allow)
		if (code==13 || code==9 || code==46)
		{
			return true;
		}
		else
		{
			if (code<48||code>57) //if not a number
			return false; //disable key press
		}
	}
}

</script>