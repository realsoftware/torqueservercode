<?php 
session_start();
include'class/MyClass.php'; 
$global_obj 	= 	new MyClass();
$conn	=	$global_obj->MyClasss();		// Start DB Connection Object

        $autosearch_manager_name_id  = $_SESSION['autosearch_manager_name_id'];
		$autosearch_dealer_name_id=$_SESSION['autosearch_dealer_name_id']  ;
		$officer_id=$_SESSION['officer_id'];
		$from_date=$_SESSION['from_date'];
		$to_date=$_SESSION['to_date'];
	    $type=	$_SESSION['type'];
	  
        $unique_code		=	TORQUE_USERID;		
		$user_type=substr($unique_code,0,3);
	 	
		 if($autosearch_manager_name_id!="")
			{
				$cond=" AND user.fk_manager_code like '%$autosearch_manager_name_id%'";
			}
			if($autosearch_dealer_name_id!="")
			{
				$cond.=" AND user.fk_dealear_code like '%$autosearch_dealer_name_id%'";
			}
			if($officer_id!="")
			{
				$cond.=" AND user.unique_code like '%$officer_id%'";
			}
			if($from_date!="" && $to_date!="")
			{
				$cond.=" AND (DATE(job_card_details.created_date) >= '$from_date' AND DATE(job_card_details.created_date)<= '$to_date')";
			}
		    if($type!="")
			{
				$cond.=" AND job_card_type like '%$type%'";
			}
			
		##########Dealer Case 
		if($user_type=='DLR')
		{
		$queryy     =   "SELECT job_card_details.fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime,

 SEC_TO_TIME(FLOOR(TIMESTAMPDIFF(SECOND, job_card_details.created_date,job_card_close_datetime))) AS TotalTimeTaken,
 
  TotalServiceLabourCost,TotalServiceOilCost,TotalServiceOutSideWorkCost,TotalServiceSpareCost

			FROM `job_card_details` 
			LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code  
			LEFT JOIN (  
                                 SELECT SUM(job_card_service_labour_cost.cost) AS TotalServiceLabourCost,job_card_service_labour_cost.fk_job_card_code
                                    FROM job_card_service_labour_cost
                                   GROUP BY job_card_service_labour_cost.fk_job_card_code
                          ) 
                          job_card_service_labour_cost ON  job_card_service_labour_cost.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_oil_charges.amount_per_liter) AS TotalServiceOilCost,job_card_service_oil_charges.fk_job_card_code
                                    FROM job_card_service_oil_charges
                                   GROUP BY job_card_service_oil_charges.fk_job_card_code
                          ) 
                          job_card_service_oil_charges ON  job_card_service_oil_charges.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_out_side_work.total) AS TotalServiceOutSideWorkCost,job_card_service_out_side_work.fk_job_card_code
                                    FROM job_card_service_out_side_work
                                   GROUP BY job_card_service_out_side_work.fk_job_card_code
                          ) 
                          job_card_service_out_side_work ON  job_card_service_out_side_work.fk_job_card_code =job_card_details.job_card_code                

    LEFT JOIN (  
                                SELECT SUM(job_card_service_spare_charge.total_cost) AS TotalServiceSpareCost,job_card_service_spare_charge.fk_job_card_code
                                    FROM job_card_service_spare_charge
                                   GROUP BY job_card_service_spare_charge.fk_job_card_code
                          ) 
                          job_card_service_spare_charge ON  job_card_service_spare_charge.fk_job_card_code =job_card_details.job_card_code 

		WHERE (fk_dealear_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND job_card_details.status=1 $cond GROUP BY job_card_details.job_card_code order by job_card_details.created_date desc";
		}
		else if($user_type=='MGR')
		{
		$queryy     =   "SELECT job_card_details.fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime,
 SEC_TO_TIME(FLOOR(TIMESTAMPDIFF(SECOND, job_card_details.created_date,job_card_close_datetime))) AS TotalTimeTaken,
 
  TotalServiceLabourCost,TotalServiceOilCost,TotalServiceOutSideWorkCost,TotalServiceSpareCost
 
			FROM `job_card_details` 
			LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code 
LEFT JOIN (  
                                 SELECT SUM(job_card_service_labour_cost.cost) AS TotalServiceLabourCost,job_card_service_labour_cost.fk_job_card_code
                                    FROM job_card_service_labour_cost
                                   GROUP BY job_card_service_labour_cost.fk_job_card_code
                          ) 
                          job_card_service_labour_cost ON  job_card_service_labour_cost.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_oil_charges.amount_per_liter) AS TotalServiceOilCost,job_card_service_oil_charges.fk_job_card_code
                                    FROM job_card_service_oil_charges
                                   GROUP BY job_card_service_oil_charges.fk_job_card_code
                          ) 
                          job_card_service_oil_charges ON  job_card_service_oil_charges.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_out_side_work.total) AS TotalServiceOutSideWorkCost,job_card_service_out_side_work.fk_job_card_code
                                    FROM job_card_service_out_side_work
                                   GROUP BY job_card_service_out_side_work.fk_job_card_code
                          ) 
                          job_card_service_out_side_work ON  job_card_service_out_side_work.fk_job_card_code =job_card_details.job_card_code                

    LEFT JOIN (  
                                SELECT SUM(job_card_service_spare_charge.total_cost) AS TotalServiceSpareCost,job_card_service_spare_charge.fk_job_card_code
                                    FROM job_card_service_spare_charge
                                   GROUP BY job_card_service_spare_charge.fk_job_card_code
                          ) 
                          job_card_service_spare_charge ON  job_card_service_spare_charge.fk_job_card_code =job_card_details.job_card_code 
			
		WHERE (fk_manager_code='$unique_code'  || job_card_details.fk_user_unique_code='$unique_code') AND job_card_details.status=1 $cond GROUP BY job_card_details.job_card_code order by job_card_details.created_date desc ";
		}
		#####################Admin Super admin case
		else
		{
	    $queryy     =   "SELECT job_card_details.fk_user_unique_code,customer_name,job_card_details.address as job_address
		,job_card_details.mobile_no,work_type,model,date_of_sale,
		registration_no,hours_completed,
engine_no,customer_complaints,expected_cost,expected_date_of_rapid,under_warranty,job_card_details.created_date as cr_date,
job_card_close_datetime,

 SEC_TO_TIME(FLOOR(TIMESTAMPDIFF(SECOND, job_card_details.created_date,job_card_close_datetime))) AS TotalTimeTaken,
  TotalServiceLabourCost,TotalServiceOilCost,TotalServiceOutSideWorkCost,TotalServiceSpareCost


 
			FROM `job_card_details` 
			LEFT JOIN user ON user.unique_code =  job_card_details.fk_user_unique_code  
            LEFT JOIN master_user_type ON master_user_type.code =  user.fk_type_of_usercode 
			LEFT JOIN `master_type_of_work` ON `master_type_of_work`.`code` =  job_card_details.fk_type_of_work_code 
		LEFT JOIN (  
                                 SELECT SUM(job_card_service_labour_cost.cost) AS TotalServiceLabourCost,job_card_service_labour_cost.fk_job_card_code
                                    FROM job_card_service_labour_cost
                                   GROUP BY job_card_service_labour_cost.fk_job_card_code
                          ) 
                          job_card_service_labour_cost ON  job_card_service_labour_cost.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_oil_charges.amount_per_liter) AS TotalServiceOilCost,job_card_service_oil_charges.fk_job_card_code
                                    FROM job_card_service_oil_charges
                                   GROUP BY job_card_service_oil_charges.fk_job_card_code
                          ) 
                          job_card_service_oil_charges ON  job_card_service_oil_charges.fk_job_card_code =job_card_details.job_card_code
            LEFT JOIN (  
                                 SELECT SUM(job_card_service_out_side_work.total) AS TotalServiceOutSideWorkCost,job_card_service_out_side_work.fk_job_card_code
                                    FROM job_card_service_out_side_work
                                   GROUP BY job_card_service_out_side_work.fk_job_card_code
                          ) 
                          job_card_service_out_side_work ON  job_card_service_out_side_work.fk_job_card_code =job_card_details.job_card_code                

    LEFT JOIN (  
                                SELECT SUM(job_card_service_spare_charge.total_cost) AS TotalServiceSpareCost,job_card_service_spare_charge.fk_job_card_code
                                    FROM job_card_service_spare_charge
                                   GROUP BY job_card_service_spare_charge.fk_job_card_code
                          ) 
                          job_card_service_spare_charge ON  job_card_service_spare_charge.fk_job_card_code =job_card_details.job_card_code                

	

		WHERE job_card_details.status=1 $cond  order by job_card_details.created_date desc  
	";
		}	

$setRec = mysqli_query($conn, $queryy);  
$columnHeader = '';  
$columnHeader = "UserID" . "\t". "CustomerName" . "\t". "Address". "\t". "MobileNo". "\t". "WorkType". "\t". "Model". "\t". "DateOfSale". "\t". "RegistrationNumber". "\t". "HoursCompleted". "\t". "EngineNumber". "\t". "CustomerComplaints". "\t". "ExpectedCost". "\t". "ExpectedDateOfRapid". "\t". "UnderWaranty". "\t". "CreatedDate". "\t". "CloseDate". "\t". "TotalTimeTaken". "\t". "TotalServiceLabourCost". "\t". "TotalServiceOilCost". "\t". "TotalServiceOutSideWorkCost". "\t". "TotalServiceSpareCost". "\t";  
$setData = '';  
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = ''; 
	
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
}  
  
header("Content-type: application/octet-stream");  
header("Content-Disposition: attachment; filename=Jobcard_Detail.xls");  
header("Pragma: no-cache");  
header("Expires: 0");  

  echo ucwords($columnHeader) . "\n" . $setData . "\n";  
 ?> 