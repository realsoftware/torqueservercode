<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->toShowAllTADADetail($id);
$closeImageCode=$info['image_code'];

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="reimbursement_list.php">Reimbursement Management</a></li>
            <li class="active">Edit Reimbursement</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=closeTada" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT REIMBURSEMENT</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">
                         <input type="hidden" name="enquiry_id" value="<?php echo $id;?>"/>
                        	<div class="row clearfix">
                                <div>
                                
								
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
								        <div class="font-12">Reimbursement Status  <span style="color:red;">*</span></div>
                                            <select type="text" class="form-control show-tick"  name="enquiry_status" id="enquiry_status" required>
											<option value="pending" <?php if($info['tada_approve_status']=='pending'){ echo "selected";}?>>Pending</option>
											<option value="approved" <?php if($info['tada_approve_status']=='approved'){ echo "selected";}?>>Approved</option>
											<option value="rejected" <?php if($info['tada_approve_status']=='rejected'){ echo "selected";}?>>Rejected</option>
											</select>
                                        </div>
                                    </div>
									 

									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Close Remark <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="remark" id="remark" placeholder="Remark" value="<?php echo $info['close_remark'];?>" >
                                        </div>
                                    </div> 
									<!--<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Upload PDF <span style="color:red;">*</span></div></div>

                                            <input type="file" class="form-control" name="pdf" id="pdf">
                                            <label class="form-label"></label>
                                        </div>-->
										
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>  
                                </div>
								
								
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<?php 
				$closeInfo=$display->closeEnquiryImageDetails($closeImageCode,'tada');
				if($closeInfo)
				{
					?>
				<h3>&nbsp;&nbsp;Reimbursement Image</h3></br>	
			<?php		
				foreach($closeInfo as $closeInfos)
				{
				?>
				
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				
                <a href="api/upload/tada/<?php echo $closeInfos['image_path']; ?>" download><img src="api/upload/tada/<?php echo $closeInfos['image_path']; ?>" width="70%"/></a>

	 </div>
	 <?php 
				} 
				}
				?>

	 </div>
								
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

<script>
$(document).ready(function() {
   val=$('#enquiry_status').val();
   	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}

});
function showDiv(val)
{
	//alert(val);
	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}
}
</script>
