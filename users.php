<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['user_info'];
@$type_search=$_GET['search'];
 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">User Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>USER MANGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                       
                           <div class="header-dropdown m-r--1">
							<?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForUserManagement('user_role_acces','MOD009');
							if($accessInfo['fk_create']>0)
							{
							?>
							 <a href="query.php?type=userserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-user.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="user_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                                <a href="add-user.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW USER</button></a>
							<?php
                            }
							}
							else{
                            ?>
							 <a href="query.php?type=userserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-user.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							
							<a href="user_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

                            <a href="add-user.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW USER</button></a>
							 <?php
							}
								?>
                            </div>
                        </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Password</th>
                                        <th>User Type</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                        <th>Dealer</th>
                                        <th>Manager</th>
                                        <th>Mobile No</th>
                                        <th>Email ID</th>
                                        <th>City</th>
                                       
                                        <th>Pin Code</th>
                                        <th>Created Date</th>
                                       
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								 if(@$type_search==1)
								 {
									 $userInfo=$info;
								 }
								 else
								 {
								 $userInfo=$display->allUserDetail();
								 }
								 //print_r($userInfo);
								 foreach($userInfo as $userInfos)
								 {
									$cat_name=$userInfos['fk_cat_code']; 
									$fk_type_of_usercode=$userInfos['fk_type_of_usercode']; 
									$fk_dealear_code=$userInfos['fk_dealear_code']; 
									$fk_manager_code=$userInfos['fk_manager_code']; 
									// if($fk_type_of_usercode=="MGR" || $fk_type_of_usercode=="DLR")
									// {
										$managerInfo=$display->getMnagerName($fk_manager_code);
										$manager_name=$managerInfo['maganer_name'];
									// }
									// if($fk_type_of_usercode=="DLR")
									// {
										
										$dealerInfo=$display->getDealerName($fk_dealear_code);
										$dealer_name=$dealerInfo['dealer_name'];
									//}
									if($userInfos['taluka_name']!="")
									{
										$city=$userInfos['taluka_name'];
									}
									else
									{
										$city=$userInfos['other_city'];
									}
                                  ?>
                                  <tr>
								  <td><?php echo $userInfos['unique_code'];?></td>
								  <td><?php echo $userInfos['password'];?></td>
								  <td><?php echo $userInfos['user_type'];?></td>
								  <td><?php echo $cat_name;?></td>
								  
								  <td><?php echo $userInfos['f_name'].' '.@$userInfos['l_name'];?></td>
								 
								  <td><?php echo $dealer_name;?></td>
								  <td><?php echo $manager_name;?></td>
								  <td><?php echo $userInfos['mobile_no'];?></td>

								  <td><?php echo $userInfos['email_id'];?></td>
								  
								  
								  <td><?php echo $city;?></td>
								
								  <td><?php echo $userInfos['pin_code'];?></td>
								  <td><?php 
								  if($userInfos['user_createdate']!="")
								  {
								  ?>
								  <span style="display:none;"><?php  echo date("Ymd H:i:s", strtotime($userInfos['user_createdate'])); ?></span>
								  <?php
								  echo date("d-m-Y H:i:s", strtotime($userInfos['user_createdate']));
								  }
								  else{
									  ?>
									   <span style="display:none;">--</span>
									  <?php
									  echo"--";
								  }
								  ?></td>
								  
								  <td><center>
								  <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_edit']==2)
							{
							?>
									            <a href="edit-user.php?id=<?php echo $userInfos['unique_code'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
							<?php }
							else
							{
								echo "-- ";
							}
									           ?>  
											   <?php
 if($accessInfo['fk_delete']==4)
							{
							?>											   
									            <a href="query.php?type=userdelete&id=<?php echo $userInfos['unique_code'];?>" onclick="return confirm('Are you sure want to delete this user?')" >
									                <i class="material-icons">delete</i>
									            </a>
							<?php }
                             else{
								 echo "--";
							 }

							} else {?>
							 <a href="edit-user.php?id=<?php echo $userInfos['unique_code'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
												
												<a href="query.php?type=userdelete&id=<?php echo $userInfos['unique_code'];?>" onclick="return confirm('Are you sure want to delete this user?')" >
									                <i class="material-icons">delete</i>
									            </a>
							
							<?php } ?>
												</center>
												
												
												</td>
								

								</tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
		      //  "order": [[ 9, "desc" ]]

		   });
});
</script>