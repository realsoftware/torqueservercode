<?php 
include('includes/header.php'); 
$id=$_GET['id'];
$info=$display->toShowEnquiryDetails($id);
$closeImageCode=$info['close_enquiry_image_uniue_code'];
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="enquiry_list.php">Enquiry Management</a></li>
            <li class="active">Edit Enquiry</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=closeenquiry" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT ENQUIRY</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">
                         <input type="hidden" name="enquiry_id" value="<?php echo $id;?>"/>
                        	<div class="row clearfix">
                                <div>
                                
								
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
								        <div class="font-12">Enquiry Status  <span style="color:red;">*</span></div>
                                            <select type="text" class="form-control show-tick" onchange="showDiv(this.value);" name="enquiry_status" id="enquiry_status" required>
											<option value="open cases" <?php if($info['enquiry_status']=='open cases'){ echo "selected";}?>>open cases</option>
											<option value="won cases" <?php if($info['enquiry_status']=='won cases'){ echo "selected";}?>>won cases</option>
											<option value="lost cases" <?php if($info['enquiry_status']=='lost cases'){ echo "selected";}?>>lost cases</option>
											<option value="dropped cases" <?php if($info['enquiry_status']=='dropped cases'){ echo "selected";}?>>dropped cases</option>
											
											</select>
                                        </div>
                                    </div>
									 
									 <div class="form-group form-float col-sm-12" id="bought_model" style="display:none;">
                                        <div class="form-line">
					                     <div class="font-12">Competition Tractor Bought Model <span style="color:red;">*</span></div>

                                            <input type="text" class="form-control" name="comp_tractor_bought" id="comp_tractor_bought"  placeholder="Competition Tractor Bought Model" value="<?php echo $info['competition_tractor_baught_madel'];?>">
                                        </div>
										<span style="color:red;" id="msg_f_name"></span>
                                    </div> 
									 <div class="form-group form-float col-sm-12" id="imp_bought" style="display:none;">
                                        <div class="form-line">
							           <div class="font-12">Implementation Bought <span style="color:red;"></span></div>

                                      <?php 
										$commodityType=$display->ShowAllImplementedInterested();
                                        
										?>
                                            <select type="text" class="form-control show-tick" onkeypress="clearFnameMsg1();" name="implementation_bought" id="implementation_bought" required>
											<?php 
											foreach($commodityType as $commodityTypes)
											{
												   $fk_product_code=$info['fk_implemented_intrested'];
											    $code =$commodityTypes['code'];
                                               if(@$fk_product_code == @$code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }	
											?>
											<option value="<?php echo $commodityTypes['code'];?>" <?php echo $aa; ?>><?php echo $commodityTypes['name'];?></option>
											<?php 
											} ?>
											</select>                                          </div>
                                    </div> 
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Reason For Lost <span style="color:red;"></span></div>

                                        <?php 
										$commodityType=$display->showMasterReasonLost();

										?>
                                            <select type="text" class="form-control show-tick" onkeypress="clearFnameMsg1();" name="reason_for_lost" id="reason_for_lost" required>
											<?php 
											foreach($commodityType as $commodityTypes)
											{
												   $fk_product_code=$info['fk_code_reason_lost'];
											    $code =$commodityTypes['code'];
                                               if(@$fk_product_code == @$code){
												 $aa = 'selected="selected"';
												 }else {
												  $aa = '';
												 }	
											?>
											<option value="<?php echo $commodityTypes['code'];?>" <?php echo $aa; ?>><?php echo $commodityTypes['name'];?></option>
											<?php 
											} ?>
											</select>                                        </div>
                                    </div> 
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
							           <div class="font-12">Remark <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="remark" id="remark" placeholder="Remark" value="<?php echo $info['close_enuiry_remark'];?>" >
                                        </div>
                                    </div> 
									<!--<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">Upload PDF <span style="color:red;">*</span></div></div>

                                            <input type="file" class="form-control" name="pdf" id="pdf">
                                            <label class="form-label"></label>
                                        </div>-->
										
                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" onclick="return formValidation();" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>  
                                </div>
								
								
						         
     

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<?php 
				$closeInfo=$display->closeEnquiryImageDetails($closeImageCode,'closeenquiry');
				if($closeInfo)
				{
					?>
					<h3>&nbsp;&nbsp;Close Enquiry Image</h3></br>
					<?php 
				foreach($closeInfo as $closeInfos)
				{
				?>
				
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				
                <a href="api/upload/enquiry/<?php echo $closeInfos['image_path']; ?>" download><img src="api/upload/enquiry/<?php echo $closeInfos['image_path']; ?>" width="70%"/></a>

	 </div>
	 <?php 
				} 
				}
				?>

	 </div>
								
                     
                        </div>
                    </div>
                    </form>
                </div>
		   </div>
   
	 </div>
	 
	 </div>

    </section>
<?php include('includes/footer.php'); ?>

<script>
$(document).ready(function() {
   val=$('#enquiry_status').val();
   	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}

});
function showDiv(val)
{
	//alert(val);
	if(val=='lost cases')
	{
		$('#bought_model').show();
		$('#imp_bought').show();
	}
	else{
		$('#bought_model').hide();
		$('#imp_bought').hide();
	}
}
</script>
