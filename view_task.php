<?php 
include('includes/header.php'); 
$unique_code=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
 $taskInfos=$display->toShowAllTaskListByID($unique_code);
								
 $fk_dealear_code=$taskInfos['fk_dealear_code']; 
 $fk_manager_code=$taskInfos['fk_manager_code']; 
									
 $managerInfo=$display->getMnagerName($fk_manager_code);
 $manager_name=$managerInfo['maganer_name'];
								
 $dealerInfo=$display->getDealerName($fk_dealear_code);
 $dealer_name=$dealerInfo['dealer_name']; 						
 
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="task_list.php">Task Management</a></li>
            <li class="active">View Task</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_code'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Activity Type</strong></div><br/>
										<?php echo $taskInfos['activity_name'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Customer Name</strong></div><br/>
										<?php 
								    
								        echo $taskInfos['customer_name'];
									
								        ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Mobile No</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Planned Date</strong></div><br/>
										<?php
                                     if($taskInfos['planned_date']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['planned_date']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Actual Completion Date</strong></div><br/>
										<?php

                               if($taskInfos['actual_completion_date']!="")
									 {
								  echo date("d-m-Y", strtotime($taskInfos['actual_completion_date']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Remarks</strong></div><br/>
										<?php echo $taskInfos['remarks'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Close Remarks</strong></div><br/>
										<?php echo $taskInfos['close_remark'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Status</strong></div><br/>
										<?php echo $taskInfos['task_status'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Dealer</strong></div><br/>
										<?php echo $dealer_name;?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Manager</strong></div><br/>
										<?php echo $manager_name;?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created Date</strong></div><br/>
										<?php 
								   if($taskInfos['created_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['created_date']));
									 }
									 else{
										 echo"--";
									 }
									 
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Close Date</strong></div><br/>
										<?php
                                  if($taskInfos['updated_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['updated_date']));
									 }
									 else{
										 echo"--";
									 }
								  
								  ?>                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approve</strong></div><br/>
										<?php if($taskInfos['aproved_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_by_remark'];?>
                                        </div>
                                    </div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									<?php
									$info=$display->showAllImageTask($taskInfos['image_unique_code'],$taskInfos['closed_image_unique_code']);
									foreach($info as $infos)
									{
									?>
									 <div class="form-group form-float col-sm-12">
                                       <img src="api/upload/task/<?php echo $infos['image_path'];?>"  width="150px" height="150px"/>
                                    </div>
									
									<?php 
									
									}?>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

