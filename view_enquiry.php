<?php 
include('includes/header.php'); 
$id=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
 $taskInfos=$display->toShowEnquiryDetails($id);
								
 $fk_dealear_code=$taskInfos['fk_dealear_code']; 
 $fk_manager_code=$taskInfos['fk_manager_code']; 
									
 $managerInfo=$display->getMnagerName($fk_manager_code);
 $manager_name=$managerInfo['maganer_name'];
								
 $dealerInfo=$display->getDealerName($fk_dealear_code);
 $dealer_name=$dealerInfo['dealer_name']; 						
 
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="enquiry_list.php">Enquiry Management</a></li>
            <li class="active">View Enquiry</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_usercode'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Customer Type</strong></div><br/>
										<?php echo $taskInfos['customer_type'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Customer Name</strong></div><br/>
										<?php 
								    
								        echo $taskInfos['customer_name'];
									
								        ?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Address</strong></div><br/>
										<?php echo $taskInfos['address'];?>
                                        </div>
                                    </div>
										<!-- <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>State</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>District</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>City</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>-->
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Pin Code</strong></div><br/>
										<?php echo $taskInfos['fk_pin_code'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Mobile No</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Model Interested</strong></div><br/>
										<?php echo $taskInfos['model_name'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Implemented Interested</strong></div><br/>
										<?php echo $taskInfos['implement_interested_type'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Implement Model</strong></div><br/>
										<?php echo $taskInfos['implement_model'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Source Of Enquiry</strong></div><br/>
										<?php echo $taskInfos['source_of_enquiery'];?>
                                        </div>
                                    </div>
										<!-- <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Tractor Application</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>-->
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Agriculture Land</strong></div><br/>
										<?php echo $taskInfos['agricultural_land'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Irrigated Land</strong></div><br/>
										<?php echo $taskInfos['irrigated_land'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Non Irrigated Land</strong></div><br/>
										<?php echo $taskInfos['non_irrigated_land'];?>
                                        </div>
                                    </div>
									<!-- <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Type Of Commodity Uses</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>-->
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Buyer Type</strong></div><br/>
										<?php echo $taskInfos['buyer_type'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Existing Tractor</strong></div><br/>
										<?php echo $taskInfos['existing_tractor'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Expected Delivery Date</strong></div><br/>
										<?php
                                     if($taskInfos['expected_delivert_date']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['expected_delivert_date']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Finanace Type</strong></div><br/>
										<?php
								         echo $taskInfos['finance_type'];
								
								        ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Finanacier Name</strong></div><br/>
										<?php echo $taskInfos['financier_name'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Remarks</strong></div><br/>
										<?php echo $taskInfos['remark'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Close Enquiry Remarks</strong></div><br/>
										<?php echo $taskInfos['close_enuiry_remark'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Type Of Enquiry</strong></div><br/>
										<?php echo $taskInfos['enquiry_status'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Dealer</strong></div><br/>
										<?php echo $dealer_name;?>
                                        </div>
                                    </div>
									 <!--<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Manager</strong></div><br/>
										<?php echo $manager_name;?>
                                        </div>
                                    </div>-->
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created Date</strong></div><br/>
										<?php 
								   if($taskInfos['cr_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['cr_date']));
									 }
									 else{
										 echo"--";
									 }
									 
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Close Date</strong></div><br/>
										<?php
                                  if($taskInfos['up_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['up_date']));
									 }
									 else{
										 echo"--";
									 }
								  
								  ?>                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approve</strong></div><br/>
										<?php if($taskInfos['approved_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_updated_remark'];?>
                                        </div>
                                    </div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									<?php
									$info=$display->showAllImageTask($taskInfos['image_unique_code'],$taskInfos['closed_image_unique_code']);
									foreach($info as $infos)
									{
									?>
									 <div class="form-group form-float col-sm-12">
                                       <img src="api/upload/task/<?php echo $infos['image_path'];?>"  width="150px" height="150px"/>
                                    </div>
									
									<?php 
									
									}?>
                                    
                                </div>
								
								
                            </div>
							 <fieldset>
                                <legend>Enquiry Visit Remarks</legend>
								<div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body table-responsive">          
                            <table class='table table-hover table-striped' width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Remark</th>
                                        <th>Image</th>
                                        <th>Created Date</th>
                                    </tr>
									</thead>
									<?php 
										$code=$taskInfos['enquiry_code'];
										$remrakInfo=$display->ShowAllEnquiryVisitRemark($code);
										$i=1;
										foreach($remrakInfo as $remrakInfos)
										{	
										?>
									<tr>
									 <td> 
									<?php echo $i; ?>
                                    </td> 
                                    <td> 
									<?php echo $remrakInfos['remark'];?>
                                    </td> 
								   <td> 
								   <?php
								   if($remrakInfos['image_path']!="")
								   {
								   ?>
							       <a href="api/upload/enquiry/<?php echo $remrakInfos['image_path'];?>" download><img src="api/upload/enquiry/<?php echo $remrakInfos['image_path'];?>" width="120px" height="120px" /></a>
								   <?php } ?>
                                    </td> 
									<td> 
									 <?php 
								  if($remrakInfos['created_date']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($remrakInfos['created_date']));
								  }
								  else
								  { echo "--";}
								  
								  ?>
                                    </td> 
								  
                                    
									</tr>
										<?php $i++; }
										?> 
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

