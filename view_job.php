<?php 
include('includes/header.php'); 
$id=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
 $taskInfos=$display->toShowAllJobCardDetail($id);
								
 $fk_dealear_code=$taskInfos['fk_dealear_code']; 
 $fk_manager_code=$taskInfos['fk_manager_code']; 
									
 $managerInfo=$display->getMnagerName($fk_manager_code);
 $manager_name=$managerInfo['maganer_name'];
								
 $dealerInfo=$display->getDealerName($fk_dealear_code);
 $dealer_name=$dealerInfo['dealer_name']; 						
 
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="jobcard_list.php">JObcard Management</a></li>
            <li class="active">View JObcard</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_user_unique_code'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Work Type</strong></div><br/>
										<?php echo $taskInfos['work_type'];?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Customer Name</strong></div><br/>
										<?php 
								    
								        echo $taskInfos['customer_name'];
									
								        ?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Address</strong></div><br/>
										<?php echo $taskInfos['job_address'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>State</strong></div><br/>
										<?php echo $taskInfos['mobile_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>District</strong></div><br/>
										<?php echo $taskInfos['dist_name'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>City</strong></div><br/>
										<?php echo $taskInfos['taluka_name'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Pin Code</strong></div><br/>
										<?php echo $taskInfos['job_pin_code'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Mobile No</strong></div><br/>
										<?php echo $taskInfos['job_mobile_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Model</strong></div><br/>
										<?php echo $taskInfos['model'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Date Of Sale</strong></div><br/>
										<?php
                                     if($taskInfos['date_of_sale']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['date_of_sale']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Registration Number</strong></div><br/>
										<?php echo $taskInfos['registration_no'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Hours Completed</strong></div><br/>
										<?php echo $taskInfos['hours_completed'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Engine Number</strong></div><br/>
										<?php echo $taskInfos['engine_no'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Customer Complaints</strong></div><br/>
										<?php echo $taskInfos['customer_complaints'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Work done other than customer complaints</strong></div><br/>
										<?php echo $taskInfos['work_done_other_than_customercomplaints'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Expected Cost</strong></div><br/>
										<?php echo $taskInfos['expected_cost'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Expected Date Of Rapid</strong></div><br/>
                                    <?php
                                     if($taskInfos['expected_date_of_rapid']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['expected_date_of_rapid']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
								  </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Under Warranty</strong></div><br/>
										<?php echo $taskInfos['under_warranty'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Company Reference Number</strong></div><br/>
										<?php echo $taskInfos['company_ref_no'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Craete Remark</strong></div><br/>
										<?php  
                                          echo $taskInfos['job_card_create_remark'];

								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Closed Remark</strong></div><br/>
										<?php
								         echo $taskInfos['close_remark'];
								
								        ?>
                                        </div>
                                    </div>
									
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created Date</strong></div><br/>
										<?php 
								   if($taskInfos['cr_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['cr_date']));
									 }
									 else{
										 echo"--";
									 }
									 
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Jobcard Close Date</strong></div><br/>
										<?php
                                  if($taskInfos['job_card_close_datetime']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['job_card_close_datetime']));
									 }
									 else{
										 echo"--";
									 }
								  
								  ?>                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approve</strong></div><br/>
										<?php if($taskInfos['approved_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_updated_remark'];?>
                                        </div>
                                    </div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Status</strong></div><br/>
										<?php echo $taskInfos['job_card_type'];?>
                                        </div>
                                    </div>

									<?php
									$info=$display->showAllImageTask($taskInfos['image_unique_code'],$taskInfos['closed_image_unique_code']);
									foreach($info as $infos)
									{
									?>
									 <div class="form-group form-float col-sm-12">
                                       <img src="api/upload/task/<?php echo $infos['image_path'];?>"  width="150px" height="150px"/>
                                    </div>
									
									<?php 
									
									}?>
                                    
                                </div>
								
								
                            </div>
							 <fieldset>
                                <legend>Service Labour Charges</legend>
								<div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body table-responsive">          
                            <table class='table table-hover table-striped' width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Description</th>
                                        <th>Time Taken</th>
                                        <th>Cost (Rs)</th>
                                        <th>Created Date</th>
                                    </tr>
									</thead>
									<?php 
										$code=$taskInfos['job_card_code'];
										$remrakInfo=$display->ShowAllLabourCharges($code);
										$i=1;
										foreach($remrakInfo as $remrakInfos)
										{	
										?>
									<tr>
									 <td> 
									<?php echo $i; ?>
                                    </td> 
                                    <td> 
									<?php echo $remrakInfos['description'];?>
                                    </td> 
									<td> 
									<?php echo $remrakInfos['time_taken'];?>
                                    </td>
								   <td> 
								   	<?php echo $remrakInfos['cost'];?>
                                    </td> 
									<td> 
									 <?php 
								  if($remrakInfos['created_date']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($remrakInfos['created_date']));
								  }
								  else
								  { echo "--";}
								  
								  ?>
                                    </td> 
								  
                                    
									</tr>
										<?php $i++; }
										?> 
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                        <fieldset>
                                <legend>Service Oil Charges</legend>
								<div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body table-responsive">          
                            <table class='table table-hover table-striped' width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Fuel Type</th>
                                        <th>Make</th>
                                        <th>Unit/Liter</th>
                                        <th>Amount (Rs)/Liter</th>
                                        <th>Created Date</th>
                                    </tr>
									</thead>
									<?php 
										$code=$taskInfos['job_card_code'];
										$remrakInfo=$display->ShowAllOilCharges($code);
										$i=1;
										foreach($remrakInfo as $remrakInfos)
										{	
										?>
									<tr>
									 <td> 
									<?php echo $i; ?>
                                    </td> 
                                    <td> 
									<?php echo $remrakInfos['oil_type'];?>
                                    </td> 
									<td> 
									<?php echo $remrakInfos['make'];?>
                                    </td> 
									<td> 
									<?php echo $remrakInfos['unit_liter'];?>
                                    </td> 
									<td> 
									<?php echo $remrakInfos['amount_per_liter'];?>
                                    </td> 
								   
									<td> 
									 <?php 
								  if($remrakInfos['cr_date']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($remrakInfos['cr_date']));
								  }
								  else
								  { echo "--";}
								  
								  ?>
                                    </td> 
								  
                                    
									</tr>
										<?php $i++; }
										?> 
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                       
 <fieldset>
                                <legend>Service Out Side Work</legend>
								<div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body table-responsive">          
                            <table class='table table-hover table-striped' width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Description</th>
                                        <th>Bill Number</th>
                                        <th>Total (Rs)</th>
                                        <th>Created Date</th>
                                    </tr>
									</thead>
									<?php 
										$code=$taskInfos['job_card_code'];
										$remrakInfo=$display->ShowAllServiceOutSideWork($code);
										$i=1;
										foreach($remrakInfo as $remrakInfos)
										{	
										?>
									<tr>
									 <td> 
									<?php echo $i; ?>
                                    </td> 
                                    <td> 
									<?php echo $remrakInfos['description'];?>
                                    </td> 
								                                       <td> 
									<?php echo $remrakInfos['bill_no'];?>
                                    </td>                                     <td> 
									<?php echo $remrakInfos['total'];?>
                                    </td> 									<td> 
									 <?php 
								  if($remrakInfos['created_date']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($remrakInfos['created_date']));
								  }
								  else
								  { echo "--";}
								  
								  ?>
                                    </td> 
								  
                                    
									</tr>
										<?php $i++; }
										?> 
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                       
 <fieldset>
                                <legend>Service Spare Charge</legend>
								<div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body table-responsive">          
                            <table class='table table-hover table-striped' width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Part Number</th>
                                        <th>Description</th>
                                        <th>Cost (Rs)</th>
                                        <th>Quantity</th>
                                        <th>Complaint Name</th>
                                        <th>Made</th>
                                        <th>Total Cost (Rs)</th>
                                        <th>Created Date</th>
                                    </tr>
									</thead>
									<?php 
										$code=$taskInfos['job_card_code'];
										$remrakInfo=$display->ShowAllSparCharges($code);
										$i=1;
										foreach($remrakInfo as $remrakInfos)
										{	
										?>
									<tr>
									 <td> 
									<?php echo $i; ?>
                                    </td> 
                                    <td> 
									<?php echo $remrakInfos['part_no'];?>
                                    </td> 
								   <td> 
									<?php echo $remrakInfos['description'];?>
                                    </td> 
									 <td> 
									<?php echo $remrakInfos['cost'];?>
                                    </td> 
									 <td> 
									<?php echo $remrakInfos['quantity'];?>
                                    </td> 
									 <td> 
									<?php echo $remrakInfos['complaint_code'];?>
                                    </td> 
									 									 <td> 
									<?php echo $remrakInfos['made_code'];?>
                                    </td> 
									 
									 									 <td> 
									<?php echo $remrakInfos['total_cost'];?>
                                    </td> 
									 
								  <td> 
									 <?php 
								  if($remrakInfos['cr_date']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($remrakInfos['cr_date']));
								  }
								  else
								  { echo "--";}
								  
								  ?>
                                    </td> 
								  
                                    
									</tr>
										<?php $i++; }
										?> 
                            </table>
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                       



					   </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

