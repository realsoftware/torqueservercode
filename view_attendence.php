<?php 
include('includes/header.php'); 
$unique_code=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$taskInfos=$display->toShowAllAttendencListByID($unique_code);

$fk_dealear_code=$taskInfos['fk_dealear_code']; 
$fk_manager_code=$taskInfos['fk_manager_code']; 
									
$managerInfo=$display->getMnagerName($fk_manager_code);
$manager_name=$managerInfo['maganer_name'];
								
$dealerInfo=$display->getDealerName($fk_dealear_code);
$dealer_name=$dealerInfo['dealer_name'];

$closeImageCode=$taskInfos['image_unique_code'];

?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="attendence_list.php">Attendence Management</a></li>
            <li class="active">View Attendence</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_user_unique_code'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>User Type</strong></div><br/>
										<?php echo $taskInfos['user_type'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Attedence Date</strong></div><br/>
										<?php 
								    if($taskInfos['attendence_date']!="")
									 {
								  
								  echo date("d-m-Y", strtotime($taskInfos['attendence_date']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>In Time</strong></div><br/>
										<?php echo $taskInfos['in_time'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Out Time</strong></div><br/>
										<?php echo $taskInfos['out_time'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Duration (hours)</strong></div><br/>
										<?php echo gmdate("H:i:s", $taskInfos['duration_hours']*3600);?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Start Location</strong></div><br/>
										<?php echo $taskInfos['attendence_in_address'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>End Location</strong></div><br/>
										<?php echo $taskInfos['attendence_out_address'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Dealer</strong></div><br/>
										<?php echo $dealer_name;?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Manager</strong></div><br/>
										<?php echo $manager_name;?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Type</strong></div><br/>
										<?php echo $taskInfos['type'];?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created Date</strong></div><br/>
										<?php 
								    if($taskInfos['date_time']!="")
									 {
								  
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['date_time']));
									 }
									 else{
										 echo "--";
									 }
								  ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approve</strong></div><br/>
										<?php if($taskInfos['aproved_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_by_remark'];?>
                                        </div>
                                    </div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									<?php
									$info=$display->showAllImage($taskInfos['image_unique_code']);
									foreach($info as $infos)
									{
									?>
									 <div class="form-group form-float col-sm-12">
									   <a href="api/upload/attendence/<?php echo $infos['image_path']; ?>" download>
                                       <img src="api/upload/attenedence/<?php echo $infos['image_path'];?>"  width="150px" height="150px"/>
									   </a>
                                    </div>
									
									<?php 
									
									}?>
                                    
                                </div>
								<!--
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								

				<?php 
			
				$closeInfo=$display->closeEnquiryImageDetails($closeImageCode,'attendence');
				
				if($closeInfo)
				{
					?>
					<h3>&nbsp;&nbsp;Attendence Image</h3></br>
					<?php 
				foreach($closeInfo as $closeInfos)
				{
				?>
				
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				
                <a href="api/upload/attendence/<?php echo $closeInfos['image_path']; ?>" download><img src="api/upload/attendence/<?php echo $closeInfos['image_path']; ?>" width="70%"/></a>

	 </div>
	 <?php 
				} 
				}
				?>

	 </div>-->
                            </div>
							  
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

