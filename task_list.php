<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['task_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Task Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TASK MANAGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						   <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForTaskManagement('user_role_acces','MOD002');
							if($accessInfo['fk_download']==5)
							{
							?>
						    <a href="query.php?type=taskserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-task.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="task_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>
 <?php
                            }
							}
							else{
                            ?>
							 <a href="query.php?type=taskserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-task.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="task_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

							<?php } ?>
                             <!-- <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTask();">Submit</button>
                           --> </div>
                        </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>Created By</th>
										<th>Task Type</th>
                                        <th>Customer Name</th>
                                        <th>Mobile No</th>
                                        <th>Planned Date</th>
                                        <th>Actual Date</th>
                                       <!-- <th>Remarks</th>-->
                                        <th>Status</th>
                                        <!--<th>Dealer</th>
                                        <th>Manager</th>
                                        <th>Created Date</th>
                                        <th>Closed Date</th>-->
                                       <!-- <th align="center">Approve <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkHeader"/></th>
                                        --><th>Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								  if(@$type_search==1)
								 {
									 $taskInfo=$info;
								 }
								 else
								 {
								 $taskInfo=$display->toShowAllTaskList();
								 }
								 //print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
									$fk_dealear_code=$taskInfos['fk_dealear_code']; 
									$fk_manager_code=$taskInfos['fk_manager_code']; 
									
										$managerInfo=$display->getMnagerName($fk_manager_code);
										$manager_name=$managerInfo['maganer_name'];
								
										$dealerInfo=$display->getDealerName($fk_dealear_code);
										$dealer_name=$dealerInfo['dealer_name'];
 
                                  ?>
                                  <tr>
								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_code'].")";//echo $taskInfos['fk_userunique_code'];?></td>
								  <td><?php echo $taskInfos['activity_name'];?></td>
								  <td><?php echo $taskInfos['customer_name'];?></td>

								  <td><?php echo $taskInfos['mobile_no'];?></td>
																 
								  <td><?php
                                     if($taskInfos['planned_date']!="")
									 {
?>
								   <span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['actual_completion_date'])); ?></span>

<?php
								  echo date("d-m-Y", strtotime($taskInfos['planned_date']));
									 }
									 else{
										 ?>
				<span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['actual_completion_date'])); ?></span>

										 <?php
										 echo "--";
									 }
								  ?></td>
								 
								  <td><?php

                               if($taskInfos['actual_completion_date']!="")
									 {
										 ?>
				<span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['actual_completion_date'])); ?></span>
 
										 <?php
								  echo date("d-m-Y", strtotime($taskInfos['actual_completion_date']));
									 }
									 else{
										 ?>
				 <span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['actual_completion_date'])); ?></span>

										 <?php
										 echo "--";
									 }
								  ?></td>
								  
								  
								<!--  <td><?php echo $taskInfos['remarks'];?></td>-->
								
								  <td><?php echo ucfirst($taskInfos['task_status']);?></td>
								 <!-- <td><?php echo $dealer_name;?></td>
								  <td><?php echo $manager_name;?></td>
								  <td><?php 
								   if($taskInfos['created_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['created_date']));
									 }
									 else{
										 echo"--";
									 }
									 
								  ?></td>
								  <td><?php
                                  if($taskInfos['updated_date']!="")
									 {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['updated_date']));
									 }
									 else{
										 echo"--";
									 }
								  
								  ?></td>-->
								 <!-- <td align="center"><input type="checkbox" <?php if($taskInfos['aproved_status']==1){ echo"checked";} ?> name="chkChild" class="chkChild" value="<?php echo $taskInfos['task_id'];?>"/></td>
								  -->
								  <td><center>
								  <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_view']==3)
							{
							?>
								
								 <a href="view_task.php?id=<?php echo $taskInfos['task_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
							<?php } 
								  if($accessInfo['fk_edit']==2)
							{
							?>
									            <a href="edit-task.php?id=<?php echo $taskInfos['task_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
							<?php }
							else
							{
								echo "-- ";
							}
									           ?>  
											   <?php
 if($accessInfo['fk_delete']==4)
							{
							?>											   
									            <a href="query.php?type=taskdelete&id=<?php echo $taskInfos['task_id'];?>" onclick="return confirm('Are you sure want to delete this data?')" >
									                <i class="material-icons">delete</i>
									            </a>
							<?php }
                             else{
								 echo "--";
							 }

							} else {?>
							 <a href="view_task.php?id=<?php echo $taskInfos['task_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
							 <a href="edit-task.php?id=<?php echo $taskInfos['task_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
												
												<a href="query.php?type=taskdelete&id=<?php echo $taskInfos['task_id'];?>" onclick="return confirm('Are you sure want to delete this task?')" >
									                <i class="material-icons">delete</i>
									            </a>
							
							<?php } ?>
												</center>
												
												
												</td>
								
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
		       // "order": [[ 10, "desc" ]]
			      "aoColumns": [
            null,
			null,
            null,
            null,
			 { "sType": "date-uk" },
			 { "sType": "date-uk" },
            null,
           // null,
            null
          
          
        ]


		   });
});

 $('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });
		
function ApprovedTask()
{
	    var task_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                task_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_task&task_id='+task_id,
			success: function(data){
			//alert(data);
			alert("Task has been approved successfully.");
		}});
}
</script>