<?php 
include('includes/header.php'); 
$task_id=$_GET['id'];
$info=$display->taskDetailByID($task_id);
$startImageCode=$info['image_uniue_code'];
$closeImageCode=$info['close_image_uniue_code'];

//print_r($info);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<style>
div.ex1 {
    margin-bottom: 8px;
}
</style>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="task_list.php">Task Management</a></li>
            <li class="active">Edit Task</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=updatetask&id=<?php echo $task_id;?>" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>Edit Task Detail</h2>
                           <?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                             		 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Planned Date <span style="color:red;">*</span></div>
										
                                            <input type="text" class="datetimepicker form-control" name="plan_date" id="plan_date"  required  placeholder="Planned Date" value="<?php echo @$info['planned_date'];?>" >
                                        </div>
									<span style="color:red;" id="msg_f_name"></span>	
                                    </div> 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Task Type<span style="color:red;">*</span></div>
										
										<select type="text" class="form-control show-tick" name="task_type" id="task_type" required>
									<option value="open" <?php if(@$info['task_status']=='open'){ echo"selected";}?>>open</option>
									<option value="close" <?php if(@$info['task_status']=='close'){ echo"selected";}?>>close</option>
											
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Start Remark <span style="color:red;"></span></div>
                                        <textarea class="form-control"  name="remarks" id="remarks" placeholder="Start Remark"><?php echo @$info['remarks'];?></textarea>
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>

                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Close Remark <span style="color:red;"></span></div>
                                        <textarea class="form-control"  name="closed_remarks" id="closed_remarks" placeholder="Close Remark"><?php echo @$info['close_remark'];?></textarea>
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>

                                    </div>
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12">Update Remark <span style="color:red;"></span></div>
                                        <textarea class="form-control"  name="updated_remarks" id="updated_remarks" placeholder="Remarks"><?php echo @$info['approved_by_remark'];?></textarea>
                                        </div>
										<span style="color:red;" id="msg_mobile_no"></span>

                                    </div>
		

                                    <input type="hidden" name="role" id="role" value="100" />

                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnAdd">SUBMIT</button>
                                    </div>    
                                </div>
								
								  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<?php 
				$startInfo=$display->startTaskImageDetails($startImageCode);
				if($startInfo)
				{
					?>
					<h3>&nbsp;&nbsp;Start Task Image</h3></br>
					<?php 
				foreach($startInfo as $startInfos)
				{
				?>
				
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				
                <a href="api/upload/task/<?php echo $startInfos['image_path']; ?>" download><img src="api/upload/task/<?php echo $startInfos['image_path']; ?>" width="70%"/></a>

	 </div>
	 <?php 
				} 
				}
				?>

	 </div>
	 
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<?php 
				$closeInfo=$display->closeEnquiryImageDetails($closeImageCode,'closeTask');
				if($closeInfo)
				{
					?>
					<h3>&nbsp;&nbsp;Close Task Image</h3></br>
					<?php 
				foreach($closeInfo as $closeInfos)
				{
				?>
				
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				
                <a href="api/upload/task/<?php echo $closeInfos['image_path']; ?>" download><img src="api/upload/task/<?php echo $closeInfos['image_path']; ?>" width="70%"/></a>

	 </div>
	 <?php 
				} 
				}
				?>

	 </div>
								
								
                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>
<script type="text/javascript" src="assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="assets/js/datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#date').bootstrapMaterialDatePicker
        ({
            time: false,
            clearButton: true
        });

        $('#time').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm'
        });

        $('#plan_date').bootstrapMaterialDatePicker
        ({
            format: 'YYYY-MM-DD'
        });
        $('#date-fr').bootstrapMaterialDatePicker
        ({
            format: 'DD/MM/YYYY',
            lang: 'fr',
            weekStart: 1, 
            cancelText : 'ANNULER',
            nowButton : true,
            switchOnClick : true
        });

        $('#date-end').bootstrapMaterialDatePicker
        ({
            weekStart: 0, format: 'DD/MM/YYYY'
        });
        $('#date-start').bootstrapMaterialDatePicker
        ({
            weekStart: 0, format: 'DD/MM/YYYY', shortTime : true
        }).on('change', function(e, date)
        {
            $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
        });

        $('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY', minDate : new Date() });
        $.material.init()
    });
</script>


<script>
		function orderETCandPointonly(e)
{
	clearFnameMsg2();
	clearFnameMsg12
	var code=e.charCode? e.charCode : e.keyCode
	if (code!=8)
	{  //if the key isn't the backspace key (which we should allow)
		if (code==13 || code==9 || code==46)
		{
			return true;
		}
		else
		{
			if (code<48||code>57) //if not a number
			return false; //disable key press
		}
	}
}

</script>