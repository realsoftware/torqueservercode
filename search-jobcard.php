<?php 
include('includes/header.php'); 

@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="jobcard_list.php">Jobcard Management</a></li>
            <li class="active">Search Jobcard</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="query.php?type=searchJobcardExport" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>SEARCH JOBCARD</h2>
                           
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                   
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
									 <div class="font-12">Manager Name <span style="color:red;"></span></div>                                        
										 <input type="text" class="form-control" name="autosearch_manager_name" onkeypress="clearFnameMsg9();" id="autosearch_manager_name" placeholder="Manager" onkeyup="CheckAutoSuggestion('manager_name',this.value);" autocomplete="off" value="<?php echo @$_POST['fk_manager_code']; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_manager_name" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_manager_name_id" id="autosearch_manager_name_id"
                value="<?php echo @$_POST['unique_code']; ?>"  />
										
										<span style="color:red;" id="msg_manager_name"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
				                        <div class="font-12">Dealer Name <span style="color:red;"></span></div>

                                            <input type="text" class="form-control"  onkeypress="clearFnameMsg10();"  name="autosearch_dealer_name" id="autosearch_dealer_name" placeholder="Dealer" onkeyup="CheckAutoSuggestionDealer('dealer_name',this.value);" autocomplete="off" value="<?php echo @$_POST['fk_dealear_code']; ?>">
										</div>
										<div id="CheckAutoSuggestionResult_dealer_name" class="auto_suggestion" style="display:none; background:#eee; width:100%;padding:8px 6px 3px 15px; "></div>
                <input type="hidden" name="autosearch_dealer_name_id" id="autosearch_dealer_name_id"
                value="<?php echo @$_POST['unique_code']; ?>"  />
				                    <span style="color:red;" id="msg_dealer_name"></span>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
					                 <div class="font-12">Officer Name <span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="officer_id" id="officer_name" placeholder="Officer" >
                                        </div>
                                    </div>
									 
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">From Date (Y-m-d)<span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="from_date" id="from_date" placeholder="From Date" >
                                        </div>
                                    </div>
									
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
								        <div class="font-12">To Date (Y-m-d)<span style="color:red;"></span></div>

                                            <input type="text" class="form-control" name="to_date" id="to_date" placeholder="To Date" >
                                        </div>
                                    </div>
									
 
	
<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
                                           
								        <div class="font-12">Jobcard Type  <span style="color:red;">*</span></div>
                                            <select class="form-control show-tick"  name="type" id="type" required>
											<option value="open" <?php if($info['job_card_type']=='open'){ echo "selected";}?>>Open</option>
											<option value="close" <?php if($info['job_card_type']=='close'){ echo "selected";}?>>Close</option>
											</select>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">
                                         <button class="btn bg-blue waves-effect pull-right" type="submit" name="btnAdd">SEARCH</button>
                                    </div>

                                   
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>
<script type="text/javascript" src="assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="assets/js/datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#from_date').bootstrapMaterialDatePicker
        ({
            format: 'YYYY-MM-DD'
        });
        $('#from_date').bootstrapMaterialDatePicker
        ({
            format: 'YYYY-MM-DD',
            lang: 'fr',
            weekStart: 1, 
            cancelText : 'ANNULER',
            nowButton : true,
            switchOnClick : true
        });

        $('#to_date1').bootstrapMaterialDatePicker
        ({
            weekStart: 0,             
			format: 'YYYY-MM-DD'

        });
        $('#to_date').bootstrapMaterialDatePicker
        ({
            weekStart: 0, format: 'YYYY-MM-DD', shortTime : false
        }).on('change', function(e, date)
        {
            $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
        });

        $('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });
        $.material.init()
    });
</script>
