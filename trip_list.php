<?php 
include('includes/header.php');
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
$info=$_SESSION['trip_info'];
@$type_search=$_GET['search'];

 ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Trip Management</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TRIP MANAGEMENT</h2>
							<?php 
                            @$msz     =      @$_SESSION['TEMP']['msz'];
                            if(@$msz)
                            {
                            @$_SESSION['TEMP']['msz']       =   '';
                            ?>
                           <p style="color:green;text-align:center;"><b><?php echo @$msz; ?></b></p>
                            <?php 
                            } 
                            ?>
                           <div class="header-dropdown m-r--1">
						    <?php
							if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
							$accessInfo=$display->getAccessDetailForTripManagement('user_role_acces','MOD004');
							if($accessInfo['fk_download']==5)
							{
							?>
						    <a href="query.php?type=tripserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-trip.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="trip_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>
<?php
                            }
							}
							else{
                            ?>
							 <a href="query.php?type=tripserachReset"><button type="button" class="btn bg-blue waves-effect">Reset</button></a>
							 <a href="search-trip.php"><button type="button" class="btn bg-blue waves-effect">Search</button></a>
							 <a href="trip_export.php"><button type="button" class="btn bg-blue waves-effect">Export</button></a>

							<?php } ?>
<!--
                              <button type="button" class="btn bg-blue waves-effect" onclick="ApprovedTrip();">Submit</button>
--></div>
                          </div>

                        <div class="body table-responsive">          
                            <table class='table table-hover table-striped' id="usertbl">
                                <thead>
                                    <tr>
                                        <th>Created By</th>
										<th>Trip Date</th>
                                        <!--<th>User Type</th>
										<th>Dealer</th>
                                        <th>Manager</th>-->
                                        <th>Start Location</th>
                                        <th>End Location</th>
                                        
                                       <!-- <th>Total Running Time</th>-->
                                        <th>Trip Time</th>
                                        <th>Total Distance Covered (km)</th>
                                        <th>Approved  Distance (km)</th>
                                        <th>Approved Amount</th>
                                       <!-- <th>Total Amount</th>-->
                                        <th>Type</th>
                                       <!-- <th>Start Date</th>
                                        <th>End Date</th>-->
                                       <!-- <th>Trip Route</th>-->

								<!-- <th align="center">Approve <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkHeader"/></th>
-->
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>  
                                 <?php
								  if(@$type_search==1)
								 {
									 $taskInfo=$info;
								 }
								 else
								 {
								 $taskInfo=$display->toShowAllTripList();
								 }
								
								// print_r($taskInfo);
								 foreach($taskInfo as $taskInfos)
								 {
									$fk_dealear_code=$taskInfos['fk_dealear_code']; 
									$fk_manager_code=$taskInfos['fk_manager_code']; 
									
										$managerInfo=$display->getMnagerName($fk_manager_code);
										$manager_name=$managerInfo['maganer_name'];
								
										$dealerInfo=$display->getDealerName($fk_dealear_code);
										$dealer_name=$dealerInfo['dealer_name'];
										
										/////////Calculate in and out time duration	
										$gmtTime=DateTime::CreateFromFormat('Y-m-d H:i:s',$taskInfos['up_date_time']);
										$out_time_duration=DateTime::CreateFromFormat('Y-m-d H:i:s',$taskInfos['cr_date_time']);
										$duration=$gmtTime->diff($out_time_duration);
										$hours=number_format((float)($duration->days*24)+$duration->h+($duration->i/60)+($duration->s/3600),3,'.','');
								$total_running_time= gmdate("H:i:s", $hours*3600);
								
 
                                  ?>
                                  <tr>
								  <!--<td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name'];?></td>-->
								  <td><?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_userunique_code'].")";//echo $taskInfos['fk_userunique_code'];?></td>
								  <td>
								   <span style="display:none;"><?php  echo date("Ymd", strtotime($taskInfos['trip_date_time'])); ?></span>
								  <?php echo date("d-m-Y", strtotime($taskInfos['trip_date_time']));?>
								  
								  
								  </td>								
								<!-- <td><?php echo $taskInfos['user_type'];?></td>
								  <td><?php echo $dealer_name;?></td>
								  <td><?php echo $manager_name;?></td>-->
								  <td><?php echo $taskInfos['trip_start_address'];?></td>
																 
								  <td><?php echo $taskInfos['trip_end_address'];?></td>
								 
								  
								  
								  <td><?php echo gmdate("H:i:s",$taskInfos['total_time_hours']*3600);?></td>
								  <!--<td><?php
                                   if($taskInfos['type']=="start")
								  {
									  echo"--";
								  }
								  else{
								  echo $total_running_time;
								  }  ?></td>-->
								  <td><?php echo $taskInfos['total_km_driven'];?></td>
								  <td><?php echo $taskInfos['approved_km'];?></td>
								  <td><?php echo $taskInfos['approved_amount'];?></td>
								  <!--<td><?php echo $taskInfos['total_amoutnt'];?></td>-->
							      <td><?php echo ucfirst($taskInfos['type']);?></td>

								  <!--<td><?php 
								  if($taskInfos['cr_date_time']!="")
								  {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['cr_date_time']));
								  }
								  else
								  { echo "--";}
								  
								  ?></td>
								  <td><?php 
								  
								  if($taskInfos['type']=="start")
								  {
									  echo"--";
								  }
								  else{
									  if($taskInfos['up_date_time']!="")
									  {
								  echo date("d-m-Y H:i:s", strtotime($taskInfos['up_date_time']));
									  }
									  else{
										  echo "--";
									  }
								  }?></td>-->
								

								<!-- <td><a href="javascript:void(0);" onclick="openFancyboxTripHistory('<?php echo $taskInfos['trip_id'];?>');">Route</a></td>-->
							<!-- <td align="center"><input type="checkbox" <?php if($taskInfos['aproved_status']==1){ echo"checked";} ?> name="chkChild" class="chkChild" value="<?php echo $taskInfos['trip_id'];?>"/></td>-->

								  <td><center>
								  <?php
								   if($usertype=="ADM" || $usertype=="MGR" || $usertype=="DLR")
							{
								  if($accessInfo['fk_view']==3)
							{
							?>
								   <a href="view_trip.php?id=<?php echo $taskInfos['trip_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
<?php } 
								  if($accessInfo['fk_edit']==2)
							{
							?>
									            <a href="edit-trip.php?id=<?php echo $taskInfos['trip_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
									            <?php }
							else
							{
								echo "-- ";
							}
									           ?>  
 <?php
 if($accessInfo['fk_delete']==4)
							{
							?>															   
									            <a href="query.php?type=tripdelete&id=<?php echo $taskInfos['trip_id'];?>" onclick="return confirm('Are you sure want to delete this trip?')" >
									                <i class="material-icons">delete</i>
									            </a>
												<?php }
                             else{
								 echo "--";
							 }

							} else {?>
							 <a href="view_trip.php?id=<?php echo $taskInfos['trip_id'];?>" >
									              <img src="assets/images/view.png" width="25px;" style="margin-top:-1em;"/>
											 </a> 
							 <a href="edit-trip.php?id=<?php echo $taskInfos['trip_id'];?>">
									                <i class="material-icons">mode_edit</i>
									            </a>
							 <a href="query.php?type=tripdelete&id=<?php echo $taskInfos['trip_id'];?>" onclick="return confirm('Are you sure want to delete this trip?')" >
									                <i class="material-icons">delete</i>
									            </a>
							
							<?php } ?>
												
												
												
												</center></td>
								  </tr>
								 <?php } ?>								  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
    $('#usertbl').DataTable({
				       // "order": [[ 16, "desc" ]]
	 "aoColumns": [
            null,
            { "sType": "date-uk" },
			null,
            null,
            null,
            null,
            null,
            null,
            null,
           // null,
            null
        ]
		   });
});
$('#chkHeader').on('change', function() {     
                $('.chkChild').prop('checked', $(this).prop("checked"));              
        });
        //deselect "checked all", if one of the listed checkbox product is unchecked amd select "checked all" if all of the listed checkbox product is checked
        $('.chkChild').change(function(){ //".checkbox" change 
		
            if($('.chkChild:checked').length == $('.chkChild').length){
                   $('#chkHeader').prop('checked',true);
            }else{
                   $('#chkHeader').prop('checked',false);
            }
        });

function ApprovedTrip()
{
	    var trip_id = []; 
	    var checked_status = []; 
            $("input:checkbox[name=chkChild]:checked").each(function() { 
                trip_id.push($(this).val()); 
				   
            }); 
             
	$.ajax({
			url: 'ajax.php?type=approved_trip&trip_id='+trip_id,
			success: function(data){
			//alert(data);
			alert("Task has been approved successfully.");
		}});
}


function openFancyboxTripHistory(id) {
	         
$.fancybox({
        'type': 'iframe',
        'href': 'trip_history.php?trip_id='+id,
		
    });
}
</script>
<!-- fancybox -->
	
	
	<script type="text/javascript" src="fancybox-2.1.7/source/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript" src="fancybox-2.1.7/lib/jquery.mousewheel.pack.js?v=3.1.3"></script>
   <link rel="stylesheet" type="text/css" href="fancybox-2.1.7/source/jquery.fancybox.css?v=2.1.5" media="screen" />
   <style>
      /* .fancybox-inner{
           height:auto !important;
           overflow:visible;
       }
       .fancybox-inner iframe{
           min-height: 600px;
       }
       .fancybox-wrap{
           top:30px !important;
       }*/
	   .progrs{
		   position:fixed;
		   z-index:99999999;
		   top:0;
		   left:0;
		   right:0;
		   bottom:0;
		   background:rgba(0, 0, 0, 0.5);
	   }
   </style>
   <!--- /fancybox --->
