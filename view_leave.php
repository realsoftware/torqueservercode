<?php 
include('includes/header.php'); 
$id=$_GET['id'];
//print_r($moduleinfo);
@$msg_class = $_SESSION['msz_class'];
@$msg       = $_SESSION['msz'];
 $taskInfos=$display->toShowAllLeaveDetail($id);
								
 $fk_dealear_code=$taskInfos['fk_dealear_code']; 
 $fk_manager_code=$taskInfos['fk_manager_code']; 
									
 $managerInfo=$display->getMnagerName($fk_manager_code);
 $manager_name=$managerInfo['maganer_name'];
								
 $dealerInfo=$display->getDealerName($fk_dealear_code);
 $dealer_name=$dealerInfo['dealer_name']; 						
 
?>

<link href="assets/css/bootstrap-select.css" rel="stylesheet">

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="leave_list.php">Leave Management</a></li>
            <li class="active">View Leave</a></li>
        </ol>

       <div class="container-fluid">                                    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form  method="post" action="" enctype="multipart/form-data">
                    <div class="card">
                       
                        <div class="body">

                        	<div class="row clearfix">
                                
                                <div>
                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Created By</strong></div><br/>
										<?php echo $taskInfos['f_name'].' '.@$taskInfos['l_name']." (".$taskInfos['fk_user_code'].")";?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Apply For Leave</strong></div><br/>
										<?php echo $taskInfos['apply_for_leave'];?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Type Of Leave</strong></div><br/>
										<?php 
								    
								        echo $taskInfos['type_of_leave'];
									
								        ?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>From Date</strong></div><br/>
										<?php                                     
										if($taskInfos['from_date']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['from_date']));
									 }
									 else{
										 echo "--";
									 }?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>To Date</strong></div><br/>
										<?php                                      
										if($taskInfos['to_date']!="")
									 {

								  echo date("d-m-Y", strtotime($taskInfos['to_date']));
									 }
									 else{
										 echo "--";
									 }?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>No Of Days</strong></div><br/>
										<?php echo $taskInfos['no_of_day'];?>
                                        </div>
                                    </div>
										 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Day Type</strong></div><br/>
										<?php echo $taskInfos['day_type'];?>
                                        </div>
                                    </div>
									
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Status</strong></div><br/>
										<?php if($taskInfos['approved_by_status']==1){ echo"Yes";}else {echo "No";} ?>
                                        </div>
                                    </div>
									 <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved Remark</strong></div><br/>
										<?php echo $taskInfos['approved_by_remark'];?>
                                        </div>
                                    </div>
									
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Leave Approved Remark</strong></div><br/>
										<?php echo $taskInfos['leave_approved_remark'];?>
                                        </div>
                                    </div>

                                    <div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Approved By</strong></div><br/>
										<?php echo $taskInfos['approved_by'];?>
                                        </div>
                                    </div>
									<div class="form-group form-float col-sm-12">
                                        <div class="form-line">
										<div class="font-12"><strong>Status</strong></div><br/>
										<?php echo $taskInfos['approved_status'];?>
                                        </div>
                                    </div>

									
                                    
                                </div>
								
								
                            </div>

					   </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>
<?php include('includes/footer.php'); ?>

